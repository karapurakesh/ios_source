//
//  CartCheckOutPropertiesModel.swift
//  SmileyServe
//
//  Created by Apple on 21/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct CartCheckOutPropertiesModel {
    
    var userid : String?
    var checkout_date : String?
    
}
