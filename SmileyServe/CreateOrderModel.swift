//
//  CreateOrderModel.swift
//  SmileyServe
//
//  Created by Apple on 21/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation

struct CreateOrderModel {
    
    var userid : String?
    var order_amount : String?
    var smileycash : String?
    var captcha : String?
  
}
