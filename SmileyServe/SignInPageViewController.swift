  //
  //  SignInPageViewController.swift
  //  SmileyServe
  //
  //  Created by Apple on 06/07/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //

  import UIKit
  import SVProgressHUD

  class SignInPageViewController: UIViewController,UITextFieldDelegate,BEMCheckBoxDelegate{
  var btn: TKTransitionSubmitButton!
  @IBOutlet var transitionSignInButton: TKTransitionSubmitButton!
  @IBOutlet var showSignInPwdCheckBox: BEMCheckBox!
  @IBOutlet var referenceShowPassword: UIButton!
  @IBOutlet var signInPasswordTextField: UITextField!
  @IBOutlet var signInEmailTextField: UITextField!
  @IBOutlet var forgotPasswordButton: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var HeightConstraint: NSLayoutConstraint!
  var bemCheckbox : BEMCheckBox!
  var button : TKTransitionSubmitButton!
  override func viewDidLoad() {
  super.viewDidLoad()
    
    transitionSignInButton.layer.cornerRadius = 5
    signUpBtn.layer.cornerRadius = 5
  forgotPasswordButton.titleLabel?.minimumScaleFactor = 8.0;
  forgotPasswordButton.titleLabel?.adjustsFontSizeToFitWidth = true;
  showSignInPwdCheckBox.delegate = self
  signInEmailTextField.delegate = self
  signInPasswordTextField.delegate = self
  signInPasswordTextField.isSecureTextEntry = true
  signInPasswordTextField.signInUnderlined()
  signInEmailTextField.signInUnderlined()
  }

  func didStartYourLoading() {
  transitionSignInButton.startLoadingAnimation()
  }
  func didTap(_ checkBox: BEMCheckBox) {
  print("check box :\(checkBox.tag):\(checkBox.on)")
  bemCheckbox = checkBox
  signInPasswordTextField.isSecureTextEntry = !signInPasswordTextField.isSecureTextEntry
  }
  @IBAction func signInButton(_ button: TKTransitionSubmitButton) {
  self.button = button
  if Reachability.isConnectedToNetwork(){
  self.userSignIn()
  }else{
  DispatchQueue.main.async {
  self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
  }
  }
  }

  func userSignIn(){
  let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
  let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
  let phoneRegex = "^[6-9][0-9]{9}$"
  let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
  if ((signInEmailTextField.text?.isEmpty)! || (signInPasswordTextField.text?.isEmpty)!){
  DispatchQueue.main.async {
  self.view.makeToast("please fill the fields", duration: 3.0, position: .center)
  }

  }else if emailTest.evaluate(with: signInEmailTextField.text) == false &&  predicate.evaluate(with: signInEmailTextField.text) == false {
  DispatchQueue.main.async {
  self.view.makeToast("Please enter a valid email id / mobile number", duration: 3.0, position: .center)
  }
  }
  else{
      DispatchQueue.main.async {
          SVProgressHUD.setDefaultStyle(.dark)
          SVProgressHUD.show(withStatus: "please wait...")
      }
  if let signInURL =  NSURL(string:Constants.URL_REQUEST_USER_LOGIN){
  var user:Login_user = Login_user()
  user.loginData = signInEmailTextField.text
  user.loginPassword = signInPasswordTextField.text
  guard let loginData = user.loginData,let loginPassword = user.loginPassword else{
  return
  }
  var  request = URLRequest(url:signInURL as URL)
  let signInParams  = ["logindata":loginData,"loginpassword":loginPassword] as [String:Any]
  request.httpMethod = "POST"
  do {
  let  json = try JSONSerialization.data(withJSONObject: signInParams)
  request.httpBody = json
  }
  catch let jsonErr  {
  print(jsonErr.localizedDescription)
  }
  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
  let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
      DispatchQueue.main.async {
          SVProgressHUD.dismiss()
      }
  guard let data = data, error == nil else
  {
  DispatchQueue.main.async {
  self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
      }
  print(error?.localizedDescription as Any )
  return
  }

  if let httpResponse = response as? HTTPURLResponse {
   print("error \(httpResponse.statusCode)")
  }
  do{
  let fetchData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
  print(fetchData)
  if let registerResponse = fetchData as? [String:Any]{
  print(registerResponse["description"]!)
  print(registerResponse["message"]!)
  if (registerResponse["code"] as! NSNumber == 200) {
  if let result = registerResponse["user_result"] as? [String:Any] {
  print(result["id"]!)
  DispatchQueue.main.async {
  UserDefaults.standard.set(result["name"], forKey: "name")
  UserDefaults.standard.set(result["email"], forKey: "email")
  UserDefaults.standard.set(result["mobile"], forKey: "mobile")
  UserDefaults.standard.set(result["id"], forKey: "id")
  }
  }
  DispatchQueue.main.async {
  UserDefaults.standard.set(registerResponse["message"]! as! String, forKey: "message")
  self.button.animate(1, completion: { () -> () in
  DispatchQueue.main.async {
  self.showSignInPwdCheckBox.isHidden = true
  DispatchQueue.main.async {
  let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
  let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
  let signupNavigation = UINavigationController(rootViewController: signupController)
  self.present(signupNavigation, animated: true, completion: nil)
  }
  }
  })
  }
  }
  else {
  DispatchQueue.main.async
  {
  self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
  }
  }
  }
  }catch let jsonErr{
  DispatchQueue.main.async
          {
      self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
      }
  print(jsonErr.localizedDescription)
  }

  }
  task.resume()
  }else{
  DispatchQueue.main.async
  {
  self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
  }
  }
  }
  }

  /* Method To Go For Home Screen */

  func movingToHome(){
  self.showSignInPwdCheckBox.isHidden = true
  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
  let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
  let signupNavigation = UINavigationController(rootViewController: signupController)
  self.present(signupNavigation, animated: true, completion: nil)
  }

  @IBAction func signUpButton(_ sender: Any) {
  self.movingToSignUp()
  }

  /* signup method */

  func movingToSignUp(){
  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
  let signupController = storyBoard.instantiateViewController(withIdentifier: "SignUp") as! SignUpController
  self.present(signupController, animated: true, completion: nil)
  }

  @IBAction func forgotPasswordButton(_ sender: Any) {
  self.forgotPassword()
  }

  /* Forgot Password Method */

  func forgotPassword(){
  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
  let signupController = storyBoard.instantiateViewController(withIdentifier: "forgot") as! ForgotPasswordController
  let signupNavigation = UINavigationController(rootViewController: signupController)
  self.present(signupNavigation, animated: true, completion: nil)
  }

  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
  if textField == signInEmailTextField{
  signInPasswordTextField.becomeFirstResponder()
  }else{

  textField.resignFirstResponder()
  }
  return true
  }
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
  self.view.endEditing(true)
  }

  func textFieldDidEndEditing(_ textField: UITextField) {

  }
  func textFieldDidBeginEditing(_ textField: UITextField) {

  }
  func isValidEmailAddress(emailAddressString: String) -> Bool {
  var returnValue = true
  let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"

  do {
  let regex = try NSRegularExpression(pattern: emailRegEx)
  let nsString = emailAddressString as NSString
  let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))

  if results.count == 0
  {
  returnValue = false
  }

  } catch let error as NSError {
  print("invalid regex: \(error.localizedDescription)")
  returnValue = false
  }
  return  returnValue
  }

  }//class

  extension UITextField {
  func signInUnderlined(){
  let border = CALayer()
  let width = CGFloat(1.0)
  border.borderColor = UIColor.white.cgColor
  border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
  border.borderWidth = width
  self.layer.addSublayer(border)
  self.layer.masksToBounds = true
  }
  }




