  //
  //  GuestViewController.swift
  //  SmileyServe
  //
  //  Created by Apple on 07/07/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //

  import UIKit
  var GuestapartmentId : String?
  class GuestViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

  var locationListData :[GuestViewControllerModel?] = []
   @IBOutlet var guestButtontitle: UIButton!
  @IBAction func guestButton(_ sender: Any) {
  DispatchQueue.main.async {
  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
  let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
  let signupNavigation = UINavigationController(rootViewController: signupController)
  self.present(signupNavigation, animated: true, completion: nil)

  }
  }


  //let cellReuseIdentifier = "cell"

  //@IBOutlet weak var heightConstraint: NSLayoutConstraint!

  // Using simple subclass to prevent the copy/paste menu
  // This is optional, and a given app may want a standard UITextField
  @IBOutlet weak var textField: NoCopyPasteUITextField!
  @IBOutlet weak var tableView: UITableView!

  // If user changes text, hide the tableView
  @IBAction func textFieldChanged(_ sender: AnyObject) {
  tableView.isHidden = true
  guestButtontitle.isHidden = false
  }

  override func viewDidLoad() {
  super.viewDidLoad()
  //self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)

guestButtontitle.layer.cornerRadius = 5
  navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"backArrow.png"), style: .plain, target: self, action: #selector(handleGuestnavigation))
  navigationItem.title = "Choose your Apartment"
  UINavigationBar.appearance().barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
  UINavigationBar.appearance().tintColor = UIColor.white
  UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
  tableView.delegate = self
  tableView.dataSource = self
  textField.delegate = self
  guestButtontitle.isHidden = false
  tableView.isHidden = true

  // Manage tableView visibility via TouchDown in textField
  textField.addTarget(self, action: #selector(textFieldActive), for: UIControlEvents.touchDown)
  self.tableView.reloadData()

  getWebService()
  }

  func handleGuestnavigation(){
  self.dismiss(animated: false, completion: nil)
  }

  //    override func viewDidLayoutSubviews()
  //    {
  //        // Assumption is we're supporting a small maximum number of entries
  //        // so will set height constraint to content size
  //        // Alternatively can set to another size, such as using row heights and setting frame
  //        heightConstraint.constant = tableView.contentSize.height
  //    }

  override func didReceiveMemoryWarning() {
  super.didReceiveMemoryWarning()
  }

  // Manage keyboard and tableView visibility
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
  {

  guestButtontitle.isHidden = false

  guard let touch:UITouch = touches.first else
  {
  return;
  }
  if touch.view != tableView
  {
  textField.endEditing(true)
  tableView.isHidden = true
  guestButtontitle.isHidden = false
  }
  }

  // Toggle the tableView visibility when click on textField
  func textFieldActive() {
  tableView.isHidden = !tableView.isHidden
  guestButtontitle.isHidden = !guestButtontitle.isHidden
  }

  // MARK: UITextFieldDelegate
  func textFieldDidEndEditing(_ textField: UITextField) {
  // TODO: Your app can do something when textField finishes editing
  print("The textField ended editing. Do something based on app requirements.")

  guestButtontitle.isHidden = false

  }
  func textFieldDidBeginEditing(_ textField: UITextField) {


  guestButtontitle.isHidden = true
  }
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
  textField.resignFirstResponder()
  return true
  }

  // MARK: UITableViewDataSource
  func numberOfSections(in tableView: UITableView) -> Int {
  return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  return locationListData.count;
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
  // Set text from the data model
  cell.textLabel?.text = (self.locationListData[indexPath.row]?.title)! + "," + (self.locationListData[indexPath.row]?.location)!
    
  // cell.textLabel?.text = listApartment[indexPath.row] as String

  cell.textLabel?.font = textField.font
  return cell
  }

  // MARK: UITableViewDelegate
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  // Row selected, so set textField to relevant value, hide tableView
  // endEditing can trigger some other action according to requirements
  textField.text = locationListData[indexPath.row]?.title!
  GuestapartmentId = locationListData[indexPath.row]?.id
  tableView.isHidden = true
  textField.endEditing(true)
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
  return 0.0
  }

  func getWebService(){

  if  let requestLocationListURL = NSURL(string: "http://smiley.inventbird.in/api/locationlist"){
  var urlRequest = URLRequest(url: requestLocationListURL as URL)
  urlRequest.httpMethod = "GET"
  let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in

  if (error != nil){

  print(error!)
  }

  if let response = response {

  print(response)
  }

  guard let data = data else {

  return
  }
 self.locationListData = [GuestViewControllerModel]()
  do {

  let listData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]

  print(listData)
  let list: [[String: String]] = listData["apartment_result"] as! [[String: String]]

    for apartment in list {
  var guestModel = GuestViewControllerModel()
  print(apartment["location"]!)
  print(apartment["title"]!)
    guard let address = apartment["address"],let apartmentID =  apartment["id"],let location = apartment["location"],let title = apartment["title"]
      else{
      
      return
          }
    guestModel.address = address
   guestModel.id     = apartmentID
   guestModel.location  = location
   guestModel.title   = title
  self.locationListData.append(guestModel)
  if self.locationListData.count > 0{
  DispatchQueue.main.async {
  self.tableView.reloadData()
  }
  }
  }

  }
  catch let jsonErr{

  print(jsonErr.localizedDescription)
  }

  })
  task.resume()

  }
  }
  }//class






