//
//  ComingTomorrowTableViewCell.swift
//  SmileyServe
//
//  Created by Apple on 23/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class ComingTomorrowTableViewCell: UITableViewCell {
  @IBOutlet weak var cmgTomorrowProductPrice: UILabel!
  
  @IBOutlet weak var cmgTomorrowProductQuantity: UILabel!
  @IBOutlet weak var cmgTomorrowDeleievryCharges: UILabel!
  @IBOutlet weak var comingTomorrowProductImage: CustomImageView!
  
  @IBOutlet weak var cmgTomorrowProductUnits: UILabel!
  @IBOutlet weak var cmgTomorrowProductName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
