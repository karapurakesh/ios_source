//
//  OrderCheckOutPaymentSuccess.swift
//  SmileyServe
//
//  Created by Apple on 27/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct OrderCheckOutPaymentSuccessModel {
  
  var userid : String?
  var order_date: String?
  var checkout_date : String?
  var smileycash : String?
  
}
