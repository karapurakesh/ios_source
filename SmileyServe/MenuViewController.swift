  //
  //  MenuViewController.swift
  //  AKSwiftSlideMenu
  //
  //  Created by Ashish on 21/09/15.
  //  Copyright (c) 2015 Kode. All rights reserved.
  //
  
  import UIKit
  
  protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
  }
  
  class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var strUserNameLabel: UILabel!
    /**
     *  Array to display menu options
     */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
     *  Transparent button to hide menu
     */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
     *  Array containing menu options
     */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
     *  Menu button which was tapped to display the menu
     */
    var btnMenu : UIButton!
    
    /**
     *  Delegate of the MenuVC
     */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //  view?.backgroundColor = UIColor(white: 1, alpha: 0.0)
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        tblMenuOptions.tableFooterView = UIView()
        tblMenuOptions.separatorStyle = UITableViewCellSeparatorStyle.none
        self.strUserNameLabel.text = UserDefaults.standard.value(forKey:"name") as? String
        // Do any additional setup after loading the view.
        tblMenuOptions.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblMenuOptions.reloadData()
        updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions(){
        
        let id = UserDefaults.standard.value(forKey: "id")
        if id != nil{
            arrayMenuOptions.append(["title":"Home", "icon":"ic_menu_home.png"])
            arrayMenuOptions.append(["title":"Profile", "icon":"Forma 1 copy 5.png"])
            //arrayMenuOptions.append(["title":"My Subscriptions", "icon":"Forma 1.png"])
            //arrayMenuOptions.append(["title":"My Calendar",  "icon":"calendar (1).png"])
            arrayMenuOptions.append(["title":"My Transactions", "icon":"ic_menu_transactions.png"])
            arrayMenuOptions.append(["title":"FAQS", "icon":"Vector Smart Object21.png"])
            arrayMenuOptions.append(["title":"Contact Us", "icon":"ic_menu_contact_us.png"])
            arrayMenuOptions.append(["title":"About Us", "icon":"AppIcon.png"])
            arrayMenuOptions.append(["title":"Share App", "icon":"shareicon.png"])
            arrayMenuOptions.append(["title":"SignOut", "icon":"ic_menu_sign_out.png"])

            tblMenuOptions.reloadData()
            
        }else{
            
            arrayMenuOptions.append(["title":"Home", "icon":"ic_menu_home.png"])
            arrayMenuOptions.append(["title":"Login", "icon":"ic_menu_sign_out.png"])
            arrayMenuOptions.append(["title":"About Us", "icon":"AppIcon.png"])

            tblMenuOptions.reloadData()
        }
        tblMenuOptions.reloadData()
        
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        
        imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
        lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
        
        //        let userApiKey = UserDefaults.standard.value(forKey: "userApiKey")
        
        
        
        //
        if btn.tag == 2{
            
            if userId == nil {
                DispatchQueue.main.async {
                    UserDefaults.standard.removeObject(forKey: "id")
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                }
            }
            
        }
        
        if btn.tag == 7  {
                        
            DispatchQueue.main.async {
                
                UserDefaults.standard.removeObject(forKey: "mobile")
                UserDefaults.standard.removeObject(forKey:"name")
                UserDefaults.standard.removeObject(forKey: "id")
                UserDefaults.standard.removeObject(forKey: "email")
                
                //            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                //          UserDefaults.standard.synchronize()
                
                DispatchQueue.main.async {
                    
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                    
                }
                
            }
        } 
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    
    
    
  }
