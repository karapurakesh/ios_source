    //
    //  MyCaalendarViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 06/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    
    import UIKit
    import FSCalendar
    import SVProgressHUD
    import SDWebImage
    class MyCalendarViewController: BaseViewController, FSCalendarDataSource, FSCalendarDelegate,UITableViewDelegate,UITableViewDataSource,FSCalendarDelegateAppearance {
        //  var decrement = Int()
        @IBOutlet var calendarShowDateLabel: DesignView!
        var cartCheckoutPropertiesResponseArray :[CartCheckOutPropertiesResponseModel?] = []
        var buttonTag = Int()
        //  var increment = Int()
        var indexpath = Int()
        var incDecUpdateQty = Int()
        var decReference = String()
        var incrementReference = String()
        var incrementQunatityLabel:Int = 0
        var buttoIndexPath = Int()
        let todayDate = Date()
        var numberOfRowsInsection : [Int] = []
        var isToday: Bool?
        var productOriginalQuantity : [String] = []
        var productQty : [Int] = []
        var qty : Int?
        var totalProductPrice : Float = 0.0
        var cancelDayOrderArrayModel :[CancelDayOrderModel?] = []
        @IBOutlet weak var calendarProductReviewView: UIView!
        @IBAction func reviewProductActionBtn(_ sender: Any) {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "reviewViewController") as! ReviewOrderViewController
            self.present(controller, animated: true, completion: nil)
        }
        @IBAction func reviewOrderBtnAction(_ sender: Any) {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "reviewViewController") as! ReviewOrderViewController
            self.present(controller, animated: true, completion: nil)
            
        }
        @IBOutlet weak var calendarProductTotalPriceLbl: UILabel!
        @IBOutlet weak var calendarReviewProductQuantityLbl: UILabel!
        @IBOutlet weak var calendarDeleteBtnReference: UIButton!
        @IBOutlet weak var calendarProductQuantity: UILabel!
        @IBOutlet weak var calendarProductPricr: UILabel!
        @IBOutlet weak var calendarDateLabel: UILabel!
        @IBOutlet weak var calendarProductDelCharges: UILabel!
        @IBOutlet weak var calendarProductUnits: UILabel!
        @IBOutlet weak var calendarProdductTitle: UILabel!
        @IBOutlet weak var calendarProductTableView : UITableView!
        @IBOutlet weak var calendarImageView: UIImageView!
        var calendarDates: [CalendarDates?] = []
        var calenProducts: [CalendarProductsModel?] = []
        var dateSelected: String?
        let userId = UserDefaults.standard.value(forKey: "id") as? String
        @IBOutlet weak var dateLabel: UILabel!
        @IBOutlet weak var productsView: UIView!
        @IBOutlet weak var productViews : UIView!
        fileprivate lazy var dateFormatter2: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
        
        var datesWithEvent: [String] = []
        var productDatesWithQuantity : [String] = []
        var productDatesquantity : [Int] = []
        var singleEventDate : [String] = []
        var twoEventDate : [String] = []
        var threeEventDate : [String] = []
        var multipleEventDate : [String] = []
        deinit {
            print("\(#function)")
        }
        
        func handleCart() {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
            let cartNavigationController = UINavigationController(rootViewController: controller)
            self.present(cartNavigationController, animated: true, completion: nil)
        }
        func calendar(_ calendar: FSCalendar, hasEventFor date: Date) -> Bool {
            let dateString = self.dateFormatter2.string(from: date as Date)
            return datesWithEvent.contains(dateString)
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            numberOfRowsInsection = [self.calenProducts.count]
            print("count",numberOfRowsInsection)
            return self.calenProducts.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            indexpath = indexPath.row
            let cell = tableView.dequeueReusableCell(withIdentifier: "calendarProductCell", for: indexPath) as! CalendarProductTableViewCell
            index
            cell.selectionStyle = .none
            cell.calendarProductQunatityDecrementBtn.addTarget(self, action: #selector(self.decerementButton), for: .touchUpInside)
            cell.calendarProductQunatityDecrementBtn.tag = indexPath.row
            cell.calendarProductQuantityBtn.addTarget(self, action: #selector(self.calendarIncrement), for:.touchUpInside)
            cell.calendarProductQuantityBtn.tag = indexPath.row
            
            
            
            
          
            //
            //    cell.calendarProductImageView.sd_setImage(with: URL(string: (self.calenProducts[indexPath.row]?.product_image)!))
            if let orderDate = self.calenProducts[indexPath.row]?.orderDate{
                let orderDate = orderDate
                let date = Date()
                let todayDate = formatter.string(from: date)
                let tomorrow = formatter.string(from: Date().tmr)
                let outputFormat = DateFormatter()
                outputFormat.locale = NSLocale(localeIdentifier:"en_US") as Locale!
                outputFormat.dateFormat = "HH:mm:ss"
                if outputFormat.string(from: date as Date) >= iosCuttOfftime && orderDate > tomorrow  {
                    let  todayDate = Date()
                    let  tomorowDate = Calendar.current.date(byAdding: .day, value: 1, to: todayDate)
                    if let tommorrowDate = tomorowDate{
                        let dayAfterTomorrow = Calendar.current.date(byAdding: .day, value: 1, to: tommorrowDate)
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        if let dayAfterTomoorowString = dayAfterTomorrow{
                            let  tomorrowDateString = dateFormatter.string(from: dayAfterTomoorowString)
                            cell.calendarProductQunatityDecrementBtn.isHidden = false
                            cell.calendarProductQuantityBtn.isHidden = false
                            self.calendarDeleteBtnReference.isHidden = false
                            self.calendarProductTableView.isHidden = false
                            self.calendarDateLabel.isHidden = false
                            self.calendarShowDateLabel.isHidden = false
                            self.calendarDeleteBtnReference.isHidden = false
                        }
                    }
                }else  if orderDate < todayDate {
                    cell.calendarProductQunatityDecrementBtn.isHidden = true
                    cell.calendarProductQuantityBtn.isHidden = true
                    self.calendarDeleteBtnReference.isHidden = true
                    self.calendarProductTableView.isHidden = false
                    self.calendarDateLabel.isHidden = false
                    self.calendarShowDateLabel.isHidden = false
                }else if orderDate == tomorrow && outputFormat.string(from: date as Date) <= iosCuttOfftime{
                    cell.calendarProductQunatityDecrementBtn.isHidden = false
                    cell.calendarProductQuantityBtn.isHidden = false
                    self.calendarDeleteBtnReference.isHidden = false
                    self.calendarProductTableView.isHidden = false
                    self.calendarDateLabel.isHidden = false
                    self.calendarShowDateLabel.isHidden = false
                    self.calendarDeleteBtnReference.isHidden = false
                }else if orderDate == tomorrow && outputFormat.string(from: date as Date) >= iosCuttOfftime {
                    cell.calendarProductQunatityDecrementBtn.isHidden = true
                    cell.calendarProductQuantityBtn.isHidden = true
                    self.calendarDeleteBtnReference.isHidden = true
                    self.calendarProductTableView.isHidden = false
                    self.calendarDateLabel.isHidden = false
                    self.calendarShowDateLabel.isHidden = false
                }else if outputFormat.string(from: date as Date) <= iosCuttOfftime && orderDate < tomorrow {
                    cell.calendarProductQunatityDecrementBtn.isHidden = true
                    cell.calendarProductQuantityBtn.isHidden = true
                    self.calendarDeleteBtnReference.isHidden = true
                    self.calendarProductTableView.isHidden = false
                    self.calendarDateLabel.isHidden = false
                    self.calendarShowDateLabel.isHidden = false
                }
                else if orderDate > todayDate {
                    cell.calendarProductQunatityDecrementBtn.isHidden = false
                    cell.calendarProductQuantityBtn.isHidden = false
                    self.calendarDeleteBtnReference.isHidden = false
                    self.calendarProductTableView.isHidden = false
                    self.calendarDateLabel.isHidden = false
                    self.calendarShowDateLabel.isHidden = false
                    self.calendarDeleteBtnReference.isHidden = false
                }
                else{
                    cell.calendarProductQunatityDecrementBtn.isHidden = true
                    cell.calendarProductQuantityBtn.isHidden = true
                    self.calendarDeleteBtnReference.isHidden = true
                    self.calendarProductTableView.isHidden = false
                    self.calendarDateLabel.isHidden = false
                    self.calendarShowDateLabel.isHidden = false
                    let  today = Date()
                    let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
                    let  dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    if let tomorrowDateString = tomorrow {
                        let tomorrowString = dateFormatter.string(from: tomorrowDateString)
                        cell.calendarProductQunatityDecrementBtn.isHidden = true
                        cell.calendarProductQuantityBtn.isHidden = true
                        self.calendarDeleteBtnReference.isHidden = true
                        self.calendarProductTableView.isHidden = false
                        self.calendarDateLabel.isHidden = false
                        self.calendarShowDateLabel.isHidden = false
                    }
                }
                cell.calendarProductImageView.setImage(from: (self.calenProducts[indexPath.row]?.product_image)!)
                
                let dcCharges = self.calenProducts[indexPath.row]?.delivery_charge
                
                if dcCharges != "0" {
                    cell.dcWidthConst.constant = 31
                    cell.rcWidthConst.constant = 21
                    cell.drGapWidthConst.constant = 8

                    cell.calendarProductDeleiveryCharges.text = self.calenProducts[indexPath.row]?.delivery_charge
                }else {
                    cell.calendarProductDeleiveryCharges.text = ""
                    cell.dcWidthConst.constant = 0
                    cell.rcWidthConst.constant = 0
                    cell.drGapWidthConst.constant = 0

                }

                cell.calendarProductPrice.text = self.calenProducts[indexPath.row]?.product_orgprice
                cell.calendarProductTitle.text = self.calenProducts[indexPath.row]?.product_name
                cell.calendarProductUnits.text = self.calenProducts[indexPath.row]?.product_units
                cell.calendarProductQunatity.text = self.calenProducts[indexPath.row]?.quantity
                UserDefaults.standard.set(cell.calendarProductQunatity.text, forKey: "qty")
            }
            
            if let menuId = self.calenProducts[indexPath.row]?.menu_id {
                
                if menuId == "15" {
                    cell.calendarProductQunatityDecrementBtn.isHidden = true
                    cell.calendarProductQuantityBtn.isHidden = true
                }
            }
            
            return cell
        }
        
        func calendarIncrement(_sender:UIButton){
            incrementQunatityLabel += 1
            buttonTag = _sender.tag
            productOriginalQuantity = UserDefaults.standard.value(forKey: "productQuantity") as! [String]
            for productQuantity in productOriginalQuantity{
                if let productQuantity = Int(productQuantity) {
                    self.productQty.append(productQuantity)
                }
            }
            print(buttonTag)
            calendarReviewProductQuantityLbl.text = "\(incrementQunatityLabel)"
            //  self.calendarProductTotalPriceLbl.text = "Rs." + (self.calenProducts[buttonTag]?.product_price)!
            if let quantity = (self.calenProducts[buttonTag]?.quantity){
                incrementReference = quantity
                if let incerementReference = Int(incrementReference) {
                    var increment = incerementReference
                    increment += 1
                    UserDefaults.standard.set(increment, forKey: "increment")
                    self.calenProducts[buttonTag]?.quantity = "\(String(describing:increment))"
                    if Reachability.isConnectedToNetwork(){
                        self.updateCartQuantity()
                    }else{
                        DispatchQueue.main.async {
                            self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                        }
                    }
                }
            }
        }
        
        func decerementButton(_sender:UIButton) {
            //  incDecUpdateQty = UserDefaults.standard.value(forKey: "increment") as! Int
            //  if  incDecUpdateQty > self.productQty[buttoIndexPath] {
            //     incDecUpdateQty -= 1
            //    self.updateCartQuantity()
            //      }
            productOriginalQuantity = UserDefaults.standard.value(forKey: "productQuantity") as! [String]
            for productQuantity in productOriginalQuantity{
                if let productQuantity = Int(productQuantity){
                    self.productQty.append(productQuantity)
                }
            }
            
            self.calendarProductTableView.isHidden = false
            buttoIndexPath = _sender.tag
            print(buttoIndexPath)
            if let quantity = (self.calenProducts[buttoIndexPath]?.quantity){
                decReference = quantity
                if let decRefernce = Int(decReference){
                    var decrement = decRefernce
                    if decrement > 0{
                        decrement -= 1
                        UserDefaults.standard.set(decrement, forKey: "decrement")
                        if productQty[buttoIndexPath] == decrement  {
                            self.calendarProductReviewView.isHidden = true
                            //  incrementQunatityLabel = 0
                            calendarReviewProductQuantityLbl.text = "\(incrementQunatityLabel)"
                            if Reachability.isConnectedToNetwork(){
                                self.updateDecrementCart()
                            }else{
                                DispatchQueue.main.async {
                                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                                }
                            }
                            self.calendarProductTableView.reloadData()
                        }
                        else if decrement == 0 {
                            if Reachability.isConnectedToNetwork(){
                                self.createConfirmationToDeleteQuantity()
                            }else{
                                DispatchQueue.main.async {
                                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                                }
                            }
                        }else if decrement > productQty[buttoIndexPath] {
                            if incrementQunatityLabel > 0{
                                incrementQunatityLabel -= 1
                                calendarReviewProductQuantityLbl.text = "\(incrementQunatityLabel)"
                            }
                            
                            //    self.calendarProductTotalPriceLbl.text = "Rs." + (self.calenProducts[buttonTag]?.product_price)!
                            if Reachability.isConnectedToNetwork(){
                                self.updateDecrementCart()
                            }else{
                                DispatchQueue.main.async {
                                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                                }
                            }
                            
                        }
                        else{
                            
                            if Reachability.isConnectedToNetwork(){
                                self.createConfirmationToUpdateDayOrder()
                            }else{
                                DispatchQueue.main.async {
                                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                                }
                            }
                            
                        }
                        self.calenProducts[buttoIndexPath]?.quantity = "\(String(describing: decrement))"
                        //  calendarReviewProductQuantityLbl.text = self.calenProducts[buttoIndexPath]?.quantity
                    }
                    //  if (Int(decReference) == 1){
                    //  self.calendarProductReviewView.isHidden = true
                    //  self.createConfirmationToDeleteQuantity()
                    //  }
                    //    }else{
                    //
                    //    }
                }
            }
        }
        
        func cancelProduct() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let cancelProductUrl  =  NSURL(string:Constants.URL_REQUEST_USER_CANCELPRODUCT) {
                var request = URLRequest(url:cancelProductUrl as URL)
                var cancelProductModel = CancelProductModel()
                cancelProductModel.userid = userId
                cancelProductModel.orderdate = self.calenProducts[buttoIndexPath]?.orderDate
                cancelProductModel.cartid  = self.calenProducts[buttoIndexPath]?.cartid
                guard let orderDate = cancelProductModel.orderdate,let userId = cancelProductModel.userid,let userCartId = cancelProductModel.cartid  else {return}
                let cancelProductParam = ["orderdate":orderDate,"userid": userId,"cartid":userCartId] as
                    [String:Any]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cancelProductParam)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    
                    do{
                        guard let updateCartJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                            return
                        }
                        if (updateCartJsonObject["code"] as! NSNumber == 200){
                            
                            DispatchQueue.main.async {
                                
                                //    if  updateCartModel.originalQuantity ==  updateCartModel.quantity{
                                //
                                //      self.calenProducts.remove(at: self.indexpath)
                                //      self.calendarProductTableView!.reloadData()
                                //    }else{
                                //        self.calendarProductTableView!.reloadData()
                                //        }
                                
                                //  let productQuantity = String(self.decrement)
                                
                                DispatchQueue.main.async {
                                    guard  let decrement = UserDefaults.standard.value(forKey: "decrement") as? Int else{
                                        return
                                    }
                                    if decrement == 0 && self.indexpath == 0 && self.calenProducts.count == 1{
                                        self.calenProducts.remove(at: self.buttoIndexPath)
                                        self.calendarShowDateLabel.isHidden = true
                                        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let calendarController = storyBoard.instantiateViewController(withIdentifier: "Calendar") as! MyCalendarViewController
                                        let CalendarNavigation = UINavigationController(rootViewController: calendarController)
                                        self.present(CalendarNavigation, animated: false, completion: nil)
                                        
                                    }else if decrement < self.productQty[self.buttoIndexPath] &&  decrement >= 1 {
                                        self.calendarShowDateLabel.isHidden = true
                                        self.calendarProductTableView!.reloadData()
                                        self.calendarProductReviewView.isHidden = true
                                    }else{
                                        self.calenProducts.remove(at: self.buttoIndexPath)
                                        //  self.calendarShowDateLabel.isHidden = true
                                        self.calendarProductReviewView.isHidden = true
                                        //  self.calenProducts[self.buttonTag]?.quantity = String(self.productQty[self.buttoIndexPath])
                                        self.calendarProductTableView!.reloadData()
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    self.view.makeToast(updateCartJsonObject["description"]! as? String, duration: 3.0, position: .center)
                                    
                                }
                                //    if self.decrement == 0 {
                                //  //  self.calendarDateLabel.isHidden = true
                                ////  self.calendarDeleteBtnReference.isHidden = true
                                //  DispatchQueue.main.async {
                                //      self.view.makeToast(updateCartJsonObject?["description"]! as! String, duration: 4.0, position: CGPoint(x: 180, y: 340))
                                //    }
                                //
                                //    }else{
                                //
                                //  DispatchQueue.main.async {
                                //  self.calendarProductReviewView.isHidden = true
                                //  self.calendarProductTableView!.reloadData()
                                //  }
                                //    }
                                
                                
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.view.makeToast(updateCartJsonObject["description"]! as? String, duration: 3.0, position: .center)
                            }
                        }
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                            print(jsonErr.localizedDescription)
                        }
                    }
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                }
            }
        }
        
        func handleNavigationArrow() {
            self.cartCheckOutDeleteAll()
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let signupNavigation = UINavigationController(rootViewController: signupController)
            self.present(signupNavigation, animated: false, completion: nil)
        }
        
        func calendarProducts() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let calendarProductsURL = NSURL(string: Constants.URL_REQUEST_USER_CALENDARPRODUCTS) {
                var request = URLRequest(url: calendarProductsURL as URL)
                var calendars: CalendarProductsModel = CalendarProductsModel()
                calendars.userid = userId
                calendars.date = dateSelected
                guard let userId =  calendars.userid,let calendarProductDate = calendars.date else {return}
                let calendarListParams = ["userid": userId, "date": calendarProductDate] as  [String: Any]
                
                request.httpMethod = "POST"
                do {
                    let json = try JSONSerialization.data(withJSONObject: calendarListParams)
                    request.httpBody = json
                }
                catch let jsonErr {
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any)
                        return
                    }
                    
                    self.calenProducts = [CalendarProductsModel]()
                    
                    do {
                        guard let calendarProductJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{
                            return
                        }
                        print(calendarProductJson)
                        print(calendarProductJson["description"]!)
                        print(calendarProductJson["message"]!)
                        if (calendarProductJson["code"] as! NSNumber == 200) {
                            let productId  = (calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]])!.filter({ $0["productid"] != nil }).map({ $0["productid"]! })
                            print(productId)
                            UserDefaults.standard.set(productId, forKey: "productId")
                            let cartId  = (calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]])!.filter({ $0["cartid"] != nil }).map({ $0["cartid"]! })
                            print(cartId)
                            UserDefaults.standard.set(cartId, forKey: "cartid")
                            let order_date = (calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]])!.filter({ $0["order_date"] != nil }).map({ $0["order_date"]! })
                            print(order_date)
                            UserDefaults.standard.set(order_date, forKey: "order_date")
                            let productQuantity  = (calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]])!.filter({ $0["qty"] != nil }).map({ $0["qty"]! })
                            print(productQuantity)
                            UserDefaults.standard.set(productQuantity, forKey: "productQuantity")
                            let product_price  = (calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]])!.filter({ $0["product_price"] != nil }).map({ $0["product_price"]! })
                            print(product_price)
                            UserDefaults.standard.set(product_price, forKey: "total_price")
                            //    if let cartTotalPrice = UserDefaults.standard.array(forKey: "product_price")  as? [String] {
                            //    var numberConverterArray = [Double]()
                            //    for totalpriceValue in cartTotalPrice{
                            //
                            //    if totalpriceValue == "nil"{
                            //    print("totalpriceValue is nil")
                            //    }else{
                            //    if let cost = Double(totalpriceValue) {
                            //    numberConverterArray.append(cost)
                            //    }
                            //    }
                            //    }
                            //
                            //    let numberPrice  = numberConverterArray.reduce(0, {sum,number in sum+number})
                            //    let totalProductValue = String(numberPrice)
                            //    UserDefaults.standard.set(totalProductValue, forKey: "totalProductValue")
                            //    if let totalProductPrice =  UserDefaults.standard.value(forKey: "totalProductValue") as? String{
                            //    DispatchQueue.main.async {
                            //    self.calendarProductTotalPriceLbl.text = "Rs." + totalProductPrice
                            //
                            //    }
                            //    }
                            //    }
                            UserDefaults.standard.set(product_price, forKey: "product_price")
                            if let calendarProductResult = calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]] {
                                for calendarProductsResponse in calendarProductResult {
                                    var calendarLists = CalendarProductsModel()
                                    guard let productCalendarName = calendarProductsResponse["product_name"] as? String, let productCalendarUnits = calendarProductsResponse["product_units"] as? String, let calendarProductDeleiverCharges = calendarProductsResponse["delivery_charge"] as? String, let productCalendarImages = calendarProductsResponse["product_image"] as? String, let productQuantity = calendarProductsResponse["qty"] as? String, let productOrderDate = calendarProductsResponse["order_date"] as? String, let productPrice = calendarProductsResponse["product_price"] as? String,let cartid = calendarProductsResponse["cartid"] as? String,let productId = calendarProductsResponse["productid"] as? String,let product_org_price = calendarProductsResponse["product_orgprice"] as? String,let product_menu_id = calendarProductsResponse["menu_id"] as? String  else {
                                        
                                        return
                                    }
                                    calendarLists.product_name = productCalendarName
                                    calendarLists.product_units = productCalendarUnits
                                    calendarLists.delivery_charge = calendarProductDeleiverCharges
                                    calendarLists.product_image = productCalendarImages
                                    calendarLists.quantity = productQuantity
                                    calendarLists.orderDate = productOrderDate
                                    calendarLists.product_price = productPrice
                                    calendarLists.cartid = cartid
                                    calendarLists.product_orgprice = product_org_price
                                    calendarLists.productid = productId
                                    calendarLists.menu_id = product_menu_id
                                    UserDefaults.standard.set(calendarLists.cartid, forKey: "cartid")
                                    self.calenProducts.append(calendarLists)
                                    
                                }
                            }
                            
                            DispatchQueue.main.async {
                                let orderDate = UserDefaults.standard.array(forKey: "order_date") as? [String]
                                for orderDateValue in orderDate!{
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "yyyy-MM-dd"
                                    //    let dateString = formatter.string(from: orderDateValue)
                                    let date = formatter.date(from: orderDateValue)
                                    formatter.dateFormat = "dd-MMM-yyyy"
                                    formatter.dateStyle = .long
                                    
                                    let finalOrderDateString = formatter.string(from: date!)
                                    
                                    //    self.calendarDateLabel.text = self.calenProducts[0]?.orderDate
                                    self.calendarDateLabel.text = finalOrderDateString
                                }
                                
                                //    self.calendarProdductTitle.text = self.calenProducts[0]?.product_name
                                //    self.calendarProductUnits.text = self.calenProducts[0]?.product_units
                                //    self.calendarProductPricr.text = self.calenProducts[0]?.product_price
                                //    self.calendarProductQuantity.text = self.calenProducts[0]?.quantity
                                //    self.calendarProductDelCharges.text = self.calenProducts[0]?.delivery_charge
                                //    self.calendarImageView.sd_setImage(with: URL(string: (self.calenProducts[0]?.product_image)!))
                                
                                self.calendarProductTableView.reloadData()
                                self.isToday = self.gregorian.isDateInToday(self.todayDate)
                                for dateWithEvent in self.datesWithEvent {
                                    let dateString = self.dateFormatter2.string(from: self.todayDate)
                                    if  self.isToday == dateWithEvent.contains(dateString){
                                        self.calendarDateLabel.isHidden = false
                                        self.calendarShowDateLabel.isHidden = false
                                        self.calendarDeleteBtnReference.isHidden = false
                                        self.calendarProductTableView.isHidden = false
                                        
                                    }
                                    
                                    //      else{
                                    //
                                    //        self.calendarDateLabel.isHidden = true
                                    //        //    self.productsView.isHidden = true
                                    //        self.calendarProductTableView.isHidden = true
                                    //        self.calendarDeleteBtnReference.isHidden = true
                                    //      }
                                }
                                
                            }
                        }
                    } catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                            print(jsonErr.localizedDescription)
                        }
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                }
                
            }
        }
        
        func calendarTodayProducts() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let calendarProductsURL = NSURL(string: Constants.URL_REQUEST_USER_CALENDARPRODUCTS){
                var request = URLRequest(url: calendarProductsURL as URL)
                var calendars: CalendarProductsModel = CalendarProductsModel()
                calendars.userid = userId
                let dateString = self.dateFormatter2.string(from: self.todayDate)
                calendars.date = dateString
                guard let userId =  calendars.userid,let calendarTodayProducts =   calendars.date else{return}
                let calendarListParams = ["userid":userId, "date": calendarTodayProducts] as  [String: Any]
                request.httpMethod = "POST"
                do {
                    let json = try JSONSerialization.data(withJSONObject: calendarListParams)
                    request.httpBody = json
                }
                catch let jsonErr {
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any)
                        return
                    }
                    
                    self.calenProducts = [CalendarProductsModel]()
                    
                    do {
                        guard let calendarProductJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else {return}
                        print(calendarProductJson)
                        print(calendarProductJson["description"]!)
                        print(calendarProductJson["message"]!)
                        
                        if (calendarProductJson["code"] as! NSNumber == 200) {
                            let productId  = (calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]])!.filter({ $0["productid"] != nil }).map({ $0["productid"]! })
                            print(productId)
                            UserDefaults.standard.set(productId, forKey: "productId")
                            let cartId  = (calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]])!.filter({ $0["cartid"] != nil }).map({ $0["cartid"]! })
                            print(cartId)
                            UserDefaults.standard.set(cartId, forKey: "cartid")
                            let order_date = (calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]])!.filter({ $0["order_date"] != nil }).map({ $0["order_date"]! })
                            print(order_date)
                            UserDefaults.standard.set(order_date, forKey: "order_date")
                            let productQuantity  = (calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]])!.filter({ $0["qty"] != nil }).map({ $0["qty"]! })
                            print(productQuantity)
                            UserDefaults.standard.set(productQuantity, forKey: "productQuantity")
                            let product_price  = (calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]])!.filter({ $0["product_price"] != nil }).map({ $0["product_price"]! })
                            print(product_price)
                            UserDefaults.standard.set(product_price, forKey: "product_price")
                            if let calendarProductResult = calendarProductJson["calenderproduct_result"] as? [[String: AnyObject]] {
                                for calendarProductsResponse in calendarProductResult {
                                    var calendarLists = CalendarProductsModel()
                                    guard let productCalendarName = calendarProductsResponse["product_name"] as? String, let productCalendarUnits = calendarProductsResponse["product_units"] as? String, let calendarProductDeleiverCharges = calendarProductsResponse["delivery_charge"] as? String, let productCalendarImages = calendarProductsResponse["product_image"] as? String, let productQuantity = calendarProductsResponse["qty"] as? String, let productOrderDate = calendarProductsResponse["order_date"] as? String, let productPrice = calendarProductsResponse["product_price"] as? String,let cartid = calendarProductsResponse["cartid"] as? String,let productId = calendarProductsResponse["productid"] as? String  else {
                                        
                                        return
                                    }
                                    calendarLists.product_name = productCalendarName
                                    calendarLists.product_units = productCalendarUnits
                                    calendarLists.delivery_charge = calendarProductDeleiverCharges
                                    calendarLists.product_image = productCalendarImages
                                    calendarLists.quantity = productQuantity
                                    calendarLists.orderDate = productOrderDate
                                    calendarLists.product_price = productPrice
                                    calendarLists.cartid = cartid
                                    calendarLists.productid = productId
                                    UserDefaults.standard.set(calendarLists.cartid, forKey: "cartid")
                                    self.calenProducts.append(calendarLists)
                                    
                                }
                            }
                            
                            DispatchQueue.main.async {
                                self.calendarDateLabel.text = self.calenProducts[0]?.orderDate
                                //    self.calendarProdductTitle.text = self.calenProducts[0]?.product_name
                                //    self.calendarProductUnits.text = self.calenProducts[0]?.product_units
                                //    self.calendarProductPricr.text = self.calenProducts[0]?.product_price
                                //    self.calendarProductQuantity.text = self.calenProducts[0]?.quantity
                                //    self.calendarProductDelCharges.text = self.calenProducts[0]?.delivery_charge
                                //    self.calendarImageView.sd_setImage(with: URL(string: (self.calenProducts[0]?.product_image)!))
                                self.calendarProductTableView.reloadData()
                                self.isToday = self.gregorian.isDateInToday(self.todayDate)
                                for dateWithEvent in self.datesWithEvent {
                                    let dateString = self.dateFormatter2.string(from: self.todayDate)
                                    if  self.isToday == dateWithEvent.contains(dateString){
                                        self.calendarDateLabel.isHidden = false
                                        self.calendarShowDateLabel.isHidden = false
                                        self.calendarDeleteBtnReference.isHidden = false
                                        self.calendarProductTableView.isHidden = false
                                        
                                    }
                                    
                                    //      else{
                                    //
                                    //        self.calendarDateLabel.isHidden = true
                                    //        //    self.productsView.isHidden = true
                                    //        self.calendarProductTableView.isHidden = true
                                    //        self.calendarDeleteBtnReference.isHidden = true
                                    //      }
                                }
                                
                            }
                        }
                    } catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                            print(jsonErr.localizedDescription)
                        }
                    }
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                }
            }
        }
        
        
        func getCalenderDates(userId: String?) {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let requestCalendarDates = NSURL(string:Constants.URL_REQUEST_USER_CALENDAR + userId!) {
                var urlRequest = URLRequest(url: requestCalendarDates as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    
                    self.calendarDates = [CalendarDates]()
                    do {
                        
                        guard let calendarJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{
                            return
                        }
                        print(calendarJsonObject)
                        guard let calendarArrayJson = calendarJsonObject["calender_result"] as? [[String:AnyObject]] else{
                            return
                        }
                        if calendarJsonObject["code"] as! NSNumber == 200 {
                            let calendarData = (calendarJsonObject["calender_result"] as? [[String: AnyObject]])!.filter({ $0["orderdate"] != nil }).map({ $0["orderdate"]! })
                            print(calendarData)
                            let productQuantity = (calendarJsonObject["calender_result"] as? [[String: AnyObject]])!.filter({ $0["qty"] != nil }).map({ $0["qty"]! })
                            print(productQuantity)
                            //  let keys = calendarJsonObject.flatMap(){ $0.0 as? String }
                            //  let values = calendarJsonObject.flatMap(){ $0.1 }
                            self.datesWithEvent = calendarData as! [String]
                            self.productDatesWithQuantity = productQuantity as! [String]
                            
                            self.productDatesWithQuantity.forEach({ (qty) in
                                self.productDatesquantity.append(Int(qty)!)
                            })
                            
                            //    for product in self.productDatesWithQuantity{
                            //
                            //    self.singleEventDate.append(<#T##newElement: String##String#>)
                            //
                            //
                            //    }
                            for dateWithEvent in self.datesWithEvent{
                                for productDateWithQuantity  in self.productDatesWithQuantity{
                                    if Int(productDateWithQuantity) == 1{
                                        self.singleEventDate.append(dateWithEvent)
                                    }else if Int(productDateWithQuantity) == 2{
                                        self.twoEventDate.append(dateWithEvent)
                                    }else if Int(productDateWithQuantity) == 3{
                                        self.threeEventDate.append(dateWithEvent)
                                    }else{
                                        self.multipleEventDate.append(dateWithEvent)
                                    }
                                }
                            }
                            //    for date in self.datesWithEvent {
                            //    let dictionary = Dictionary(elements: Array(zip(self.datesWithEvent, self.productDatesWithQuantity)))
                            //    for (key, list) in dictionary {
                            //
                            //              }
                            //      }//
                            
                            
                            //  self.productDatesWithQuantity.forEach { (qty) in
                            //  self.productDatesquantity.append(Int(qty)!)
                            //    }
                            for calendarArray in calendarArrayJson {
                                var calendar = CalendarDates()
                                guard let orderDates = calendarArray["orderdate"] as? String, let quantity = calendarArray["qty"] as? String else {
                                    return
                                }
                                
                                calendar.orderDate = orderDates
                                calendar.quantity = quantity
                                UserDefaults.standard.set( calendar.quantity, forKey: "quantity")
                                UserDefaults.standard.set(calendar.orderDate, forKey: "orderDate")
                                self.calendarDates.append(calendar)
                                DispatchQueue.main.async {
                                    self.calendar.reloadData()
                                }
                            }
                            
                            DispatchQueue.main.async {
                                self.calendar.reloadData()
                            }
                            
                        } else {
                            
                            //    DispatchQueue.main.async {
                            //    self.view.makeToast(calendarJsonObject["description"] as! String, duration: 4.0, position: CGPoint(x: 180, y: 340))
                            //    }
                        }
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                        DispatchQueue.main.async {
                            self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                        }
                    }
                })
                task.resume()
            }
        }
        
        @IBOutlet
        weak var calendar: FSCalendar!
        @IBOutlet
        weak var calendarHeightConstraint: NSLayoutConstraint!
        
        fileprivate var lunar: Bool = false {
            didSet {
                self.calendar.reloadData()
            }
        }
        
        fileprivate var theme: Int = 0 {
            didSet {
                switch (theme) {
                case 0:
                    self.calendar.appearance.weekdayTextColor = UIColor(red: 14 / 255.0, green: 69 / 255.0, blue: 221 / 255.0, alpha: 1.0)
                    self.calendar.appearance.headerTitleColor = UIColor(red: 14 / 255.0, green: 69 / 255.0, blue: 221 / 255.0, alpha: 1.0)
                    self.calendar.appearance.eventDefaultColor = UIColor(red: 31 / 255.0, green: 119 / 255.0, blue: 219 / 255.0, alpha: 1.0)
                    self.calendar.appearance.selectionColor = UIColor(red: 31 / 255.0, green: 119 / 255.0, blue: 219 / 255.0, alpha: 1.0)
                    self.calendar.appearance.headerDateFormat = "MMMM yyyy"
                    self.calendar.appearance.todayColor = UIColor(red: 198 / 255.0, green: 51 / 255.0, blue: 42 / 255.0, alpha: 1.0)
                    self.calendar.appearance.borderRadius = 1.0
                    self.calendar.appearance.headerMinimumDissolvedAlpha = 0.2
                case 1:
                    self.calendar.appearance.weekdayTextColor = UIColor.red
                    self.calendar.appearance.headerTitleColor = UIColor.darkGray
                    self.calendar.appearance.eventDefaultColor = UIColor.green
                    self.calendar.appearance.selectionColor = UIColor.blue
                    self.calendar.appearance.headerDateFormat = "yyyy-MM"
                    self.calendar.appearance.todayColor = UIColor.red
                    self.calendar.appearance.borderRadius = 1.0
                    self.calendar.appearance.headerMinimumDissolvedAlpha = 0.0
                case 2:
                    self.calendar.appearance.weekdayTextColor = UIColor.red
                    self.calendar.appearance.headerTitleColor = UIColor.red
                    self.calendar.appearance.eventDefaultColor = UIColor.green
                    self.calendar.appearance.selectionColor = UIColor.blue
                    self.calendar.appearance.headerDateFormat = "yyyy/MM"
                    self.calendar.appearance.todayColor = UIColor.orange
                    self.calendar.appearance.borderRadius = 0
                    self.calendar.appearance.headerMinimumDissolvedAlpha = 1.0
                default:
                    break
                }
            }
        }
        
        fileprivate let formatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
        
        fileprivate let gregorian: NSCalendar! = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        //
        //    fileprivate let datesWithCat = ["2015/05/05", "2015/06/05", "2015/07/05", "2015/08/05", "2015/09/05", "2015/10/05", "2015/11/05", "2015/12/05", "2016/01/06",
        //    "2016/02/06", "2016/03/06", "2016/04/06", "2016/05/06", "2016/06/06", "2016/07/06"]
        
        
        // MARK:- Life cycle
        
        override func viewDidAppear(_ animated: Bool) {
            //  self.cartCheckOutDeleteAll()
            self.calendar.reloadData()
        }
        
        
        func boolToString(value: Bool?) -> String {
            if let value = value {
                return "\(value)"
            }
            else {
                return "<None>"
                // or you may return nil here. The return type would have to be String? in that case.
            }
        }
        override func viewDidLoad() {
            super.viewDidLoad()
            calendarDeleteBtnReference.layer.cornerRadius = 5
            calendar.appearance.cellShape = .rectangle
            self.calendarTodayProducts()
            self.getCartCountValue(userId: userId!)
            self.calendarProductReviewView.isHidden = true
            self.calendar.reloadData()
            self.calendarProductTableView.separatorStyle = .none
            getCalenderDates(userId: userId)
            if UIDevice.current.model.hasPrefix("iPad") {
                self.calendarHeightConstraint.constant = 400
            }
            self.calendar.appearance.caseOptions = [.headerUsesUpperCase, .weekdayUsesUpperCase]
            let scopeGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
            self.calendar.addGestureRecognizer(scopeGesture)
            // For UITest
            self.calendar.accessibilityIdentifier = "calendar"
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_tab_cart.png"), style: .plain, target: self, action: #selector(handleCart))
            navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
            
            
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
            self.navigationController?.navigationBar.tintColor =  UIColor.white
            self.title = "My calendar"
            self.calendarProductTableView.isHidden = true
            calendarDateLabel.isHidden = true
            self.calendarShowDateLabel.isHidden = true
            self.calendarDeleteBtnReference.isHidden = true
            serverCallForPaymentGatewayDeatils()
        }
        
        
        
        func serverCallForPaymentGatewayDeatils(){
            if Reachability.isConnectedToNetwork(){
                paymentGatewayDetails()
            }else{
                self.view.makeToast("No Internet connection available", duration: 3.0, position: .center)
            }
        }
        
        /* payment gate way details */
        
        func paymentGatewayDetails(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_PAYMENTGATEWAYDETAILS ) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    do {
                        
                        guard let paymentGatewayJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        print(paymentGatewayJson)
                        guard let cutOffTime = paymentGatewayJson["ios_order_cutoff_time"] as? String else{
                            return
                        }
                        
                        iosCuttOfftime = cutOffTime
                        
                    }  catch let jsonErr {
                        
                        print(jsonErr.localizedDescription)
                    }
                    
                })
                task.resume()
            }
        }
        
        
        
        
        
        func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
            let  dateString = self.dateFormatter2.string(from: date)
            let dictionary = Dictionary(elements: Array(zip(datesWithEvent, productDatesquantity)))
            for (key,value) in dictionary{
                if key == dateString{
                    return Int(value)
                }
            }
            return  0
        }
        
        func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventColorFor date: Date) -> UIColor? {
            let dateString = self.dateFormatter2.string(from: date)
            if self.datesWithEvent.contains(dateString) {
                return UIColor.purple
            }
            return nil
        }
        func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
            let  dateKey = self.dateFormatter2.string(from: date)
            let dic = Dictionary(elements: Array(zip(datesWithEvent, productDatesquantity)))
            for (key,value) in dic {
                if key == dateKey{
                    return [UIColor.magenta, appearance.eventDefaultColor, UIColor.black]
                }
            }
            return nil
        }
        
        
        func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
            let day: Int! = self.gregorian.component(.day, from: date)
            //    return [13, 24].contains(day) ? UIImage(named: "icon_cat") : nil
            
            let  dateKey = self.dateFormatter2.string(from: date)
            let dic = Dictionary(elements: Array(zip(datesWithEvent, productDatesquantity)))
            for (key,value) in dic {
                if key == dateKey && value > 2 {
                    return [10,16].contains(day) ? UIImage(named:"cross_symbol.png") : nil
                    
                }
            }
            return nil
        }
        
        // MARK:- FSCalendarDelegate
        
        //    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        //        print("change page to \(self.formatter.string(from: calendar.currentPage))")
        //    }
        
        func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
            
            print("calendar did select date \(self.formatter.string(from: date))")
            dateSelected = "\(self.formatter.string(from: date))"
            if Reachability.isConnectedToNetwork(){
                self.calendarProducts()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                }
            }
            
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let  todayDate = formatter.string(from: date)
            for dateWithEvent in self.datesWithEvent {
                
                if self.dateSelected == dateWithEvent {
                    self.calendarDateLabel.isHidden = false
                    self.calendarShowDateLabel.isHidden = false
                    self.calendarDeleteBtnReference.isHidden = false
                    //    self.productsView.isHidden = false
                    
                    self.calendarProductTableView.isHidden = false
                }
                    
                else{
                    
                    self.calendarDateLabel.isHidden = true
                    self.calendarShowDateLabel.isHidden = true
                    //    self.productsView.isHidden = true
                    self.calendarProductTableView.isHidden = true
                    self.calendarDeleteBtnReference.isHidden = true
                }
            }
            
            //        if monthPosition == .previous || monthPosition == .next {
            //            calendar.setCurrentPage(date, animated: true)
            //        }
        }
        
        func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
            self.calendarHeightConstraint.constant = bounds.height
            self.view.layoutIfNeeded()
        }
        
        // MARK:- Navigation
        @IBAction func calendarOrderDeleteBtnAction(_ sender: Any) {
            
            if Reachability.isConnectedToNetwork(){
                self.createConfirmationToDeleteDayOrder()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                }
            }
            
        }
        
        func cancelDayOrderProducts() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let cancelDayOrderProductsURL = NSURL(string: Constants.URL_REQUEST_USER_CANCELDAYORDER){
                var request = URLRequest(url: cancelDayOrderProductsURL as URL)
                var calendcancelDayOrder: CancelDayOrderModel = CancelDayOrderModel()
                calendcancelDayOrder.userid = userId
                //    calendcancelDayOrder.orderdate = calendarDateLabel.text
                calendcancelDayOrder.orderdate = self.calenProducts[indexpath]?.orderDate
                
                
                
                guard let userId = calendcancelDayOrder.userid,let orderDate = calendcancelDayOrder.orderdate else{return}
                let cancelDayOrderListParams: [String: Any] = ["userid": userId, "orderdate": orderDate]
                request.httpMethod = "POST"
                do {
                    let json = try JSONSerialization.data(withJSONObject: cancelDayOrderListParams)
                    request.httpBody = json
                }
                catch let jsonErr {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any)
                        return
                    }
                    self.cancelDayOrderArrayModel = [CancelDayOrderModel]()
                    do {
                        guard let cancelDayOrderProductJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{
                            return
                        }
                        
                        print("cancelDayOrderProductJson",cancelDayOrderProductJson)
                        if (cancelDayOrderProductJson["code"] as! NSNumber == 200) {
                            DispatchQueue.main.async {
                                self.calendarProductTableView.isHidden = true
                                DispatchQueue.main.async {
                                    self.calendarShowDateLabel.isHidden = true
                                    self.calendarDateLabel.isHidden = true
                                    self.view.makeToast(cancelDayOrderProductJson["description"]! as? String, duration: 3.0, position: .center)
                                    self.calendar.reloadData()
                                    self.perform(#selector(self.handleHome), with: self, afterDelay: 1)
                                }
                            }
                        }else{
                            
                            DispatchQueue.main.async {
                                self.view.makeToast(cancelDayOrderProductJson["description"]! as? String, duration: 3.0, position: .center)
                            }
                        }
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
                
            }
        }
        func handleHome(){
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let calendarController = storyBoard.instantiateViewController(withIdentifier: "Calendar") as! MyCalendarViewController
            let CalendarNavigation = UINavigationController(rootViewController: calendarController)
            self.present(CalendarNavigation, animated: false, completion: nil)
            
        }
        
        /* updatecart function (POST Method)*/
        
        func updateCart(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let updateCartUrl  =  NSURL(string:Constants.URL_REQUEST_UPDATECART) {
                var request = URLRequest(url:updateCartUrl as URL)
                var updateCartModel = UpdateCartModel()
                updateCartModel.cartId = self.calenProducts[buttoIndexPath]?.cartid
                updateCartModel.currentAction = "dec"
                updateCartModel.enable_smileycash = "1"
                updateCartModel.order_date = self.calenProducts[buttoIndexPath]?.orderDate
                updateCartModel.originalQuantity = decReference
                let decrement = UserDefaults.standard.value(forKey: "decrement") as! Int
                updateCartModel.quantity = String(decrement)
                updateCartModel.userId = userId
                guard let cartId = updateCartModel.cartId,let currentAction = updateCartModel.currentAction,let smileyCashEnable =  updateCartModel.enable_smileycash,let orderDate =  updateCartModel.order_date,let originalQuantity = updateCartModel.originalQuantity,let quantity =  updateCartModel.quantity,let userId =  updateCartModel.userId else{return}
                let updateCartListParams:[String:Any] = ["userid":userId,"cartid": cartId ,"qty": quantity,"order_date": orderDate,"originalqty": originalQuantity,"currentaaction": currentAction,"enable_smileycash": smileyCashEnable]
                
                print("updateCartListParams",updateCartListParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: updateCartListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    
                    do{
                        guard let updateCartJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                            return
                        }
                        if (updateCartJsonObject["code"] as! NSNumber == 200){
                            
                            print("updateCartJsonObject",updateCartJsonObject)
                            DispatchQueue.main.async {
                                //    if  updateCartModel.originalQuantity ==  updateCartModel.quantity{
                                //
                                //      self.calenProducts.remove(at: self.indexpath)
                                //      self.calendarProductTableView!.reloadData()
                                //    }else{
                                //        self.calendarProductTableView!.reloadData()
                                //        }
                                
                                //  let productQuantity = String(self.decrement)
                                
                                DispatchQueue.main.async {
                                    guard let decrement = UserDefaults.standard.value(forKey: "decrement") as? Int else{
                                        return
                                    }
                                    if   decrement == 0 && self.indexpath == 0 && self.calenProducts.count == 1{
                                        self.calenProducts.remove(at: self.buttoIndexPath)
                                        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let calendarController = storyBoard.instantiateViewController(withIdentifier: "Calendar") as! MyCalendarViewController
                                        let CalendarNavigation = UINavigationController(rootViewController: calendarController)
                                        self.present(CalendarNavigation, animated: false, completion: nil)
                                        
                                    }else if decrement < self.productQty[self.buttoIndexPath] &&  decrement >= 1 {
                                        self.calendarProductTableView!.reloadData()
                                        self.calendarProductReviewView.isHidden = true
                                    }else{
                                        self.calenProducts.remove(at: self.indexpath)
                                        self.calendarProductTableView!.reloadData()
                                        self.calendarProductReviewView.isHidden = true
                                        
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    
                                    self.view.makeToast(updateCartJsonObject["description"]! as? String, duration: 3.0, position: .center)
                                    
                                }
                                //    if self.decrement == 0 {
                                //  //  self.calendarDateLabel.isHidden = true
                                ////  self.calendarDeleteBtnReference.isHidden = true
                                //  DispatchQueue.main.async {
                                //      self.view.makeToast(updateCartJsonObject?["description"]! as! String, duration: 4.0, position: CGPoint(x: 180, y: 340))
                                //    }
                                //
                                //    }else{
                                //
                                //  DispatchQueue.main.async {
                                //  self.calendarProductReviewView.isHidden = true
                                //  self.calendarProductTableView!.reloadData()
                                //  }
                                //    }
                                
                                
                            }
                        }else{
                            
                            DispatchQueue.main.async {
                                self.view.makeToast(updateCartJsonObject["description"]! as? String, duration: 3.0, position: .center)
                            }
                        }
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                }
            }
        }
        
        func updateDecrementCart(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let updateCartUrl  =  NSURL(string:Constants.URL_REQUEST_UPDATECART) {
                var request = URLRequest(url:updateCartUrl as URL)
                var updateCartModel = UpdateCartModel()
                updateCartModel.cartId = self.calenProducts[buttoIndexPath]?.cartid
                updateCartModel.currentAction = "dec"
                updateCartModel.enable_smileycash = "0"
                updateCartModel.order_date = self.calenProducts[buttoIndexPath]?.orderDate
                updateCartModel.originalQuantity = decReference
                let decrement = UserDefaults.standard.value(forKey: "decrement") as! Int
                updateCartModel.quantity = String(decrement)
                updateCartModel.userId = userId
                guard let cartId = updateCartModel.cartId,let currentAction = updateCartModel.currentAction,let smileyCashEnable =  updateCartModel.enable_smileycash,let orderDate =  updateCartModel.order_date,let originalQuantity = updateCartModel.originalQuantity,let quantity =  updateCartModel.quantity,let userId =  updateCartModel.userId else{return}
                let updateCartListParams:[String:Any] = ["userid":userId,"cartid": cartId ,"qty": quantity,"order_date": orderDate,"originalqty": originalQuantity,"currentaaction": currentAction,"enable_smileycash": smileyCashEnable]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: updateCartListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    
                    do{
                        guard let updateCartJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                            return
                            
                        }
                        if (updateCartJsonObject["code"] as! NSNumber == 200){
                            DispatchQueue.main.async {
                                self.calendarProductTableView!.reloadData()
                                if self.productQty[self.buttoIndexPath] == decrement && self.calenProducts.count > 1 {
                                    self.calendarProductReviewView.isHidden = true
                                    self.incrementQunatityLabel = 0
                                    
                                }
                                    
                                    //   self.calendarReviewProductQuantityLbl.text = "\(self.incrementQunatityLabel)"
                                    
                                else  if self.productQty[self.buttoIndexPath] == decrement && self.calenProducts.count == 1{
                                    self.calendarProductReviewView.isHidden = true
                                    self.incrementQunatityLabel = 0
                                }
                                guard let product_price = self.calenProducts[self.buttonTag]?.product_price else{
                                    return
                                    
                                }
                                
                                if let product_price = Float(product_price){
                                    if product_price > 0 {
                                        self.totalProductPrice -= product_price
                                        let total_price =  self.totalProductPrice
                                        self.calendarProductTotalPriceLbl.text = String(total_price)
                                    }
                                }
                                
                                //    self.cartCheckoutProperties()
                            }
                            DispatchQueue.main.async {
                                self.view.makeToast(updateCartJsonObject["description"]! as? String, duration: 3.0, position: .center)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.view.makeToast(updateCartJsonObject["description"]! as? String, duration: 3.0, position: .center)
                            }
                        }
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                }
                
            }
        }
        
        /* update the cart for increasing the product quantity */
        
        func updateCartQuantity(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let updateCartUrl  =  NSURL(string:Constants.URL_REQUEST_UPDATECART) {
                var request = URLRequest(url:updateCartUrl as URL)
                var updateCartModel = UpdateCartModel()
                updateCartModel.cartId = self.calenProducts[buttonTag]?.cartid
                updateCartModel.currentAction = "inc"
                updateCartModel.enable_smileycash = "0"
                updateCartModel.order_date = self.calenProducts[buttonTag]?.orderDate
                updateCartModel.originalQuantity = String(productQty[buttonTag])
                let increment = UserDefaults.standard.value(forKey: "increment") as! Int
                updateCartModel.quantity = String(increment)
                updateCartModel.userId = userId
                guard let cartId = updateCartModel.cartId,let currentAction = updateCartModel.currentAction,let smileyCashEnable =  updateCartModel.enable_smileycash,let orderDate =  updateCartModel.order_date,let originalQuantity = updateCartModel.originalQuantity,let quantity =  updateCartModel.quantity,let userId =  updateCartModel.userId else{return}
                let updateCartListParams:[String:Any] = ["userid":userId,"cartid": cartId ,"qty": quantity,"order_date": orderDate,"originalqty": originalQuantity,"currentaaction": currentAction,"enable_smileycash":  smileyCashEnable]
                
                print(updateCartListParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: updateCartListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    //    if (jsonErr as NSError?)?.code == -1005 {
                    //        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                    //            let downloadGroup = DispatchGroup()
                    //            downloadGroup.enter()
                    //            downloadGroup.wait(timeout: DispatchTime.now() + Double(5000000000))
                    //            // Wait 5 seconds before trying again.
                    //            downloadGroup.leave()
                    //            DispatchQueue.main.async(execute: {() -> Void in
                    //                //Main Queue stuff here
                    //               self.updateCartQuantity()                           })
                    //        })
                    //        return
                    //    }
                    print(jsonErr.localizedDescription)
                }
                
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    
                    do{
                        guard let updateCartJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                            return
                        }
                        if (updateCartJsonObject["code"] as! NSNumber == 200){
                            DispatchQueue.main.async {
                                self.calendarProductTableView.reloadData()
                                self.calendarProductReviewView.isHidden = false
                                self.calendarProductTableView.isHidden = false
                                guard let product_price = self.calenProducts[self.buttonTag]?.product_price else{
                                    return
                                }
                                
                                if let product_price = Float(product_price) {
                                    self.totalProductPrice += product_price
                                    let total_price =  self.totalProductPrice
                                    self.calendarProductTotalPriceLbl.text = String(total_price)
                                    
                                }
                                
                                //      self.cartCheckoutProperties()
                                //    self.calendarProductTotalPriceLbl.text = "Rs." + (self.calenProducts[self.buttonTag]?.product_price)!
                                
                            }
                            
                            DispatchQueue.main.async {
                                
                                self.view.makeToast(updateCartJsonObject["description"]! as? String, duration: 3.0, position: .center)
                            }
                        }else{
                            
                            DispatchQueue.main.async {
                                self.calendarProductTableView.reloadData()
                                DispatchQueue.main.async {
                                    self.view.makeToast(updateCartJsonObject["description"]! as? String, duration: 3.0, position: .center)
                                }
                                
                            }
                        }
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                }
                
            }
        }
        func updateCartDecrementQuantity(){
            
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            let updateCartUrl  =  NSURL(string:Constants.URL_REQUEST_UPDATECART)
            var request = URLRequest(url:updateCartUrl! as URL)
            var updateCartModel = UpdateCartModel()
            updateCartModel.cartId = self.calenProducts[buttonTag]?.cartid
            updateCartModel.currentAction = "inc"
            updateCartModel.enable_smileycash = "0"
            updateCartModel.order_date = self.calenProducts[buttonTag]?.orderDate
            updateCartModel.originalQuantity = String(productQty[buttonTag])
            updateCartModel.quantity = String(incDecUpdateQty)
            updateCartModel.userId = userId
            let updateCartListParams:[String:Any] = ["userid": updateCartModel.userId!,"cartid": updateCartModel.cartId! ,"qty": updateCartModel.quantity!,"order_date": updateCartModel.order_date!,"originalqty": updateCartModel.originalQuantity!,"currentaaction": updateCartModel.currentAction!,"enable_smileycash":  updateCartModel.enable_smileycash!]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: updateCartListParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                if (error != nil) {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                }
                if let response = response {
                    print(response)
                }
                guard let data = data else {
                    return
                }
                
                do{
                    let updateCartJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                    if (updateCartJsonObject?["code"] as! NSNumber == 200){
                        DispatchQueue.main.async {
                            
                            self.calendarProductTableView.reloadData()
                            self.calendarProductReviewView.isHidden = false
                            self.calendarProductTableView.isHidden = false
                        }
                    }else{
                        
                        DispatchQueue.main.async {
                            self.calendarProductTableView.reloadData()
                            self.view.makeToast(updateCartJsonObject?["description"]! as? String, duration: 3.0, position: .center)
                        }
                    }
                }
                catch let jsonErr{
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to fecth data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            task.resume()
        }
        
        /// CartCheckoutProperties Function
        func cartCheckoutProperties(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            let date = Date()
            let todayDate = formatter.string(from: date)
            let cartCheckoutPropertiesUrl =   NSURL(string:Constants.URL_REQUEST_USER_CARTCHECKOUTPROPERTIES)
            var request = URLRequest(url:cartCheckoutPropertiesUrl! as URL)
            var cartCheckOutPropertiesModel = CartCheckOutPropertiesModel()
            cartCheckOutPropertiesModel.userid = userId
            cartCheckOutPropertiesModel.checkout_date = todayDate
            let cartCheckOutListParams:[String:Any] = ["userid":cartCheckOutPropertiesModel.userid!,"checkout_date":cartCheckOutPropertiesModel.checkout_date!]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: cartCheckOutListParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )
                    return
                }
                self.cartCheckoutPropertiesResponseArray = [CartCheckOutPropertiesResponseModel]()
                do{
                    let cartCheckOutPropertiesJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    if let totalProductPrice = cartCheckOutPropertiesJsonObject["total_price"] as? Float{
                        print(totalProductPrice)
                        UserDefaults.standard.set(totalProductPrice, forKey: "total_price")
                        DispatchQueue.main.async {
                            self.calendarProductTotalPriceLbl.text = "Rs." + String(totalProductPrice)
                            UserDefaults.standard.set(totalProductPrice, forKey: "total_price")
                        }
                    }
                    if let totalQuantity = cartCheckOutPropertiesJsonObject["total_qty"] as? Int{
                        print(totalQuantity)
                        UserDefaults.standard.set(totalQuantity, forKey: "total_qty")
                        DispatchQueue.main.async {
                            //    self.calendarReviewProductQuantityLbl.text = String(totalQuantity)
                        }
                        
                    }
                    DispatchQueue.main.async {
                        var cartCheckPropertiesResponse = CartCheckOutPropertiesResponseModel()
                        guard let total_items = cartCheckOutPropertiesJsonObject ["total_items"],let total_qty = cartCheckOutPropertiesJsonObject["total_qty"],let total_price = cartCheckOutPropertiesJsonObject["total_price"],let total_product_price =
                            cartCheckOutPropertiesJsonObject["total_product_price"],let total_deliver_charges = cartCheckOutPropertiesJsonObject["total_deliver_charges"],let item_ids = cartCheckOutPropertiesJsonObject["item_ids"],let order_date = cartCheckOutPropertiesJsonObject["order_date"] else {
                                
                                return
                        }
                        
                        cartCheckPropertiesResponse.item_ids = item_ids as? String
                        cartCheckPropertiesResponse.total_items = total_items as? String
                        cartCheckPropertiesResponse.total_qty = total_qty as? String
                        cartCheckPropertiesResponse.total_price = total_price as? String
                        cartCheckPropertiesResponse.total_product_price = total_product_price as? String
                        cartCheckPropertiesResponse.total_deliver_charges = total_deliver_charges as? String
                        cartCheckPropertiesResponse.order_date = order_date as? String
                        self.cartCheckoutPropertiesResponseArray.append(cartCheckPropertiesResponse)
                    }
                }
                catch let jsonErr{
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to fecth data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
                
            }
            
            task.resume()
            
        }
        
        
        
        /* creating the function for canceldayorder in My Calendar */
        func createConfirmationToDeleteDayOrder(){
            let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure to cancel all products for this date?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                
                if Reachability.isConnectedToNetwork(){
                    self.cancelDayOrderProducts()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    }
                }
                
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        
        func createConfirmationToUpdateDayOrder(){
            
            let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to decrease quantity", preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
                let decrement = UserDefaults.standard.value(forKey: "decrement") as! Int
                self.calenProducts[self.buttoIndexPath]?.quantity = String(decrement + 1)
                self.calendarProductTableView.reloadData()
                
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                if Reachability.isConnectedToNetwork(){
                    self.updateCart()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    }
                }
                
                //        DispatchQueue.main.async {
                //       self.calendarProductTableView.reloadData()
                //        }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        
        func createConfirmationToDeleteQuantity(){
            let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to delete?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
                let decrement = UserDefaults.standard.value(forKey: "decrement") as! Int
                self.calenProducts[self.buttoIndexPath]?.quantity = String(decrement + 1)
                self.calendarProductTableView.reloadData()
                
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                
                if Reachability.isConnectedToNetwork(){
                    self.cancelProduct()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    }
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        func getCartCountValue(userId:String){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_CARTCOUNT + userId) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    
                    do {
                        guard let cartData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                            return
                        }
                        
                        DispatchQueue.main.async {
                            if  let  cartCount = cartData["cart_count"] {
                                if cartCount as! Int == 0{
                                    self.navigationItem.rightBarButtonItem?.removeBadge()
                                }else{
                                    self.navigationItem.rightBarButtonItem?.addBadge(number: cartCount as! Int)
                                }
                                
                            }
                            
                        }
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async
                            {
                                self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                                
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                })
                task.resume()
                
            }else{
                DispatchQueue.main.async
                    {
                        self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                        
                }
                
            }
        }
        
        
        
        func createConfirmationUpdateQuantity(){
            let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to Update the Quantity?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
                let increment = UserDefaults.standard.value(forKey: "increment") as! Int
                self.calenProducts[self.indexpath]?.quantity = String(increment - 1)
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                if Reachability.isConnectedToNetwork(){
                    self.updateCartQuantity()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    }
                }
                
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        func cartCheckOutDeleteAll() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            let date = Date()
            let todayDate = formatter.string(from: date)
            if let cartCheckoutDeleteAllUrl =   NSURL(string:Constants.URL_REQUEST_USER_CARTCHECKOUTDELETEALL) {
                var request = URLRequest(url:cartCheckoutDeleteAllUrl as URL)
                var cartCheckOutDeleteAllModel = CartDeleteAll()
                cartCheckOutDeleteAllModel.userid = userId
                cartCheckOutDeleteAllModel.checkout_date = todayDate
                guard let userId = cartCheckOutDeleteAllModel.userid,let checkOutDate = cartCheckOutDeleteAllModel.checkout_date else {return}
                let cartCheckOutListParams:[String:Any] = ["user_id":userId,"checkout_date":checkOutDate]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cartCheckOutListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let cartCheckOutDeleteAllJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                            return
                        }
                        if ( cartCheckOutDeleteAllJsonObject["code"] as! NSNumber == 200){
                            //  DispatchQueue.main.async {
                            //  SVProgressHUD.dismiss()
                            //  self.view.makeToast(cartCheckOutDeleteAllJsonObject["description"]! as! String, duration: 4.0, position: CGPoint(x: 180, y: 340))
                            //  DispatchQueue.main.async {
                            //  self.perform(#selector(self.perfromDelayToHome), with: self, afterDelay: 1)
                            //  }
                            //
                            //  }
                        }else{
                            
                            //  self.view.makeToast(cartCheckOutDeleteAllJsonObject["description"]! as! String, duration: 4.0, position: CGPoint(x: 180, y: 340))
                        }
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                }
                
                
            }
        }
        
        
        
    }//class
    extension Date {
        var yesterdays: Date {
            return Calendar.current.date(byAdding: .day, value: -1, to: self)!
        }
        var tomorrows: Date {
            return Calendar.current.date(byAdding: .day, value: 1, to: self)!
        }
        var noons: Date {
            return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
        }
        var months: Int {
            return Calendar.current.component(.month,  from: self)
        }
        var isLastDayOfMonths: Bool {
            return tomorrows.month != month
        }
    }
    extension String {
        func boolValueFromString() -> Bool {
            return NSString(string: self).boolValue
        }
    }
    extension Dictionary {
        init(elements:[(Key, Value)]) {
            self.init()
            for (key, value) in elements {
                updateValue(value, forKey: key)
            }
        }
    }
    
    
