//
//  UpdatePasswordController.swift
//  SmileyServe
//
//  Created by Apple on 07/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit
import SVProgressHUD

class UpdatePasswordController: UIViewController,UITextFieldDelegate {
    @IBOutlet var updateConfirmTextField: UITextField!
    @IBOutlet var updatePasswordTextField: UITextField!
    @IBOutlet weak var updateBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBtn.layer.cornerRadius = 5
        updateConfirmTextField.delegate = self
        updatePasswordTextField.delegate = self
        updateConfirmTextField.updatePasswordunderlined()
        updatePasswordTextField.updatePasswordunderlined()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleBack))
        UINavigationBar.appearance().barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        self.displayToast()
        
    }
    
    func displayToast(){
        DispatchQueue.main.async {
            self.view.makeToast(UserDefaults.standard.value(forKey: "forgototp")
                as? String, duration: 3.0, position: .center)
        }
    }
    
    func handleBack(){
        updateConfirmTextField.resignFirstResponder()
        updatePasswordTextField.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func updatePassword(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            self.updatePassword()
        }else{
            DispatchQueue.main.async {
                self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
            }
        }
    }
    func updatePassword(){
        if ((updatePasswordTextField.text?.isEmpty)! || (updateConfirmTextField.text?.isEmpty)!){
            DispatchQueue.main.async {
                self.view.makeToast("please fill the password fields", duration: 3.0, position: .center)
            }
        }else if !(updatePasswordTextField.text?.isValidPassword)! || !(updateConfirmTextField.text?.isValidPassword)! {
            DispatchQueue.main.async {
                self.view.makeToast("passwords should be minimum six characters", duration: 3.0, position: .center)
            }
        }else if updatePasswordTextField.text != updateConfirmTextField.text{
            DispatchQueue.main.async {
                self.view.makeToast("passwords does not match", duration: 3.0, position: .center)
            }
        }
        else{
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let signupURL =  NSURL(string:Constants.URL_REQUEST_USER_UPDATEPASSWORD){
                var updatePwd:UpdatePassword  = UpdatePassword()
                updatePwd.userNewPassword = updatePasswordTextField.text
                guard let updatePassword = updatePwd.userNewPassword,let otpCode = UserDefaults.standard.value(forKey: "otpcode") else{return}
                var  request = URLRequest(url:signupURL as URL)
                let updateUserParams:[String:Any] = ["newpassword":updatePassword,"otpcode": otpCode]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: updateUserParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                      SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do {
                        let fetchData = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
                        if let registerResponse = fetchData as? [String:Any] {
                            print(registerResponse["message"]!)
                            print(registerResponse["description"]!)
                            
                            DispatchQueue.main.async {
                                if (registerResponse["code"] as! NSNumber == 200) {
                              
                                    DispatchQueue.main.async {
                                        self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
                                        
                                    }
                                    DispatchQueue.main.async {
                                        self.perform(#selector(self.handleDelayToast), with: self, afterDelay: 3)
                                    }
                                }
                                    
                                else {
                                    DispatchQueue.main.async {
                                        
                        self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
                                    }
                                }
                            }
                        }
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                        
            self.view.makeToast("Unable To Fetch Your Data", duration: 3.0, position: .center)
                        print(jsonErr.localizedDescription)
                        }
                        
                    }
                }
                
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Your Data", duration: 3.0, position: .center)
                }
            }
        }
    }
    func handleDelayToast(){
        DispatchQueue.main.async {
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
            //  let signInnavigationcontroller = UINavigationController(rootViewController: signInController)
            self.present(signInController, animated: false, completion: nil)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == updatePasswordTextField{
            updateConfirmTextField.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
    }
    
}//class

extension UITextField {
    func updatePasswordunderlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

/*
 
 navigationItem.leftBarButtonItem = UIBarButtonItem(title: "back", style: .plain, target: self, action: #selector(handleBack))
 navigationItem.title = "UpdatePassword"
 @IBAction func handleUpdatePassword(_ sender: Any) {
 self.dismiss(animated: true, completion: nil)
 
 }
 */


