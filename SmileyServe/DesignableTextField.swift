//
//  DesignableTextField.swift
//  SmileyServe
//
//  Created by Apple on 11/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit
@IBDesignable

class DesignableTextField: UITextField {
    
    @IBInspectable var rightImageView:UIImage?{
        
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftImageView:UIImage?{
        
        didSet {
            updateView()
        }
    }

    
    @IBInspectable var rightPadding: CGFloat = 0 {
        
        didSet {
            
            updateView()
        }
        
        
        
    }
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        
        didSet {
            
            updateView()
        }
        
        
        
    }

    func updateView(){
        
        if let leftImage = leftImageView {
            
            leftViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: 30, height: 30))
            imageView.image = leftImage
            
            var width = leftPadding + 30
            
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line{
                
                width = width + 5
                
            }

            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 30))
            view.addSubview(imageView)
            leftView = view
            
        }
        
        else  if let image = rightImageView{
            
            rightViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: rightPadding, y: 0, width: 20, height: 20))
            imageView.image = image
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            view.addSubview(imageView)
            rightView = view

        }
        else{
            
            
            leftViewMode = .never
            rightViewMode = .never
        }
        
        
    }
    

   
}//classs
