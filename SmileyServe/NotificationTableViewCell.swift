//
//  NotificationTableViewCell.swift
//  SmileyServe
//
//  Created by Apple on 09/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

       @IBOutlet weak var notificationDate: UILabel!
    @IBOutlet weak var notificationMessage: UILabel!
    @IBOutlet weak var notificationTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
