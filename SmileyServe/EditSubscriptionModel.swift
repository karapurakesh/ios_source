//
//  EditSubscriptionModel.swift
//  SmileyServe
//
//  Created by Apple on 21/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct EditSubscriptionModel {
    
    var userId : String?
    var orderId : String?
    var itemId : String?
    
}
