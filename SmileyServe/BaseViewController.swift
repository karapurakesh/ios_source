  //
  //  BaseViewController.swift
  //  AKSwiftSlideMenu
  //
  //  Created by Ashish on 21/09/15.
  //  Copyright (c) 2015 Kode. All rights reserved.
  //
  
  import UIKit
  
  class BaseViewController: UIViewController, SlideMenuDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        let topViewController : UIViewController = self.navigationController!.topViewController!
        print("View Controller is : \(topViewController) \n", terminator: "")
        switch(index){
        case 0:
            print("Home\n", terminator: "")
            
            self.openViewControllerBasedOnIdentifier("Home")
            
            break
        case 1:
            print("Play\n", terminator: "")
            
            self.openViewControllerBasedOnIdentifier("Profile")
            
            break
        case 2:
            print("Play\n", terminator: "")
            
            self.openViewControllerBasedOnIdentifier("Transaction")
            
            break
        case 3:
            print("Play\n", terminator: "")
            
            self.openViewControllerBasedOnIdentifier("Faqs")
            
            break
        case 4:
            print("Play\n", terminator: "")
            self.openViewControllerBasedOnIdentifier("ContactUS")
            
            break
        case 5:
            print("Play\n", terminator: "")
            //self.openViewControllerBasedOnIdentifier("aboutUsViewController")
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let aboutUsViewController = storyBoard.instantiateViewController(withIdentifier: "aboutUsViewController") as! AboutUsViewController
            self.navigationController!.pushViewController(aboutUsViewController, animated: true)

            break
        case 6:
            
            

           // let shareText = "Hi there,I made SmileyServe as my trusted daily needs partner.They deliver fresh Vegetables,Fruits,Flowers,Breads,Milk,Pooja Needs,Groceries and other daily needs at our doorstep every morning before 8 am.\nYou can try app using below link\nhttp://onelink.to/sserve\nThe best part I liked is the product quality and No minimum order feature.\nWhy to store when you can get fresh everyday"
            
            let mutableMultiLineString = """
                             Hi there,I made *SmileyServe* as my trusted daily needs partner.They deliver fresh Vegetables,Fruits,Flowers,Breads,Milk,Pooja Needs,Groceries and other daily needs at our doorstep every morning before 8 am.

                             You can try app using below link

                             http://onelink.to/sserve

                             The best part I liked is the product quality and No minimum order feature.

                             Why to store when you can get fresh everyday.
                             """
            
            let vc = UIActivityViewController(activityItems: [mutableMultiLineString], applicationActivities: [])
            vc.completionWithItemsHandler = {(activityType: UIActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
                if !completed {
                    // User canceled
                    return
                }
                // User completed activity
                
                print("completed")
            }
            present(vc, animated: true)
          

            break
        case 7:
 
            break
            
        default:
            print("default\n", terminator: "")
        }
    }
    
   
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }
    
    func addSlideMenuButton(){
        let btnShowMenu = UIButton(type: UIButtonType.system)
        btnShowMenu.setImage(self.defaultMenuImage(), for: UIControlState())
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
    }
    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width
            , height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width:UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
    }
    
    
  }
 
  extension String{
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
  }
