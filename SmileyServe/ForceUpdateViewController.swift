//
//  InternetViewController.swift
//  SmileyServe
//
//  Created by Apple on 02/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class ForceUpdateViewController: UIViewController  {
    var userId = UserDefaults.standard.value(forKey: "id") as? String
    @IBOutlet weak var forceUpdateLabel: UILabel!
    @IBOutlet weak var updateBtnReference: UIButton!
    @IBAction func forceUpdateBtnAction(_ sender: Any) {
        if let url = URL(string: "https://itunes.apple.com/us/app/smileyserve-daily-needs-app/id1289731714?ls=1&mt=8"),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.openURL(url)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        updateBtnReference.layer.cornerRadius = 5
    }
    
    
}
