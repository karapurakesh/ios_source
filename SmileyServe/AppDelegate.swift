    //
    //  AppDelegate.swift
    //  SmileyServe
    //
    //  Created by Apple on 04/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    
    import UIKit
    import CoreData
    import Firebase
    import FirebaseInstanceID
    import FirebaseMessaging
    import UserNotifications
    import IQKeyboardManagerSwift
    import AudioToolbox
    import BRYXBanner
    var token : String?
    let deviceType = "ios"
    var pushCount:Int = 0
    @available(iOS 10.0, *)
    @UIApplicationMain
    class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate{
        var window: UIWindow?
        let gcmMessageIDKey = "gcm.message_id"
        var userId = UserDefaults.standard.value(forKey: "id") as? String
        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
            
            let  userId = UserDefaults.standard.value(forKey: "id") as? String
            IQKeyboardManager.sharedManager().enable = true
            IQKeyboardManager.sharedManager().enableAutoToolbar = true
            UIApplication.shared.statusBarStyle = .lightContent
            if #available(iOS 10.0, *) {
                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.current().delegate = self
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(
                    options: authOptions,
                    completionHandler: { granted, error in
                        if granted{
                            DispatchQueue.main.async {
                                application.registerForRemoteNotifications()
                            }
                        } else {
                            print("User Notification permission denied: \(String(describing: error?.localizedDescription))")
                        }
                })
                // For iOS 10 data message (sent via FCM)
                Messaging.messaging().delegate = self
                let current = UNUserNotificationCenter.current()
                current.getNotificationSettings(completionHandler: { (settings) in
                    if settings.authorizationStatus == .notDetermined {
                        // Means you can request
                    }
                    
                    if settings.authorizationStatus == .denied {
                        // User should enable notifications from settings & privacy
                        // show an alert or sth
                    }
                    
                    if settings.authorizationStatus == .authorized {
                        // It's okay, no need to request
                    }
                })
            } else {
                if UIApplication.shared.isRegisteredForRemoteNotifications {
                    print("APNS-YES")
                } else {
                    print("APNS-NO")
                }
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                application.registerUserNotificationSettings(settings)
            }
            //application.registerForRemoteNotifications()
            FirebaseApp.configure()
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.tokenRefreshNotification),
                                                   name:.InstanceIDTokenRefresh,
                                                   object: nil)
            return true
            
        }
        
        func tokenRefreshNotification(notification: NSNotification) {
            //  print("refresh token call")
            guard let contents = InstanceID.instanceID().token()
                else {
                    return
            }
            // let refreshedToken = FIRInstanceID.instanceID().token()!
            print("InstanceID token: \(contents)")
            UserDefaults.standard.set(contents, forKey: "fcmToken")
            // UserDefaults.standardUserDefaults().set(contents, forKey: "deviceToken");
            // Connect to FCM since connection may have failed when attempted before having a token.
            if let token = InstanceID.instanceID().token(){
                print(token)
            }
            guard  let userApiKey = userId else{return}
            let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
            if let refreshedToken = InstanceID.instanceID().token() {
                print("InstanceID token: \(refreshedToken)")
                let paramCheckVersionCode = "userId=\(userApiKey)&fcmToken=\(refreshedToken)&versionCode=\(version)&deviceType=\(deviceType)"
                self.checkVersionCode(Constants.URL_REQUEST_USER_CHECKVERSIONCODE, "\(paramCheckVersionCode)")
                let paramString = "userId=\(userApiKey)&fcmToken=\(refreshedToken)"
                updateFcmToken(Constants.URL_REQUEST_USER_FCMTOKEN, "\(paramString)")
            }
            connectToFcm()
            
        }
        
        func connectToFcm() {
            // Won't connect since there is no token
            guard InstanceID.instanceID().token() != nil else {
                return
            }
            
            // Disconnect previous FCM connection if it exists.
            Messaging.messaging().disconnect()
            Messaging.messaging().connect { (error) in
                if error != nil {
                    print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
                } else {
                    print("Connected to FCM.")
                }
            }
        }
        
        
        func applicationWillResignActive(_ application: UIApplication) {
            // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
            // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        }
        
        func applicationDidEnterBackground(_ application: UIApplication) {
            // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
            // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
            
            Messaging.messaging().disconnect()
            print("Disconnected from FCM.")
        }
        
        func applicationWillEnterForeground(_ application: UIApplication) {
            // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        }
        
        func applicationDidBecomeActive(_ application: UIApplication) {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
            //Application become active connect to fcm
            connectToFcm()
        }
        
        func applicationWillTerminate(_ application: UIApplication) {
            // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
            // Saves changes in the application's managed object context before the application terminates.
            self.saveContext()
        }
        
        //    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //        for i in 0..<deviceToken.count {
        //            token = token! + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        //        }
        //      }
        
        
        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
            print("APNs device token: \(String(describing: token))")
            let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
            guard  let userApiKey = userId else{return}
            if let refreshedToken = InstanceID.instanceID().token() {
                print("InstanceID token: \(refreshedToken)")
                UserDefaults.standard.set(refreshedToken, forKey: "fcmToken")
                let paramCheckVersionCode = "userId=\(userApiKey)&fcmToken=\(refreshedToken)&versionCode=\(version)&deviceType=\(deviceType)"
                self.checkVersionCode(Constants.URL_REQUEST_USER_CHECKVERSIONCODE, "\(paramCheckVersionCode)")
                let paramString = "userId=\(userApiKey)&fcmToken=\(refreshedToken)"
                updateFcmToken(Constants.URL_REQUEST_USER_FCMTOKEN, "\(paramString)")
            }
        }
        
        
        func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
            print("Registration failed!")
        }
        // MARK: - Core Data stack
        
        @available(iOS 10.0, *)
        lazy var persistentContainer: NSPersistentContainer = {
            /*
             The persistent container for the application. This implementation
             creates and returns a container, having loaded the store for the
             application to it. This property is optional since there are legitimate
             error conditions that could cause the creation of the store to fail.
             */
            //    @available(iOS 10.0, *)
            let container = NSPersistentContainer(name: "SmileyServe")
            container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                if let error = error as NSError? {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    fatalError("Unresolved error \(error), \(error.userInfo)")
                }
            })
            return container
        }()
        
        // MARK: - Core Data Saving support
        
        func saveContext () {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
        // Firebase notification received
        @available(iOS 10.0, *)
        //    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (_ options: UNNotificationPresentationOptions) -> Void) {
        //
        //                print("Handle push from foreground\(notification.request.content.userInfo)")
        //                let dict = notification.request.content.userInfo["aps"] as? NSDictionary
        //                let d: [String: Any] = dict!["alert"] as! [String: Any]
        //                let body: String = d["body"] as! String
        //                let title: String = d["title"] as! String
        //                print("Title:\(title) + body:\(body)")
        //                self.showAlertAppDelegate(title: title, message: body, buttonTitle: "ok", window: self.window!)
        ////        let body = notification.request.content.body
        ////        let feedbackGenerator = UINotificationFeedbackGenerator()
        ////        feedbackGenerator.notificationOccurred(.success)
        ////     AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate), nil)
        ////    self.showAlertAppDelegate(title: "hi app is in foreground", message: "smiley serve", buttonTitle: "ok", window: self.window!)
        //
        //    }
        // Receive displayed notifications for iOS 10 devices.
        
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    willPresent notification: UNNotification,
                                    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
            let userInfo = notification.request.content.userInfo
            // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            
            // Print full message.
            print(userInfo)
            print("userInfo",notification.request.content.userInfo)
            guard let aps = notification.request.content.userInfo["aps"] as? [String:AnyObject] else{
                return
            }
            print("FCm APNS",aps)
            if let alert = aps["alert"] as? String{
                print("FCM alert",alert)
            }
            
            
            // Change this to your preferred presentation option
            completionHandler([.alert, .badge, .sound])
        }
        
        @available(iOS 10.0, *)
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    didReceive response: UNNotificationResponse,
                                    withCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            let userInfo = response.notification.request.content.userInfo
            // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            print("response center",response.notification.request.content.userInfo)
            // Print full message.
            print(userInfo)
            if UIApplication.shared.applicationState == .inactive{
                print("Inactive state")
                completionHandler(UIBackgroundFetchResult.newData)
                
            }else if UIApplication.shared.applicationState == .background{
                print("Background state")
                completionHandler(UIBackgroundFetchResult.newData)
            }else{
                print("Foreground state")
                completionHandler(UIBackgroundFetchResult.newData)
            }
            completionHandler(UIBackgroundFetchResult.newData)
        }
        
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                         fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired till the user taps on the notification launching the application.
            // TODO: Handle data of notification
            // Print message ID.
            
            Messaging.messaging().appDidReceiveMessage(userInfo)
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            // Print full message.
            print(userInfo)
            if UIApplication.shared.applicationState == .inactive{
                print("Inactive state")
                completionHandler(UIBackgroundFetchResult.newData)
                
            }else if UIApplication.shared.applicationState == .background{
                print("Background state")
                completionHandler(UIBackgroundFetchResult.newData)
            }else{
                print("Foreground state")
                completionHandler(UIBackgroundFetchResult.newData)
            }
            completionHandler(UIBackgroundFetchResult.newData)
        }
        
        func showAlertAppDelegate(title: String, message: String, buttonTitle: String, window: UIWindow) {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
            window.rootViewController?.present(alert, animated: false, completion: nil)
        }
        
        func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            print("received background fetch")
            completionHandler(UIBackgroundFetchResult.newData)
            
        }
        
        func updateFcmToken(_ url: String, _ paramString: String) {
            if let url: NSURL = NSURL(string: url){
                let session = URLSession.shared
                let request = NSMutableURLRequest(url: url as URL)
                request.httpMethod = "POST"
                request.httpBody = paramString.data(using: String.Encoding.utf8)
                let task = session.dataTask(with: request as URLRequest) {
                    (
                    data, response, error) in
                    guard let data = data else{
                        return
                    }
                    self.extract_data(data)
                }
                task.resume()
            }
        }
        
        
        func extract_data(_ data:Data?) {
            let json: Any?
            if(data == nil)
            {
                return
            }
            do {
                guard let data = data else{return}
                json = try JSONSerialization.jsonObject(with:data, options: [])
            }
            catch
            {
                return
            }
            guard let stringvalue = json as? NSDictionary else
                
            {
                return
            }
            
            print(stringvalue)
            let stringresponse = stringvalue["error"] as? String
            DispatchQueue.main.async {
                if stringresponse == "false" {
                    print(stringvalue["message"]!)
                }
            }
        }
        
        func checkVersionCode(_ url: String, _ paramCheckVersionCode: String) {
            if let url: NSURL = NSURL(string: url){
                let session = URLSession.shared
                let request = NSMutableURLRequest(url: url as URL)
                request.httpMethod = "POST"
                request.httpBody = paramCheckVersionCode.data(using: String.Encoding.utf8)
                let task = session.dataTask(with: request as URLRequest) {
                    (
                    data, response, error) in
                    guard let data = data else{
                        return
                    }
                    self.extractVersionCode(data)
                }
                task.resume()
            }
        }
        
        func extractVersionCode(_ data: Data?) {
            let json: Any?
            if(data == nil)
            {
                return
            }
            guard let data = data else{
                return
            }
            do {
                json = try JSONSerialization.jsonObject(with:data, options:.mutableContainers)
            }
            catch
            {
                return
            }
            guard let stringvalue = json as? NSDictionary else
            {
                return
            }
            print(stringvalue)
            let stringresponse = stringvalue["error"] as? String
            DispatchQueue.main.async {
                if stringresponse == "false" {
                    //    self.window = UIWindow(frame: UIScreen.main.bounds)
                    //    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    //    let homeController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    //    let homeNavigation = UINavigationController(rootViewController: homeController)
                    //    self.window?.rootViewController = homeNavigation
                    //    self.window?.makeKeyAndVisible()
                } else {
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let forceUpdateViewController = storyBoard.instantiateViewController(withIdentifier: "ForceUpdateViewController") as! ForceUpdateViewController
                    self.window?.rootViewController = forceUpdateViewController
                    self.window?.makeKeyAndVisible()
                }
            }
        }
    }//end of the delegate
    
    
    extension AppDelegate : MessagingDelegate {
        // Receive data message on iOS 10 devices while app is in the foreground.
        func application(received remoteMessage: MessagingRemoteMessage) {
            print("custom data",remoteMessage.appData)
            guard let customData = remoteMessage.appData["data"] as? String else{
                return
            }
            do {
                if let responseData = customData.data(using: String.Encoding.utf8)
                {
                    guard let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? [String:AnyObject] else{
                        return
                    }
                    
                    print("custom data serilization",jsonData)
                    guard let title = jsonData["title"] as? String,let message = jsonData["message"] as? String else {
                        return
                    }
                    print("custom title",title)
                    print("custom message",message)
                    let banner = Banner(title:title, subtitle: message,image: UIImage(named: "ic_smileyserve_logo.png"),backgroundColor: UIColor(red:31.0/255.0, green:136.0/255.0, blue:255.0/255.0, alpha:1.000))
                    banner.dismissesOnTap = true
                    banner.show(duration: 3.0)
                    
                }
            } catch {
                print(error.localizedDescription)
            }
            
        }
        func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
            print("Firebase Messaing Token",fcmToken)
        }
    }
    /*
     
     //
     //        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
     //        if launchedBefore  {
     //            print("Not first launch.")
     //        } else {
     //            print("First launch, setting UserDefault.")
     //            UserDefaults.standard.set(true, forKey: "launchedBefore")
     //        }
     
     
     if #available(iOS 10, *) {
     UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
     application.registerForRemoteNotifications()
     }
     // iOS 9 support
     else if #available(iOS 9, *) {
     UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
     UIApplication.shared.registerForRemoteNotifications()
     }
     // iOS 8 support
     else if #available(iOS 8, *) {
     UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
     UIApplication.shared.registerForRemoteNotifications()
     }
     // iOS 7 support
     else {
     application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
     }
     
     
     
     //                if jsonData is [String:Any] {
     //                    // This part is just for example from my project, you need to change it for your custom model
     //                    let errorData = ErrorData(JSON: jsonData as! [String:Any])!
     //                    print("Error Data : \(String(describing: errorData?.id))")
     //                    // use your custom model object after that
     //                }
     
     */
    
    
    
