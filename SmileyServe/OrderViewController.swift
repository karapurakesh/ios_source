    //
    //  OrderViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 02/08/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    
    import UIKit
    import SVProgressHUD
    import QuartzCore
    var tomorrowDate : String?
    var iosCuttOfftime = String()
    
    enum Weekday : Int{
        case Sunday = 1,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday
    }
    
    var selectedDate: Date?
    let userId = UserDefaults.standard.value(forKey: "id") as? String
    class OrderViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITextFieldDelegate, WWCalendarTimeSelectorProtocol {
        
        @IBOutlet weak var rcdcGapWidthConstant: NSLayoutConstraint!
        @IBOutlet weak var rcWidthConstant: NSLayoutConstraint!
        @IBOutlet weak var dcWidthConstant: NSLayoutConstraint!
        @IBOutlet weak var addToCartBtn: UIButton!
        @IBOutlet var orderScrollView: UIScrollView!
        let apartmentId = UserDefaults.standard.value(forKey: "apartmentId") as? String
        var sunLabelReference = Int()
        var monLabelReference = Int()
        var tueLabelReference = Int()
        var wedLabelReference = Int()
        var thuLabelReference = Int()
        var friLabelReference = Int()
        var satLabelReference = Int()
        var strSelctedDateType = "from"
        var startDate: Date?
        var endDate: Date?
        var dateDifferenceArray:[Int] = []
        @IBOutlet weak var cartCountLabel: UILabel!
        @IBOutlet weak var endDateTextField: UITextField!
        @IBOutlet weak var startDateTextField: UITextField!
        @IBOutlet weak var productUnits: UILabel!
        @IBOutlet weak var delieveryCharges: UILabel!
        @IBOutlet weak var productPrice: UILabel!
        @IBOutlet weak var productTitle: UILabel!
        @IBOutlet weak var productImage: CustomImageView!
        @IBOutlet weak var sunButtonRef: UIButton!
        @IBOutlet weak var sunLabel: UILabel!
        @IBOutlet weak var sunDecBtnRef: UIButton!
        @IBOutlet weak var productAlreadySubscribedLabel : UILabel!
        @IBAction func sunIncBtnRef(_ sender: Any) {
            if let sunLabelIncrementText = sunLabel.text{
                if let sunIncvalue = Int(sunLabelIncrementText) {
                    var sunIncrementValue = sunIncvalue
                    sunIncrementValue += 1
                    sunLabel.text = String(sunIncrementValue)
                }
            }
            
        }
        
        @IBAction func sunDecBtnActn(_ sender: Any) {
            if let sunDecrementText = sunLabel.text{
                if let sunDecrementvalue = Int(sunDecrementText){
                    var sunDecValue = sunDecrementvalue
                    if sunDecValue > 0 {
                        sunDecValue -= 1
                        sunLabel.text = String(sunDecValue)
                    }
                }
            }
        }
        
        @IBOutlet weak var monBtnRef: UIButton!
        @IBOutlet weak var monLabel: UILabel!
        @IBOutlet weak var monBtnDecRef: UIButton!
        @IBAction func monIncBtnAction(_ sender: Any) {
            if let monIncrementText = monLabel.text{
                if let monIncvalue = Int(monIncrementText){
                    var monIncrementValue = monIncvalue
                    monIncrementValue += 1
                    monLabel.text = String(monIncrementValue)
                }
            }
        }
        
        @IBAction func monDecBtnAction(_ sender: Any) {
            if let monDecrementText = monLabel.text{
                if let monDecrementvalue = Int(monDecrementText){
                    var monDecValue = monDecrementvalue
                    if monDecValue > 0 {
                        monDecValue -= 1
                        monLabel.text = String(monDecValue)
                    }
                }
            }
        }
        @IBOutlet weak var tueBtnIncRef: UIButton!
        @IBOutlet weak var tueLabel: UILabel!
        @IBOutlet weak var tueDecRefBtn: UIButton!
        @IBAction func tueBtnAction(_ sender: Any) {
            if let tueIncrementText = tueLabel.text{
                if let tueIncValue = Int(tueIncrementText){
                    var tueIncrementValue = tueIncValue
                    tueIncrementValue += 1
                    tueLabel.text = String(tueIncrementValue)
                }
            }
        }
        
        @IBAction func tueDecBtnAction(_ sender: Any) {
            if let tueDecrementText = tueLabel.text{
                if let tueDecvalue = Int(tueDecrementText){
                    var tueDecrementValue = tueDecvalue
                    if tueDecrementValue > 0 {
                        tueDecrementValue -= 1
                        tueLabel.text = String(tueDecrementValue)
                    }
                }
            }
            
        }
        @IBOutlet weak var wedBtnRef: UIButton!
        @IBOutlet weak var wedLabel: UILabel!
        @IBOutlet weak var wedBtnDecRef: UIButton!
        @IBAction func wedBtnAction(_ sender: Any) {
            if let wedIncrementText = wedLabel.text{
                if let wedIncValue = Int(wedIncrementText){
                    var wedIncrementValue = wedIncValue
                    wedIncrementValue += 1
                    wedLabel.text = String(wedIncrementValue)
                    
                }
            }
            
        }
        
        @IBAction func webDecBtnAction(_ sender: Any) {
            if let wedDecrementText = wedLabel.text{
                if  let wedDecvalue = Int(wedDecrementText) {
                    var wedDecrementValue = wedDecvalue
                    if wedDecrementValue > 0 {
                        wedDecrementValue -= 1
                        wedLabel.text = String(wedDecrementValue)
                    }
                }
            }
        }
        
        @IBOutlet weak var thuBtnRef: UIButton!
        @IBOutlet weak var thuLabel: UILabel!
        @IBOutlet weak var thuBtnDecRef: UIButton!
        @IBAction func thuBtnAction(_ sender: Any) {
            if let thuIncrementLabeltext = thuLabel.text{
                if let thuIncLabelvalue = Int(thuIncrementLabeltext){
                    var thuIncrementValue = thuIncLabelvalue
                    thuIncrementValue += 1
                    thuLabel.text = String(thuIncrementValue)
                }
            }
        }
        
        @IBAction func thuBtnDecAction(_ sender: Any) {
            if let thuLabelDecrementText = thuLabel.text {
                if let thuLabelvalue = Int(thuLabelDecrementText){
                    var thuDecrementValue = thuLabelvalue
                    if thuDecrementValue > 0 {
                        thuDecrementValue -= 1
                        thuLabel.text = String(thuDecrementValue)
                    }
                }
            }
        }
        
        
        @IBOutlet weak var friBtnRef: UIButton!
        @IBOutlet weak var friLabel: UILabel!
        @IBOutlet weak var friDecBtnRef: UIButton!
        @IBAction func friBtnAction(_ sender: Any) {
            if let friIncrementText = friLabel.text{
                if let friIncValue = Int(friIncrementText){
                    var friIncrementValue = friIncValue
                    friIncrementValue += 1
                    friLabel.text = String(friIncrementValue)
                }
            }
        }
        
        @IBAction func friDecBtnAction(_ sender: Any) {
            if let friDecrementText = friLabel.text{
                if let friDecvalue = Int(friDecrementText){
                    var friDecrementValue = friDecvalue
                    if friDecrementValue > 0 {
                        friDecrementValue -= 1
                        friLabel.text = String(friDecrementValue)
                    }
                }
            }
        }
        
        @IBOutlet weak var satBtnRef: UIButton!
        @IBOutlet weak var satLabel: UILabel!
        @IBOutlet weak var satDecBtnRef: UIButton!
        @IBAction func satBtnIncAction(_ sender: Any) {
            if let satIncrementtext = satLabel.text{
                if let satIncvalue = Int(satIncrementtext){
                    var satIncrementValue = satIncvalue
                    satIncrementValue += 1
                    satLabel.text = String(satIncrementValue)
                }
            }
        }
        
        @IBAction func satDecBtnAction(_ sender: Any) {
            if  let satDecrementText = satLabel.text{
                if let satDecvalue = Int(satDecrementText) {
                    var satDecrementValue = satDecvalue
                    if satDecrementValue > 0 {
                        satDecrementValue -= 1
                        satLabel.text = String(satDecrementValue)
                    }
                }
            }
        }
        
        
        fileprivate var singleDate: Date = Date()
        fileprivate var multipleDates: [Date] = []
        var productUnit = ""
        var productTitles = ""
        var productPrices = ""
        var deleieveryCharge = ""
        var productImages = ""
        var productId = ""
        var menuId = ""
        @IBOutlet var cartBtnReference: UIBarButtonItem!
        override func viewDidLoad() {
            super.viewDidLoad()
            addToCartBtn.layer.cornerRadius = 5
            orderScrollView.delaysContentTouches = false
            self.productAlreadySubscribedLabel.shadow()
            /* checking the productDetails */
            self.productAlreadySubscribedLabel.isHidden = true
            if Reachability.isConnectedToNetwork(){
                self.productDetails()
            }else
            {
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    
                }
            }
            

            if deleieveryCharge  != "0" {
                self.dcWidthConstant.constant = 36
                self.rcWidthConstant.constant = 24
                self.rcdcGapWidthConstant.constant = 12
                delieveryCharges.text = deleieveryCharge

            }else {
                self.dcWidthConstant.constant = 0
                self.rcWidthConstant.constant = 0
                self.rcdcGapWidthConstant.constant = 0

                delieveryCharges.text = ""

            }
            
            productPrice.text     = productPrices
            productTitle.text     = productTitles
            productUnits.text     = productUnit
            // productImage.orderImageDownload(url: productImages)
            productImage.setImage(from: productImages)
            /* initializing cartcount */
            self.getCartCountValue(userId: userId!)
            /* confugure custom calnedar*/
            
            startDateTextField.delegate = self
            endDateTextField.delegate = self
            self.startDateTextField.addTarget(self, action: #selector(self.startDateTextFieldActive), for: UIControlEvents.touchDown)
            self.endDateTextField.addTarget(self, action: #selector(self.endDatetextFieldActive), for: UIControlEvents.touchDown)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let now = Date()
            let dateString = formatter.string(from: now)
            startDate = now
            endDate = now
            //  let formateDate = dateFormatter.date(from: tomorrowString)!
            //  dateFormatter.dateFormat = "dd-MM-yyyy"
            //   let tomorrowdate = formateDate.stringFromFormat(tomorrowString)
            let  today = Date()
            let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
            let  dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let tomorrowString = dateFormatter.string(from: tomorrow!)
            tomorrowDate = tomorrowString
            let date = NSDate()
            let outputFormat = DateFormatter()
            outputFormat.locale = NSLocale(localeIdentifier:"en_US") as Locale!
            outputFormat.dateFormat = "HH:mm:ss"
            if outputFormat.string(from: date as Date) >= iosCuttOfftime {
                let  todayDate = Date()
                let  tomorowDate = Calendar.current.date(byAdding: .day, value: 1, to: todayDate)
                let dayAfterTomorrow = Calendar.current.date(byAdding: .day, value: 1, to: tomorowDate!)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let tomorrowDateString = dateFormatter.string(from: dayAfterTomorrow!)
                startDateTextField.text = tomorrowDateString
                endDateTextField.text = tomorrowDateString
                getDayOfWeekString(today: tomorrowDateString)
                
            }else{
                
                let  today = Date()
                let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
                let  dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let tomorrowString = dateFormatter.string(from: tomorrow!)
                startDateTextField.text = tomorrowString
                endDateTextField.text = tomorrowString
                getDayOfWeekString(today: tomorrowString)
                
            }
            
            
            /* creating corner radius for UILabel*/
            sunLabel.layer.cornerRadius = sunLabel.frame.width / 2
            sunLabel.layer.masksToBounds = true
            monLabel.layer.cornerRadius = monLabel.frame.width / 2
            monLabel.layer.masksToBounds = true
            tueLabel.layer.cornerRadius = tueLabel.frame.width / 2
            tueLabel.layer.masksToBounds = true
            wedLabel.layer.cornerRadius = wedLabel.frame.width / 2
            wedLabel.layer.masksToBounds = true
            thuLabel.layer.cornerRadius = thuLabel.frame.width / 2
            thuLabel.layer.masksToBounds = true
            friLabel.layer.cornerRadius = friLabel.frame.width / 2
            friLabel.layer.masksToBounds = true
            satLabel.layer.cornerRadius = satLabel.frame.width / 2
            satLabel.layer.masksToBounds = true
            //    cartCountLabel.layer.cornerRadius = cartCountLabel.frame.width / 2
            //    cartCountLabel.layer.masksToBounds = true
            UserDefaults.standard.removeObject(forKey: "launchedBefore")
            
            
        }
        
        override func viewDidAppear(_ animated: Bool) {
            
        }
        @discardableResult func getDayOfWeekString(today: String) -> String? {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            if let todayDate = formatter.date(from: today) {
                let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
                let myComponents = myCalendar.components(.weekday, from: todayDate)
                let weekDay = myComponents.weekday
                switch weekDay! {
                case 1:
                    monBtnRef.isUserInteractionEnabled = false
                    monBtnDecRef.isUserInteractionEnabled = false
                    monLabel.isHidden = true
                    tueBtnIncRef.isUserInteractionEnabled = false
                    tueDecRefBtn.isUserInteractionEnabled = false
                    tueLabel.isHidden = true
                    wedBtnRef.isUserInteractionEnabled = false
                    wedBtnDecRef.isUserInteractionEnabled = false
                    wedLabel.isHidden = true
                    thuBtnRef.isUserInteractionEnabled = false
                    thuBtnDecRef.isUserInteractionEnabled = false
                    thuLabel.isHidden = true
                    friBtnRef.isUserInteractionEnabled = false
                    friDecBtnRef.isUserInteractionEnabled = false
                    friLabel.isHidden = true
                    satBtnRef.isUserInteractionEnabled = false
                    satDecBtnRef.isUserInteractionEnabled = false
                    satLabel.isHidden = true
                    sunLabel.text = "0"
                    sunLabelReference = Int(sunLabel.text!)!
                    return "sun"
                case 2:
                    sunButtonRef.isUserInteractionEnabled = false
                    sunDecBtnRef.isUserInteractionEnabled = false
                    sunLabel.isHidden = true
                    tueBtnIncRef.isUserInteractionEnabled = false
                    tueDecRefBtn.isUserInteractionEnabled = false
                    tueLabel.isHidden = true
                    wedBtnRef.isUserInteractionEnabled = false
                    wedBtnDecRef.isUserInteractionEnabled = false
                    wedLabel.isHidden = true
                    thuBtnRef.isUserInteractionEnabled = false
                    thuBtnDecRef.isUserInteractionEnabled = false
                    thuLabel.isHidden = true
                    friBtnRef.isUserInteractionEnabled = false
                    friDecBtnRef.isUserInteractionEnabled = false
                    friLabel.isHidden = true
                    satBtnRef.isUserInteractionEnabled = false
                    satDecBtnRef.isUserInteractionEnabled = false
                    satLabel.isHidden = true
                    monLabel.text = "0"
                    monLabelReference = Int(monLabel.text!)!
                    print("Mon")
                    return "Mon"
                case 3:
                    monBtnRef.isUserInteractionEnabled = false
                    monBtnDecRef.isUserInteractionEnabled = false
                    monLabel.isHidden = true
                    sunButtonRef.isUserInteractionEnabled = false
                    sunDecBtnRef.isUserInteractionEnabled = false
                    sunLabel.isHidden = true
                    wedBtnRef.isUserInteractionEnabled = false
                    wedBtnDecRef.isUserInteractionEnabled = false
                    wedLabel.isHidden = true
                    thuBtnRef.isUserInteractionEnabled = false
                    thuBtnDecRef.isUserInteractionEnabled = false
                    thuLabel.isHidden = true
                    friBtnRef.isUserInteractionEnabled = false
                    friDecBtnRef.isUserInteractionEnabled = false
                    friLabel.isHidden = true
                    satBtnRef.isUserInteractionEnabled = false
                    satDecBtnRef.isUserInteractionEnabled = false
                    satLabel.isHidden = true
                    tueLabel.text = "0"
                    tueLabelReference = Int(tueLabel.text!)!
                    
                    return "Tue"
                case 4:
                    monBtnRef.isUserInteractionEnabled = false
                    monBtnDecRef.isUserInteractionEnabled = false
                    monLabel.isHidden = true
                    tueBtnIncRef.isUserInteractionEnabled = false
                    tueDecRefBtn.isUserInteractionEnabled = false
                    tueLabel.isHidden = true
                    sunButtonRef.isUserInteractionEnabled = false
                    sunDecBtnRef.isUserInteractionEnabled = false
                    sunLabel.isHidden = true
                    thuBtnRef.isUserInteractionEnabled = false
                    thuBtnDecRef.isUserInteractionEnabled = false
                    thuLabel.isHidden = true
                    friBtnRef.isUserInteractionEnabled = false
                    friDecBtnRef.isUserInteractionEnabled = false
                    friLabel.isHidden = true
                    satBtnRef.isUserInteractionEnabled = false
                    satDecBtnRef.isUserInteractionEnabled = false
                    satLabel.isHidden = true
                    wedLabel.text = "0"
                    wedLabelReference = Int(wedLabel.text!)!
                    return "Wed"
                case 5:
                    monBtnRef.isUserInteractionEnabled = false
                    monBtnDecRef.isUserInteractionEnabled = false
                    monLabel.isHidden = true
                    tueBtnIncRef.isUserInteractionEnabled = false
                    tueDecRefBtn.isUserInteractionEnabled = false
                    tueLabel.isHidden = true
                    wedBtnRef.isUserInteractionEnabled = false
                    wedBtnDecRef.isUserInteractionEnabled = false
                    wedLabel.isHidden = true
                    sunButtonRef.isUserInteractionEnabled = false
                    sunDecBtnRef.isUserInteractionEnabled = false
                    sunLabel.isHidden = true
                    friBtnRef.isUserInteractionEnabled = false
                    friDecBtnRef.isUserInteractionEnabled = false
                    friLabel.isHidden = true
                    satBtnRef.isUserInteractionEnabled = false
                    satDecBtnRef.isUserInteractionEnabled = false
                    satLabel.isHidden = true
                    thuLabel.text = "0"
                    thuLabelReference = Int(thuLabel.text!)!
                    return "Thu"
                case 6:
                    monBtnRef.isUserInteractionEnabled = false
                    monBtnDecRef.isUserInteractionEnabled = false
                    monLabel.isHidden = true
                    tueBtnIncRef.isUserInteractionEnabled = false
                    tueDecRefBtn.isUserInteractionEnabled = false
                    tueLabel.isHidden = true
                    wedBtnRef.isUserInteractionEnabled = false
                    wedBtnDecRef.isUserInteractionEnabled = false
                    wedLabel.isHidden = true
                    thuBtnRef.isUserInteractionEnabled = false
                    thuBtnDecRef.isUserInteractionEnabled = false
                    thuLabel.isHidden = true
                    sunButtonRef.isUserInteractionEnabled = false
                    sunDecBtnRef.isUserInteractionEnabled = false
                    sunLabel.isHidden = true
                    satBtnRef.isUserInteractionEnabled = false
                    satDecBtnRef.isUserInteractionEnabled = false
                    satLabel.isHidden = true
                    friLabel.text = "0"
                    friLabelReference = Int(friLabel.text!)!
                    return "Fri"
                case 7:
                    monBtnRef.isUserInteractionEnabled = false
                    monBtnDecRef.isUserInteractionEnabled = false
                    monLabel.isHidden = true
                    tueBtnIncRef.isUserInteractionEnabled = false
                    tueDecRefBtn.isUserInteractionEnabled = false
                    tueLabel.isHidden = true
                    wedBtnRef.isUserInteractionEnabled = false
                    wedBtnDecRef.isUserInteractionEnabled = false
                    wedLabel.isHidden = true
                    thuBtnRef.isUserInteractionEnabled = false
                    thuBtnDecRef.isUserInteractionEnabled = false
                    thuLabel.isHidden = true
                    friBtnRef.isUserInteractionEnabled = false
                    friDecBtnRef.isUserInteractionEnabled = false
                    friLabel.isHidden = true
                    sunButtonRef.isUserInteractionEnabled = false
                    sunDecBtnRef.isUserInteractionEnabled = false
                    sunLabel.isHidden = true
                    satLabel.text = "0"
                    satLabelReference = Int(satLabel.text!)!
                    return "Sat"
                default:
                    print("Error fetching days")
                    return "Day"
                }
            } else {
                return nil
            }
            
        }
        
        
        func endDatetextFieldActive() {
            strSelctedDateType = "to"
            let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
            let  today = Date()
            let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
            selector.optionCurrentDate = tomorrow!
            let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
            if launchedBefore  {
                print("Not first launch.")
                if startDate! <= singleDate && startDate! <= today {
                    endDate = singleDate
                    endDateTextField.text = singleDate.stringFromFormat("yyyy-MM-dd")
                    selector.optionCurrentDate = singleDate
                }else{
                    let  today = Date()
                    let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
                    selector.optionCurrentDate = tomorrow!
                }
            } else {
                print("First launch, setting UserDefault.")
                let  today = Date()
                let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
                selector.optionCurrentDate = tomorrow!
                UserDefaults.standard.set(true, forKey: "launchedBefore")
            }
            
            //    selector.optionCurrentDate = singleDate
            selector.delegate = self
            let  date = Date()
            let outputFormat = DateFormatter()
            outputFormat.locale = NSLocale(localeIdentifier:"en_US") as Locale!
            outputFormat.dateFormat = "HH:mm:ss"
            if outputFormat.string(from: date as Date) >= iosCuttOfftime {
                let  todayDate = Date()
                let  tomorowDate = Calendar.current.date(byAdding: .day, value: 1, to: todayDate)
                let dayAfterTomorrow = Calendar.current.date(byAdding: .day, value: 1, to: tomorowDate!)
                selector.optionCurrentDate = dayAfterTomorrow!
            }
            //    }else if outputFormat.string(from: date as Date) <= iosCuttOfftime{
            //    let  today = Date()
            //    let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
            //    selector.optionCurrentDate = tomorrow!
            //    }
            //    }else if selector.optionCurrentDate == singleDate{
            //    selector.optionCurrentDate = singleDate
            //    }
            //    }else if  outputFormat.string(from: date as Date) <= iosCuttOfftime {
            //        let  today = Date()
            //        let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
            //        selector.optionCurrentDate = tomorrow!
            //        }
            selector.optionCurrentDates = Set(multipleDates)
            selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
            selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
            selector.optionStyles.showDateMonth(true)
            selector.optionStyles.showMonth(false)
            selector.optionStyles.showYear(true)
            selector.optionStyles.showTime(false)
            present(selector, animated: true, completion: nil)
            print(strSelctedDateType)
            
        }
        
        func startDateTextFieldActive() {
            let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
            strSelctedDateType = "from"
            selector.delegate = self
            let  date = Date()
            let outputFormat = DateFormatter()
            outputFormat.locale = NSLocale(localeIdentifier:"en_US") as Locale!
            outputFormat.dateFormat = "HH:mm:ss"
            if outputFormat.string(from: date as Date) >= iosCuttOfftime {
                
                let  todayDate = Date()
                let  tomorowDate = Calendar.current.date(byAdding: .day, value: 1, to: todayDate)
                let dayAfterTomorrow = Calendar.current.date(byAdding: .day, value: 1, to: tomorowDate!)
                selector.optionCurrentDate = dayAfterTomorrow!
                
            }else{
                
                let  today = Date()
                let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
                selector.optionCurrentDate = tomorrow!
            }
            
            
            selector.optionCurrentDates = Set(multipleDates)
            selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
            selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
            selector.optionStyles.showDateMonth(true)
            selector.optionStyles.showMonth(false)
            selector.optionStyles.showYear(true)
            selector.optionStyles.showTime(false)
            present(selector, animated: true, completion: nil)
            
            
        }
        @IBAction func startDatetextFieldChanged(_ sender: AnyObject) {
            
            let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
            selector.delegate = self
            let  date = Date()
            let outputFormat = DateFormatter()
            outputFormat.locale = NSLocale(localeIdentifier:"en_US") as Locale!
            outputFormat.dateFormat = "HH:mm:ss"
            if outputFormat.string(from: date as Date) >= iosCuttOfftime {
                
                let  todayDate = Date()
                let  tomorowDate = Calendar.current.date(byAdding: .day, value: 1, to: todayDate)
                let dayAfterTomorrow = Calendar.current.date(byAdding: .day, value: 1, to: tomorowDate!)
                selector.optionCurrentDate = dayAfterTomorrow!
                
            }else{
                
                let  today = Date()
                let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
                selector.optionCurrentDate = tomorrow!
            }
            selector.optionCurrentDates = Set(multipleDates)
            selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
            selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
            selector.optionStyles.showDateMonth(true)
            selector.optionStyles.showMonth(false)
            selector.optionStyles.showYear(true)
            selector.optionStyles.showTime(false)
        }
        @IBAction func endDatetextFieldChanged(_ sender: AnyObject) {
            let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
            selector.optionCurrentDate = singleDate
            selector.delegate = self
            let  date = Date()
            let outputFormat = DateFormatter()
            outputFormat.locale = NSLocale(localeIdentifier:"en_US") as Locale!
            outputFormat.dateFormat = "HH:mm:ss"
            if outputFormat.string(from: date as Date) >= iosCuttOfftime {
                let  todayDate = Date()
                let  tomorowDate = Calendar.current.date(byAdding: .day, value: 1, to: todayDate)
                let dayAfterTomorrow = Calendar.current.date(byAdding: .day, value: 1, to: tomorowDate!)
                selector.optionCurrentDate = dayAfterTomorrow!
            }else if outputFormat.string(from: date as Date) <= iosCuttOfftime  {
                let  today = Date()
                let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
                selector.optionCurrentDate = tomorrow!
            }
            selector.optionCurrentDates = Set(multipleDates)
            selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
            selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
            selector.optionStyles.showDateMonth(true)
            selector.optionStyles.showMonth(false)
            selector.optionStyles.showYear(true)
            selector.optionStyles.showTime(false)
        }
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            
            
            
        }
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            
            if textField == startDateTextField{
                return false
            }else if textField == endDateTextField{
                return false
            }else{
                return true
            }
            
        }
        
        //  func textFieldDidEndEditing(_ textField: UITextField) {
        //  if startDateTextField == textField {
        //  startDateTextField.text = selectedDate?.stringFromFormat("yyyy-MM-dd")
        //
        //  } else {
        //
        //
        //  endDateTextField.text = selectedDate?.stringFromFormat("yyyy-MM-dd")
        //
        //  }
        //
        //    }
        
        @IBAction func cartBarBtn(_ sender: Any) {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
            let cartNavigationController = UINavigationController(rootViewController: controller)
            self.present(cartNavigationController, animated: true, completion: nil)
        }
        
        @IBAction func backToProducts(_ sender: Any) {
            self.dismiss(animated: false, completion: nil)
        }
        
        @IBAction func helpBtn(_ sender: Any) {
            //
            //
            //  let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //  let vc = storyboard.instantiateViewController(withIdentifier: "popover")
            //  vc.modalPresentationStyle = .popover
            //  vc.preferredContentSize = CGSize(width: 300, height: 300)
            //  let popover = vc.popoverPresentationController!
            //  popover.delegate = self
            //  popover.permittedArrowDirections = .down
            //  popover.sourceView = sender as? UIView
            //  popover.sourceRect = (sender as AnyObject).bounds
            //  present(vc, animated: false, completion: nil)
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let myAlert = storyboard.instantiateViewController(withIdentifier: "popover") as! PopViewController
                myAlert.modalPresentationStyle =  UIModalPresentationStyle.overCurrentContext
                myAlert.modalTransitionStyle   =  UIModalTransitionStyle.crossDissolve
                self.present(myAlert, animated: true, completion: nil)
            }
        }
        
        func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
            return .none
        }
        
        func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
            print("Selected \n\(date)\n---")
            //     UserDefaults.standard.removeObject(forKey: "launchedBefore")
            selectedDate = date
            selector.optionCurrentDate = date
            print(date.weekday)
            singleDate = date
            print(singleDate)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let now = Date()
            let dateString = formatter.string(from: now)
            print(strSelctedDateType)
            let todays = Date()
            var tomorrows = Calendar.current.date(byAdding: .day, value: 1, to: todays)
            let dateFormatters = DateFormatter()
            dateFormatters.dateFormat = "dd-MM-yyyy"
            let tomorrowString = dateFormatters.string(from: tomorrows!)
            if (strSelctedDateType == "from") {
                if endDate! >= date {
                    startDate = date
                    startDateTextField.text = singleDate.stringFromFormat("dd-MM-yyyy")
                } else {
                    
                    //    self.view.makeToast("start date must be older", duration: 4.0, position: CGPoint(x: 180, y: 340))
                    self.view.makeToast("start date must be older", duration: 3.0, position: .center)
                    
                    //  startDateTextField.text = tomorrowDate
                    let date = NSDate()
                    let outputFormat = DateFormatter()
                    outputFormat.locale = NSLocale(localeIdentifier:"en_US") as Locale!
                    outputFormat.dateFormat = "HH:mm:ss"
                    if outputFormat.string(from: date as Date) >= iosCuttOfftime {
                        let  todayDate = Date()
                        let  tomorowDate = Calendar.current.date(byAdding: .day, value: 1, to: todayDate)
                        let dayAfterTomorrow = Calendar.current.date(byAdding: .day, value: 1, to: tomorowDate!)
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy"
                        
                        let tomorrowDateString = dateFormatter.string(from: dayAfterTomorrow!)
                        startDateTextField.text = tomorrowDateString
                        endDateTextField.text = tomorrowDateString
                        getDayOfWeekString(today: tomorrowDateString)
                        
                    }else{
                        
                        let  today = Date()
                        let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
                        let  dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy"
                        let tomorrowString = dateFormatter.string(from: tomorrow!)
                        startDateTextField.text = tomorrowString
                        endDateTextField.text = tomorrowString
                        getDayOfWeekString(today: tomorrowString)
                        
                    }
                }
                
            } else {
                
                if startDate! <= date {
                    endDate = date
                    endDateTextField.text = singleDate.stringFromFormat("dd-MM-yyyy"
                    )
                } else {
                    
                    //    self.view.makeToast("end date must be older", duration: 4.0, position: CGPoint(x: 180, y: 340))
                    self.view.makeToast("end date must be older", duration: 3.0, position: .center)
                    //    selector.optionCurrentDate = singleDate
                    let date = NSDate()
                    let outputFormat = DateFormatter()
                    outputFormat.locale = NSLocale(localeIdentifier:"en_US") as Locale!
                    outputFormat.dateFormat = "HH:mm:ss"
                    if outputFormat.string(from: date as Date) >= iosCuttOfftime {
                        let  todayDate = Date()
                        let  tomorowDate = Calendar.current.date(byAdding: .day, value: 1, to: todayDate)
                        let dayAfterTomorrow = Calendar.current.date(byAdding: .day, value: 1, to: tomorowDate!)
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy"
                        
                        let tomorrowDateString = dateFormatter.string(from: dayAfterTomorrow!)
                        startDateTextField.text = tomorrowDateString
                        endDateTextField.text = tomorrowDateString
                        getDayOfWeekString(today: tomorrowDateString)
                        selector.optionCurrentDate = dayAfterTomorrow!
                        
                    }else{
                        let  today = Date()
                        let  tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
                        let  dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy"
                        let tomorrowString = dateFormatter.string(from: tomorrow!)
                        startDateTextField.text = tomorrowString
                        endDateTextField.text = tomorrowString
                        getDayOfWeekString(today: tomorrowString)
                        selector.optionCurrentDate = tomorrow!
                    }
                }
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            
            let start = dateFormatter.date(from: startDateTextField.text!)!
            let end = dateFormatter.date(from: endDateTextField.text!)!
            let diff = Date.daysBetween(start: start, end: end) // 365
            print("diffrence between dates :",diff)
            self.dateDifferenceArray.append(diff)
            /* creating the calendar Logic */
            self.clearDatesLayout()
            let startDates = dateFormatter.date(from:startDateTextField.text!)!
            let endDates = dateFormatter.date(from:endDateTextField.text!)!
            let datesBetweenArray = Date().generateDatesArrayBetweenTwoDates(startDate:startDates , endDate: endDates)
            for dateArray in datesBetweenArray {
                let calendar = Calendar.current
                // let day = calendar.component(.day, from: dateArray)
                let weeday = calendar.component(.weekday, from: dateArray)
                let weekday = Weekday.self
                switch weeday {
                case weekday.Sunday.rawValue:
                    sunButtonRef.isUserInteractionEnabled = true
                    sunDecBtnRef.isUserInteractionEnabled = true
                    sunLabel.isHidden = false
                    sunLabel.text = "0"
                    break;
                case weekday.Monday.rawValue:
                    monBtnRef.isUserInteractionEnabled = true
                    monBtnDecRef.isUserInteractionEnabled = true
                    monLabel.isHidden = false
                    monLabel.text = "0"
                    break;
                case weekday.Tuesday.rawValue:
                    tueBtnIncRef.isUserInteractionEnabled = true
                    tueDecRefBtn.isUserInteractionEnabled = true
                    tueLabel.isHidden = false
                    tueLabel.text = "0"
                    break;
                case weekday.Wednesday.rawValue:
                    wedBtnRef.isUserInteractionEnabled = true
                    wedBtnDecRef.isUserInteractionEnabled = true
                    wedLabel.isHidden = false
                    wedLabel.text = "0"
                    break;
                case weekday.Thursday.rawValue:
                    thuBtnRef.isUserInteractionEnabled = true
                    thuBtnDecRef.isUserInteractionEnabled = true
                    thuLabel.isHidden = false
                    thuLabel.text = "0"
                    break;
                case weekday.Friday.rawValue:
                    friBtnRef.isUserInteractionEnabled = true
                    friDecBtnRef.isUserInteractionEnabled = true
                    friLabel.isHidden = false
                    friLabel.text = "0"
                    break;
                case weekday.Saturday.rawValue:
                    satBtnRef.isUserInteractionEnabled = true
                    satDecBtnRef.isUserInteractionEnabled = true
                    satLabel.isHidden = false
                    satLabel.text = "0"
                    break;
                default:
                    clearDatesLayout()
                    break;
                }
            }
        }
        
        //  private func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: Date) {
        //  //        print("Selected Multiple Dates \n\(dates)\n---")
        //  singleDate = dates
        //  endDateTextField.text = dates.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        //  }
        
        func clearDatesLayout(){
            monBtnRef.isUserInteractionEnabled = false
            monBtnDecRef.isUserInteractionEnabled = false
            monLabel.isHidden = true
            monLabel.text = "0"
            tueBtnIncRef.isUserInteractionEnabled = false
            tueDecRefBtn.isUserInteractionEnabled = false
            tueLabel.isHidden = true
            tueLabel.text = "0"
            wedBtnRef.isUserInteractionEnabled = false
            wedBtnDecRef.isUserInteractionEnabled = false
            wedLabel.isHidden = true
            wedLabel.text = "0"
            thuBtnRef.isUserInteractionEnabled = false
            thuBtnDecRef.isUserInteractionEnabled = false
            thuLabel.isHidden = true
            thuLabel.text = "0"
            friBtnRef.isUserInteractionEnabled = false
            friDecBtnRef.isUserInteractionEnabled = false
            friLabel.isHidden = true
            friLabel.text = "0"
            sunButtonRef.isUserInteractionEnabled = false
            sunDecBtnRef.isUserInteractionEnabled = false
            sunLabel.isHidden = true
            sunLabel.text = "0"
            satBtnRef.isUserInteractionEnabled = false
            satDecBtnRef.isUserInteractionEnabled = false
            satLabel.isHidden = true
            satLabel.text = "0"
            
        }
        
        func getCartCountValue(userId: String) {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_CARTCOUNT + userId) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    
                    do {
                        
                        guard let cartData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        DispatchQueue.main.async {
                            if let cartCount = cartData["cart_count"] {
                                if cartCount as! Int == 0{
                                    self.cartBtnReference.removeBadge()
                                }else{
                                    self.cartBtnReference.addBadge(number: cartCount as! Int)
                                }
                            }
                        }
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                })
                task.resume()
                
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        
        @IBAction func addToCartBtnAction(_sender: Any) {
            if Reachability.isConnectedToNetwork(){
                self.addToCart()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    
                }
            }
        }
        
        /* Adding the product To Cart Method */
        func addToCart(){
            //CONVERT FROM NSDate to String
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let subscribeProductUrl =  NSURL(string:Constants.URL_REQUEST_USER_SUBSCRIBEPRODUCT){
                var  request = URLRequest(url:subscribeProductUrl as URL)
                var subscribeProduct = SubscribeProductModel()
                subscribeProduct.productid = productId
                subscribeProduct.userid = userId
                guard let startDateString = startDateTextField.text else{
                    return
                }
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                guard   let startDateNew = dateFormatter.date(from:startDateString) else{
                    return
                }
                let newdateFormatter = DateFormatter()
                newdateFormatter.dateFormat = "yyyy-MM-dd"
                let startDateStringNew = newdateFormatter.string(from:startDateNew)
                guard let endDateString = endDateTextField.text else{
                    return
                }
                let enddateFormatter = DateFormatter()
                enddateFormatter.dateFormat = "dd-MM-yyyy"
                guard  let endDateNew = enddateFormatter.date(from:endDateString) else{
                    return
                }
                let newEnddateFormatter = DateFormatter()
                newEnddateFormatter.dateFormat = "yyyy-MM-dd"
                let endDateStringNew = newdateFormatter.string(from:endDateNew)
                subscribeProduct.startdate = startDateStringNew
                subscribeProduct.enddate = endDateStringNew
                subscribeProduct.sunday = sunLabel.text
                subscribeProduct.monday = monLabel.text
                subscribeProduct.tuesday = tueLabel.text
                subscribeProduct.wednesday = wedLabel.text
                subscribeProduct.thursday = thuLabel.text
                subscribeProduct.friday = friLabel.text
                subscribeProduct.satday = satLabel.text
                subscribeProduct.orderid = "0"
                subscribeProduct.menuId = self.menuId
                guard let productId =  subscribeProduct.productid,let userId =  subscribeProduct.userid,let startDate = subscribeProduct.startdate,let endDate = subscribeProduct.enddate,let sunday = subscribeProduct.sunday,let monday = subscribeProduct.monday,let tuesday = subscribeProduct.tuesday ,let wednesday = subscribeProduct.wednesday,let thursday = subscribeProduct.thursday,let friday = subscribeProduct.friday ,let satday =  subscribeProduct.satday,let orderId = subscribeProduct.orderid, let menuId = subscribeProduct.menuId else{return}
                
                let subscribeProductParams  = ["productid":productId,"userid":userId,"startdate":startDate,"enddate":endDate,"monday":monday,"tuesday":tuesday,"wednesday":wednesday,"thursday":thursday,"friday":friday,"satday":satday,"orderid":orderId,"sunday": sunday, "menuid": menuId] as [String:Any]
                
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: subscribeProductParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        let fetchSubscribeProductJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        print(fetchSubscribeProductJson)
                        if let subscribeProductResponse = fetchSubscribeProductJson as? [String:Any]{
                            print(subscribeProductResponse["description"]!)
                            print(subscribeProductResponse["message"]!)
                            if (subscribeProductResponse["code"] as! NSNumber == 200) {
                                DispatchQueue.main.async {
                                    
                                    self.view.makeToast(subscribeProductResponse["description"]! as! String, duration: 3.0, position: .center)
                                    
                                    
                                }
                                DispatchQueue.main.async {
                                    self.perform(#selector(self.perfromDelayToCart), with: self, afterDelay: 1)
                                }
                            }else{
                                
                                DispatchQueue.main.async {
                                    
                                    self.view.makeToast(subscribeProductResponse["description"]! as! String, duration: 3.0, position: .center)
                                }
                            }
                        }
                    }catch let jsonerror {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            
                        }
                        print(jsonerror.localizedDescription as Any)
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    
                }
            }
        }
        
        func perfromDelayToCart() {
            DispatchQueue.main.async{
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
                let cartNavigationController = UINavigationController(rootViewController: controller)
                self.present(cartNavigationController, animated: true, completion: nil)
                
            }
        }
        
        /* Product Details */
        
        func productDetails(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let productDetailsUrl =  NSURL(string:Constants.URL_REQUEST_PRODUCTDETAILS){
                var  request = URLRequest(url:productDetailsUrl as URL)
                var productDetailsModel:ProductDetailsModel = ProductDetailsModel()
                productDetailsModel.apartmentid = UserDefaults.standard.value(forKey: "apartmentId") as? String
                productDetailsModel.productid = productId
                productDetailsModel.userid = userId
                guard let userApartmentId = productDetailsModel.apartmentid,let userProductId = productDetailsModel.productid,let userId =   productDetailsModel.userid else{return}
                let signInParams  = ["apartmentid": userApartmentId,"productid": userProductId,"userid": userId] as [String:Any]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: signInParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        return
                    }
                    do{
                        let productDetailsJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        print(productDetailsJson)
                        if let productDetailsResponse = productDetailsJson as? [String:Any]{
                            print(productDetailsResponse["description"]!)
                            print(productDetailsResponse["message"]!)
                            if (productDetailsResponse["code"] as! NSNumber == 200) {
                                DispatchQueue.main.async {
                                    if let productDetailsExistMessage = productDetailsResponse["product_exist_message"] as? String{
                                        self.productAlreadySubscribedLabel.isHidden = false
                                        self.productAlreadySubscribedLabel.text = productDetailsExistMessage
                                    }
                                }
                            }
                            else {
                                DispatchQueue.main.async
                                    {
                                        //  self.view.makeToast(productDetailsResponse["description"]! as! String, duration: 4.0, position: CGPoint(x: 180, y: 340))
                                        
                                }
                            }
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
    }//class
    
    
    
    extension UIImageView {
        
        func orderImageDownload(url: String) {
            let urlRequest = URLRequest(url: URL(string: url)!)
            let task = URLSession.shared.dataTask(with: urlRequest) { (data, reponse, error) in
                
                guard let data = data else {
                    return
                    
                }
                
                if error != nil {
                    
                    print(error!)
                    
                }
                
                DispatchQueue.main.async {
                    
                    self.image = UIImage(data: data)
                }
            }
            task.resume()
        }
    }
    
    /* creating the extension to calculate the difference between dates */
    
    extension Date {
        
        func daysBetween(date: Date) -> Int {
            return Date.daysBetween(start: self, end: date)
        }
        
        static func daysBetween(start: Date, end: Date) -> Int {
            let calendar = Calendar.current
            
            // Replace the hour (time) of both dates with 00:00
            let date1 = calendar.startOfDay(for: start)
            let date2 = calendar.startOfDay(for: end)
            
            let a = calendar.dateComponents([.day], from: date1, to: date2)
            return a.value(for: .day)!
        }
    }
    
    extension Date{
        
        func generateDatesArrayBetweenTwoDates(startDate: Date , endDate:Date) ->[Date]
        {
            var datesArray: [Date] =  [Date]()
            var startDate = startDate
            let calendar = Calendar.current
            
            let fmt = DateFormatter()
            fmt.dateFormat = "yyyy-MM-dd"
            datesArray.removeAll()
            while startDate <= endDate {
                
                datesArray.append(startDate)
                startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
                
            }
            return datesArray
        }
    }
    extension UILabel {
        func shadow() {
            self.layer.shadowColor = self.textColor.cgColor
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowRadius = 3.0
            self.layer.shadowOpacity = 0.5
            self.layer.masksToBounds = false
            self.layer.shouldRasterize = true
        }
    }
    
    extension UIViewController{
        
    }
    
