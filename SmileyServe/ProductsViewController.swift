    //
    //  ProductsViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 22/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    
    import UIKit
    import SVProgressHUD
    
    class ProductsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
        
        @IBOutlet weak var orderBtnWidConst: NSLayoutConstraint!
        var strItemId = ""
        var strItemTitle = ""
        var strEnableStatus = ""
        @IBOutlet var productTableView: UITableView!
        @IBOutlet var productMainImage: CustomImageView!
        var productItems:[ProductsList?] = []
        var productItemsWithStatus:[ProductsList?] = []
        var rightCartButtonItem = UIBarButtonItem()
        var  buttonTag : Int!
        

        var cartUserId = UserDefaults.standard.value(forKey: "id") as? String

        
        override func viewDidLoad() {
            
            productTableView.isHidden = true
            super.viewDidLoad()
            self.productItems = [ProductsList]()
            navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
            self.navigationController?.navigationBar.tintColor =  UIColor.white
            self.navigationController?.navigationBar.isTranslucent = false

            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
            self.productNetworkCalls()
            
            rightCartButtonItem = UIBarButtonItem(image: UIImage(named:"ic_cart_new"), style: .plain, target: self, action: #selector(handleCart))
            self.navigationItem.setRightBarButtonItems([rightCartButtonItem], animated: true)


        }
        func handleCart(){
            if userId != nil {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
                let cartNavigationController = UINavigationController(rootViewController: controller)
                self.present(cartNavigationController, animated: true, completion: nil)
            }else{
                
                DispatchQueue.main.async {
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                }
                
            }
        }
        func productNetworkCalls(){
            if Reachability.isConnectedToNetwork(){
                self.productList()
//                if userId != nil{
//                    self.getCartCountValue(userId: userId!)
//                    self.paymentGatewayDetails()
//
//                }
            }else
            {
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)  }
            }
        }
        @IBAction func backToHomeBtnAction(_ sender: Any) {
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let signupNavigation = UINavigationController(rootViewController: signupController)
            self.present(signupNavigation, animated: true, completion: nil)
            
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return productItems.count + 1
        }
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return CGFloat.leastNormalMagnitude
        }
       
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if indexPath.row == self.productItems.count {
                
                if self.productTableView.isHidden == true {
                    
                    return 0

                }else{
                    return 65

                }
            }else {
                
                let deliveryCharges = self.productItems[indexPath.row]?.deleieveryCharges
                let discountPercentage = self.productItems[indexPath.row]?.discount_percentage
                if discountPercentage == "0" && deliveryCharges == "0" {
                    
                    return 125
                    
                }else {
                    return 155
                }
                    
                
            }

        }

        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if indexPath.row == self.productItems.count {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ThatsAllCell", for: indexPath) as! ProductCustomCell

                return cell
                
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as! ProductCustomCell
                
                if self.productItems[indexPath.row]?.selling_type == "1" {
                    //subscribe
                    cell.productOrderButtonRef.addTarget(self, action: #selector(ProductsViewController.productSubscribe), for: .touchUpInside)

                    
                }else {
                    //add
                    cell.productOrderButtonRef.addTarget(self, action: #selector(ProductsViewController.productAdd), for: .touchUpInside)

                }
                
                
                
                cell.incBtn.layer.shadowColor = UIColor.darkGray.cgColor
                cell.incBtn.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
                cell.incBtn.layer.shadowOpacity = 1.0
                cell.incBtn.layer.shadowRadius = 0.0
                cell.incBtn.layer.masksToBounds = false
                cell.incBtn.layer.cornerRadius = 4.0
                
                cell.quantityBtn.layer.shadowColor = UIColor.lightGray.cgColor
                cell.quantityBtn.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
                cell.quantityBtn.layer.shadowOpacity = 1.0
                cell.quantityBtn.layer.shadowRadius = 0.0
                cell.quantityBtn.layer.masksToBounds = false
                
                cell.decBtn.layer.shadowColor = UIColor.darkGray.cgColor
                cell.decBtn.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
                cell.decBtn.layer.shadowOpacity = 1.0
                cell.decBtn.layer.shadowRadius = 0.0
                cell.decBtn.layer.masksToBounds = false
                cell.decBtn.layer.cornerRadius = 4.0
                
                
                cell.incBtn.addTarget(self, action: #selector(ProductsViewController.increaseInstantQuantity), for: .touchUpInside)
                cell.incBtn.tag = indexPath.row

                cell.decBtn.addTarget(self, action: #selector(ProductsViewController.decreaseInstantQuantity), for: .touchUpInside)
                cell.decBtn.tag = indexPath.row

                cell.productOrderButtonRef.tag = indexPath.row
                
                //print("cart_qty",self.productItems[indexPath.row]?.cart_qty)
                
                if self.productItems[indexPath.row]?.cart_qty != "0" && self.productItems[indexPath.row]?.subscribed_type == "2" {
                    cell.qcStackView.isHidden = false
                    cell.productOrderButtonRef.isHidden = true
                    cell.quantityBtn.setTitle(self.productItems[indexPath.row]?.cart_qty, for: .normal)
                }else {
                    cell.qcStackView.isHidden = true
                    cell.productOrderButtonRef.isHidden = false

                }
                
                cell.selectionStyle = .none
                cell.productOrderButtonRef.tag = indexPath.row
                cell.productUnits.text = self.productItems[indexPath.row]?.productUnits
                cell.productTitle.text = self.productItems[indexPath.row]?.productTitle
                cell.productPrice.text = "D.C : Rs. " + (self.productItems[indexPath.row]?.deleieveryCharges)!

                let tap = MyTapGesture(target: self, action: #selector(ProductsViewController.imageTapped))
                tap.tagNumber = indexPath.row
                cell.productImage.addGestureRecognizer(tap)
                
                if self.productItems[indexPath.row]?.deleieveryCharges == "0"
                {
                    let delChrView = cell.delCharStackView.arrangedSubviews[1]
                    delChrView.isHidden = true
                    
                }else{

                    let delChrView = cell.delCharStackView.arrangedSubviews[1]
                    delChrView.isHidden = false
                }
                
                cell.productImage.setImage(from:( self.productItems[indexPath.row]?.productImage)!)
                
                if let disAmount = self.productItems[indexPath.row]?.discounted_price {
                    
                    let disA = "\(disAmount)"
                    cell.discountedAmnt.text = "Rs. " + disA

                }
            

                let discountPercentage = self.productItems[indexPath.row]?.discount_percentage
                let percenVal = "\(discountPercentage ?? "empty")% OFF"
                cell.percentageAmnt.text =  percenVal
                
                if discountPercentage == "0" {
                   
                   let secondView = cell.stackViewActPrice.arrangedSubviews[1]
                    secondView.isHidden = true
                    
                  let delChrView = cell.delCharStackView.arrangedSubviews[0]
                    delChrView.isHidden = true
                    
                }else {
                    let secondView = cell.stackViewActPrice.arrangedSubviews[1]
                    secondView.isHidden = false
                    let delChrView = cell.delCharStackView.arrangedSubviews[0]
                    delChrView.isHidden = false
                }
                
                let actualprice = self.productItems[indexPath.row]?.actual_price
                let resultStr = "Rs. \(actualprice ?? "empty")"
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: resultStr)
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                
                cell.actualAmnt.attributedText = attributeString
                cell.sellingType = self.productItems[indexPath.row]?.selling_type

                cell.stockstatus = self.productItems[indexPath.row]?.stock_status
                
                return cell
            }
           
            
        }
        
        
        
        func increaseInstantQuantity(_sender:UIButton) {
            

            let buttonTag = _sender.tag
            let myIndexPath = IndexPath(row: buttonTag, section: 0)
            
            let cell = productTableView.cellForRow(at: myIndexPath) as! ProductCustomCell
            
            let cartId = (self.productItems[buttonTag]?.cartid)!
            
            print("Increase_CartID", cartId)

            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let changeQuantityUrl =   NSURL(string:Constants.URL_REQUEST_INSTANT_CHANGE_QNTY){
                var request = URLRequest(url:changeQuantityUrl as URL)
                
                let changeQuantityListParams = ["cartid":cartId,"action":"inc","userid": cartUserId!] as [String:Any]
                print("changeQuantityListParams",changeQuantityListParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: changeQuantityListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let changeQuantityJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print("changeQuantityJson",changeQuantityJson)
                        if (changeQuantityJson["code"] as! NSNumber == 200) {
                            DispatchQueue.main.async {
                                
                                let items_count = changeQuantityJson["items_count"] as! String
                                print("items_count",items_count)
                                
                                cell.quantityBtn.setTitle(items_count, for: .normal)
                                
                                
                            }
                        } else{
                            
                            DispatchQueue.main.async {
                                
                            }
                            
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
            
        }
        
        func decreaseInstantQuantity(_sender:UIButton) {
            
            //_sender.isHidden = false
            let buttonTag = _sender.tag
            let myIndexPath = IndexPath(row: buttonTag, section: 0)
            
            let cell = productTableView.cellForRow(at: myIndexPath) as! ProductCustomCell
            
            let cartId = (self.productItems[buttonTag]?.cartid)!
            
            print("Decrease_CartID", cartId)

            
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let changeQuantityUrl =   NSURL(string:Constants.URL_REQUEST_INSTANT_CHANGE_QNTY){
                var request = URLRequest(url:changeQuantityUrl as URL)
                
                let changeQuantityListParams = ["cartid":cartId,"action":"dec","userid": cartUserId!] as [String:Any]
                print(changeQuantityListParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: changeQuantityListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let changeQuantityJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print(changeQuantityJson)
                        if (changeQuantityJson["code"] as! NSNumber == 200) {
                            DispatchQueue.main.async {
                                
//                                let result  = changeQuantityJson["total_price"] as! String
//
//                                let doubleValue = result.floatValue.rounded()
//                                print("floatValue", String(doubleValue))
//                                self.netProductAmount.text = "Rs." + String(doubleValue)
//
//                                UserDefaults.standard.set(String(doubleValue), forKey: "totalProductValue")
//
                                
                                
                                let items_count = changeQuantityJson["items_count"] as! String
                                print("items_count",items_count)
                                
                                
                                if items_count == "0" {
                                    self.buttonTag = _sender.tag
                                    cell.qcStackView.isHidden = true
                                    cell.productOrderButtonRef.isHidden = false
                                    
                                    self.InstantcartDelete()
                                }else{
                                    cell.quantityBtn.setTitle(items_count, for: .normal)
                                }
                            }
                        } else{
                            
                            DispatchQueue.main.async {
                                
                            }
                            
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }


        }
        
        
        func InstantcartDelete() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let cartDeleteUrl =  NSURL(string:Constants.URL_REQUEST_USER_CARTDELETE){
                var  request = URLRequest(url:cartDeleteUrl as URL)
                var cartDeleteModelRef = CartDeleteModel()
                cartDeleteModelRef.userid = cartUserId
                //  cartDeleteModelRef.productid = UserDefaults.standard.value(forKey: "cartProductId") as? String
                cartDeleteModelRef.productid = self.productItems[buttonTag]?.productId
                guard let userId = cartDeleteModelRef.userid,let productId =  cartDeleteModelRef.productid else{return}
                let deleteCartParams  = ["userid":userId,"productid":productId] as [String:Any]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: deleteCartParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        let cartDeleteJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        
                        print(cartDeleteJson)
                        if let cartDeleteResponse = cartDeleteJson as? [String:Any]{
                            print(cartDeleteResponse["description"]!)
                            print(cartDeleteResponse["message"]!)
                            if (cartDeleteResponse["code"] as! NSNumber == 200) {
                                DispatchQueue.main.async {
                                    if  let  cartCount = cartDeleteResponse["cart_count"] {
                                        if cartCount as! Int == 0{
                                            self.rightCartButtonItem.removeBadge()
                                        }else{  self.rightCartButtonItem.addBadge(number: cartCount as! Int)
                                            
                                        }
                                    }
                                    
                                }
                                
                                
                                
                            }else {
                                
                                DispatchQueue.main.async {
                                    self.view.makeToast(cartDeleteResponse["description"]! as? String, duration: 3.0, position: .center)
                                    
                                }
                            }
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        
        /* method for order button based on indexpath*/
        
        func productSubscribe(_sender:UIButton) {
            
                let buttonTag = _sender.tag
                print(buttonTag)
                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let orderController = storyBoard.instantiateViewController(withIdentifier: "Order") as! OrderViewController
                orderController.productPrices = (self.productItems[
                    buttonTag]?.productPrice)!
                orderController.productTitles = (self.productItems[buttonTag]?.productTitle)!
                orderController.productUnit = (self.productItems[buttonTag]?.productUnits)!
                orderController.productId = (self.productItems[buttonTag]?.productId)!
                orderController.productImages = (self.productItems[buttonTag]?.productImage)!
                orderController.deleieveryCharge = (self.productItems[buttonTag]?.deleieveryCharges)!
                orderController.menuId = (self.productItems[buttonTag]?.menuId)!
                self.present(orderController, animated: false, completion: nil)
            
        }
        
        func productAdd(_sender:UIButton) {
            if strItemTitle == "Newspaper"
            {
                self.createConfirmationToProceedOrder(_sender: _sender)
                
            }else {
//                _sender.isHidden = true
//                let buttonTag =  _sender.tag
//                let myIndexPath = IndexPath(row: buttonTag, section: 0)
//                let cell = productTableView.cellForRow(at: myIndexPath) as! ProductCustomCell
//                cell.qcStackView.isHidden = false

                self.addToCartForInstantOrder(_sender: _sender)
            }
        }
        
        
        
        
        
        func getCartCountValue(userId:String){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_CARTCOUNT + userId) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    
                    
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    
                    do {
                        guard let cartData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                            return
                        }
                        
                        DispatchQueue.main.async {
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                            }
                            if  let  cartCount = cartData["cart_count"] {
                                //    self.navigationItem.rightBarButtonItem?.addBadge(number: cartCount as! Int)
                                if cartCount as! Int == 0{
                                    self.rightCartButtonItem.removeBadge()
                                }else{  self.rightCartButtonItem.addBadge(number: cartCount as! Int)
                                    
                                }
                            }
                        }
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async
                            {
                                self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                                
                                
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                })
                task.resume()
                
            }else{
                DispatchQueue.main.async
                    {
                        self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                        
                        
                }
                
            }
        }
        func addToCartForInstantOrder(_sender : UIButton){
            
            
            let buttonTagCart = _sender.tag
            
            print("buttonTagCart",buttonTagCart)
            
            
            //CONVERT FROM NSDate to String
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let instantProductUrl =  NSURL(string:Constants.URL_REQUEST_INSTANT_ORDER){
                
                print("instantProductUrl",instantProductUrl)
                var  request = URLRequest(url:instantProductUrl as URL)
                var insatntProduct = InstantProductModel()
                insatntProduct.productid = (self.productItems[buttonTagCart]?.productId)!
                insatntProduct.userid = userId
                insatntProduct.menuId = (self.productItems[buttonTagCart]?.menuId)!
                guard let productId =  insatntProduct.productid,let userId =  insatntProduct.userid,let menuId = insatntProduct.menuId else{return}
                
                let instantProductParams  = ["productid":productId,"userid":userId,"menuid": menuId,"quantity": "1"] as [String:Any]
                
                print("InstantroductParams",instantProductParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: instantProductParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        let fetchInstantProductJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        print("fetchInstantProductJson",fetchInstantProductJson)
                        if let insatntProductResponse = fetchInstantProductJson as? [String:Any]{
                            print(insatntProductResponse["description"]!)
                            print(insatntProductResponse["message"]!)
                            if (insatntProductResponse["code"] as! NSNumber == 200) {
                                
                                
                                DispatchQueue.main.async {
                                    
                                    
                                    _sender.isHidden = true
                                    let buttonTag =  _sender.tag
                                    let myIndexPath = IndexPath(row: buttonTag, section: 0)
                                    let cell = self.productTableView.cellForRow(at: myIndexPath) as! ProductCustomCell
                                    cell.qcStackView.isHidden = false
                                    
                                    if let loCartID = insatntProductResponse["cart_id"] as? NSNumber {
                                        
                                        let buttonTag = _sender.tag

                                        self.productItems[buttonTag]?.cartid = loCartID.stringValue
                                        
                                    }
                                    
                                    self.view.makeToast(insatntProductResponse["description"]! as? String, duration: 3.0, position: .center)
                                    
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                                        
                                            if  let  cartCount = insatntProductResponse["cart_count"] {
                                                if cartCount as! Int == 0{
                                                    self.rightCartButtonItem.removeBadge()
                                                }else{  self.rightCartButtonItem.addBadge(number: cartCount as! Int)
                                                    
                                                }
                                            }

                                    }
                                    
                                }
                              }else{
                                
                                DispatchQueue.main.async {
                                    
                                    self.view.makeToast(insatntProductResponse["description"]! as? String, duration: 3.0, position: .center)
                                }
                            }
                        }
                    }catch let jsonerror {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            
                        }
                        print(jsonerror.localizedDescription as Any)
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    
                }
            }
    }
    
    
        func imageTapped(sender : MyTapGesture) {
            //let buttonTag = _sender.tag
            
            print("imageTapped")
            
            let buttonTag = sender.tagNumber
            print(buttonTag)
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let orderController = storyBoard.instantiateViewController(withIdentifier: "Order") as! OrderViewController
            orderController.productPrices = (self.productItems[
                buttonTag]?.productPrice)!
            orderController.productTitles = (self.productItems[buttonTag]?.productTitle)!
            orderController.productUnit = (self.productItems[buttonTag]?.productUnits)!
            orderController.productId = (self.productItems[buttonTag]?.productId)!
            orderController.productImages = (self.productItems[buttonTag]?.productImage)!
            orderController.deleieveryCharge = (self.productItems[buttonTag]?.deleieveryCharges)!
            orderController.menuId = (self.productItems[buttonTag]?.menuId)!
            self.present(orderController, animated: false, completion: nil)
           
        }
        
        
        
        /* creating the function for canceldayorder in My Calendar */
        func createConfirmationToProceedOrder(_sender:UIButton){
            let alertController = UIAlertController(title: "Please Read !!!", message: "We deliver Newspaper for next 30 days, It can not be cancelled. Refer FAQ 12 for more info. Click Yes to Continue", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                
                if Reachability.isConnectedToNetwork(){
                    
                    print("New Paper FLow")
                    
                    self.addToCart(_sender: _sender)
                    
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    }
                }
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        
        func addToCart(_sender:UIButton){
            
            
            let buttonTagCart = _sender.tag

            print("buttonTagCart",buttonTagCart)
            

            //CONVERT FROM NSDate to String
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let subscribeProductUrl =  NSURL(string:Constants.URL_REQUEST_USER_SUBSCRIBEPRODUCT){
                
                print("SubscribeProduct",subscribeProductUrl)
                var  request = URLRequest(url:subscribeProductUrl as URL)
                var subscribeProduct = SubscribeProductModel()
                subscribeProduct.productid = (self.productItems[buttonTagCart]?.productId)!
                subscribeProduct.userid = userId
               // subscribeProduct.menuId = (self.productItems[buttonTagCart]?.menuId)!

                let todayDate = Date()
                
                let firstDay = Calendar.current.date(byAdding: .day, value: 1, to: todayDate) //finding 30th day from today

                let newdateFormatter = DateFormatter()
                newdateFormatter.dateFormat = "yyyy-MM-dd"
                let startDateStringNew = newdateFormatter.string(from:firstDay!)
                
                print("startDateStringNew",startDateStringNew)
                
                let finalDay = Calendar.current.date(byAdding: .day, value: 29, to: firstDay!) //finding 30th day from today

                print("EndDateStringNew",finalDay ?? "Date Nil")

                let newEnddateFormatter = DateFormatter()
                newEnddateFormatter.dateFormat = "yyyy-MM-dd"
                let endDateStringNew = newdateFormatter.string(from:finalDay!)
                subscribeProduct.startdate = startDateStringNew
                subscribeProduct.enddate = endDateStringNew
                subscribeProduct.sunday = "1"
                subscribeProduct.monday = "1"
                subscribeProduct.tuesday = "1"
                subscribeProduct.wednesday = "1"
                subscribeProduct.thursday = "1"
                subscribeProduct.friday = "1"
                subscribeProduct.satday = "1"
                subscribeProduct.orderid = "0"
                subscribeProduct.menuId = (self.productItems[buttonTagCart]?.menuId)!
                guard let productId =  subscribeProduct.productid,let userId =  subscribeProduct.userid,let startDate = subscribeProduct.startdate,let endDate = subscribeProduct.enddate,let sunday = subscribeProduct.sunday,let monday = subscribeProduct.monday,let tuesday = subscribeProduct.tuesday ,let wednesday = subscribeProduct.wednesday,let thursday = subscribeProduct.thursday,let friday = subscribeProduct.friday ,let satday =  subscribeProduct.satday,let orderId = subscribeProduct.orderid, let menuId = subscribeProduct.menuId else{return}
                
                let subscribeProductParams  = ["productid":productId,"userid":userId,"startdate":startDate,"enddate":endDate,"monday":monday,"tuesday":tuesday,"wednesday":wednesday,"thursday":thursday,"friday":friday,"satday":satday,"orderid":orderId,"sunday": sunday,"menuid": menuId] as [String:Any]
                
                print("subscribeProductParams",subscribeProductParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: subscribeProductParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        let fetchSubscribeProductJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        print(fetchSubscribeProductJson)
                        if let subscribeProductResponse = fetchSubscribeProductJson as? [String:Any]{
                            print(subscribeProductResponse["description"]!)
                            print(subscribeProductResponse["message"]!)
                            if (subscribeProductResponse["code"] as! NSNumber == 200) {
                                
                                DispatchQueue.main.async {
                                    
                                    self.view.makeToast(subscribeProductResponse["description"]! as? String, duration: 3.0, position: .center)
                                    
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                                        self.perfromDelayToCart(_sender: _sender)
                                    }

                                }

                            }else{
                                
                                DispatchQueue.main.async {
                                    
                                    self.view.makeToast(subscribeProductResponse["description"]! as? String, duration: 3.0, position: .center)
                                }
                            }
                        }
                    }catch let jsonerror {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            
                        }
                        print(jsonerror.localizedDescription as Any)
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    
                }
            }
        }
        
        
        func perfromDelayToCart(_sender:UIButton) {
            
            let buttonTagCart = _sender.tag

            DispatchQueue.main.async{
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
              //  controller.newsProduct = "true"
              //  controller.paperProductPrice = (self.productItems[buttonTagCart]?.productPrice)!
                let cartNavigationController = UINavigationController(rootViewController: controller)
                self.present(cartNavigationController, animated: true, completion: nil)
                
            }
        }
        
        func productList(){
            
           
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if  let productUrl =  NSURL(string:Constants.URL_REQUEST_USER_PRODUCTSLIST) {
                var request = URLRequest(url:productUrl as URL)
                var products:Products = Products()
                products.apartmentId = UserDefaults.standard.value(forKey: "apartmentId") as? String
                products.userId = UserDefaults.standard.value(forKey: "id") as? String
                products.categoryId = strItemId
                guard let userId = products.userId,let userApartmentId = products.apartmentId,let userCategoryId = products.categoryId else{return}
                let productListParams = ["userid":userId,"apartmentid":userApartmentId,"categoryid":userCategoryId] as [String:AnyObject]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: productListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.productTableView.isHidden = false

                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        self.productItems = [ProductsList]()
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let fetchData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print("fetchData",fetchData)

                        
                        if let productResponse = fetchData["products_result"] as? [[String:AnyObject]]{
                            for productResult in productResponse {
                                let productsItem = ProductsList()
                                guard let productImage = productResult["productimage"] as? String , let productUnit = productResult["productunits"] as? String,let productprice =  productResult["productprice"] as? String,let deleiverycharge = productResult["deliverycharges"] as? String,let productTitle = productResult["title"] as? String,let productId = productResult["id"] as? String,let menuId = productResult["menu_id"] as? String, let sellingType = productResult["selling_type"] as? String else{
                                        
                                        return
                                }
                                
                                if let stockStatus = productResult["stock_status"] as? String {
                                    
                                    productsItem.stock_status = stockStatus

                                }else{
                                    productsItem.stock_status = ""
                                }
                                
                                if let actualPrice = productResult["actual_price"] as? String {
                                    productsItem.actual_price = actualPrice

                                }else{
                                    productsItem.actual_price = ""
                                }
                                
                                if let discounPercentage = productResult["discount_percentage"] as? String {
                                    productsItem.discount_percentage = discounPercentage

                                }else{
                                    productsItem.discount_percentage = ""
                                }
                                
                                if let discountedPrice = productResult["discounted_price"] as? Int {
                                    productsItem.discounted_price = discountedPrice

                                }else{
                                    productsItem.discounted_price = 0
                                }
                                
                                if let cartQty = productResult["cart_qty"] as? String {
                                    productsItem.cart_qty = cartQty
                                }else {
                                    productsItem.cart_qty = "0"

                                }
                                
                                if let cartidlo = productResult["cartid"] as? String {
                                    productsItem.cartid = cartidlo
                                }else{
                                    productsItem.cartid = "0"
                                }
                                
                                if let subscribedType = productResult["subscribed_type"] as? String {
                                    productsItem.subscribed_type = subscribedType
                                }else{
                                    productsItem.subscribed_type = "-1"
                                    
                                }
                                
                                
                                productsItem.selling_type = sellingType
                                productsItem.productImage = productImage
                                productsItem.deleieveryCharges = deleiverycharge
                                productsItem.productPrice = productprice
                                productsItem.productUnits = productUnit
                                productsItem.productTitle = productTitle
                                productsItem.productId = productId
                                productsItem.menuId = menuId
                                
                                if productsItem.stock_status == "0" {
                                    self.productItemsWithStatus.append(productsItem)
                                }else{
                                    self.productItems.append(productsItem)
                                }
                              //  print("stockStatus",productsItem.stock_status)

                                
                                print("self.productItems :",self.productItems)

                            }
                            DispatchQueue.main.async {
                                
                                
                                if let menudatails = fetchData["menu_details"] as? [String:AnyObject] {
                                    if  let menu = menudatails["menu_details"] as? [String:AnyObject] {
                                        
                                        self.productMainImage.setImage(from: (menu["banner_image"] as? String)!)
                                    }
                                }
                                
                                print("self.productItemsWithStatus",self.productItemsWithStatus)

                                self.productItems.append(contentsOf: self.productItemsWithStatus)

                                self.productTableView.reloadData()
                            }
                            
                            self.getCartCountValue(userId: userId)
                            self.paymentGatewayDetails()
                            
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)  }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)  }
            }
        }
        /* payment gate way details */
        
        func paymentGatewayDetails(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_PAYMENTGATEWAYDETAILS ) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    do {
                        
                        guard let paymentGatewayJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        print(paymentGatewayJson)
                        guard let cutOffTime = paymentGatewayJson["ios_order_cutoff_time"] as? String else{
                            return
                        }
                        
                        iosCuttOfftime = cutOffTime
                        
                    }  catch let jsonErr {
                        
                        print(jsonErr.localizedDescription)
                    }
                    
                })
                task.resume()
            }
        }
        
        
    }//class
    
    
    extension UIImageView{
        
        func productImageDownload(url:String){
            let urlRequest = URLRequest(url: URL(string:url)!)
            
            let task = URLSession.shared.dataTask(with: urlRequest) { (data, reponse, error) in
                
                guard let data = data else {
                    
                    return
                    
                }
                
                if error != nil{
                    
                    print(error!)
                    
                }
                
                DispatchQueue.main.async {
                    
                    self.image = UIImage(data: data)
                }
            }
            task.resume()
        }
    }
    
    extension UIImageView{
        
        func productMainImageDownload(url:String){
            let urlRequest = URLRequest(url: URL(string:url)!)
            
            let task = URLSession.shared.dataTask(with: urlRequest) { (data, reponse, error) in
                
                guard let data = data else {
                    
                    return
                    
                }
                
                if error != nil{
                    
                    print(error!)
                    
                }
                
                DispatchQueue.main.async {
                    
                    self.image = UIImage(data: data)
                    
                }
            }
            task.resume()
        }
    }

    
    class MyTapGesture: UITapGestureRecognizer {
        var tagNumber = Int()
    }
