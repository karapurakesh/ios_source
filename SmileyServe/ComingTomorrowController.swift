  //
  //  ComingTomorrowController.swift
  //  SmileyServe
  //
  //  Created by Apple on 15/08/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //

  import UIKit
  import SVProgressHUD

  class ComingTomorrowController: UIViewController {
  var tomorrowCartModel :[TomorrowCartResultResponseModel?] = []
  var userId = UserDefaults.standard.value(forKey: "id")
  @IBOutlet weak var comingTomorrowOrdersNotFound: UIView!
  @IBOutlet var comingTomorrowTableView : UITableView!
  override func viewDidLoad() {
  super.viewDidLoad()
  self.comingTomorrowTableView.separatorStyle = .none
  self.comingTomorrowOrdersNotFound.isHidden = true
  self.comingTomorrowTableView.isHidden = true
  if Reachability.isConnectedToNetwork(){
  self.getTomorrowCart(userId: userId as! String)
  }else{
  DispatchQueue.main.async {
   self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
    }
    
  }
    
    navigationBarAppearance()
    
  }
    
    func navigationBarAppearance(){
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        self.navigationController?.navigationBar.tintColor =  UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
        self.title = "Coming Tomorrow"
    }
    
    
    func handleNavigationArrow(){
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let signupNavigation = UINavigationController(rootViewController: signupController)
            self.present(signupNavigation, animated: false, completion: nil)
            
            
        }
    }
  @IBAction func handleComingTmrBarBtn(_ sender: Any) {
    DispatchQueue.main.async {
      let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
      let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
      let signupNavigation = UINavigationController(rootViewController: signupController)
      self.present(signupNavigation, animated: false, completion: nil)
      
    }
  }

  func getTomorrowCart(userId:String){
    DispatchQueue.main.async {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.show(withStatus: "please wait...")
    }

  if let tomorrowCartUrl = NSURL(string: Constants.URL_REQUEST_TOMORROWCART + userId) {
  var urlRequest = URLRequest(url: tomorrowCartUrl as URL)
  urlRequest.httpMethod = "GET"
  let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
    DispatchQueue.main.async {
        SVProgressHUD.dismiss()
    }

  if (error != nil) {
    DispatchQueue.main.async {
    self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
    }
    
    }
  if let response = response {
  print(response)
  }
  guard let data = data else {
  return
  }

  self.tomorrowCartModel = [TomorrowCartResultResponseModel]()
  do {

  guard let tomorrowCartJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
  print(tomorrowCartJsonObject)
  if tomorrowCartJsonObject["code"] as! NSNumber == 200 {
  if let tomorrowCartResult = tomorrowCartJsonObject["tomorrowcart_result"] as? [[String:AnyObject]] {
  for tomorrowCartStringResult in tomorrowCartResult{
  var tomorrowCartModelResult = TomorrowCartResultResponseModel()
  guard let tmrProductName = tomorrowCartStringResult["product_name"] as? String,let tmrProductImage = tomorrowCartStringResult["product_image"] as? String,let tmrProductUnits = tomorrowCartStringResult["product_units"] as? String,let tmrProductPrice = tomorrowCartStringResult["product_price"] as? String,let tmrProductQuantity = tomorrowCartStringResult["qty"] as? String,let tmrDelieveryCharges = tomorrowCartStringResult["delivery_charge"] as? String else {

  return
  }

  tomorrowCartModelResult.deleivery_charge = tmrDelieveryCharges
  tomorrowCartModelResult.product_image = tmrProductImage
  tomorrowCartModelResult.product_units = tmrProductUnits
  tomorrowCartModelResult.product_name = tmrProductName
  tomorrowCartModelResult.product_price = tmrProductPrice
  tomorrowCartModelResult.qty = tmrProductQuantity
  self.tomorrowCartModel.append(tomorrowCartModelResult)

  }
  if self.tomorrowCartModel.count > 0 {
  DispatchQueue.main.async {
  self.comingTomorrowTableView.reloadData()
  self.comingTomorrowTableView.isHidden = false
  }
  }
  }
  }else{

  DispatchQueue.main.async {
  self.comingTomorrowOrdersNotFound.isHidden = false
  }
  }
  }
  catch let jsonErr {
DispatchQueue.main.async {
    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
        
    }
  print(jsonErr.localizedDescription)
  }

  })
  task.resume()

  }else{
    DispatchQueue.main.async {
    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)

    }
    
  }
  }
  }//class

  /* creating the coming tomorrow cart tableview delegates */

  extension ComingTomorrowController:UITableViewDataSource{
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  return self.tomorrowCartModel.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  let cell = tableView.dequeueReusableCell(withIdentifier: "cmgTmrCell", for: indexPath) as! ComingTomorrowTableViewCell
  cell.isUserInteractionEnabled = false
  cell.selectionStyle = .none
  cell.cmgTomorrowProductName.text = self.tomorrowCartModel[indexPath.row]?.product_name
  cell.cmgTomorrowProductPrice.text = "Rs. " + (self.tomorrowCartModel[indexPath.row]?.product_price)! + "  D.C: Rs. " + (self.tomorrowCartModel[indexPath.row]?.deleivery_charge)!
  cell.cmgTomorrowProductUnits.text = self.tomorrowCartModel[indexPath.row]?.product_units
  cell.comingTomorrowProductImage.setImage(from: (self.tomorrowCartModel[indexPath.row]?.product_image)!)
  cell.cmgTomorrowProductQuantity.text = self.tomorrowCartModel[indexPath.row]?.qty
  return cell

  }

  }









