//
//  CountriesViewController.swift
//  
//
//  Created by mac on 27/11/17.
//
//

import UIKit



class MCountry: NSObject {
    let name: String
    var section: Int?
    
    init(name: String) {
        self.name = name
    }
}

struct MSection {
    var countries: [MCountry] = []
    
    mutating func addCountry(_ country: MCountry) {
        countries.append(country)
    }
}


@objc public protocol MCountryPickerDelegate: class {
    func countryPicker(_ picker: CountriesViewController, didSelectCountryWithName name: String)
}

open class CountriesViewController: UIViewController , UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource{

    var unsourtedCountries = [MCountry] ()
    
    fileprivate var filteredList = [MCountry]()
    
    @IBOutlet weak var tableView: UITableView!
    private var searchString:String = ""
    @IBOutlet weak var searchBar: UISearchBar!

    override open func viewDidLoad() {
        super.viewDidLoad()

        self.configureNavigationBar()
        
        
        /// Configure tableVieew
        tableView.sectionIndexTrackingBackgroundColor   = UIColor.clear
        tableView.sectionIndexBackgroundColor           = UIColor.clear
        tableView.keyboardDismissMode   = .onDrag

        
        /// Add delegates
        searchBar.delegate      = self
        tableView.dataSource    = self
        tableView.delegate      = self
        // Do any additional setup after loading the view.
        searchBar.showsCancelButton = false
    }
    
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchString = searchText
        print("searchString",filter(searchString))
        tableView.reloadData()
    }
    
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        filteredList.removeAll()
        tableView.reloadData()

    }

        
    fileprivate func searchForText(_ text: String) {
        
        print(filter(text))
        tableView.reloadData()
    }
    
    fileprivate func filter(_ searchText: String) -> [MCountry] {
        filteredList.removeAll()
        
        sections.forEach { (section) -> () in
            section.countries.forEach({ (country) -> () in
                if country.name.characters.count >= searchText.characters.count {
                    let result = country.name.compare(searchText, options: [.caseInsensitive, .diacriticInsensitive], range: searchText.characters.startIndex ..< searchText.characters.endIndex)
                    if result == .orderedSame {
                        filteredList.append(country)
                    }
                }
            })
        }
        
        return filteredList
    }
    //MARK: UItableViewDelegate,UItableViewDataSource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        if (searchBar.text?.characters.count)! > 0 {
            return 1
        }
        return sections.count

    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if (searchBar.text?.characters.count)! > 0 {
            return filteredList.count
        }
        return sections[section].countries.count
    }
    
    public  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        /// Obtain a cell
        let cell: UITableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else {
                return UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "Cell")
            }
            return cell
        }()
        

        let country: MCountry!
        if searchBar.text!.characters.count > 0 {
            country = filteredList[(indexPath as NSIndexPath).row]
        } else {
            country = sections[(indexPath as NSIndexPath).section].countries[(indexPath as NSIndexPath).row]
            
        }
        
        cell.textLabel?.text = country.name


        return cell
    }
    
    open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !sections[section].countries.isEmpty {
            return self.collation.sectionTitles[section] as String
        }
        return ""
    }
    
    
    
    public func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return collation.sectionIndexTitles
    }
    
    open func tableView(_ tableView: UITableView,
                                 sectionForSectionIndexTitle title: String,
                                 at index: Int)
        -> Int {
            return collation.section(forSectionIndexTitle: index)
    }
    

    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    open weak var delegate: MCountryPickerDelegate?
    open var didSelectCountryClosure: ((String) -> ())?

    convenience public init(completionHandler: @escaping ((String) -> ())) {
        self.init()
        self.didSelectCountryClosure = completionHandler
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar.resignFirstResponder()
        
        let country: MCountry!
        if searchBar.text!.characters.count > 0 {
            country = filteredList[(indexPath as NSIndexPath).row]
        } else {
            country = sections[(indexPath as NSIndexPath).section].countries[(indexPath as NSIndexPath).row]
            
        }
        //print("SECTION", indexPath.row)
        delegate?.countryPicker(self, didSelectCountryWithName: country.name)
        didSelectCountryClosure?(country.name)

    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(toggleBack))

    }
    
    
    private func configureNavigationBar() {
        var naviBarFrame = self.navigationController?.navigationBar.frame
        naviBarFrame?.size.height = 50
        let rect = naviBarFrame ?? CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: 50)
        self.navigationController?.navigationBar.setValue(rect, forKey: "frame")
    }
    
    
    
    @objc public func toggleBack() {
        
        self.navigationController!.popViewController(animated: true)
    }
    
    fileprivate var _sections: [MSection]?
    fileprivate let collation = UILocalizedIndexedCollation.current()
        as UILocalizedIndexedCollation
    fileprivate var sections: [MSection] {
        
        if _sections != nil {
            return _sections!
        }
        
        let countries: [MCountry] = unsourtedCountries.map { country in
            let country = MCountry(name: country.name)
            country.section = collation.section(for: country, collationStringSelector: #selector(getter: MCountry.name))
            return country
        }
        
        // create empty sections
        var sections = [MSection]()
        for _ in 0..<self.collation.sectionIndexTitles.count {
            sections.append(MSection())
        }
        
        // put each country in a section
        for country in countries {
            sections[country.section!].addCountry(country)
        }
        
        // sort each section
        for section in sections {
            var s = section
            s.countries = collation.sortedArray(from: section.countries, collationStringSelector: #selector(getter: MCountry.name)) as! [MCountry]
        }
        
        _sections = sections
        
        return _sections!
    }
}


