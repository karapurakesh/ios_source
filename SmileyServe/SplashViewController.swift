  //
  //  SplashViewController.swift
  //  SmileyServe
  //
  //  Created by Apple on 24/07/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //
  
  import UIKit
  import SwiftSpinner
  import KDLoadingView
  import Shimmer
  class SplashViewController: UIViewController {
    @IBOutlet weak var loadingView: KDLoadingView!
    @IBOutlet var retryRefButton : UIButton!
    var strUserApiKey : String?
    var strUserMobile : String?
    var srtUserName   : String?
    @IBOutlet weak var noInternetConnectionView : FBShimmeringView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.startAnimating()
        retryRefButton.isHidden = true
        perform(#selector(handleSplash), with: nil, afterDelay: 2)
        self.noInternetConnectionView.isHidden = true
        strUserApiKey  =   UserDefaults.standard.value(forKey:"id") as? String
        strUserMobile  =   UserDefaults.standard.value(forKey:"mobile") as? String
        srtUserName    =   UserDefaults.standard.value(forKey:"name") as? String
    }
    override func viewDidAppear(_ animated: Bool) {
        self.shimmer()
        loadingView.startAnimating()
    }
    
    func shimmer(){
        let label = UILabel(frame: self.noInternetConnectionView.bounds)
        label.textAlignment = .center
        label.text = "No Internet Connection"
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.setSizeFont(sizeFont: 22)
        label.textColor = UIColor.init(netHex: 0x00C9FF)
        self.noInternetConnectionView.contentView = label
        self.noInternetConnectionView.isShimmering = true
    }
    
    func handleSplash() {
        loadingView.startAnimating()
        if Reachability.isConnectedToNetwork(){
            loadingView.startAnimating()
            print("Internet Connection Available!")
            if strUserApiKey == nil{
                //  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //  let signupController = storyBoard.instantiateViewController(withIdentifier: "welcome") as! InitialViewController
                //  self.present(signupController, animated: true, completion: nil)
                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                self.present(signInController, animated: false, completion: nil)
                
            }else if strUserMobile == nil ||  srtUserName == nil {
                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let updateProfile = storyBoard.instantiateViewController(withIdentifier: "update") as! UpdateProfileController
                let updateProfileNavigation = UINavigationController(rootViewController: updateProfile)
                self.present(updateProfileNavigation, animated: false, completion: nil)
                
            } else {
                DispatchQueue.main.async {
                    //    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    //    let forceUpdateController = storyBoard.instantiateViewController(withIdentifier: "ForceUpdateViewController") as! ForceUpdateViewController
                    //    self.present(forceUpdateController, animated: true, completion: nil)
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    let signupNavigation = UINavigationController(rootViewController: signupController)
                    self.present(signupNavigation, animated: true, completion: nil)
                }
                
            }
        }
        else{
            retryRefButton.isHidden = false
            loadingView.stopAnimating()
            self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
            noInternetConnectionView.isHidden = false
        }
    }
    
    @IBAction func retryBtnAction(_sender:Any){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupController = storyBoard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
        self.present(signupController, animated: false, completion: nil)
    }
    
  }//class
  
  extension Date {
    var yes: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
    var tmr: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var mon: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tmr.month != month
    }
  }
  /* Extension for incresing the font size for UILaebl */
  extension UILabel {
    func setSizeFont (sizeFont: Double) {
        self.font =
            UIFont(name: self.font.fontName, size: CGFloat(sizeFont))!
        self.sizeToFit()
    }
  }
  
