//
//  CalendarProductsModel.swift
//  SmileyServe
//
//  Created by Apple on 09/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct CalendarProductsModel {
    
    var userid : String?
    var date   : String?
    var cartid  : String?
    var productid : String?
    var product_price : String?
    var product_name : String?
    var product_image : String?
    var product_units : String?
    var quantity  : String?
    var product_orgprice : String?
    var  delivery_charge : String?
    var orderDate  : String?
    var menu_id : String?
    
    

    init() {
        //
    }
    
    
    init(calendar:CalendarProductsModel) {
        
        self.userid = calendar.userid
        self.date   = calendar.date
        self.cartid = calendar.cartid
        self.productid = calendar.productid
        self.product_price = calendar.product_price
        self.product_name = calendar.product_name
        self.product_image = calendar.product_image
        self.product_units = calendar.product_units
        self.quantity   = calendar.quantity
        self.product_orgprice = calendar.product_orgprice
        self.delivery_charge = calendar.delivery_charge
        self.orderDate = calendar.orderDate
        
    }
    
    
}
