  //
  //  SignUpController.swift
  //  SmileyServe
  //
  //  Created by Apple on 04/07/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //
  
  import UIKit
  import Font_Awesome_Swift
  import SVProgressHUD
  
  class SignUpController: UIViewController,UITextFieldDelegate,BEMCheckBoxDelegate {
    
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet var termsSelectionCheckBox: BEMCheckBox!
    var bemCheckBox : BEMCheckBox!
    @IBOutlet var signUpConfirmPwdTextField: UITextField!
    @IBOutlet var signUpPasswordTextField: UITextField!
    @IBOutlet var signUpMobileTextField: UITextField!
    @IBOutlet var signUpEmailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        signUpBtn.layer.cornerRadius = 5
        //        navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named:"backArrow.png"), style: .plain, target: self, action: #selector(handleNavigationItem))
        //                navigationItem.title = "SignUp Page"
        //        navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
        //        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        //        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        signUpEmailTextField.delegate = self
        signUpMobileTextField.delegate = self
        signUpPasswordTextField.delegate = self
        signUpConfirmPwdTextField.delegate = self
        termsSelectionCheckBox.delegate = self
        signUpConfirmPwdTextField.signupTextFieldUnderlined()
        signUpMobileTextField.signupTextFieldUnderlined()
        signUpEmailTextField.signupTextFieldUnderlined()
        signUpPasswordTextField.signupTextFieldUnderlined()
        
    }
    
    //     func handleNavigationItem(){
    //
    //     self.dismiss(animated: true, completion: nil)
    //
    //    }
    
    
    
    func didTap(_ checkBox: BEMCheckBox) {
        bemCheckBox = checkBox
        //print("check box :\(checkBox.tag):\(checkBox.on)")
    }
    
    @IBAction func signInLoginButton(_ sender: Any) {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
        self.present(signInController, animated: false, completion: nil)

    }
    
    
    @IBAction func signUpButton(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            self.signUp()
        }else{
            DispatchQueue.main.async {
                self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
            }
        }
    }
    
    func signUp(){
        let providedEmailAddress = signUpEmailTextField.text
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        if signUpEmailTextField.text == "" || signUpMobileTextField.text == "" || signUpPasswordTextField.text == "" || signUpConfirmPwdTextField.text == "" || !(self.bemCheckBox != nil) {
            DispatchQueue.main.async {
                self.view.makeToast("please fill all fields are mandatory", duration: 3.0, position: .center)
                
            }
        }
        else if signUpPasswordTextField.text != signUpConfirmPwdTextField.text{
            self.view.makeToast("passwords does not match", duration: 3.0, position: .center)
            
            
        }else if (signUpEmailTextField.text?.isEmpty)!{
            self.view.makeToast("enter email", duration: 3.0, position: .center)
            
        }else if (signUpMobileTextField.text?.isEmpty)!{
            self.view.makeToast("please fill mobile number", duration: 3.0, position: .center)
            
        }else if (signUpPasswordTextField.text?.isEmpty)! && (signUpConfirmPwdTextField.text?.isEmpty)!{
            self.view.makeToast("please fill the passwords", duration: 3.0, position: .center)
        }else if !isEmailAddressValid
        {
            
            self.view.makeToast("email address is not valid", duration: 3.0, position: .center)
        }
        else if !((signUpMobileTextField.text?.isPhone())!){
            self.view.makeToast("phone number is not valid", duration: 3.0, position: .center)
            
        }else if !(signUpPasswordTextField.text?.isValidPassword)!{
            
            self.view.makeToast("passwords should be minimum six characters", duration: 3.0, position: .center)
        }else if (signUpMobileTextField.text?.isBlank)!{
            
            self.view.makeToast("Try to fill the space in mobiletextfield", duration: 3.0, position: .center)
        }else if (signUpEmailTextField.text?.isBlank)!{
            
            self.view.makeToast("Try to fill the space in emailtextfield", duration: 3.0, position: .center)
            
        }else if (self.bemCheckBox.on != true){
            
            self.view.makeToast("Please Accept Terms & Conditions", duration: 3.0, position: .center)
            
        }
        else{
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            var user:Struct_User = Struct_User()
            user.userEmail = signUpEmailTextField.text
            UserDefaults.standard.set(user.userEmail, forKey: "email")
            user.userMobile = signUpMobileTextField.text
            UserDefaults.standard.set(user.userMobile, forKey: "mobile")
            user.userPassword = signUpPasswordTextField.text
            user.userConfirmPassword = signUpConfirmPwdTextField.text
            if let signupURL =  NSURL(string:Constants.URL_REQUEST_USER_REGISTER) {
                var  request = URLRequest(url:signupURL as URL)
                guard let userEmail =  user.userEmail,let userMobile = user.userMobile,let userPassword =  user.userPassword,let userConfirmPassword =  user.userConfirmPassword else {return}
                let  signupParams = ["useremail":userEmail,"userpassword":userPassword,"usermobile":userMobile] as [String:Any]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: signupParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        
                    }
                    
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do {
                        
                        let fetchData = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
                        print(fetchData)
                        if let registerResponse = fetchData as? [String:Any] {
                            print(registerResponse["message"]!)
                            print(registerResponse["description"]!)
                            DispatchQueue.main.async {
                                if (registerResponse["code"] as! NSNumber == 200) {
                                    DispatchQueue.main.async {
                                        
                                        self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
                                        UserDefaults.standard.set(registerResponse["description"]! as? String, forKey: "otpsent")
                                        self.handleToOptScreen()
                                    }
                                }
                                else {
                                    DispatchQueue.main.async {
                                        self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
                                    }
                                }
                            }
                        }
                    }
                        
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                            print(jsonErr.localizedDescription)
                        }
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                }
                
            }
        }
    }
    
    func handleToOptScreen(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let otpcontroller = storyBoard.instantiateViewController(withIdentifier: "otp") as! OtpViewController
        //  let otpNavigationController = UINavigationController(rootViewController: otpcontroller)
        self.present(otpcontroller, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == signUpEmailTextField{
            signUpMobileTextField.becomeFirstResponder()
        }else if textField == signUpMobileTextField{
            
            signUpPasswordTextField.becomeFirstResponder()
            
        }else if textField == signUpPasswordTextField{
            
            signUpConfirmPwdTextField.becomeFirstResponder()
        }else{
            
            
            textField.resignFirstResponder()
        }
        
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func displayAlertMessage(messageToDisplay: String)
    {
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            print("Ok button tapped");
            
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    func isPhoneNumberValid(phoneAddressString: String) -> Bool {
        
        var isPhoneNumber: Bool {
            do {
                let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
                let matches = detector.matches(in: phoneAddressString, options: [], range: NSMakeRange(0, phoneAddressString.characters.count))
                if let res = matches.first {
                    return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == phoneAddressString.characters.count
                } else {
                    return false
                }
            } catch {
                return false
            }
        }
        return true
    }
    
    
  }//class
  
  /* creating an extension for the underlining the textfields */
  
  extension UITextField {
    func signupTextFieldUnderlined(){
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
  }
  
  extension String {
    
    //To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    //Validate Email
    
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    //validate Password
    var isValidPassword: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
            if(regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil){
                
                if(self.characters.count>=6 && self.characters.count<=20){
                    return true
                }else{
                    return false
                }
            }else{
                return false
            }
        } catch {
            return false
        }
    }
    
    //Phone Validator
    
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.characters.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.characters.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
  }
  
  extension String {
    
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "^[6-9][0-9]{9}$"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    private func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
