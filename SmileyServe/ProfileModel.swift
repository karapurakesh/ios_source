//
//  Profile.swift
//  SmileyServe
//
//  Created by Apple on 27/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct Profile {
    
    
    var id : String?
    var name : String?
    var mobile : String?
    var email : String?
    var address : String?
    var area : String?
    var state : String?
    var city : String?
    var pincode : String?
    var status : String?
    var apartment_id : String?
    var apartment : String?
    var plotno : String?
    var block : String?
    var block_id : String?
    
    init() {
        
        //
    }
    
    
    init(profile:Profile) {
         self.id     = profile.id
         self.name   = profile.name
         self.email   = profile.email
         self.mobile  = profile.mobile
         self.state = profile.state
         self.city = profile.city
         self.address = profile.address
         self.pincode = profile.pincode
         self.status = profile.status
         self.apartment = profile.apartment
         self.apartment = profile.apartment

        self.apartment_id = profile.apartment_id
        self.block        = profile.block
        self.block_id = profile.block_id
        self.plotno    = profile.plotno
        self.area = profile.area


        
        
        
        
    }
    
    
}
