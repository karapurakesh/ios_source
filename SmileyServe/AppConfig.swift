    //
    //  AppConfig.swift
    //  SmileyServe
    //
    //  Created by Apple on 06/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    
    import Foundation
    class Constants {
        //static let BASE_URL = "http://192.168.1.14/app/api/"
        
        // static let BASE_URL   = "http://smiley.inventbird.in/api/"
        
        static let BASE_URL   = "http://smileyserve.com/api/"
       // static let BASE_URL   = "http://smileyserve.com/beta/api/"
        
        static let URL_REQUEST_INSTANT_ORDER = BASE_URL + "instantCart"
        static let URL_REQUEST_INSTANT_CHANGE_QNTY = BASE_URL + "incDec"

        static let URL_REQUEST_USER_REGISTER = BASE_URL + "signup"
        
        static let URL_REQUEST_USER_LOGIN = BASE_URL + "login"
        
        static let URL_REQUEST_USER_FORGOTPASSWORD = BASE_URL + "forgotpassword"
        
        static let URL_REQUEST_USER_USERVERIFICATION = BASE_URL + "userverification"
        
        static let URL_REQUEST_USER_UPDATEPROFILE = BASE_URL + "updateprofile"
        
        static let URL_REQUEST_USER_UPDATEPASSWORD = BASE_URL + "resetpassword"
        
        static let URL_REQUEST_USER_FORGOTVERIFICATION = BASE_URL + "checkforgotverification"
        
        static let URL_REQUEST_USER_RESENDSIGNUPOTP = BASE_URL + "resendsignupotp"
        
        static let URL_REQUEST_USER_FAQ = BASE_URL + "faq"
        
        static let URL_REQUEST_USER_SLIDER = BASE_URL + "slider"
        
        static let URL_REQUEST_USER_MENULIST = BASE_URL + "menulist"
        
        static let  URL_REQUEST_USER_SMILEYCASH = BASE_URL + "smileycash"
        
        static let URL_REQUEST_USER_PRODUCTSLIST = BASE_URL + "products"
        
        static let URL_REQUEST_USER_USERDATA = BASE_URL + "profile/"
        
        static let URL_REQUEST_USER_BLOCKLISTDATA = BASE_URL + "getBlocksList/"
        
        static let URL_REQUEST_USER_CONTACTDETAILS = BASE_URL + "contactdetails"
        
        static let URL_REQUEST_USER_CONTACTSUBJECTLIST = BASE_URL + "contactsubjectlist"
        
        static let URL_REQUEST_USER_ADDCONTACTUS = BASE_URL + "addcontactus"
        
        static let URL_REQUEST_USER_NOTIFICATIONLIST = BASE_URL + "notificationslist"
        
        static let URL_REQUEST_USER_CALENDARPRODUCTS = BASE_URL + "calenderproducts"
        
        static let URL_REQUEST_USER_CALENDAR = BASE_URL + "calender/"
        
        static let URL_REQUEST_USER_CARTCOUNT = BASE_URL + "cartcount/"
        
        static let URL_REQUEST_USER_FCMTOKEN = BASE_URL + "updateFcmToken"
        
        static let URL_REQUEST_USER_CHECKVERSIONCODE = BASE_URL + "checkVersionCode"
        
        static let URL_REQUEST_USER_SUBSCRIBEPRODUCT = BASE_URL + "subscribeproduct"
        
        static let URL_REQUEST_USER_CARTLIST = BASE_URL + "cartlist/"
        
        static let URL_REQUEST_USER_CARTDELETE = BASE_URL + "cartdelete"
        
        static let URL_REQUEST_USER_CLEARCART = BASE_URL + "clearcart"

        static let URL_REQUEST_USER_SUBSCRIPTION = BASE_URL + "subscription/"
        
        static let URL_REQUEST_USER_TOMORROWCART = BASE_URL + "tommorowcart/"
        
        static let URL_REQUEST_USER_CARTPROPERTIES = BASE_URL + "cartproperties/"
        
        static let URL_REQUEST_USER_CREATEORDER = BASE_URL + "createorder"
        
        static let URL_REQUEST_USER_CANCELDAYORDER = BASE_URL + "canceldayorder"
        
        static let URL_REQUEST_USER_CANCELPRODUCT = BASE_URL + "cancelproduct"
        
        static let URL_REQUEST_USER_EDITSUBSCRIPTION = BASE_URL + "editsubscription"
        
        static let URL_REQUEST_USER_ENDSUBSCRIPTION = BASE_URL + "endsubscription"
        
        static let URL_REQUEST_USER_ORDERS = BASE_URL + "orders"
        
        static let URL_REQUEST_USER_SUBSCRIPTIONSUCCESS = BASE_URL + "subscriptionsuccess"
        
        static let URL_REQUEST_USER_CARTCHECKOUT = BASE_URL + "cartcheckout"
        
        static let URL_REQUEST_USER_CARTCHECKOUTPROPERTIES = BASE_URL + "cartcheckoutproperties"
        
        static let URL_REQUEST_USER_ORDERCHECKOUTPAYMENTSUCCESS = BASE_URL + "ordercheckoutpaymentsuccess"
        
        static let URL_REQUEST_USER_CARTCHECKOUTDELETE = BASE_URL + "cartcheckoutdelete"
        
        static let URL_REQUEST_USER_CARTCHECKOUTDELETEALL = BASE_URL + "cartcheckoutdeleteall"
        
        static let URL_REQUEST_USER_ORDERPAYMENTSUCCESS = BASE_URL + "orderpaymentsuccess/"
        
        static let URL_REQUEST_USER_ORDERPAYMENTFAIL = BASE_URL + "orderpaymentfail/"
        
        static let URL_REQUEST_USER_PAYMENTGATEWAYDETAILS = BASE_URL + "paymentgatewaydetails"
        
        static let URL_REQUEST_UPDATESUBSCRIPTION  = BASE_URL + "updatesubscription"
        
        static let URL_REQUEST_TOMORROWCART  = BASE_URL + "tommorowcart/"
        
        static let URL_REQUEST_UPDATECART  = BASE_URL + "updatecart"
        
        static let URL_REQUEST_SUBSCRIPTIONSUCCESS  = BASE_URL + "subscriptionsuccess"
        
        static let URL_REQUEST_PRODUCTDETAILS  = BASE_URL + "productdetails"
        
        static let URL_REQUEST_ADD_MONEY = BASE_URL + "addMoney"
        
        
    }
