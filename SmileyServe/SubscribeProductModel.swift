//
//  SubscribeProductModel.swift
//  SmileyServe
//
//  Created by Apple on 21/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct SubscribeProductModel {
    
    
    var productid : String?
    var userid : String?
    var startdate : String?
    var enddate : String?
    var sunday : String?
    var monday : String?
    var tuesday : String?
    var wednesday : String?
    var thursday : String?
    var friday  : String?
    var satday : String?
    var orderid : String?
    var menuId : String?
    
    
}

struct InstantProductModel {
    
    
    var productid : String?
    var userid : String?
    var quantity : String?
    var menuId : String?
    
    
}
