//
//  SignOutViewController.swift
//  SmileyServe
//
//  Created by Apple on 06/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class SignOutViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       // self.addSlideMenuButton()
        self.title = "SignOut"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_navigationarrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
    }
    func handleNavigationArrow(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        let signupNavigation = UINavigationController(rootViewController: signupController)
        self.present(signupNavigation, animated: false, completion: nil)
    }

  }
