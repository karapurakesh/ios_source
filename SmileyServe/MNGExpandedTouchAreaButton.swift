//
//  MNGExpandedTouchAreaButton.swift
//  SmileyServe
//
//  Created by Apple on 06/10/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class MNGExpandedTouchAreaButton: UIButton {
  
//  @IBInspectable var margin:CGFloat = 20.0
  override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
    //increase touch area for control in all directions by 20
    
// let area = CGRectInset(self.bounds, -margin, -margin)
//
//    return area.contains(point)
        var relativeFrame = self.bounds
        var hitTestEdgeInsets = UIEdgeInsetsMake(-44, -44, -44, -44)
        var hitFrame = UIEdgeInsetsInsetRect(relativeFrame, hitTestEdgeInsets)
        return hitFrame.contains(point)
  }
  
}

