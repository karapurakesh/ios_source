    //
    //  OtpViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 06/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //

    import UIKit
    import Font_Awesome_Swift
    import SVProgressHUD
    class OtpViewController: UIViewController,UITextFieldDelegate {
        
        @IBOutlet weak var verifyBtn: UIButton!
        @IBOutlet weak var resendOtpBtn: UIButton!
    @IBOutlet var otpTimer: UILabel!
    @IBOutlet var otpVerifyTextField: UITextField!
    var timer = Timer()
    var time = 30
    var resendTimer = 30
    override func viewDidLoad() {
    super.viewDidLoad()
        verifyBtn.layer.cornerRadius = 5
    //        underlineTextField()
    //        otpVerifyTextField.setLeftViewFAIcon(icon: .FAEnvelope, leftViewMode: .always, textColor: .blue, backgroundColor: .clear, size: nil)
     timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(OtpViewController.decreaseTimer), userInfo: nil, repeats: true)
    otpVerifyTextField.delegate = self
    otpVerifyTextField.otpUnderlined()
    navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigation))
    UINavigationBar.appearance().barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
    UINavigationBar.appearance().tintColor = UIColor.white
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    self.displayToast()

    }
    
    func displayToast(){
    DispatchQueue.main.async {
    self.view.makeToast(UserDefaults.standard.value(forKey: "otpsent")
                as? String, duration: 3.0, position: .center)
        }
        }
        
    func handleNavigation() {
    self.dismiss(animated: true, completion: nil)

    }
    func decreaseTimer(){
    if time > 0{
    time = time - 1
    if time == 0 {
    otpTimer.isHidden = true
        resendOtpBtn.isEnabled = true
        resendOtpBtn.alpha = 1.0
    }else{
    otpTimer.isHidden = false
        resendOtpBtn.isEnabled = false
        resendOtpBtn.alpha = 0.5
    otpTimer.text = "Please Wait.." + String(time)
    }
    }
    }

    @IBAction func verifyOtpButton(_ sender: Any) {
    if Reachability.isConnectedToNetwork(){
    self.otpController()
    }else{
    DispatchQueue.main.async {
    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
    }
    }
    }

    func otpController(){
    var otp:USerOtpValidation = USerOtpValidation()
    otp.mobile = otpVerifyTextField.text
    if (otp.mobile?.isEmpty)! {
    DispatchQueue.main.async {
    self.view.makeToast("please fill otp", duration: 3.0, position: .center)
    }
    }
    else {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
   
    if  let signupURL =  NSURL(string:Constants.URL_REQUEST_USER_USERVERIFICATION) {
    var  request = URLRequest(url:signupURL as URL)
    guard let otp = otp.mobile else {return}
    let  otpParams:[String:Any] = ["otpcode":otp]
    request.httpMethod = "POST"
    do {
    let  json = try JSONSerialization.data(withJSONObject: otpParams)
    request.httpBody = json
    }
    catch let jsonErr  {
    print(jsonErr.localizedDescription)
    }

    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    guard let data = data, error == nil else

    {
    DispatchQueue.main.async {
    self.view.makeToast("Unable to connect server", duration: 3.0, position: .center)
        }
    print(error?.localizedDescription as Any )
    return
    }

    do{
    let fetchData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    print(fetchData)
    if let registerResponse = fetchData as? [String:Any]{
    print(registerResponse["description"]!)
    print(registerResponse["message"]!)
    print(registerResponse["user_result"]!)
    if let strUserData = registerResponse["user_result"]! as? [String:Any] {
    DispatchQueue.main.async {
    if (registerResponse["code"] as! NSNumber == 200){
    UserDefaults.standard.set(strUserData["id"], forKey: "id")
    UserDefaults.standard.set(strUserData["email"], forKey: "email")
    UserDefaults.standard.set(strUserData["mobile"], forKey: "mobile")
//    UserDefaults.standard.set(strUserData["name"], forKey: "name")
    DispatchQueue.main.async {
    self.handleUpdateProfileController()
    }
    }
    else {
    DispatchQueue.main.async {
    self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
    }
    }
    }
    }
    }
    }
    catch let jsonErr{
    DispatchQueue.main.async {
   self.view.makeToast("unable to fecth your data", duration: 3.0, position: .center)
    print(jsonErr.localizedDescription)
    }
    }
    }
    task.resume()
    }else{
    DispatchQueue.main.async {
    DispatchQueue.main.async {
    self.view.makeToast("unable to fecth your data", duration: 3.0, position: .center)

    }
    }
    }
    }
    }

    func handleUpdateProfileController(){
    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let signInController = storyBoard.instantiateViewController(withIdentifier: "update") as! UpdateProfileController
    let signInnavigationcontroller = UINavigationController(rootViewController: signInController)
    self.present(signInnavigationcontroller, animated: true, completion: nil)

    }
     
    func resendOtpDecreaseTimer(){
    if resendTimer > 0{
            resendTimer = resendTimer - 1
            if resendTimer == 0{
                otpTimer.isHidden = true
            }else{
                otpTimer.isHidden = false
                otpTimer.text = "Please Wait.." + String(resendTimer)
    }
    }
    }
    @IBAction func resendOtpButton(_ sender: Any) {
    if Reachability.isConnectedToNetwork(){
    timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(OtpViewController.resendOtpDecreaseTimer), userInfo: nil, repeats: true)
    self.resendOtp()
    }else{
    DispatchQueue.main.async {
    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)

    }
    }
    }

    func resendOtp(){
    var resendSignupOtp:ResendOtp = ResendOtp()
    if let userMobile = UserDefaults.standard.value(forKey: "mobile") as? String {
    resendSignupOtp.resendOtp = userMobile
    if (resendSignupOtp.resendOtp?.isEmpty)!{
    self.view.makeToast("no valid number", duration: 3.0, position: .center)

    } else{
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
  
    if let resendOtp =  NSURL(string:Constants.URL_REQUEST_USER_RESENDSIGNUPOTP){
    var  request = URLRequest(url:resendOtp as URL)
    guard  let mobile =   resendSignupOtp.resendOtp else{return}
    let resendOtpParams:[String:Any] = ["mobile":mobile]
    request.httpMethod = "POST"
    do {
    let  json = try JSONSerialization.data(withJSONObject: resendOtpParams)
    request.httpBody = json
    }
    catch let jsonErr  {
    print(jsonErr.localizedDescription)
    }
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        DispatchQueue.main.async {
      SVProgressHUD.dismiss()
        }
    guard let data = data, error == nil else

    {
    print(error?.localizedDescription as Any )
    return
    }
    do{
    let fetchData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    if let registerResponse = fetchData as? [String:Any]{
    print(registerResponse["description"]!)
    print(registerResponse["message"]!)
    if (registerResponse["code"] as! NSNumber == 200){
    DispatchQueue.main.async {
   self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
    }

    }
    else {

    DispatchQueue.main.async {
    self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
    }
    }
    }
    }catch let jsonErr{
 self.view.makeToast("unable to fecth your data", duration: 3.0, position: .center)
    print(jsonErr.localizedDescription)
    }
    }
    task.resume()
    }else{

    self.view.makeToast("unable to fecth your data", duration: 3.0, position: .center)

    }
    }
    }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.view.endEditing(true)
    }
        
        
    @IBAction func handleBackBtnNavigation(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        }
        

    }//class

    extension UITextField {
    func otpUnderlined() {
    let border = CALayer()
    let width = CGFloat(1.0)
    border.borderColor = UIColor.black.cgColor
    border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
    border.borderWidth = width
    self.layer.addSublayer(border)
    self.layer.masksToBounds = true
    }
    }

    /*


    func underlineTextField(){


    let border = CALayer()

    let width = CGFloat(2.0)

    border.frame = CGRect(x: 0, y: otpVerifyTextField.frame.height - width, width: otpVerifyTextField.frame.width, height: otpVerifyTextField.frame.height)

    border.borderWidth = width

    border.borderColor = UIColor.black.cgColor

    otpVerifyTextField.layer.addSublayer(border)

    otpVerifyTextField.layer.masksToBounds = true


    }




    */


