//
//  AboutUsViewController.swift
//  SmileyServe
//
//  Created by rakesh karapu on 16/04/18.
//  Copyright © 2018 smileyserve. All rights reserved.
//

import UIKit
import SafariServices

class AboutUsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var versionNo: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        
        versionNo.text = appVersion
        
        // Do any additional setup after loading the view.
        
        self.tableView.tableFooterView = UIView()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationUPdateProfile))
        
        self.title = "About Us"
        
        shareBtn.layer.cornerRadius = 5
        shareBtn.layer.borderColor =  UIColor.white.cgColor
        shareBtn.layer.borderWidth = 1
        
    }
    
    @IBAction func shareAction(_ sender: Any) {
        let mutableMultiLineString = """
                             Hi there,I made *SmileyServe* as my trusted daily needs partner.They deliver fresh Vegetables,Fruits,Flowers,Breads,Milk,Pooja Needs,Groceries and other daily needs at our doorstep every morning before 8 am.

                             You can try app using below link

                             http://onelink.to/sserve

                             The best part I liked is the product quality and No minimum order feature.

                             Why to store when you can get fresh everyday.
                             """
        
        let vc = UIActivityViewController(activityItems: [mutableMultiLineString], applicationActivities: [])
        vc.completionWithItemsHandler = {(activityType: UIActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            if !completed {
                // User canceled
                return
            }
            // User completed activity
            
            print("completed")
        }
        present(vc, animated: true)
        
    }
    func handleNavigationUPdateProfile() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        let signupNavigation = UINavigationController(rootViewController: signupController)
        self.present(signupNavigation, animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }else {
            return 75
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
        }else if indexPath.row == 1 {
            print("TERMS AND CONDITIONS")
                        
            if let url = URL(string: "http://smileyserve.com/termsandconditions") {
                
                let svc = SFSafariViewController(url: url)
                self.present(svc, animated: true, completion: nil)

            }
            
            
        }else {
            print("PRIVACY POLICY")
            
            if let url = URL(string: "http://smileyserve.com/privacypolicy") {
                
                let svc = SFSafariViewController(url: url)
                self.present(svc, animated: true, completion: nil)
                
            }
        }
        
        if let selectionIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectionIndexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Bundle.main.loadNibNamed("TeamsTableViewCell", owner: self, options: nil)?.first as! TeamsTableViewCell
        
        if indexPath.row == 0 {
            
            cell.nameLbl.font = UIFont.systemFont(ofSize: 18)

            cell.setData("SmileyServe is a subscription based e-commerce app-only platform. Common person daily needs like Milk, Curd, Water, Newspaper etc are delivered everyday. Very clear intention is that to provide better, simple and track able service to customers.")
            
        }else if indexPath.row == 1 {
            
            cell.nameLbl.font = UIFont.systemFont(ofSize: 20)

            cell.setData("TERMS AND CONDITIONS")
            
        }else {
            cell.nameLbl.font = UIFont.systemFont(ofSize: 20)

            cell.setData("PRIVACY POLICY")
            
        }
        
        return cell
        
    }
    
}
