    //
    //  UpdateProfile.swift
    //  SmileyServe
    //
    //  Created by Apple on 10/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //

    import Foundation

    struct UpdateProfile {

    var userid     : String?
    var username   : String?
    var usermobile : String?
    var userEmail  : String?
    var plotno     : String?
    var apartment  : String?
    var block      : String?
    var blockid    : String?
    var state      : String?
    var city       : String?
    var area       : String?
    var address    : String?
    var pincode    :String?

    init() {
    //
    }

    init(updateuser:UpdateProfile) {

    self.userid       = updateuser.userid
    self.username     = updateuser.username
    self.usermobile   = updateuser.usermobile
    self.plotno       = updateuser.plotno
    self.apartment    = updateuser.apartment
    self.block        = updateuser.block
    self.blockid      = updateuser.blockid
    self.state        = updateuser.state
    self.city         = updateuser.city
    self.area         = updateuser.area
    self.userEmail    = updateuser.userEmail
    self.address      = updateuser.address
    self.pincode      = updateuser.pincode
        

    }

    }
