//
//  AddContactsModel.swift
//  SmileyServe
//
//  Created by Apple on 07/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct AddContactsModel{
    
    var name : String?
    var mobile : String?
    var email : String?
    var message : String?
    var subject : String?
    
    
    init() {
        //
    }
    
    
    init(addContacts:AddContactsModel) {
        
        self.email = addContacts.email
        self.message = addContacts.message
        self.mobile = addContacts.mobile
        self.subject = addContacts.subject
        self.name = addContacts.name
        
        
    }
    
    
    
    
}
