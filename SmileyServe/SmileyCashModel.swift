//
//  SmileyCash.swift
//  SmileyServe
//
//  Created by Apple on 21/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct SmileyCash {
    
    var userid : String?
    var required_count : String?
    var page_number : String?
    
    init() {
        //
    }
    
    
    init(smiley:SmileyCash) {
        
        self.userid = smiley.userid
        self.required_count = smiley.required_count
        self.page_number = smiley.page_number
    }
}
