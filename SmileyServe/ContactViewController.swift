//
//  ContactViewController.swift
//  SmileyServe
//
//  Created by rakesh karapu on 09/10/18.
//  Copyright © 2018 smileyserve. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var sscrollView: UIScrollView!
    @IBOutlet weak var superViewW: UIView!
    @IBOutlet weak var callUsBtn: UIButton!
    @IBOutlet weak var writeUsBtn: UIButton!
    // @IBOutlet weak var backgroudView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                sscrollView.isScrollEnabled = true
            case 1334:
                print("iPhone 6/6S/7/8")
                sscrollView.isScrollEnabled = false

            case 2208:
                print("iPhone 6+/6S+/7+/8+")
                sscrollView.isScrollEnabled = false

            case 2436:
                print("iPhone X")
                sscrollView.isScrollEnabled = false

            default:
                print("unknown")
                sscrollView.isScrollEnabled = false

            }
        }
        
        superViewW.layer.cornerRadius = 0.5
        superViewW.clipsToBounds = true
        callUsBtn.layer.cornerRadius = 5
        writeUsBtn.layer.cornerRadius = 5

        callUsBtn.layer.masksToBounds = true
        writeUsBtn.layer.masksToBounds = true

       // callUsBtn.clipsToBounds = true
        //writeUsBtn.clipsToBounds = true
       // backgroudView?.backgroundColor = UIColor(white: 1, alpha: 0.5)
        //backgroudView.isOpaque = false
       // backgroudView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
     
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationUPdateProfile))
        self.title = "Contact Us"
        navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        self.navigationController?.navigationBar.tintColor =  UIColor.white
        // Do any additional setup after loading the view.


    }
    
    override func viewWillLayoutSubviews() {

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
        
    }
    
  
    @IBAction func callUsBtnAct(_ sender: Any) {
        
        guard let number = URL(string: "telprompt://+919393931340") else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(number)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(number)
        }
        
    }
    @IBAction func writeUsbtnAct(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let contactusViewController = storyBoard.instantiateViewController(withIdentifier: "Contact") as! ContactUsViewController
        self.navigationController?.pushViewController(contactusViewController, animated: true)

       // openViewControllerBasedOnIdentifier("Contact")
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }
    
    
    func handleNavigationUPdateProfile() {
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
//        let signupNavigation = UINavigationController(rootViewController: signupController)
//        self.present(signupNavigation, animated: false, completion: nil)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
