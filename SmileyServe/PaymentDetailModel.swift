//
//  PaymentDetailModel.swift
//  SmileyServe
//
//  Created by Apple on 01/09/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation

struct PaymentDetailModel {
  
  var debit_amount : Int?
  var credit_amount : String?
  var smiley_cash : String?
  
  
}
