//
//  FaqDefaultViewController.swift
//  SmileyServe
//
//  Created by Apple on 18/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class FaqDefaultViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        self.navigationController?.navigationBar.tintColor =  UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_navigationarrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
         self.title = "FAQS"

            }

    
    func handleNavigationArrow(){
        
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        let signupNavigation = UINavigationController(rootViewController: signupController)
        self.present(signupNavigation, animated: false, completion: nil)
        
        
    }

}
