//
//  LoginUser.swift
//  SmileyServe
//
//  Created by Apple on 06/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation



class Login: AnyObject {
    var view:AnyObject?
    var userLogin:Login_user?
    
    init(view:AnyObject,user:Login_user) {
        self.view = view
        self.userLogin = user
      }
    
    
    func Login() {
        
        let loginURL =  NSURL(string:Constants.URL_REQUEST_USER_LOGIN)
        var  request = URLRequest(url:loginURL! as URL)
        let  signupParams:[String:Any] = ["logindata":userLogin!.loginData!,"loginpassword":userLogin!.loginPassword!]
        request.httpMethod = "POST"
        do{
            let  json = try JSONSerialization.data(withJSONObject: signupParams)
            request.httpBody = json
            }catch let jsonErr{
        print(jsonErr.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let data = data, error == nil else
                
            {
                print(error?.localizedDescription as Any )
                
                return            }
            do{
                let fetchData = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
                
                if let userResponse = fetchData as? [String:Any]{
                    
                    
                    print(userResponse["message"]!)
                    
                    print(userResponse["description"]!)
                    
                    
                    
                        }}
            catch let jsonErr{
            print(jsonErr.localizedDescription)
            }
            
            
        }
        
        
        task.resume()
        
        }

}//class
