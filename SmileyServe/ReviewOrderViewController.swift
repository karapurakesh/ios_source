    //
    //  ReviewOrderViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 24/08/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    import UIKit
    import SVProgressHUD
    class ReviewOrderViewController: UIViewController,RazorpayPaymentCompletionProtocol, BEMCheckBoxDelegate{
        var bemCheckBox : BEMCheckBox!
        var smileyCashEnable : String?
        var checkoutIdIndexPath  = Int()
        
        var paymentActionVal :Bool = false
        var checksumTransactValue :Bool = false
        var cartUserId = UserDefaults.standard.value(forKey: "id") as? String

        var merchant:PGMerchantConfiguration!

        var buttonTag : Int!
        private  var razorpay : Razorpay!
        @IBOutlet var useSmileyCashCheckBox: BEMCheckBox!
        var cartCheckOutResponseArray :[CartCheckOutResponseModel?] = []
        var cartCheckoutPropertiesResponseArray :[CartCheckOutPropertiesResponseModel?] = []
        var userId = UserDefaults.standard.value(forKey: "id")
        @IBOutlet weak var reviewOrderTableView: UITableView!
        @IBOutlet weak var netToatalProductAmount: UILabel!
        @IBOutlet weak var smileyCashLabelValue: UILabel!
        @IBOutlet weak var smileyCashBtnRef: UIButton!
        @IBOutlet weak var smileyCashGateWayView: UIView!
        @IBOutlet weak var paymentGateWayView: UIView!
        fileprivate let formatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
        override func viewDidLoad() {
            super.viewDidLoad()
            self.reviewOrderTableView.isHidden = true
            self.smileyCashGateWayView.isHidden = true
            self.paymentGateWayView.isHidden = true
            self.useSmileyCashCheckBox.delegate = self
            initializeView()
            
            setMerchant()
        }
        
        func initializeView(){
            if Reachability.isConnectedToNetwork(){
                self.paymentGateWayDeatails()
                self.cartCheckout()
                self.cartCheckoutProperties()
                self.smileyCash()
            }else
            {
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    
                }
            }
        }
        
        func setMerchant(){
            merchant  = PGMerchantConfiguration.default()!
            //user your checksum urls here or connect to paytm developer team for this or use default urls of paytm
            merchant.checksumGenerationURL = "http://smileyserve.com/beta/generateChecksum.php";
            merchant.checksumValidationURL = "http://smileyserve.com/beta/verifyChecksum.php";
            
            // Set the client SSL certificate path. Certificate.p12 is the certificate which you received from Paytm during the registration process. Set the password if the certificate is protected by a password.
            merchant.clientSSLCertPath = nil; //[[NSBundle mainBundle]pathForResource:@"Certificate" ofType:@"p12"];
            merchant.clientSSLCertPassword = nil; //@"password";
            
            //configure the PGMerchantConfiguration object specific to your requirements
            /*  merchant.merchantID = "Smiley32368638707578";//paste here your merchant id  //mandatory
             merchant.website = "APP_STAGING";//mandatory
             merchant.industryID = "Retail";//mandatory
             merchant.channelID = "WAP"; //provided by PG WAP //mandatory*/
            
            //Prod
            merchant.merchantID = "SmileP27807646019839";//paste here your merchant id  //mandatory
            merchant.website = "SmilePWAP";//mandatory
            merchant.industryID = "Retail109";//mandatory
            merchant.channelID = "WEB"; //provided by PG WAP //mandatory
            
        }
        
        @IBAction func paymentGateWayBtnAction(_ sender: Any) {
            DispatchQueue.main.async {
                //     self.razorpay = Razorpay.initWithKey("rzp_test_uJmv0AK2JLwkj0", andDelegate: self)
                self.razorpay = Razorpay.initWithKey("rzp_live_2c8aqqKX844kbZ", andDelegate: self)
                self.showPaymentForm()
                
            }
        }
        
        @IBAction func paymentBtnAtn(_ sender: Any) {
            DispatchQueue.main.async {
                //self.razorpay = Razorpay.initWithKey("rzp_test_uJmv0AK2JLwkj0", andDelegate: self)
                self.razorpay = Razorpay.initWithKey("rzp_live_2c8aqqKX844kbZ", andDelegate: self)
                self.showPaymentForm()
                
            }
        }
        func didTap(_ checkBox: BEMCheckBox) {
            print("check box :\(checkBox.tag):\(checkBox.on)")
            bemCheckBox = checkBox
            if checkBox.on {
                guard let smileyCashAmount = UserDefaults.standard.value(forKey: "smileyCashValue") as? String else {
                    return
                }
                guard  let totalProdctPrice = UserDefaults.standard.value(forKey: "total_price") as? Float else{
                    return
                }
                //  let smileyCash : Float = NSString(string: smileyCashAmount).floatValue
                //  let toatalPrice : Float = NSString(string: totalProdctPrice).floatValue
                let floatV = Float(smileyCashAmount)!
                if floatV >= totalProdctPrice {
                    smileyCashEnable = "1"
                    self.createSmileyCashPayDialog()
                    self.paymentGateWayView.isHidden = true
                }else{
                    smileyCashEnable = "0"
                    self.netToatalProductAmount.text = "Rs." + String(totalProdctPrice - floatV)
                    self.paymentGateWayView.isHidden = false
                    UserDefaults.standard.set(totalProdctPrice - floatV, forKey: "totalFloatValue")
                }
            }else{
                self.paymentGateWayView.isHidden = false
                guard  let totalProdctPrice = UserDefaults.standard.value(forKey: "total_price") as? Float else{
                    return
                }
                self.netToatalProductAmount.text = "Rs." + String(totalProdctPrice)
                self.bemCheckBox = nil
            }
        }
        
        /* creating an alert dialog for paying using smileycash */
        func createSmileyCashPayDialog(){
            let alertController = UIAlertController(title: "Confirmation Message", message: "Do you want to pay Amount Using Smiley Cash?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                self.paymentGateWayView.isHidden = false
                self.bemCheckBox.on = !self.bemCheckBox.on
                self.bemCheckBox = nil
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                if Reachability.isConnectedToNetwork(){
                    self.orderCheckoutPaymentSuccess()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                        
                    }
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        /* RazorPay paymentform */
        
        func showPaymentForm() {
            DispatchQueue.main.async {
                if let totalAmountValue = UserDefaults.standard.value(forKey: "total_price") as? Float {
                    let convertedIntTotalValue = totalAmountValue
                    let convertedCurrencyValue = Int(convertedIntTotalValue) * 100
                    if self.bemCheckBox != nil{
                        if let totalFloatValue = UserDefaults.standard.value(forKey: "totalFloatValue") as? Float{
                            let convertedFloatCurrency = Int(totalFloatValue) * 100
                            let options = ["amount":convertedFloatCurrency,"payment_capture":"1",
                                           "image": UIImage(named:"512x512.png") as Any, "name": "SmileyServe", "description": UserDefaults.standard.value(forKey: "name")!, "prefill": ["email": UserDefaults.standard.value(forKey: "email"), "contact": UserDefaults.standard.value(forKey: "mobile")], "theme": ["color": "#206AE0"]] as [String : Any]
                            self.razorpay.open(options)
                        }
                    }else{
                        let options = ["amount":convertedCurrencyValue,"payment_capture":"1",
                                       "image": UIImage(named:"512x512.png") as Any, "name": "SmileyServe", "description": UserDefaults.standard.value(forKey: "name")!, "prefill": ["email": UserDefaults.standard.value(forKey: "email"), "contact": UserDefaults.standard.value(forKey: "mobile")], "theme": ["color": "#206AE0"]] as [String : Any]
                        self.razorpay.open(options)
                        
                        
                        
                    }
                }
            }
        }
        
        func onPaymentSuccess(_ payment_id: String) {
            self.orderCheckoutPaymentUsingGateway()
            UIAlertView.init(title: "Payment Successful", message: "", delegate: self, cancelButtonTitle: "OK").show()
        }
        
        func onPaymentError(_ code: Int32, description str: String) {
            
            UIAlertView.init(title: "Error", message: str, delegate: self, cancelButtonTitle: "OK").show()
            
        }
        
        @IBAction func reviewRemoveAllCartProducts(_ sender: Any) {
            self.createConfirmationToDeleteCheckOutAllDialog()
        }
        
        @IBAction func reviewOrderBarBtnAction(_ sender: Any) {
            createConfirmationToExitReviewPageDialog()
            
        }
        
        /* create a Alert Dialog for displaying smileycashpay */
        
        func createConfirmationToExitReviewPageDialog(){
            let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to exit this page?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                if Reachability.isConnectedToNetwork(){
                    self.cartCheckOutDeleteAll()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                        
                    }
                }
                
                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let homeController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                let signupNavigation = UINavigationController(rootViewController: homeController)
                self.present(signupNavigation, animated: true, completion: nil)
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        
        func paymentGateWayDeatails(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let paymentGatewayDetailsUrl = NSURL(string: Constants.URL_REQUEST_USER_PAYMENTGATEWAYDETAILS ) {
                var urlRequest = URLRequest(url: paymentGatewayDetailsUrl as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    do {
                        
                        guard let paymentGatewayJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        print(paymentGatewayJson)
                        if  let orderCuttoff_time = paymentGatewayJson["order_cutoff_time"] {
                            print(orderCuttoff_time)
                            
                        }
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                })
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    
                }
            }
        }
        
        func cartCheckout(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            let date = Date()
            let todayDate = formatter.string(from: date)
            if let cartCheckoutUrl =   NSURL(string:Constants.URL_REQUEST_USER_CARTCHECKOUT){
                var request = URLRequest(url:cartCheckoutUrl as URL)
                var cartCheckOutModel = CartCheckOutModel()
                cartCheckOutModel.userid = userId as? String
                cartCheckOutModel.checkout_date = todayDate
                guard let userId =  cartCheckOutModel.userid,let checkOutDate = cartCheckOutModel.checkout_date else {return}
                let cartCheckOutListParams:[String:Any] = ["userid":userId,"checkout_date":checkOutDate]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cartCheckOutListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    self.cartCheckOutResponseArray.removeAll()
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    self.cartCheckOutResponseArray = [CartCheckOutResponseModel]()
                    do{
                        guard let cartCheckOutJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print(cartCheckOutJsonObject)
                        print(cartCheckOutJsonObject["description"]!)
                        print(cartCheckOutJsonObject["message"]!)
                        if (cartCheckOutJsonObject["code"] as! NSNumber == 200) {
                            if let cartCheckOutResult = cartCheckOutJsonObject["checkout_result"] as? [[String:AnyObject]]{
                                for cartCheckOutResponse in cartCheckOutResult {
                                    if cartCheckOutResponse.count > 0 {
                                        var cartModelResultsString = CartCheckOutResponseModel()
                                        guard let reviewProductTitle = cartCheckOutResponse["product_name"],let reviewProductImage = cartCheckOutResponse["product_image"],let reviewProductUnits = cartCheckOutResponse["product_units"],let reviewProductPrice = cartCheckOutResponse["selling_price"],let reviewProductDeleiveryCharges = cartCheckOutResponse["deliverycharge"],let reviewOrderDate = cartCheckOutResponse["order_date"],let current_quantity = cartCheckOutResponse["current_qty"],let current_total = cartCheckOutResponse["current_total"] ,let checkout_id = cartCheckOutResponse["checkoutid"],let productid = cartCheckOutResponse["productid"],let product_org_price = cartCheckOutResponse["product_org_price"]  else {
                                            return
                                        }
                                        
                                        cartModelResultsString.order_date = reviewOrderDate as? String
                                        cartModelResultsString.deliverycharge = reviewProductDeleiveryCharges as? String
                                        cartModelResultsString.product_name = reviewProductTitle as? String
                                        cartModelResultsString.product_image = reviewProductImage as? String
                                        cartModelResultsString.product_units = reviewProductUnits as? String
                                        cartModelResultsString.selling_price = reviewProductPrice as? String
                                        cartModelResultsString.current_total = current_total as? String
                                        cartModelResultsString.current_qty = current_quantity as? String
                                        cartModelResultsString.checkoutid = checkout_id as? String
                                        cartModelResultsString.productid = productid as? String
                                        cartModelResultsString.product_org_price = product_org_price as? String
                                        UserDefaults.standard.set(cartModelResultsString.checkoutid, forKey: "checkoutid")
                                        self.cartCheckOutResponseArray.append(cartModelResultsString)
                                        DispatchQueue.main.async {
                                            self.reviewOrderTableView.reloadData()
                                        }
                                    }
                                }
                            }
                            
                            DispatchQueue.main.async {
                                self.reviewOrderTableView.reloadData()
                                self.reviewOrderTableView.isHidden = false
                                self.smileyCashGateWayView.isHidden = false
                                self.paymentGateWayView.isHidden = false
                            }
                        }else{
                            self.reviewOrderTableView.isHidden = true
                            self.smileyCashGateWayView.isHidden = true
                            self.paymentGateWayView.isHidden = true
                            DispatchQueue.main.async {
                                self.view.makeToast(cartCheckOutJsonObject["description"]! as? String, duration: 3.0, position: .center)
                            }
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)  }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)  }
            }
        }
        
        func cartCheckoutProperties(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            let date = Date()
            let todayDate = formatter.string(from: date)
            if let cartCheckoutPropertiesUrl =   NSURL(string:Constants.URL_REQUEST_USER_CARTCHECKOUTPROPERTIES){
                var request = URLRequest(url:cartCheckoutPropertiesUrl as URL)
                var cartCheckOutPropertiesModel = CartCheckOutPropertiesModel()
                cartCheckOutPropertiesModel.userid = userId as? String
                cartCheckOutPropertiesModel.checkout_date = todayDate
                guard let userId =  cartCheckOutPropertiesModel.userid,let checkoutDate = cartCheckOutPropertiesModel.checkout_date else{return}
                let cartCheckOutListParams:[String:Any] = ["userid":userId,"checkout_date":checkoutDate]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cartCheckOutListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    
                    self.cartCheckoutPropertiesResponseArray = [CartCheckOutPropertiesResponseModel]()
                    do{
                        guard let cartCheckOutPropertiesJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        if let totalProductPrice = cartCheckOutPropertiesJsonObject["total_price"] as? Float{
                            print(totalProductPrice)
                            UserDefaults.standard.set(totalProductPrice, forKey: "total_price")
                            DispatchQueue.main.async {
                                self.netToatalProductAmount.text = "Rs." + String(totalProductPrice)
                                UserDefaults.standard.set(totalProductPrice, forKey: "total_price")
                                
                            }
                        }
                        DispatchQueue.main.async {
                            var cartCheckPropertiesResponse = CartCheckOutPropertiesResponseModel()
                            guard let total_items = cartCheckOutPropertiesJsonObject ["total_items"],let total_qty = cartCheckOutPropertiesJsonObject["total_qty"],let total_price = cartCheckOutPropertiesJsonObject["total_price"],let total_product_price =
                                cartCheckOutPropertiesJsonObject["total_product_price"],let total_deliver_charges = cartCheckOutPropertiesJsonObject["total_deliver_charges"],let item_ids = cartCheckOutPropertiesJsonObject["item_ids"],let order_date = cartCheckOutPropertiesJsonObject["order_date"] else {
                                    
                                    return
                            }
                            
                            cartCheckPropertiesResponse.item_ids = item_ids as? String
                            cartCheckPropertiesResponse.total_items = total_items as? String
                            cartCheckPropertiesResponse.total_qty = total_qty as? String
                            cartCheckPropertiesResponse.total_price = total_price as? String
                            cartCheckPropertiesResponse.total_product_price = total_product_price as? String
                            cartCheckPropertiesResponse.total_deliver_charges = total_deliver_charges as? String
                            cartCheckPropertiesResponse.order_date = order_date as? String
                            self.cartCheckoutPropertiesResponseArray.append(cartCheckPropertiesResponse)
                        }
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        func cartCheckOutDeleteAll() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            let date = Date()
            let todayDate = formatter.string(from: date)
            if let cartCheckoutDeleteAllUrl =   NSURL(string:Constants.URL_REQUEST_USER_CARTCHECKOUTDELETEALL){
                var request = URLRequest(url:cartCheckoutDeleteAllUrl as URL)
                var cartCheckOutDeleteAllModel = CartDeleteAll()
                cartCheckOutDeleteAllModel.userid = userId as? String
                cartCheckOutDeleteAllModel.checkout_date = todayDate
                guard let userId =  cartCheckOutDeleteAllModel.userid,let checkOutDate = cartCheckOutDeleteAllModel.checkout_date else{return}
                let cartCheckOutListParams:[String:Any] = ["user_id":userId,"checkout_date":checkOutDate]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cartCheckOutListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let cartCheckOutDeleteAllJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        if ( cartCheckOutDeleteAllJsonObject["code"] as! NSNumber == 200){
                            DispatchQueue.main.async {
                                self.reviewOrderTableView.isHidden = true
                                self.view.makeToast(cartCheckOutDeleteAllJsonObject["description"]! as? String, duration: 3.0, position: .center)
                                DispatchQueue.main.async {
                                    self.perform(#selector(self.perfromDelayToHome), with: self, afterDelay: 1)
                                }
                                
                            }
                        }else{
                            self.view.makeToast(cartCheckOutDeleteAllJsonObject["description"]! as? String, duration: 3.0, position: .center)
                        }
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        func cartCheckOutDelete() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            let date = Date()
            let todayDate = formatter.string(from: date)
            if let cartCheckoutDeleteUrl =   NSURL(string:Constants.URL_REQUEST_USER_CARTCHECKOUTDELETE){
                var request = URLRequest(url:cartCheckoutDeleteUrl as URL)
                var cartCheckOutDeleteModel = CartCheckoutDeleteModel()
                cartCheckOutDeleteModel.userid = userId as? String
                //  cartCheckOutDeleteModel.checkoutid = UserDefaults.standard.value(forKey: "checkoutid") as? String
                cartCheckOutDeleteModel.checkoutid = self.cartCheckOutResponseArray[buttonTag]?.checkoutid
                cartCheckOutDeleteModel.checkout_date = todayDate
                guard let userId = cartCheckOutDeleteModel.userid,let checkOutdate = cartCheckOutDeleteModel.checkout_date,let checkOutId =  cartCheckOutDeleteModel.checkoutid  else{return}
                let cartCheckOutListParams:[String:Any] = ["userid":userId,"checkout_date":checkOutdate,"checkoutid":  checkOutId]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cartCheckOutListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    
                    do{
                        guard let cartCheckOutDeleteAllJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        
                        if ( cartCheckOutDeleteAllJsonObject["code"] as! NSNumber == 200){
                            DispatchQueue.main.async {
                                if self.checkoutIdIndexPath == 0 && self.cartCheckOutResponseArray.count == 1 {
                                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                                    let signupNavigation = UINavigationController(rootViewController: signupController)
                                    self.present(signupNavigation, animated: false, completion: nil)
                                }else{
                                    
                                    self.cartCheckOutResponseArray.remove(at: self.checkoutIdIndexPath)
                                    self.reviewOrderTableView!.reloadData()
                                    self.view.makeToast(cartCheckOutDeleteAllJsonObject["description"]! as? String, duration: 3.0, position: .center)
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let controller = storyBoard.instantiateViewController(withIdentifier: "reviewViewController") as! ReviewOrderViewController
                                    self.present(controller, animated: true, completion: nil)
                                }
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.view.makeToast(cartCheckOutDeleteAllJsonObject["description"]! as? String, duration: 3.0, position: .center)
                            }
                            
                        }
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)    }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)    }
            }
        }
        
        
        func perfromDelayToHome() {
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let signupNavigation = UINavigationController(rootViewController: homeController)
            self.present(signupNavigation, animated: true, completion: nil)
        }
        
        /* creating dialog for remove all from the cart */
        
        func createConfirmationToDeleteCheckOutAllDialog(){
            let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want Delete All?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                if Reachability.isConnectedToNetwork() {
                    self.cartCheckOutDeleteAll()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    }
                    
                }
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        
        /* creating a function for ordercheckoutpaymentsuccess */
        
        func orderCheckoutPaymentSuccess(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            let date = Date()
            let todayDate = formatter.string(from: date)
            if let orderCheckoutPaymentSuccessUrl =   NSURL(string:Constants.URL_REQUEST_USER_ORDERCHECKOUTPAYMENTSUCCESS){
                var request = URLRequest(url:orderCheckoutPaymentSuccessUrl as URL)
                var orderPaymentSuccessModelObject = OrderCheckOutPaymentSuccessModel()
                orderPaymentSuccessModelObject.userid = userId as? String
                orderPaymentSuccessModelObject.checkout_date = todayDate
                orderPaymentSuccessModelObject.smileycash = smileyCashEnable
                guard let userId =  orderPaymentSuccessModelObject.userid,let checkoutDate =  orderPaymentSuccessModelObject.checkout_date,let smileyCash = orderPaymentSuccessModelObject.smileycash else{return}
                let cartCheckOutListParams:[String:Any] = ["userid":userId,"checkout_date":checkoutDate,"smileycash": smileyCash]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cartCheckOutListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    
                    self.perfromDelayToHome()
                    
                    do{
                        guard let orderPaymentSuccessJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print(orderPaymentSuccessJsonObject)
                        DispatchQueue.main.async {
                            self.view.makeToast("order updated Successfully", duration: 3.0, position: .center)
                            DispatchQueue.main.async {
                                self.perform(#selector(self.perfromDelayToHome), with: self, afterDelay: 1)
                            }
                        }
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        /* Function To Perform the ordercheckoutpayment success using paymentgateway */
        func orderCheckoutPaymentUsingGateway() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            let date = Date()
            let todayDate = formatter.string(from: date)
            if let orderCheckoutPaymentSuccessUrl =   NSURL(string:Constants.URL_REQUEST_USER_ORDERCHECKOUTPAYMENTSUCCESS){
                var request = URLRequest(url:orderCheckoutPaymentSuccessUrl as URL)
                var orderPaymentSuccessModelObject = OrderCheckOutPaymentSuccessModel()
                orderPaymentSuccessModelObject.userid = userId as? String
                orderPaymentSuccessModelObject.checkout_date = todayDate
                if (bemCheckBox != nil){
                    orderPaymentSuccessModelObject.smileycash = "1"
                }else{
                    orderPaymentSuccessModelObject.smileycash = "0"
                }
                guard let userId =  orderPaymentSuccessModelObject.userid,let checkoutDate =  orderPaymentSuccessModelObject.checkout_date,let smileyCash = orderPaymentSuccessModelObject.smileycash else{return}
                let cartCheckOutListParams:[String:Any] = ["userid":userId,"checkout_date":checkoutDate,"smileycash": smileyCash]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cartCheckOutListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    
                    //self.perfromDelayToHome()
                    
                    do{
                        guard let orderPaymentSuccessJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print(orderPaymentSuccessJsonObject)
                        DispatchQueue.main.async {
                            self.view.makeToast("order updated Successfully", duration: 3.0, position: .center)
                            DispatchQueue.main.async {
                                self.perform(#selector(self.perfromDelayToHome), with: self, afterDelay: 1)
                            }
                        }
                        DispatchQueue.main.async {
                            self.perform(#selector(self.perfromDelayToHome), with: self, afterDelay: 1)
                        }
                        
                        if ( orderPaymentSuccessJsonObject["code"] as! NSNumber == 200){
                            
                            DispatchQueue.main.async {
                                DispatchQueue.main.async {
                                    self.perform(#selector(self.perfromDelayToHome), with: self, afterDelay: 1)
                                }
                                
                            }
                        }else{
                            
                            DispatchQueue.main.async {
                            }
                        }
                        
                    }
                    catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        
        
        
        func smileyCash(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let smileyCash =   NSURL(string:Constants.URL_REQUEST_USER_SMILEYCASH){
                var request = URLRequest(url:smileyCash as URL)
                var cashAmount:SmileyCash = SmileyCash()
                cashAmount.page_number = "1"
                cashAmount.userid = userId as? String
                cashAmount.required_count = "1"
                guard let userId = cashAmount.userid else{return}
                let cashListParams:[String:Any] = ["userid":userId,"page_number":cashAmount.page_number!,"required_count":cashAmount.required_count!]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cashListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let fetchSmileyData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print(fetchSmileyData)
                        DispatchQueue.main.async {
//                            guard let  smileyCashValue:Float = fetchSmileyData["smiley_cash"] as? Float else{return}
//                            UserDefaults.standard.set(smileyCashValue, forKey: "smileyCashValue")
//                            self.smileyCashLabelValue.text = "Rs." + "\(smileyCashValue)"
//                            UserDefaults.standard.set( self.smileyCashLabelValue.text, forKey: "smileyCash")
//
                            
                            if  let  smileyCashValue = fetchSmileyData["smiley_cash"]{
                                
                                UserDefaults.standard.set("\(smileyCashValue)", forKey: "smileyCashValue")
                                
                                let sCash = "Rs. " + "\(smileyCashValue)"
                                
                                 self.smileyCashLabelValue.text = sCash
                                
                            }
                            
                            
                            
                        }
                        
                        
                        if (fetchSmileyData["code"] as! NSNumber == 200) {
                            
                        }
                        if let smileyResult = fetchSmileyData["smileycash_result"] as? [[String:AnyObject]]{
                            for smileyResponse in smileyResult {
                                
                                if smileyResponse.count > 0 {
                                    
                                    guard (smileyResponse["id"] as? String) != nil else{
                                        
                                        return
                                    }
                                    
                                    print(smileyResponse["id"] as! String)
                                    
                                }
                            }
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        func cartCheckoutDeleteBtnHandle(_sender:UIButton){
            buttonTag = _sender.tag
            print(buttonTag)
            self.showCartAlertDialog(indexPath: buttonTag)
        }
        
        /* create a dialog for delete cartcheckoutproperties */
        
        func showCartAlertDialog(indexPath:Int){
            
            let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to delete this item?", preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
                
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                
                if Reachability.isConnectedToNetwork(){
                    self.cartCheckOutDelete()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                        
                    }
                    
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        @IBAction func paymentGateWayButton(_ sender: Any) {
            
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let image = UIImage(named: "paytm")
            let imageView = UIImageView()
            imageView.image = image
            imageView.frame =  CGRect(x: 10, y: 0, width: 60, height: 60)
            actionSheetController.view.addSubview(imageView)
            
            
            let image2 = UIImage(named: "razor")
            let imageView1 = UIImageView()
            imageView1.image = image2
            imageView1.frame =  CGRect(x: 10, y: 60, width: 50, height: 50)
            actionSheetController.view.addSubview(imageView1)
            
            
            
            // create an action
            let firstAction: UIAlertAction = UIAlertAction(title: "BHIM/CC/DC/NB", style: .default) { action -> Void in
                
                //   print("First Action pressed")
                //self.paymentActionVal = true
                
           /*     DispatchQueue.main.async {
                    if let totalAmountValue = UserDefaults.standard.value(forKey: "total_price") as? Float {
                        let convertedIntTotalValue = totalAmountValue
                        let convertedCurrencyValue = Int(convertedIntTotalValue) * 100
                        if self.bemCheckBox != nil{
                            if let totalFloatValue = UserDefaults.standard.value(forKey: "totalFloatValue") as? Float{
                                let convertedFloatCurrency = Int(totalFloatValue) * 100
                            }
                        }else{
                          
                            
                        }
                    }
                }*/
                
                
                let randomNum:UInt32 = arc4random() // range is 0 to 99
                // convert the UInt32 to some other  types
                // let randomTime:TimeInterval = TimeInterval(randomNum)
                //let someInt:Int = Int(randomNum)
                let someString:String = String(randomNum)
                
                UserDefaults.standard.set(someString, forKey: "orderid")

                
                self.checksumHash(completionHandler: { (true) in
                    
                    self.createPayment()
                    
                })
            }
            
            
            let secondAction: UIAlertAction = UIAlertAction(title: "Other Wallets", style: .default) { action -> Void in
                
                DispatchQueue.main.async {
                    //self.razorpay = Razorpay.initWithKey("rzp_test_uJmv0AK2JLwkj0", andDelegate: self)
                    self.razorpay = Razorpay.initWithKey("rzp_live_2c8aqqKX844kbZ", andDelegate: self)
                    self.showPaymentForm()
                    
                }
            }
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
            
            actionSheetController.addAction(firstAction)
            actionSheetController.addAction(secondAction)
            actionSheetController.addAction(cancelAction)
            
            present(actionSheetController, animated: true, completion: nil)
            
        }
        
        
        
        func createPayment(){
            
//            DispatchQueue.main.async {
//                SVProgressHUD.setDefaultStyle(.dark)
//                SVProgressHUD.show(withStatus: "please wait...")
//            }
            
            var orderDict = [String : Any]()
            
            
            if let totalAmountValue = UserDefaults.standard.value(forKey: "total_price") as? Float {
                    let convertedCurrencyValue = Int(totalAmountValue) * 100
                    if self.bemCheckBox != nil{
                        if  let toatlProductReducedVaue = UserDefaults.standard.value(forKey: "totalFloatValue") as? Double{
                            let convertedValue = Float(toatlProductReducedVaue) //* 100
                            //use convertedValue
                            let value = String(describing:convertedValue)
                            
                            orderDict["TXN_AMOUNT"] = value; // amount to charge
                            print("TXN_AMOUNT_Latest",value)
                            
                            //  Prod
                            
                            orderDict["MID"] = "SmileP27807646019839";//paste here your merchant id   //mandatory
                            orderDict["CHANNEL_ID"] = "WAP"; // paste here channel id                       // mandatory
                            orderDict["INDUSTRY_TYPE_ID"] = "Retail109";//paste industry type              //mandatory
                            orderDict["WEBSITE"] = "SmilePWAP";// paste website
                            let orderIDSTr = UserDefaults.standard.value(forKey: "orderid") as! String
                            orderDict["ORDER_ID"] = orderIDSTr;//change order id every time on new transaction
                            // orderDict["REQUEST_TYPE"] = "DEFAULT";// remain same
                            orderDict["CUST_ID"] = self.cartUserId; // change acc. to your database user/customers
                            orderDict["MOBILE_NO"] = UserDefaults.standard.value(forKey: "mobile");// optional
                            orderDict["EMAIL"] =  UserDefaults.standard.value(forKey: "email"); //optional
                            
                            orderDict["CALLBACK_URL"] = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDSTr)"
                            // orderDict["CALLBACK_URL"] = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
                            let checksumHsh = UserDefaults.standard.value(forKey: "CHECKSUMHASH_RES") as! String
                            
                            
                            //https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=<orderid>
                            
                            print("checksum::",checksumHsh)
                            orderDict["CHECKSUMHASH"] = checksumHsh
                            print("orderDict::",orderDict)
                            
                            DispatchQueue.main.async {
                                let pgOrder = PGOrder(params: orderDict )
                                
                                let canBtn = UIButton.init(frame: CGRect(x: 0, y:20 , width: 100, height: 30))
                                canBtn.setTitle("Cancel", for: .normal)
                                //canBtn.tintColor = UIColor.red
                                
                                let topView = UIView(frame: CGRect(x: 0, y: 15, width: self.view.frame.width, height: 60))
                                topView.backgroundColor = UIColor(red:253.0/255.0, green:109.0/255.0, blue:64/255.0, alpha:1.000)
                                topView.addSubview(canBtn)
                                
                                
                                let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
                                
                                transaction!.serverType = eServerTypeProduction
                                transaction!.merchant = self.merchant
                                transaction!.loggingEnabled = true
                                transaction!.delegate = self
                                transaction?.topBar = topView
                                transaction?.cancelButton =  canBtn
                                
                                self.present(transaction!, animated: true, completion: {
                                    SVProgressHUD.dismiss()
                                    
                                })
                            }
                            
                            
                        }
                    }else{
                        //use convertedCurrencyValue
                        
                        print("TXN_AMOUNTELSE",totalAmountValue)
                        
                        let value = String(describing:totalAmountValue)
                        orderDict["TXN_AMOUNT"] = value; // amount to charge
                        
                        //  Prod
                        
                        orderDict["MID"] = "SmileP27807646019839";//paste here your merchant id   //mandatory
                        orderDict["CHANNEL_ID"] = "WAP"; // paste here channel id                       // mandatory
                        orderDict["INDUSTRY_TYPE_ID"] = "Retail109";//paste industry type              //mandatory
                        orderDict["WEBSITE"] = "SmilePWAP";// paste website
                        let orderIDSTr = UserDefaults.standard.value(forKey: "orderid") as! String
                        orderDict["ORDER_ID"] = orderIDSTr;//change order id every time on new transaction
                        // orderDict["REQUEST_TYPE"] = "DEFAULT";// remain same
                        orderDict["CUST_ID"] = self.cartUserId; // change acc. to your database user/customers
                        orderDict["MOBILE_NO"] = UserDefaults.standard.value(forKey: "mobile");// optional
                        orderDict["EMAIL"] =  UserDefaults.standard.value(forKey: "email"); //optional
                        
                        orderDict["CALLBACK_URL"] = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDSTr)"
                        // orderDict["CALLBACK_URL"] = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
                        let checksumHsh = UserDefaults.standard.value(forKey: "CHECKSUMHASH_RES") as! String
                        
                        
                        //https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=<orderid>
                        
                        orderDict["CHECKSUMHASH"] = checksumHsh
                        print("orderDict::",orderDict)
                        
                        DispatchQueue.main.async {
                            let pgOrder = PGOrder(params: orderDict )
                            
                            let canBtn = UIButton.init(frame: CGRect(x: 0, y:20 , width: 100, height: 30))
                            canBtn.setTitle("Cancel", for: .normal)
                            //canBtn.tintColor = UIColor.red
                            
                            let topView = UIView(frame: CGRect(x: 0, y: 15, width: self.view.frame.width, height: 60))
                            topView.backgroundColor = UIColor(red:253.0/255.0, green:109.0/255.0, blue:64/255.0, alpha:1.000)
                            topView.addSubview(canBtn)
                            
                            
                            let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
                            
                            transaction!.serverType = eServerTypeProduction
                            transaction!.merchant = self.merchant
                            transaction!.loggingEnabled = true
                            transaction!.delegate = self
                            transaction?.topBar = topView
                            transaction?.cancelButton =  canBtn
                            
                            self.present(transaction!, animated: true, completion: {
                                SVProgressHUD.dismiss()
                                
                            })
                        }
                        
                        
                    }
            }
            
        }
        
        func checksumHash(completionHandler: @escaping CompletionHandler) {
            
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            
            let url = prepareChecksum()
            
            print("URLCHECKSUM_",url)
            
            var request = URLRequest(url:url as URL)
            
            print("createOrderUrl",url)
            
            request.httpMethod = "GET"
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                
                guard let data = data, error == nil else
                {
                    print(error?.localizedDescription as Any )
                    return
                }
                do{
                    guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                    
                    guard let checksumHashRes = createOrderJson["CHECKSUMHASH"] as? String else{
                        return
                    }
                    
                    print("CheckSumHash_Rakesh",checksumHashRes)
                    
                    UserDefaults.standard.set(checksumHashRes, forKey: "CHECKSUMHASH_RES")
                    UserDefaults.standard.synchronize()
                    
                    completionHandler(true)
                    
                    //                    DispatchQueue.main.async {
                    //                        SVProgressHUD.dismiss()
                    //                    }
                    
                }catch let jsonErr{
                    
                    completionHandler(false)
                    
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            task.resume()
        }
        
        func prepareChecksum() -> URL{
            
            let orderIDSTr = UserDefaults.standard.value(forKey: "orderid") as! String
            
            
            
            //staging  var url = URL.init(string: "http://smileyserve.com/beta/generateChecksum.php");
            var url = URL.init(string: "http://smileyserve.com/generateChecksum.php"); //prod
            
            
            
            if (url != nil) {
                
                
                if checksumTransactValue {
                    
                    let mobileNo = UserDefaults.standard.value(forKey: "mobile") as! String
                    let email = UserDefaults.standard.value(forKey: "email") as! String
                    
                    url = addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "MID", value: "SmileP27807646019839"),
                                                                URLQueryItem.init(name: "CHANNEL_ID", value: "WAP"),
                                                                URLQueryItem.init(name: "INDUSTRY_TYPE_ID", value: "Retail109"),
                                                                URLQueryItem.init(name: "WEBSITE", value: "SmilePWAP"),
                                                                URLQueryItem.init(name: "TXN_AMOUNT", value: ""),
                                                                URLQueryItem.init(name: "ORDER_ID", value: orderIDSTr),
                                                                URLQueryItem.init(name: "CUST_ID", value: self.cartUserId),
                                                                URLQueryItem.init(name: "CALLBACK_URL", value: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDSTr)"),
                                                                URLQueryItem.init(name: "MOBILE_NO", value: mobileNo),
                                                                URLQueryItem.init(name: "EMAIL", value: email)])
                }else {
                    
                    let mobileNo = UserDefaults.standard.value(forKey: "mobile") as! String
                    let email = UserDefaults.standard.value(forKey: "email") as! String
                    
                    
                    if let totalAmountValue = UserDefaults.standard.value(forKey: "total_price") as? Float {
                            //  let convertedCurrencyValue = Int(convertedIntTotalValue) * 100
                            if self.bemCheckBox != nil{
                                if  let toatlProductReducedVaue = UserDefaults.standard.value(forKey: "totalFloatValue") as? Double{
                                    let convertedValue = Float(toatlProductReducedVaue) //* 100
                                    //use convertedValue
                                    
                                    let finalValue = String(describing:convertedValue)
                                    
                                    print("finalValue",finalValue)
                                    url = addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "MID", value: "SmileP27807646019839"),
                                                                                URLQueryItem.init(name: "CHANNEL_ID", value: "WAP"),
                                                                                URLQueryItem.init(name: "INDUSTRY_TYPE_ID", value: "Retail109"),
                                                                                URLQueryItem.init(name: "WEBSITE", value: "SmilePWAP"),
                                                                                URLQueryItem.init(name: "TXN_AMOUNT", value: finalValue),
                                                                                URLQueryItem.init(name: "ORDER_ID", value: orderIDSTr),
                                                                                URLQueryItem.init(name: "CUST_ID", value: self.cartUserId),
                                                                                URLQueryItem.init(name: "CALLBACK_URL", value: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDSTr)"),
                                                                                URLQueryItem.init(name: "MOBILE_NO", value: mobileNo),
                                                                                URLQueryItem.init(name: "EMAIL", value: email)])
                                }
                            }else{
                                //use convertedCurrencyValue
                                
                                let finalValue = String(describing:totalAmountValue)
                                print("finalValueElse",finalValue)
                                
                                url = addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "MID", value: "SmileP27807646019839"),
                                                                            URLQueryItem.init(name: "CHANNEL_ID", value: "WAP"),
                                                                            URLQueryItem.init(name: "INDUSTRY_TYPE_ID", value: "Retail109"),
                                                                            URLQueryItem.init(name: "WEBSITE", value: "SmilePWAP"),
                                                                            URLQueryItem.init(name: "TXN_AMOUNT", value: finalValue),
                                                                            URLQueryItem.init(name: "ORDER_ID", value: orderIDSTr),
                                                                            URLQueryItem.init(name: "CUST_ID", value: self.cartUserId),
                                                                            URLQueryItem.init(name: "CALLBACK_URL", value: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDSTr)"),
                                                                            URLQueryItem.init(name: "MOBILE_NO", value: mobileNo),
                                                                            URLQueryItem.init(name: "EMAIL", value: email)])
                                
                            }
                    }
                }
                
                
            }
            
            return url!
        }
        
        func addQueryParams(url: URL, newParams: [URLQueryItem]) -> URL? {
            let urlComponents = NSURLComponents.init(url: url, resolvingAgainstBaseURL: false)
            guard urlComponents != nil else { return nil; }
            if (urlComponents?.queryItems == nil) {
                urlComponents!.queryItems = [];
            }
            urlComponents!.queryItems!.append(contentsOf: newParams);
            return urlComponents?.url;
        }
        
        func statusCheckService() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            
            self.checksumHash { (true) in
                
                let checksumHsh = UserDefaults.standard.value(forKey: "CHECKSUMHASH_RES") as! String
                
                print("checksumHsh_transaction_Success",checksumHsh)
                
                let orderIDSTr = UserDefaults.standard.value(forKey: "orderid") as! String
                
                
                var url = URL.init(string: "https://securegw.paytm.in/merchant-status/getTxnStatus");
                
                var orderDict = [String : Any]()
                
                orderDict["MID"] = "SmileP27807646019839";//paste here your merchant id   //mandatory
                orderDict["CHECKSUMHASH"] = checksumHsh; // paste here channel id                       // mandatory
                orderDict["ORDER_ID"] = orderIDSTr;//paste industry type              //mandatory
                
                
                if let theJSONData = try? JSONSerialization.data(
                    withJSONObject: orderDict,
                    options: []) {
                    let theJSONText = String(data: theJSONData,
                                             encoding: .ascii)
                    print("JSON string = \(theJSONText!)")
                    
                    url = self.addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "JsonData", value: theJSONText)])
                    
                    
                }
                
                
                print("StatusCheckURL_Final",url)
                if (url != nil) {
                    
                    var request = URLRequest(url: url!)
                    
                    //     print("createOrderUrl",url)
                    
                    request.httpMethod = "GET"
                    
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                        
                        
                        guard let data = data, error == nil else
                        {
                            print(error?.localizedDescription as Any )
                            return
                        }
                        do{
                            guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                            print("createOrderJson",createOrderJson)
                            
                            guard let paytmOrderID = createOrderJson["ORDERID"] as? String else{
                                return
                            }
                            
                            let orderIDSTr = UserDefaults.standard.value(forKey: "orderid") as! String
                            
                            print("paytmOrderID",paytmOrderID)
                            //self.orderPaymentSuccess(order_id: orderIDSTr, payTmorderID: paytmOrderID)
                            
                            //                            DispatchQueue.main.async {
                            //                                SVProgressHUD.dismiss()
                            //                            }
                            

                            DispatchQueue.main.async {
                                UIAlertView.init(title: "Payment Successful", message: "", delegate: self, cancelButtonTitle: "OK").show()
                                
                                self.orderCheckoutPaymentUsingGateway()


                            }

                            
                        }catch let jsonErr{
                            
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            }
                            print(jsonErr.localizedDescription)
                        }
                    }
                    task.resume()
                }
            }
            
            
        }
        
        
    }//class
    
    
    
    extension ReviewOrderViewController : UITableViewDataSource{
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return self.cartCheckOutResponseArray.count
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            checkoutIdIndexPath = indexPath.row
            let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as! ReviewProductsTableViewCell
            cell.reviewDeleteCartBtnActionRef.addTarget(self, action: #selector(ReviewOrderViewController.cartCheckoutDeleteBtnHandle), for: .touchUpInside)
            cell.selectionStyle = .none
            cell.reviewDeleteCartBtnActionRef.tag = indexPath.row
            cell.reviewOrderDate.text = "DD:" + (self.cartCheckOutResponseArray[indexPath.row]?.order_date)!
            cell.reviewOrderDelieveryCharges.text = "Delievery Charge Rs." +  (self.cartCheckOutResponseArray[indexPath.row]?.deliverycharge)!
            cell.reviewOrderProductName.text =  self.cartCheckOutResponseArray[indexPath.row]?.product_name
            cell.reviewOrderProductUnits.text =  self.cartCheckOutResponseArray[indexPath.row]?.product_units
            cell.reviewOrderPricew.text =  "Rs." + (self.cartCheckOutResponseArray[indexPath.row]?.product_org_price)!
            cell.reviewQuantityTotalPrice.text =
                "Rs." + (self.cartCheckOutResponseArray[indexPath.row]?.current_total)!
            cell.reviewOrderImageView.setImage(from: (self.cartCheckOutResponseArray[indexPath.row]?.product_image)!
                
            )
            
            return cell
            
        }
    }
    
    extension String {
        var floatValue: Float {
            return (self as NSString).floatValue
        }
    }
    
    /*all actions related to transaction are catched here*/
    extension ReviewOrderViewController : PGTransactionDelegate{
        func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
            print(responseString)
            //self.showAlert(title: "hurrayy", message: "transaction done")
            controller.dismiss(animated: true) {
//                guard  let orderid = UserDefaults.standard.value(forKey: "orderid") as? NSNumber else{
//                    return
//                }
                self.checksumTransactValue = true
                
                self.statusCheckService()
                // self.presentAlertWithTitle(title: "Payment Successful", message: "Your order placed successfully")
            }
            
        }
        
        func didCancelTrasaction(_ controller: PGTransactionViewController!) {
            print("Cancelled")
            self.dismiss(animated: (controller != nil), completion: nil)

        }
        
        func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
            print(error)
            self.dismiss(animated: (controller != nil), completion: nil)

        }
        
    }
    
