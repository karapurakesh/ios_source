    //
    //  CartViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 02/08/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    
    import UIKit
    import SVProgressHUD
    import Font_Awesome_Swift
    typealias CompletionHandler = (_ success:Bool) -> Void
    
    class CartViewController: UIViewController,RazorpayPaymentCompletionProtocol,UITableViewDelegate,UITableViewDataSource,BEMCheckBoxDelegate {
        
        @IBOutlet weak var subscriptionsTotalAmount: UILabel!
        
        @IBOutlet weak var subscriptionsCount: UILabel!
        
        @IBOutlet weak var subscribeViewHtConst: NSLayoutConstraint!
        @IBOutlet weak var arrowImageView: UIImageView!
        @IBOutlet weak var subscribeView: UIView!
        var newsProduct = ""
        var paperProductPrice = ""
        
        var merchant:PGMerchantConfiguration!
        
        //  @IBOutlet weak var paytmTotalAmount: UILabel!
        //  @IBOutlet weak var paytmBtn: UIButton!
        var smileyCashEnable : String?
        @IBOutlet var shopNowBtnReference: UIButton!
        private  var razorpay : Razorpay!
        var rowIndex : Int?
        var sender : UIButton?
        var  buttonTag : Int!
        var totalValue : Double?
        var paymentActionVal :Bool = false
        var checksumTransactValue :Bool = false
        var tapGesture = UITapGestureRecognizer()
        
        @IBOutlet var useSmileyCashCheckBox: BEMCheckBox!
        var bemCheckBox : BEMCheckBox!
        @IBOutlet weak var smileyCashAmountLabel: UILabel!
        @IBOutlet weak var netProductAmount : UILabel!
        var cartUserId = UserDefaults.standard.value(forKey: "id") as? String
        @IBOutlet weak var cartProductsNotFound: UIView!
        @IBOutlet weak var cartProductTableView: UITableView!
        @IBOutlet weak var cartProductSmileyCashView : UIView!
        @IBOutlet weak var cartProductRazorpayView : UIView!
        @IBOutlet weak var smileyCashReferenceButton : UIButton!
        var cartListProductsArray:[CartListProductResponse?] = []
        var cartListInstantProductsArray:[CartListProductResponse?] = []
        
        var subOpened : Bool = true
        override func viewDidLoad() {
            super.viewDidLoad()
            
            print("paperProductPrice",paperProductPrice)
            print("newsProduct",newsProduct)
            
            
            tapGesture = UITapGestureRecognizer(target: self, action: #selector(CartViewController.myviewTapped(_:)))
            tapGesture.numberOfTapsRequired = 1
            tapGesture.numberOfTouchesRequired = 1
            subscribeView.addGestureRecognizer(tapGesture)
            subscribeView.isUserInteractionEnabled = true
            
            
            //rightCartButtonItem.removeBadge()
            self.paymentGatewayDetails()
            shopNowBtnReference.layer.cornerRadius = 22
            shopNowBtnReference.layer.masksToBounds = true
            self.cartProductTableView.isHidden = true
            self.cartProductsNotFound.isHidden = true
            self.cartProductSmileyCashView.isHidden = true
            self.cartProductRazorpayView.isHidden = true
            // paytmBtn.isHidden = true
            // paytmTotalAmount.isHidden = true
            self.cartProductTableView.separatorStyle = .none
            self.cartlistNetworkCalls()
            navigationBarAppearance()
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            
            let image : UIImage = UIImage(named:"downArrow")!
            arrowImageView.image = image
            
            setMerchant()
            
            if self.cartListProductsArray.count == 0 {
                self.subscribeViewHtConst.constant = 0
                arrowImageView.isHidden = true
                
            }else {
                self.subscribeViewHtConst.constant = 55
                arrowImageView.isHidden = false
               

            }
            
        }
        
        func myviewTapped(_ sender: UITapGestureRecognizer) {
            
            //            if self.subOpened == true {
            //                self.subOpened = true
            //            }else{
            //                self.subOpened = false
            //            }
            //
           
            
            if self.subOpened != true {
                self.subOpened = false
                let image : UIImage = UIImage(named:"downArrow")!
                arrowImageView.image = image
            }else{
                let image : UIImage = UIImage(named:"upArrow")!
                arrowImageView.image = image
            }
            self.cartProductTableView.reloadData()
            self.subscriptionsCount.text = String(self.cartListProductsArray.count)
            
            
        }
        
        func setMerchant(){
            merchant  = PGMerchantConfiguration.default()!
            //user your checksum urls here or connect to paytm developer team for this or use default urls of paytm
            merchant.checksumGenerationURL = "http://smileyserve.com/beta/generateChecksum.php";
            merchant.checksumValidationURL = "http://smileyserve.com/beta/verifyChecksum.php";
            
            // Set the client SSL certificate path. Certificate.p12 is the certificate which you received from Paytm during the registration process. Set the password if the certificate is protected by a password.
            merchant.clientSSLCertPath = nil; //[[NSBundle mainBundle]pathForResource:@"Certificate" ofType:@"p12"];
            merchant.clientSSLCertPassword = nil; //@"password";
            
            //configure the PGMerchantConfiguration object specific to your requirements
            /*  merchant.merchantID = "Smiley32368638707578";//paste here your merchant id  //mandatory
             merchant.website = "APP_STAGING";//mandatory
             merchant.industryID = "Retail";//mandatory
             merchant.channelID = "WAP"; //provided by PG WAP //mandatory*/
            
            //Prod
            merchant.merchantID = "SmileP27807646019839";//paste here your merchant id  //mandatory
            merchant.website = "SmilePWAP";//mandatory
            merchant.industryID = "Retail109";//mandatory
            merchant.channelID = "WEB"; //provided by PG WAP //mandatory
            
        }
        
        
        
        func navigationBarAppearance(){
            navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
            self.navigationController?.navigationBar.tintColor =  UIColor.white
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
            
            
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: nil, style: .plain, target: self, action: #selector(clearCartConfirmation))
            
            self.navigationItem.rightBarButtonItem?.title = "Clear Cart"
            
            self.title = "Cart"
            
        }
        
        
        
        func handleNavigationArrow(){
            DispatchQueue.main.async {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                let signupNavigation = UINavigationController(rootViewController: signupController)
                self.present(signupNavigation, animated: false, completion: nil)
            }
        }
        
        func cartlistNetworkCalls(){
            if Reachability.isConnectedToNetwork() {
                if let userId = cartUserId{
                    self.cartList(userId: userId)
                    self.smileyCash()
                    useSmileyCashCheckBox.delegate  = self
                }
            }else{
                self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
            }
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
        }
        
        /* Creating Button Action For SmileyCash Alert Dialog */
        
        @IBAction func showAlertSmileyCashBtnAction(_ sender: UIButton) {
            self.sender = UIButton()
            
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if section == 0 {
                
                if self.cartListInstantProductsArray.count == 0 {
                    return self.cartListProductsArray.count

                }else {

                    if self.subOpened == false {
                        self.subOpened = true
                        return 0

                    }else {
                        self.subOpened = false
                        return self.cartListProductsArray.count

                    }
                }
                
            }else{
                return self.cartListInstantProductsArray.count
            }
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {

            return 2
            
        }
        
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if indexPath.section == 0 {
                return 148
            }else {
                return 100
            }
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            rowIndex = indexPath.row
            
            if indexPath.section == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "cartCell", for: indexPath) as! CartProductTableViewCell
                cell.selectionStyle = .none
                cell.clipsToBounds = true
                cell.cartDeleteBTnRef.addTarget(self, action: #selector(CartViewController.cartDeleteBtnHandle), for: .touchUpInside)
                cell.cartDeleteBTnRef.tag = indexPath.row
                if self.cartListProductsArray.count > 0 {
                    cell.cartProductName.text = self.cartListProductsArray[indexPath.row]?.product_name
                    cell.cartProductQuantity.text = "Qty:" + (self.cartListProductsArray[indexPath.row]?.qty)!
                    cell.cartProductUnits.text = self.cartListProductsArray[indexPath.row]?.product_units
                    
                    let dcValue = self.cartListProductsArray[indexPath.row]?.deleiverycharge!
                    if dcValue != "0"{
                        
                        cell.cartProductPrice.text = "Rs." + (self.cartListProductsArray[indexPath.row]?.product_org_price)! + "      Delievery Charge : Rs." +  (self.cartListProductsArray[indexPath.row]?.deleiverycharge)!
                        
                    }else {
                        cell.cartProductPrice.text = "Rs." + (self.cartListProductsArray[indexPath.row]?.product_org_price)!
                    }
                    
                    
                    cell.cartProductImage.setImage(from: (self.cartListProductsArray[indexPath.row]?.product_image)!)
                    cell.cartFromDate.text = "From:" + convertToShowFormatDate(dateString:(self.cartListProductsArray[indexPath.row]?.start_date)! ) +    "    To:" + convertToShowFormatDate(dateString: (self.cartListProductsArray[indexPath.row]?.enddate)!)
                    
                    if let days = [(self.cartListProductsArray[indexPath.row]?.days)] as? [String]{
                        let dayName = days
                        var stringArray = [String]()
                        for day in dayName{
                            stringArray = day.components(separatedBy: ",")
                            for String in stringArray{
                                switch String {
                                case "7":
                                    cell.sundayBtnRef.backgroundColor =   UIColor.blue
                                    break
                                case "1":
                                    cell.mondayBtnRef.backgroundColor =   UIColor.blue
                                    break
                                case "2":
                                    cell.tuesdayBtnRef.backgroundColor =  UIColor.blue
                                    break
                                case "3":
                                    cell.wednesdayBtnRef.backgroundColor = UIColor.blue
                                    break
                                case "4":
                                    cell.thursdayBtnRef.backgroundColor = UIColor.blue
                                    break
                                case "5":
                                    cell.fridayBtnRef.backgroundColor = UIColor.blue
                                    break
                                case "6":
                                    cell.saturdayBtnRef.backgroundColor = UIColor.blue
                                    break
                                default:
                                    print("no days found")
                                }
                            }
                        }
                    }
                }
                
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "instantCell", for: indexPath) as! CartInsatntProductCell
                cell.clipsToBounds = true
                
                cell.selectionStyle = .none
                cell.incBtn.addTarget(self, action: #selector(CartViewController.increaseInstantQuantity), for: .touchUpInside)
                cell.incBtn.tag = indexPath.row
                
                cell.decBtn.addTarget(self, action: #selector(CartViewController.decreaseInstantQuantity), for: .touchUpInside)
                
                cell.decBtn.tag = indexPath.row
                
                if self.cartListInstantProductsArray.count > 0 {
                    cell.cartProductName.text = self.cartListInstantProductsArray[indexPath.row]?.product_name
                    //cell.cartProductQuantity.text = "Qty:" + (self.cartListProductsArray[indexPath.row]?.qty)!
                    cell.cartProductUnits.text = self.cartListInstantProductsArray[indexPath.row]?.product_units
                    // cell.deliveryDate.text = self.cartListInstantProductsArray[indexPath.row]?.enddate
                    
                    let dcValue = self.cartListInstantProductsArray[indexPath.row]?.deleiverycharge!
                    if dcValue != "0"{
                        
                        cell.cartProductPrice.text = "Rs." + (self.cartListInstantProductsArray[indexPath.row]?.total_price)! + "      Delievery Charge : Rs." +  (self.cartListInstantProductsArray[indexPath.row]?.deleiverycharge)!
                        
                    }else {
                        cell.cartProductPrice.text = "Rs." + (self.cartListInstantProductsArray[indexPath.row]?.total_price)!
                    }
                    
                    cell.qauntityValueBtn.setTitle((self.cartListInstantProductsArray[indexPath.row]?.qty)!, for: .normal)
                    cell.cartProductImage.setImage(from: (self.cartListInstantProductsArray[indexPath.row]?.product_image)!)
                    
                }
                return cell
                
                
            }
            
        }
        
        
        func increaseInstantQuantity(_sender:UIButton) {
            print("increaseInstantQuantity")
            let buttonTag = _sender.tag
            let myIndexPath = IndexPath(row: buttonTag, section: 1)
            
            let cell = cartProductTableView.cellForRow(at: myIndexPath) as! CartInsatntProductCell
            
            let cartId = (self.cartListInstantProductsArray[buttonTag]?.cartid)!
            
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let changeQuantityUrl =   NSURL(string:Constants.URL_REQUEST_INSTANT_CHANGE_QNTY){
                var request = URLRequest(url:changeQuantityUrl as URL)
                
                let changeQuantityListParams = ["cartid":cartId,"action":"inc","userid": cartUserId!] as [String:Any]
                print(changeQuantityListParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: changeQuantityListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let changeQuantityJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print("changeQuantityJson",changeQuantityJson)
                        if (changeQuantityJson["code"] as! NSNumber == 200) {
                            DispatchQueue.main.async {
                                
                                let items_count = changeQuantityJson["items_count"] as! String
                                
                                cell.qauntityValueBtn.setTitle(items_count, for: .normal)
                                
                                let itemsQuantity = changeQuantityJson["item_total_price"] as! String
                                cell.cartProductPrice.text = "Rs." + itemsQuantity
                                
                                
                                let result  = changeQuantityJson["total_price"] as! String
                                
                                let doubleValue = result.floatValue.rounded()
                                print("floatValue", String(doubleValue))
                                self.netProductAmount.text = "Rs." + String(doubleValue)
                                UserDefaults.standard.set(String(doubleValue), forKey: "totalProductValue")
                                
                                
                            }
                        } else{
                            
                            DispatchQueue.main.async {
                                
                            }
                            
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        func decreaseInstantQuantity(_sender:UIButton) {
            let buttonTag = _sender.tag
            let myIndexPath = IndexPath(row: buttonTag, section: 1)
            
            let cell = cartProductTableView.cellForRow(at: myIndexPath) as! CartInsatntProductCell
            
            let cartId = (self.cartListInstantProductsArray[buttonTag]?.cartid)!
            
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            if let changeQuantityUrl =   NSURL(string:Constants.URL_REQUEST_INSTANT_CHANGE_QNTY){
                var request = URLRequest(url:changeQuantityUrl as URL)
                
                let changeQuantityListParams = ["cartid":cartId,"action":"dec","userid": cartUserId!] as [String:Any]
                print(changeQuantityListParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: changeQuantityListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let changeQuantityJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print(changeQuantityJson)
                        if (changeQuantityJson["code"] as! NSNumber == 200) {
                            DispatchQueue.main.async {
                                
                                let result  = changeQuantityJson["total_price"] as! String
                                
                                let doubleValue = result.floatValue.rounded()
                                print("floatValue", String(doubleValue))
                                self.netProductAmount.text = "Rs." + String(doubleValue)
                                
                                UserDefaults.standard.set(String(doubleValue), forKey: "totalProductValue")
                                
                                
                                let itemsQuantity = changeQuantityJson["item_total_price"] as! String
                                cell.cartProductPrice.text = "Rs." + itemsQuantity
                                
                                
                                let items_count = changeQuantityJson["items_count"] as! String
                                print("items_count",items_count)
                                
                                
                                if items_count == "0" {
                                    self.buttonTag = _sender.tag
                                    cell.qauntityValueBtn.setTitle(items_count, for: .normal)
                                    
                                    self.InstantcartDelete()
                                    
                                }else{
                                    cell.qauntityValueBtn.setTitle(items_count, for: .normal)
                                }
                            }
                        } else{
                            
                            DispatchQueue.main.async {
                                
                            }
                            
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
            
        }
        
        /* To show date in string format */
        
        func convertToShowFormatDate(dateString: String) -> String {
            
            let dateFormatterDate = DateFormatter()
            dateFormatterDate.dateFormat = "yyyy-MM-dd" //Your date format
            let serverDate: Date = dateFormatterDate.date(from: dateString)! //according to date format your date string
            let dateFormatterString = DateFormatter()
            dateFormatterString.dateFormat = "dd-MM-yyyy" //Your New Date format as per requirement change it own
            let newDate: String = dateFormatterString.string(from: serverDate) //pass Date here
            print(newDate) // New formatted Date string
            return newDate
            
            
        }
        
        /* method for order button based on indexpath*/
        func cartDeleteBtnHandle(_sender:UIButton) {
            buttonTag = _sender.tag
            print(buttonTag)
            self.showAlertDialog(indexPath: buttonTag)
        }
        
        func insatnatCartDeleteBtnHandle(_sender:UIButton) {
            buttonTag = _sender.tag
            print(buttonTag)
            self.showAlertDialogForInstatOrder(indexPath: buttonTag)
        }
        
        
        func showDaysOrderedInCart(_sender:UIButton){
            let days = UserDefaults.standard.value(forKey: "days") as? String
            print(days!)
            let buttonDaysTag = _sender.tag
            print(buttonDaysTag)
        }
        
        @IBAction func razorpayBtnAction(_ sender: Any) {
            DispatchQueue.main.async {
                self.proceedToPayWithPaymentGateway()
            }
            DispatchQueue.main.async {
                self.razorpay = Razorpay.initWithKey("rzp_live_2c8aqqKX844kbZ", andDelegate: self)
                //  self.razorpay = Razorpay.initWithKey("rzp_test_uJmv0AK2JLwkj0", andDelegate: self)
                
                self.showPaymentForm()
                
            }
        }
        
        //
        //  @IBAction func paymentGatewayButton(_ sender: Any) {
        //
        //  self.razorpay = Razorpay.initWithKey("rzp_test_uJmv0AK2JLwkj0", andDelegate: self)
        //   showPaymentForm()
        //  }
        
        /* Function performing checkbox active and inactive */
        
        func didTap(_ checkBox: BEMCheckBox) {
            
            print("check box :\(checkBox.tag):\(checkBox.on)")
            bemCheckBox = checkBox
            //    var totalProductValue = 0
            var number = [Double]()
            if let smileyCashAmount = (UserDefaults.standard.value(forKey: "smileyCashValue") as? String) {
                if  let totalCartPriceArray = UserDefaults.standard.array(forKey: "total_price") as? [String] {
                    for totalpriceValue in totalCartPriceArray{
                        if let wholePrice = Double(totalpriceValue) {
                            number.append(wholePrice)
                        }
                    }
                    
                    
                    let numberPrice  = number.reduce(0, {sum,number in sum+number})
                    let totalProductValue = String(numberPrice)
                    UserDefaults.standard.set(totalProductValue, forKey: "totalProductValue")
                    //  let orderAmount = UserDefaults.standard.value(forKey: "cartTotalPrice") as? String
                    if let smileyCash = Double(smileyCashAmount){
                        if let totalProductPrice = Double(totalProductValue){
                            if checkBox.on {
                                if smileyCash  >= totalProductPrice {
                                    smileyCashEnable = "1"
                                    self.createSmileyCashPayDialog(title:"Thank You",message:"Use Smiley Cash")
                                    self.cartProductRazorpayView.isHidden = true
                                    
                                }else{
                                    print("please use payment gateway your smileycash amount is low to pay the order")
                                    self.cartProductRazorpayView.isHidden = false
                                    self.netProductAmount.text = "Rs." + String(totalProductPrice-smileyCash)
                                    UserDefaults.standard.set(totalProductPrice-smileyCash, forKey: "totalValue")
                                    // self.paytmTotalAmount.text = "Rs." + String(totalProductPrice-smileyCash)
                                    smileyCashEnable = "0"
                                }
                            }else{
                                self.bemCheckBox = nil
                                self.cartProductRazorpayView.isHidden = false
                                self.netProductAmount.text = "Rs." +  String(totalProductPrice)
                                //  self.paytmTotalAmount.text = "Rs." +  String(totalProductPrice)
                            }
                        }
                    }
                }
            }
        }
        
        /* Razorpay paymentform calling Method*/
        
        func showPaymentForm() {
            DispatchQueue.main.async {
                if let totalAmountValue = UserDefaults.standard.value(forKey: "totalProductValue") as? String {
                    if  let convertedIntTotalValue = Float(totalAmountValue) {
                        let convertedCurrencyValue = Int(convertedIntTotalValue) * 100
                        if self.bemCheckBox != nil{
                            if  let toatlProductReducedVaue = UserDefaults.standard.value(forKey: "totalValue") as? Double{
                                let convertedValue = Int(toatlProductReducedVaue) * 100
                                let options = ["amount":convertedValue,"payment_capture":"1","currency":"INR",             "image": UIImage(named:"512x512.png") as Any, "name": "SmileyServe", "description": UserDefaults.standard.value(forKey: "name")!, "prefill": ["email": UserDefaults.standard.value(forKey: "email") as? String, "contact": UserDefaults.standard.value(forKey: "mobile")], "theme": ["color": "#206AE0"]] as [String : Any]
                                self.razorpay.open(options)
                            }
                        }else{
                            let options = ["amount":convertedCurrencyValue,"payment_capture":"1","currency":"INR",
                                           "image": UIImage(named:"512x512.png") as Any, "name": "SmileyServe", "description": UserDefaults.standard.value(forKey: "name")!, "prefill": ["email": UserDefaults.standard.value(forKey: "email") as? String, "contact": UserDefaults.standard.value(forKey: "mobile")], "theme": ["color": "#206AE0"]] as [String : Any]
                            self.razorpay.open(options)
                            
                        }
                    }
                }
            }
        }
        
        func onPaymentSuccess(_ payment_id: String) {
            guard  let orderid = UserDefaults.standard.value(forKey: "orderid") as? NSNumber else{
                return
            }
            self.orderPaymentSuccess(order_id: "\(String(describing: orderid))", payTmorderID: "")
            
            self.presentAlertWithTitle(title: "Payment Successful", message: "Your order placed successfully")
        }
        
        func onPaymentError(_ code: Int32, description str: String) {
            guard  let orderid = UserDefaults.standard.value(forKey: "orderid") as? NSNumber else{
                return
            }
            
            self.orderPaymentFail(order_id: "\(String(describing: orderid))")
            
            //UIAlertView.init(title: "Error", message: str, delegate: self, cancelButtonTitle: "OK").show()
            //  self.presentAlertWithTitle(title: "Error", message: "Your order not placed")
        }
        @IBAction func backToHomeScreen(_ sender: Any) {
            //    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //    let orderController = storyBoard.instantiateViewController(withIdentifier: "Order") as! OrderViewController
            //    self.present(orderController, animated: true, completion: nil)
            //    self.dismiss(animated: false, completion: nil)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let signupNavigation = UINavigationController(rootViewController: signupController)
            self.present(signupNavigation, animated: true, completion: nil)
        }
        @IBAction func shopNow(_ sender: UIButton) {
            DispatchQueue.main.async {
                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                let signupNavigation = UINavigationController(rootViewController: signupController)
                self.present(signupNavigation, animated: true, completion: nil)
            }
        }
        func showAlertDialog(indexPath:Int){
            let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to delete this item?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                if Reachability.isConnectedToNetwork(){
                    self.cartDelete()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                        
                    }
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        func showAlertDialogForInstatOrder(indexPath:Int){
            let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to delete this item?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                if Reachability.isConnectedToNetwork(){
                    self.InstantcartDelete()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                        
                    }
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        func cartList(userId:String){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_CARTLIST + userId) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    
                    self.cartListProductsArray = [CartListProductResponse]()
                    do {
                        
                        guard let cartListData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        print("cartListData",cartListData)
                        
                        if (cartListData["code"] as! NSNumber == 200) {
                            let result  = (cartListData["cart_result"] as? [[String: AnyObject]])!.filter({ $0["total_price"] != nil }).map({ $0["total_price"]! })
                            print("CartList_Result:",result)
                            print("AMOUNT",cartListData["subscriptions_total_amount"] ?? "EMPTY")
                            
                            if let totalValue = cartListData["subscriptions_total_amount"] as? NSNumber {
                                
                                DispatchQueue.main.async {
                                    self.subscriptionsTotalAmount.text =   "Rs." + totalValue.stringValue
                                }
                            }
                            
                            UserDefaults.standard.set(result, forKey: "total_price")
                            if let cartTotalPrice = UserDefaults.standard.array(forKey: "total_price")  as? [String] {
                                var numberConverterArray = [Float]()
                                for totalpriceValue in cartTotalPrice{
                                    if totalpriceValue == "nil"{
                                        print("totalpriceValue is nil")
                                    }else{
                                        if let cost = Float(totalpriceValue) {
                                            numberConverterArray.append(cost)
                                        }
                                    }
                                }
                                let numberPrice  = numberConverterArray.reduce(0, {sum,number in sum+number})
                                let totalProductValue = String(numberPrice.rounded(.toNearestOrAwayFromZero))
                                UserDefaults.standard.set(totalProductValue, forKey: "totalProductValue")
                                if let totalProductPrice =  UserDefaults.standard.value(forKey: "totalProductValue") as? String{
                                    DispatchQueue.main.async {
                                        self.netProductAmount.text = "Rs." + totalProductPrice
                                        // self.paytmTotalAmount.text = "Rs." + totalProductPrice
                                        
                                    }
                                }
                            }
                            let daysresult  = (cartListData["cart_result"] as? [[String: AnyObject]])!.filter({ $0["days"] != nil }).map({ $0["days"]! })
                            print(daysresult
                            )
                            UserDefaults.standard.set(daysresult, forKey: "daysresult")
                            if let cartListProductResult = cartListData["cart_result"] as? [[String: AnyObject]] {
                                for cartListProductResponse in cartListProductResult {
                                    var cartListProducts = CartListProductResponse()
                                    guard let cartProductName = cartListProductResponse["product_name"] as? String,let
                                        cartProductImage = cartListProductResponse["product_image"] as? String,let cartProductUnits = cartListProductResponse["product_units"] as? String,let cartProductPrice = cartListProductResponse["product_price"] as? String,let cartProductDeleiveryCharges = cartListProductResponse["deliverycharge"] as?String,let cartProductQuantity =  cartListProductResponse["qty"] as? String,let cartStartDate = cartListProductResponse["start_date"] as? String,let cartEndDate = cartListProductResponse["enddate"] as? String ,let cartProductId = cartListProductResponse["productid"] as? String,let cartTotalPrice = cartListProductResponse["total_price"] as? String,let days = cartListProductResponse["days"] as? String,let product_org_price = cartListProductResponse["product_org_price"] as? String, let product_sellingType = cartListProductResponse["selling_type"] as? String, let cartId = cartListProductResponse["cartid"] as? String, let subscribed_type = cartListProductResponse["subscribed_type"] as? String else{
                                            return
                                    }
                                    
                                    cartListProducts.subscribed_type = subscribed_type
                                    cartListProducts.cartid = cartId
                                    cartListProducts.product_name = cartProductName
                                    cartListProducts.product_image = cartProductImage
                                    cartListProducts.product_price = cartProductPrice
                                    cartListProducts.product_units = cartProductUnits
                                    cartListProducts.deleiverycharge = cartProductDeleiveryCharges
                                    cartListProducts.qty = cartProductQuantity
                                    cartListProducts.enddate = cartEndDate
                                    cartListProducts.start_date = cartStartDate
                                    cartListProducts.productid = cartProductId
                                    cartListProducts.product_org_price = product_org_price
                                    cartListProducts.total_price = cartTotalPrice
                                    cartListProducts.days = days
                                    cartListProducts.product_sellingType = product_sellingType
                                    //   cartListProducts.cartid = cartCartId
                                    
                                    if subscribed_type == "2"{
                                        self.cartListInstantProductsArray.append(cartListProducts)
                                        
                                    }else{
                                        self.cartListProductsArray.append(cartListProducts)
                                        
                                    }
                                    UserDefaults.standard.set( cartListProducts.total_price , forKey: "cartTotalPrice")
                                    UserDefaults.standard.set( cartListProducts.productid, forKey: "cartProductId")
                                    UserDefaults.standard.set( cartListProducts.product_name, forKey: "product_name")
                                    UserDefaults.standard.set( cartListProducts.days, forKey: "days")
                                    DispatchQueue.main.async {
                                        self.cartProductTableView.reloadData()
                                        self.subscriptionsCount.text = String(self.cartListProductsArray.count)
                                    }
                                }
                            }
                            
                            DispatchQueue.main.async {
                                
                                //hideChange
                                if self.cartListProductsArray.count == 0 {
                                    self.subscribeViewHtConst.constant = 0
                                    self.arrowImageView.isHidden = true

                                }else {
                                    self.subscribeViewHtConst.constant = 55
                                    self.arrowImageView.isHidden = false

                                }
                                
                                
                                if self.cartListProductsArray.count > 0 || self.cartListInstantProductsArray.count > 0{
                                    self.cartProductTableView.reloadData()
                                    self.subscriptionsCount.text = String(self.cartListProductsArray.count)
                                    
                                    self.cartProductTableView.isHidden = false
                                    self.cartProductSmileyCashView.isHidden = false
                                    self.cartProductRazorpayView.isHidden = false
                                    self.cartProductsNotFound.isHidden = true
                                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                                    
                                    // self.paytmBtn.isHidden = false
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                if self.cartListProductsArray.count == 0{
                                    self.cartProductsNotFound.isHidden = false
                                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                                }
                            }
                        }
                    }
                    catch let jsonErr {
                        print(jsonErr.localizedDescription)
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To fetch Data", duration: 3.0, position: .center)
                            
                            
                        }
                    }
                    
                })
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To fetch Data", duration: 3.0, position: .center)
                    
                }
            }
        }
        
        func InstantcartDelete() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let cartDeleteUrl =  NSURL(string:Constants.URL_REQUEST_USER_CARTDELETE){
                var  request = URLRequest(url:cartDeleteUrl as URL)
                var cartDeleteModelRef = CartDeleteModel()
                cartDeleteModelRef.userid = cartUserId
                //  cartDeleteModelRef.productid = UserDefaults.standard.value(forKey: "cartProductId") as? String
                cartDeleteModelRef.productid = self.cartListInstantProductsArray[buttonTag]?.productid
                guard let userId = cartDeleteModelRef.userid,let productId =  cartDeleteModelRef.productid else{return}
                let deleteCartParams  = ["userid":userId,"productid":productId] as [String:Any]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: deleteCartParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        let cartDeleteJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        if let cartDeleteResponse = cartDeleteJson as? [String:Any]{
                            print(cartDeleteResponse["description"]!)
                            print(cartDeleteResponse["message"]!)
                            if (cartDeleteResponse["code"] as! NSNumber == 200) {
                                DispatchQueue.main.async {
                                    if self.rowIndex == 0 && self.cartListInstantProductsArray.count == 1{
                                        //  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        //  let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                                        //  let signupNavigation = UINavigationController(rootViewController: signupController)
                                        //  self.present(signupNavigation, animated: false, completion: nil)
                                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
                                        let cartNavigationController = UINavigationController(rootViewController: controller)
                                        self.present(cartNavigationController, animated: false, completion: nil)
                                    }else{
                                        
                                        if self.cartListInstantProductsArray.count > 0 && self.cartListInstantProductsArray.count > self.rowIndex! {
                                            self.cartListInstantProductsArray.remove(at: self.rowIndex!)
                                            self.cartProductTableView!.reloadData()
                                        }
                                        if let userId = self.cartUserId{
                                            self.cartList(userId:userId)
                                        }
                                        self.view.makeToast(cartDeleteResponse["description"]! as? String)
                                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
                                        let cartNavigationController = UINavigationController(rootViewController: controller)
                                        self.present(cartNavigationController, animated: false, completion: nil)
                                        
                                    }
                                }
                                
                            }else {
                                
                                DispatchQueue.main.async {
                                    self.view.makeToast(cartDeleteResponse["description"]! as? String, duration: 3.0, position: .center)
                                    
                                }
                            }
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        func cartDelete() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let cartDeleteUrl =  NSURL(string:Constants.URL_REQUEST_USER_CARTDELETE){
                var  request = URLRequest(url:cartDeleteUrl as URL)
                var cartDeleteModelRef = CartDeleteModel()
                cartDeleteModelRef.userid = cartUserId
                //  cartDeleteModelRef.productid = UserDefaults.standard.value(forKey: "cartProductId") as? String
                cartDeleteModelRef.productid = self.cartListProductsArray[buttonTag]?.productid
                guard let userId = cartDeleteModelRef.userid,let productId =  cartDeleteModelRef.productid else{return}
                let deleteCartParams  = ["userid":userId,"productid":productId] as [String:Any]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: deleteCartParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        let cartDeleteJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        if let cartDeleteResponse = cartDeleteJson as? [String:Any]{
                            print(cartDeleteResponse["description"]!)
                            print(cartDeleteResponse["message"]!)
                            if (cartDeleteResponse["code"] as! NSNumber == 200) {
                                DispatchQueue.main.async {
                                    if self.rowIndex == 0 && self.cartListProductsArray.count == 1{
                                        //  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        //  let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                                        //  let signupNavigation = UINavigationController(rootViewController: signupController)
                                        //  self.present(signupNavigation, animated: false, completion: nil)
                                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
                                        let cartNavigationController = UINavigationController(rootViewController: controller)
                                        self.present(cartNavigationController, animated: false, completion: nil)
                                    }else{
                                        
                                        if self.cartListProductsArray.count > 0 && self.cartListProductsArray.count > self.rowIndex! {
                                            self.cartListProductsArray.remove(at: self.rowIndex!)
                                            self.cartProductTableView!.reloadData()
                                        }
                                        if let userId = self.cartUserId{
                                            self.cartList(userId:userId)
                                        }
                                        self.view.makeToast(cartDeleteResponse["description"]! as? String)
                                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
                                        let cartNavigationController = UINavigationController(rootViewController: controller)
                                        self.present(cartNavigationController, animated: false, completion: nil)
                                        
                                    }
                                }
                                
                            }else {
                                
                                DispatchQueue.main.async {
                                    self.view.makeToast(cartDeleteResponse["description"]! as? String, duration: 3.0, position: .center)
                                    
                                }
                            }
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        
        func clearCartConfirmation(){
            let alertController = UIAlertController(title: "", message: "Are you sure you want to clear the cart?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                
                if Reachability.isConnectedToNetwork(){
                    
                    self.clearCart()
                    
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    }
                }
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        func clearCart() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let cartDeleteUrl =  NSURL(string:Constants.URL_REQUEST_USER_CLEARCART){
                var  request = URLRequest(url:cartDeleteUrl as URL)
                var cartDeleteModelRef = CartDeleteModel()
                cartDeleteModelRef.userid = cartUserId
                //  cartDeleteModelRef.productid = UserDefaults.standard.value(forKey: "cartProductId") as? String
                guard let userId = cartDeleteModelRef.userid else{return}
                let deleteCartParams  = ["userid":userId] as [String:Any]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: deleteCartParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        let cartDeleteJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        if let cartDeleteResponse = cartDeleteJson as? [String:Any]{
                            print(cartDeleteResponse["description"]!)
                            print(cartDeleteResponse["message"]!)
                            if (cartDeleteResponse["code"] as! NSNumber == 200) {
                                DispatchQueue.main.async {
                                    if self.rowIndex == 0 && self.cartListProductsArray.count == 1{
                                        //  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        //  let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                                        //  let signupNavigation = UINavigationController(rootViewController: signupController)
                                        //  self.present(signupNavigation, animated: false, completion: nil)
                                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
                                        let cartNavigationController = UINavigationController(rootViewController: controller)
                                        self.present(cartNavigationController, animated: false, completion: nil)
                                    }else{
                                        
                                        if self.cartListProductsArray.count > 0 && self.cartListProductsArray.count > self.rowIndex! {
                                            self.cartListProductsArray.remove(at: self.rowIndex!)
                                            self.cartProductTableView!.reloadData()
                                        }
                                        
                                        if let userId = self.cartUserId{
                                            self.cartList(userId:userId)
                                        }
                                        self.view.makeToast(cartDeleteResponse["description"]! as? String)
                                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
                                        let cartNavigationController = UINavigationController(rootViewController: controller)
                                        self.present(cartNavigationController, animated: false, completion: nil)
                                        
                                    }
                                }
                                
                            }else {
                                
                                DispatchQueue.main.async {
                                    self.view.makeToast(cartDeleteResponse["description"]! as? String, duration: 3.0, position: .center)
                                    
                                }
                            }
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        /* create a Alert Dialog for displaying smileycashpay */
        
        func createSmileyCashPayDialog(title:String,message: String){
            let alertController = UIAlertController(title: "Confirmation Message", message: "Do you want to pay Amount Using Smiley Cash?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
                self.cartProductRazorpayView.isHidden = false
                self.bemCheckBox.on = !self.bemCheckBox.on
                self.bemCheckBox = nil
                
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                
                if Reachability.isConnectedToNetwork(){
                    self.proceedToPayWithSmileyCash()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    }
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        func smileyCash(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let smileyCash =   NSURL(string:Constants.URL_REQUEST_USER_SMILEYCASH){
                var request = URLRequest(url:smileyCash as URL)
                var cashAmount:SmileyCash = SmileyCash()
                cashAmount.page_number = "1"
                cashAmount.userid = cartUserId
                cashAmount.required_count = "1"
                
                guard let pageNumber =  cashAmount.page_number ,let userId =  cashAmount.userid,let requiredCount =  cashAmount.required_count else{return}
                let cashListParams:[String:Any] = ["userid":userId,"page_number":pageNumber,"required_count":requiredCount]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cashListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        
                        guard let fetchSmileyData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{ return}
                        print(fetchSmileyData)
                        DispatchQueue.main.async {
                            //                            guard let  smileyCashValue:Float = fetchSmileyData["smiley_cash"] as? Float else{return}
                            //                            UserDefaults.standard.set("\(smileyCashValue)", forKey: "smileyCashValue")
                            //                            self.smileyCashAmountLabel.text = "Rs." + "\(smileyCashValue)"
                            
                            if  let  smileyCashValue = fetchSmileyData["smiley_cash"]{
                                
                                UserDefaults.standard.set("\(smileyCashValue)", forKey: "smileyCashValue")
                                
                                let sCash = "Rs. " + "\(smileyCashValue)"
                                
                                self.smileyCashAmountLabel.text = sCash
                                
                            }
                            
                            
                            
                            
                        }
                        if (fetchSmileyData["code"] as! NSNumber == 200) {
                            if let smileyResult = fetchSmileyData["smileycash_result"] as? [[String:AnyObject]]{
                                for smileyResponse in smileyResult {
                                    
                                    if smileyResponse.count > 0 {
                                        guard (smileyResponse["id"] as? String) != nil else{
                                            return
                                        }
                                        print(smileyResponse["id"] as! String)
                                    }
                                }
                            }
                        }else{
                            DispatchQueue.main.async {
                                //   self.view.makeToast(fetchSmileyData["description"]! as! String, duration: 4.0, position: CGPoint(x: 180, y: 340))
                            }
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    
                }
                
            }
        }
        
        func addQueryParams(url: URL, newParams: [URLQueryItem]) -> URL? {
            let urlComponents = NSURLComponents.init(url: url, resolvingAgainstBaseURL: false)
            guard urlComponents != nil else { return nil; }
            if (urlComponents?.queryItems == nil) {
                urlComponents!.queryItems = [];
            }
            urlComponents!.queryItems!.append(contentsOf: newParams);
            return urlComponents?.url;
        }
        
        
        
        
        
        /* Function for paying the order with smiley cash */
        
        func proceedToPayWithSmileyCash(){
            
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let createOrderUrl =   NSURL(string:Constants.URL_REQUEST_USER_CREATEORDER){
                var request = URLRequest(url:createOrderUrl as URL)
                var createOrder:CreateOrderModel = CreateOrderModel()
                createOrder.order_amount = UserDefaults.standard.value(forKey: "cartTotalPrice") as? String
                //  createOrder.smileycash = UserDefaults.standard.value(forKey: "smileyCashValue") as? String
                createOrder.smileycash = "1"
                createOrder.userid = cartUserId
                createOrder.captcha = "Smileyserve"
                guard let smileyCash =  createOrder.smileycash,let userId =  createOrder.userid,let captcha = createOrder.captcha,let orderAmount = createOrder.order_amount else{return}
                let createOrderListParams = ["user_id":userId,"smileycash":smileyCash,"captcha":captcha,"order_amount":orderAmount] as [String:Any]
                print("SC_createOrderListParams",createOrderListParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: createOrderListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print("createOrderJson_Response",createOrderJson)
                        if (createOrderJson["code"] as! NSNumber == 200) {
                            DispatchQueue.main.async {
                                guard let orderid = createOrderJson["order_id"] as? NSNumber else{
                                    
                                    return
                                }
                                self.orderPaymentSuccess(order_id: "\(orderid)", payTmorderID: "")
                                
                            }
                            
                        } else{
                            
                            DispatchQueue.main.async {
                                guard let orderid = createOrderJson["order_id"] as? NSNumber else{
                                    
                                    return
                                }
                                self.orderPaymentFail(order_id:"\(orderid)")
                                
                                UserDefaults.standard.set(orderid, forKey: "orderid")
                            }
                            
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        /* Proceed To Pay With Payment Gateway */
        
        
        func proceedToPayWithPaymentGateway(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let createOrderUrl =   NSURL(string:Constants.URL_REQUEST_USER_CREATEORDER){
                var request = URLRequest(url:createOrderUrl as URL)
                var createOrder:CreateOrderModel = CreateOrderModel()
                if (bemCheckBox != nil){
                    if let totalAmountValue = UserDefaults.standard.value(forKey: "totalValue") as? Double {
                        createOrder.smileycash = "1"
                        createOrder.order_amount = String(totalAmountValue)
                    }
                    
                }else{
                    createOrder.smileycash = "0"
                    createOrder.order_amount =  UserDefaults.standard.value(forKey: "totalProductValue") as? String
                }
                
                createOrder.userid = cartUserId
                createOrder.captcha = "Smileyserve"
                guard let smileyCash =  createOrder.smileycash,let userId =  createOrder.userid,let captcha = createOrder.captcha,let orderAmount = createOrder.order_amount else{return}
                let createOrderListParams = ["user_id":userId,"smileycash":smileyCash,"captcha":captcha,"order_amount":orderAmount] as [String:Any]
                print(createOrderListParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: createOrderListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print(createOrderJson)
                        if (createOrderJson["code"] as! NSNumber == 200) {
                            DispatchQueue.main.async {
                                guard let orderid = createOrderJson["order_id"] as? NSNumber else{
                                    return
                                }
                                UserDefaults.standard.set(orderid, forKey: "orderid")
                                
                                if self.paymentActionVal {
                                    self.checksumHash(completionHandler: { (true) in
                                        
                                        self.createPayment()
                                        
                                    })
                                }
                                
                            }
                        } else{
                            
                            DispatchQueue.main.async {
                                guard let orderid = createOrderJson["order_id"] as? NSNumber else{
                                    return
                                }
                                self.orderPaymentFail(order_id:"\(orderid)")
                                UserDefaults.standard.set(orderid, forKey: "orderid")
                            }
                            
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        func proceedToPayUsingPaymentGateway(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let createOrderUrl =   NSURL(string:Constants.URL_REQUEST_USER_CREATEORDER){
                var request = URLRequest(url:createOrderUrl as URL)
                var createOrder:CreateOrderModel = CreateOrderModel()
                createOrder.order_amount = UserDefaults.standard.value(forKey: "cartTotalPrice") as? String
                createOrder.smileycash = "0"
                createOrder.userid = cartUserId
                createOrder.captcha = "Smileyserve"
                guard let orderAmount =  createOrder.order_amount,let smileyCash = createOrder.smileycash,let userId =  createOrder.userid,let captcha = createOrder.captcha else{return}
                let createOrderListParams:[String:Any] = ["user_id":userId,"smileycash":smileyCash,"captcha":captcha,"order_amount":orderAmount]
                print(createOrderListParams)
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: createOrderListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print(createOrderJson)
                        print(createOrderJson["description"]!)
                        print(createOrderJson["message"]!)
                        if (createOrderJson["code"] as! NSNumber == 200) {
                            DispatchQueue.main.async {
                                guard let orderid = createOrderJson["order_id"] as? NSNumber else{
                                    
                                    return
                                }
                                self.orderPaymentSuccess(order_id: "\(orderid)", payTmorderID: "")
                            }
                            
                        } else{
                            
                            DispatchQueue.main.async {
                                guard let orderid = createOrderJson["order_id"] as? NSNumber else{
                                    
                                    return
                                }
                                self.orderPaymentFail(order_id:"\(orderid)")
                                UserDefaults.standard.set(orderid, forKey: "orderid")
                            }
                            
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    
                }
                
            }
        }
        
        
        func delayToHomeScreenToDisplaySuccessToast(){
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let signupNavigation = UINavigationController(rootViewController: signupController)
            self.present(signupNavigation, animated: true, completion: nil)
            
        }
        
        func paymentGatewayDetails(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_PAYMENTGATEWAYDETAILS ) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    do {
                        
                        guard let paymentGatewayJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        print(paymentGatewayJson)
                        
                    }  catch let jsonErr {
                        
                        print(jsonErr.localizedDescription)
                    }
                    
                })
                task.resume()
            }
        }
        
        func orderPaymentSuccess(order_id : String, payTmorderID : String) {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            var serviceURL = NSURL(string: Constants.URL_REQUEST_USER_ORDERPAYMENTSUCCESS)
            
            if payTmorderID.count > 0 {
                
                let local = Constants.URL_REQUEST_USER_ORDERPAYMENTSUCCESS + order_id
                
                serviceURL = NSURL(string: local.appendingFormat("/%@",payTmorderID))
                
                //serviceURL = local?.a
                
            }else {
                serviceURL = NSURL(string: Constants.URL_REQUEST_USER_ORDERPAYMENTSUCCESS + order_id)
                
            }
            
            
            var urlRequest = URLRequest(url: serviceURL as! URL)
            urlRequest.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                if (error != nil) {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                }
                if let response = response {
                    print(response)
                }
                guard let data = data else {
                    return
                }
                do {
                    
                    guard let orderPaymentSuccessJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                    print("orderPaymentSuccessJson_cart",orderPaymentSuccessJson)
                    if (orderPaymentSuccessJson["code"] as! NSNumber == 200) {
                        DispatchQueue.main.async {
                            self.view.makeToast(orderPaymentSuccessJson["description"]! as? String, duration: 3.0, position: .center)
                            
                            self.perform(#selector(self.delayToHomeScreenToDisplaySuccessToast), with: self, afterDelay: 1)
                        }
                        
                    }else{
                        
                        DispatchQueue.main.async {
                            self.view.makeToast(orderPaymentSuccessJson["description"]! as? String, duration: 3.0, position: .center)  }
                    }
                }catch let jsonErr {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
            })
            task.resume()
        }
        
        func orderPaymentFail(order_id : String) {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_ORDERPAYMENTFAIL + order_id) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    do {
                        
                        guard let orderPaymentFailJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        print(orderPaymentFailJson)
                        if (orderPaymentFailJson["code"] as! NSNumber == 200) {
                            DispatchQueue.main.async {
                                self.view.makeToast(orderPaymentFailJson["description"]! as? String, duration: 3.0, position: .center)
                                self.perform(#selector(self.delayToHomeScreenToDisplaySuccessToast), with: self, afterDelay: 3)
                            }
                            
                        }else{
                            
                            DispatchQueue.main.async {
                                self.view.makeToast(orderPaymentFailJson["description"]! as? String, duration: 3.0, position: .center)
                            }
                        }
                    }catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        
                        print(jsonErr.localizedDescription)
                    }
                })
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
                
            }
        }
        
        
        
        @IBAction func paymentGateWayBtnAction(_ sender: Any) {
            
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let image = UIImage(named: "paytm")
            let imageView = UIImageView()
            imageView.image = image
            imageView.frame =  CGRect(x: 10, y: 0, width: 60, height: 60)
            actionSheetController.view.addSubview(imageView)
            
            
            let image2 = UIImage(named: "razor")
            let imageView1 = UIImageView()
            imageView1.image = image2
            imageView1.frame =  CGRect(x: 10, y: 60, width: 50, height: 50)
            actionSheetController.view.addSubview(imageView1)
            
            
            
            // create an action
            let firstAction: UIAlertAction = UIAlertAction(title: "BHIM/CC/DC/NB", style: .default) { action -> Void in
                
                //   print("First Action pressed")
                self.paymentActionVal = true
                self.proceedToPayWithPaymentGateway()
            }
            
            let secondAction: UIAlertAction = UIAlertAction(title: "Other Wallets", style: .default) { action -> Void in
                
                //  print("Second Action pressed")
                DispatchQueue.main.async {
                    self.proceedToPayWithPaymentGateway()
                }
                DispatchQueue.main.async {
                    self.razorpay = Razorpay.initWithKey("rzp_live_2c8aqqKX844kbZ", andDelegate: self)
                    //  self.razorpay = Razorpay.initWithKey("rzp_test_uJmv0AK2JLwkj0", andDelegate: self)
                    
                    self.showPaymentForm()
                    
                }
            }
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
            
            actionSheetController.addAction(firstAction)
            actionSheetController.addAction(secondAction)
            actionSheetController.addAction(cancelAction)
            
            present(actionSheetController, animated: true, completion: nil)
            
        }
        
        
        func createPayment(){
            
            //            DispatchQueue.main.async {
            //                SVProgressHUD.setDefaultStyle(.dark)
            //                SVProgressHUD.show(withStatus: "please wait...")
            //            }
            
            var orderDict = [String : Any]()
            
            
            if let totalAmountValue = UserDefaults.standard.value(forKey: "totalProductValue") as? String {
                if  let convertedIntTotalValue = Float(totalAmountValue) {
                    let convertedCurrencyValue = Int(convertedIntTotalValue) * 100
                    if self.bemCheckBox != nil{
                        if  let toatlProductReducedVaue = UserDefaults.standard.value(forKey: "totalValue") as? Double{
                            let convertedValue = Float(toatlProductReducedVaue) //* 100
                            //use convertedValue
                            let value = String(describing:convertedValue)
                            
                            orderDict["TXN_AMOUNT"] = value; // amount to charge
                            print("TXN_AMOUNT_Latest",value)
                            
                            //  Prod
                            
                            orderDict["MID"] = "SmileP27807646019839";//paste here your merchant id   //mandatory
                            orderDict["CHANNEL_ID"] = "WAP"; // paste here channel id                       // mandatory
                            orderDict["INDUSTRY_TYPE_ID"] = "Retail109";//paste industry type              //mandatory
                            orderDict["WEBSITE"] = "SmilePWAP";// paste website
                            let orderid = UserDefaults.standard.value(forKey: "orderid") as! NSNumber
                            let orderIDSTr =  String(describing: orderid)
                            orderDict["ORDER_ID"] = orderIDSTr;//change order id every time on new transaction
                            // orderDict["REQUEST_TYPE"] = "DEFAULT";// remain same
                            orderDict["CUST_ID"] = self.cartUserId; // change acc. to your database user/customers
                            orderDict["MOBILE_NO"] = UserDefaults.standard.value(forKey: "mobile");// optional
                            orderDict["EMAIL"] =  UserDefaults.standard.value(forKey: "email"); //optional
                            
                            orderDict["CALLBACK_URL"] = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDSTr)"
                            // orderDict["CALLBACK_URL"] = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
                            let checksumHsh = UserDefaults.standard.value(forKey: "CHECKSUMHASH_RES") as! String
                            
                            
                            //https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=<orderid>
                            
                            print("checksum::",checksumHsh)
                            orderDict["CHECKSUMHASH"] = checksumHsh
                            print("orderDict::",orderDict)
                            
                            DispatchQueue.main.async {
                                let pgOrder = PGOrder(params: orderDict )
                                
                                let canBtn = UIButton.init(frame: CGRect(x: 0, y:20 , width: 100, height: 30))
                                canBtn.setTitle("Cancel", for: .normal)
                                //canBtn.tintColor = UIColor.red
                                
                                let topView = UIView(frame: CGRect(x: 0, y: 15, width: self.view.frame.width, height: 60))
                                topView.backgroundColor = UIColor(red:253.0/255.0, green:109.0/255.0, blue:64/255.0, alpha:1.000)
                                topView.addSubview(canBtn)
                                
                                
                                let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
                                
                                transaction!.serverType = eServerTypeProduction
                                transaction!.merchant = self.merchant
                                transaction!.loggingEnabled = true
                                transaction!.delegate = self
                                transaction?.topBar = topView
                                transaction?.cancelButton =  canBtn
                                
                                self.present(transaction!, animated: true, completion: {
                                    SVProgressHUD.dismiss()
                                    
                                })
                            }
                            
                            
                            
                        }
                    }else{
                        //use convertedCurrencyValue
                        
                        print("TXN_AMOUNTELSE",convertedIntTotalValue)
                        
                        let value = String(describing:convertedIntTotalValue)
                        orderDict["TXN_AMOUNT"] = value; // amount to charge
                        
                        //  Prod
                        
                        orderDict["MID"] = "SmileP27807646019839";//paste here your merchant id   //mandatory
                        orderDict["CHANNEL_ID"] = "WAP"; // paste here channel id                       // mandatory
                        orderDict["INDUSTRY_TYPE_ID"] = "Retail109";//paste industry type              //mandatory
                        orderDict["WEBSITE"] = "SmilePWAP";// paste website
                        let orderid = UserDefaults.standard.value(forKey: "orderid") as! NSNumber
                        let orderIDSTr =  String(describing: orderid)
                        orderDict["ORDER_ID"] = orderIDSTr;//change order id every time on new transaction
                        // orderDict["REQUEST_TYPE"] = "DEFAULT";// remain same
                        orderDict["CUST_ID"] = self.cartUserId; // change acc. to your database user/customers
                        orderDict["MOBILE_NO"] = UserDefaults.standard.value(forKey: "mobile");// optional
                        orderDict["EMAIL"] =  UserDefaults.standard.value(forKey: "email"); //optional
                        
                        orderDict["CALLBACK_URL"] = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDSTr)"
                        // orderDict["CALLBACK_URL"] = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
                        let checksumHsh = UserDefaults.standard.value(forKey: "CHECKSUMHASH_RES") as! String
                        
                        
                        //https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=<orderid>
                        
                        orderDict["CHECKSUMHASH"] = checksumHsh
                        print("orderDict::",orderDict)
                        
                        DispatchQueue.main.async {
                            let pgOrder = PGOrder(params: orderDict )
                            
                            let canBtn = UIButton.init(frame: CGRect(x: 0, y:20 , width: 100, height: 30))
                            canBtn.setTitle("Cancel", for: .normal)
                            //canBtn.tintColor = UIColor.red
                            
                            let topView = UIView(frame: CGRect(x: 0, y: 15, width: self.view.frame.width, height: 60))
                            topView.backgroundColor = UIColor(red:253.0/255.0, green:109.0/255.0, blue:64/255.0, alpha:1.000)
                            topView.addSubview(canBtn)
                            
                            
                            let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
                            
                            transaction!.serverType = eServerTypeProduction
                            transaction!.merchant = self.merchant
                            transaction!.loggingEnabled = true
                            transaction!.delegate = self
                            transaction?.topBar = topView
                            transaction?.cancelButton =  canBtn
                            
                            self.present(transaction!, animated: true, completion: {
                                SVProgressHUD.dismiss()
                                
                            })
                        }
                        
                        
                        
                        
                        
                        
                    }
                }
            }
            
        }
        
        
        
        func prepareChecksum() -> URL{
            
            let orderid = UserDefaults.standard.value(forKey: "orderid") as! NSNumber
            
            let orderIDSTr =  String(describing: orderid)
            
            
            //staging  var url = URL.init(string: "http://smileyserve.com/beta/generateChecksum.php");
            var url = URL.init(string: "http://smileyserve.com/generateChecksum.php"); //prod
            
            if (url != nil) {
                
                
                if checksumTransactValue {
                    
                    let mobileNo = UserDefaults.standard.value(forKey: "mobile") as! String
                    let email = UserDefaults.standard.value(forKey: "email") as! String
                    
                    url = addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "MID", value: "SmileP27807646019839"),
                                                                URLQueryItem.init(name: "CHANNEL_ID", value: "WAP"),
                                                                URLQueryItem.init(name: "INDUSTRY_TYPE_ID", value: "Retail109"),
                                                                URLQueryItem.init(name: "WEBSITE", value: "SmilePWAP"),
                                                                URLQueryItem.init(name: "TXN_AMOUNT", value: ""),
                                                                URLQueryItem.init(name: "ORDER_ID", value: orderIDSTr),
                                                                URLQueryItem.init(name: "CUST_ID", value: self.cartUserId),
                                                                URLQueryItem.init(name: "CALLBACK_URL", value: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDSTr)"),
                                                                URLQueryItem.init(name: "MOBILE_NO", value: mobileNo),
                                                                URLQueryItem.init(name: "EMAIL", value: email)])
                }else {
                    
                    let mobileNo = UserDefaults.standard.value(forKey: "mobile") as! String
                    let email = UserDefaults.standard.value(forKey: "email") as! String
                    
                    
                    if let totalAmountValue = UserDefaults.standard.value(forKey: "totalProductValue") as? String {
                        if  let convertedIntTotalValue = Float(totalAmountValue) {
                            //  let convertedCurrencyValue = Int(convertedIntTotalValue) * 100
                            if self.bemCheckBox != nil{
                                if  let toatlProductReducedVaue = UserDefaults.standard.value(forKey: "totalValue") as? Double{
                                    let convertedValue = Float(toatlProductReducedVaue) //* 100
                                    //use convertedValue
                                    
                                    let finalValue = String(describing:convertedValue)
                                    
                                    print("finalValue",finalValue)
                                    url = addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "MID", value: "SmileP27807646019839"),
                                                                                URLQueryItem.init(name: "CHANNEL_ID", value: "WAP"),
                                                                                URLQueryItem.init(name: "INDUSTRY_TYPE_ID", value: "Retail109"),
                                                                                URLQueryItem.init(name: "WEBSITE", value: "SmilePWAP"),
                                                                                URLQueryItem.init(name: "TXN_AMOUNT", value: finalValue),
                                                                                URLQueryItem.init(name: "ORDER_ID", value: orderIDSTr),
                                                                                URLQueryItem.init(name: "CUST_ID", value: self.cartUserId),
                                                                                URLQueryItem.init(name: "CALLBACK_URL", value: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDSTr)"),
                                                                                URLQueryItem.init(name: "MOBILE_NO", value: mobileNo),
                                                                                URLQueryItem.init(name: "EMAIL", value: email)])
                                }
                            }else{
                                //use convertedCurrencyValue
                                
                                let finalValue = String(describing:convertedIntTotalValue)
                                print("finalValueElse",finalValue)
                                
                                url = addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "MID", value: "SmileP27807646019839"),
                                                                            URLQueryItem.init(name: "CHANNEL_ID", value: "WAP"),
                                                                            URLQueryItem.init(name: "INDUSTRY_TYPE_ID", value: "Retail109"),
                                                                            URLQueryItem.init(name: "WEBSITE", value: "SmilePWAP"),
                                                                            URLQueryItem.init(name: "TXN_AMOUNT", value: finalValue),
                                                                            URLQueryItem.init(name: "ORDER_ID", value: orderIDSTr),
                                                                            URLQueryItem.init(name: "CUST_ID", value: self.cartUserId),
                                                                            URLQueryItem.init(name: "CALLBACK_URL", value: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIDSTr)"),
                                                                            URLQueryItem.init(name: "MOBILE_NO", value: mobileNo),
                                                                            URLQueryItem.init(name: "EMAIL", value: email)])
                                
                            }
                        }
                    }
                }
                
                
            }
            
            return url!
        }
        
        
        func checksumHash(completionHandler: @escaping CompletionHandler) {
            
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            
            let url = prepareChecksum()
            
            print("URLCHECKSUM_",url)
            
            var request = URLRequest(url:url as URL)
            
            print("createOrderUrl",url)
            
            request.httpMethod = "GET"
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                
                guard let data = data, error == nil else
                {
                    print(error?.localizedDescription as Any )
                    return
                }
                do{
                    guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                    
                    guard let checksumHashRes = createOrderJson["CHECKSUMHASH"] as? String else{
                        return
                    }
                    
                    print("CheckSumHash_Rakesh",checksumHashRes)
                    
                    UserDefaults.standard.set(checksumHashRes, forKey: "CHECKSUMHASH_RES")
                    UserDefaults.standard.synchronize()
                    
                    completionHandler(true)
                    
                    //                    DispatchQueue.main.async {
                    //                        SVProgressHUD.dismiss()
                    //                    }
                    
                }catch let jsonErr{
                    
                    completionHandler(false)
                    
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            task.resume()
        }
        
        func statusCheckService() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            
            self.checksumHash { (true) in
                
                let checksumHsh = UserDefaults.standard.value(forKey: "CHECKSUMHASH_RES") as! String
                
                print("checksumHsh_transaction_Success",checksumHsh)
                
                let orderid = UserDefaults.standard.value(forKey: "orderid") as! NSNumber
                
                let orderIDSTr =  String(describing: orderid)
                
                var url = URL.init(string: "https://securegw.paytm.in/merchant-status/getTxnStatus");
                
                var orderDict = [String : Any]()
                
                orderDict["MID"] = "SmileP27807646019839";//paste here your merchant id   //mandatory
                orderDict["CHECKSUMHASH"] = checksumHsh; // paste here channel id                       // mandatory
                orderDict["ORDER_ID"] = orderIDSTr;//paste industry type              //mandatory
                
                
                if let theJSONData = try? JSONSerialization.data(
                    withJSONObject: orderDict,
                    options: []) {
                    let theJSONText = String(data: theJSONData,
                                             encoding: .ascii)
                    print("JSON string = \(theJSONText!)")
                    
                    url = self.addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "JsonData", value: theJSONText)])
                    
                    
                }
                
                
                if (url != nil) {
                    
                    var request = URLRequest(url: url!)
                    
                    //     print("createOrderUrl",url)
                    
                    request.httpMethod = "GET"
                    
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                        
                        
                        guard let data = data, error == nil else
                        {
                            print(error?.localizedDescription as Any )
                            return
                        }
                        do{
                            guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                            print("createOrderJson",createOrderJson)
                            
                            guard let paytmOrderID = createOrderJson["ORDERID"] as? String else{
                                return
                            }
                            
                            let orderid = UserDefaults.standard.value(forKey: "orderid") as! NSNumber
                            let orderIDSTr =  String(describing: orderid)
                            
                            print("paytmOrderID",paytmOrderID)
                            self.orderPaymentSuccess(order_id: orderIDSTr, payTmorderID: paytmOrderID)
                            
                   
                        }catch let jsonErr{
                            
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            }
                            print(jsonErr.localizedDescription)
                        }
                    }
                    task.resume()
                }
            }
            
            
        }
        
        
    }//CartViewController
    
    /* Extension for creating the floating point value */
    extension FloatingPoint {
        
        public static var ulpOfOne: Self {
            return Self(1).ulp
        }
        
        @_transparent
        public func rounded(_ rule: FloatingPointRoundingRule) -> Self {
            var lhs = self
            lhs.round(rule)
            return lhs
        }
        
        @_transparent
        public func rounded() -> Self {
            return rounded(.toNearestOrAwayFromZero)
        }
        @_transparent
        public mutating func round() {
            round(.toNearestOrAwayFromZero)
        }
        
        @_transparent
        public var nextDown: Self {
            return -(-self).nextUp
        }
        
    }
    
    extension UIViewController {
        
        func presentAlertWithTitle(title: String, message : String)
        {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) {
                (action: UIAlertAction) in print("Youve pressed OK Button")
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    /*all actions related to transaction are catched here*/
    extension CartViewController : PGTransactionDelegate{
        func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
            print(responseString)
            //self.showAlert(title: "hurrayy", message: "transaction done")
            controller.dismiss(animated: true) {
                guard  let orderid = UserDefaults.standard.value(forKey: "orderid") as? NSNumber else{
                    return
                }
                self.checksumTransactValue = true
                
                self.statusCheckService()
                // self.presentAlertWithTitle(title: "Payment Successful", message: "Your order placed successfully")
            }
            
        }
        
        func didCancelTrasaction(_ controller: PGTransactionViewController!) {
            print("Cancelled")
            
            self.dismiss(animated: true) {
                guard  let orderid = UserDefaults.standard.value(forKey: "orderid") as? NSNumber else{
                    return
                }
                
                self.orderPaymentFail(order_id: "\(String(describing: orderid))")
            }
            
        }
        
        func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
            print(error)
            self.dismiss(animated: true) {
                guard  let orderid = UserDefaults.standard.value(forKey: "orderid") as? NSNumber else{
                    return
                }
                
                self.orderPaymentFail(order_id: "\(String(describing: orderid))")
            }
        }
        
    }
    
    
