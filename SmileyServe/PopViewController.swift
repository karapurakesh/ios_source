  //
  //  PopViewController.swift
  //  SmileyServe
  //
  //  Created by Apple on 08/08/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //

  import UIKit
  class PopViewController: UIViewController {
  override func viewDidLoad() {
  super.viewDidLoad()
  view.backgroundColor = UIColor.clear
  view.isOpaque = false
      }
  @IBAction func closeBtn(_ sender: Any) {
  self.dismiss(animated: false, completion: nil)
  }
  }
