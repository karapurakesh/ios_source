//
//  CartCheckOutPropertiesResponseModel.swift
//  SmileyServe
//
//  Created by Apple on 27/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct CartCheckOutPropertiesResponseModel {
  
  var total_items : String?
  var total_qty : String?
  var total_price : String?
  var total_product_price : String?
  var total_deliver_charges : String?
  var item_ids : String?
  var order_date : String?
}
