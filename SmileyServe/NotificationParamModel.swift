//
//  NotificationParamModel.swift
//  SmileyServe
//
//  Created by Apple on 09/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct NotificationParamModel{
    
    var userid : String?
    var required_count : String?
    var page_number : String?
    
    init() {
        //
    }
    
    
    init(notParams:NotificationParamModel) {
        
        self.userid           = notParams.userid
        self.page_number      = notParams.page_number
        self.required_count   = notParams.required_count
    }
    
    
}
