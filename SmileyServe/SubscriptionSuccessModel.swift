//
//  SubscriptionSuccessModel.swift
//  SmileyServe
//
//  Created by Apple on 04/09/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation

struct SubscriptionSuccessModel {
  
  var userid : String?
  var checkoutdate : String?
  var enablesmileycash : String?

}
