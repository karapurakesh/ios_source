    //
    //  ContactUsViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 06/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    
    import UIKit
    import SVProgressHUD
    var contactListData:[ContactsModel?] = []
    var subjectId : String?
    var contactListArray = [String]()
    
    class ContactUsViewController: BaseViewController,LBZSpinnerDelegate,UITextViewDelegate{
        
        var strContactMobile = UserDefaults.standard.value(forKey:"mobile") as? String
        var strContactEmail = UserDefaults.standard.value(forKey:"email") as? String
        var strContactName = UserDefaults.standard.value(forKey:"name") as? String
        @IBOutlet weak var messageTextView: UITextView!
        @IBOutlet var userEmailTextField: UITextField!
        @IBOutlet var userMobileNumberTxtField: UITextField!
        @IBOutlet var userNameTextField: UITextField!
        @IBOutlet weak var submitBtn: UIButton!
        @IBOutlet weak var dropDownSelection: LBZSpinner!
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.submitBtn.layer.cornerRadius = 5
            //self.addSlideMenuButton()
            self.creatingPaddingForUITextView()
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
            self.title = "ContactUs"
            userNameTextField.contactUnderlined()
            userEmailTextField.contactUnderlined()
            userMobileNumberTxtField.contactUnderlined()
            dropDownSelection.delegate = self
            messageTextView.delegate = self
            navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
            self.navigationController?.navigationBar.tintColor =  UIColor.white
            self.getUserContactDetails()
            /* create disable keyboard */
        }
        
        func getUserContactDetails(){
            if Reachability.isConnectedToNetwork() {
                self.getContactDetails()
                self.getProblemList()
            } else {
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    
                }
            }
        }
        
        func textViewDidBeginEditing(_ textView: UITextView)
        {
            if (textView.text == "Type your message")
            {
                textView.text = ""
                textView.textColor = .black
            }
            //textView.becomeFirstResponder() //Optional
        }
        
        func textViewDidEndEditing(_ textView: UITextView)
        {
            if (textView.text == "") || textView.text.isEmpty
            {
                textView.text = "Type your message"
                textView.textColor = .lightGray
            }
            textView.resignFirstResponder()
        }
        func spinnerChoose(_ spinner:LBZSpinner, index:Int,value:String) {
            var spinnerName = ""
            if spinner == dropDownSelection { spinnerName = "spinnerTop" }
            print("Spinner : \(spinnerName) : { Index : \(index) - \(value) }")
            subjectId = contactListData[index]?.subject_id
        }
        
        func handleNavigationArrow() {
//            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_navigationarrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
//            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
//            let signupNavigation = UINavigationController(rootViewController: signupController)
//            self.present(signupNavigation, animated: false, completion: nil)
            // self.dismiss(animated: false, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }
        
        
        func getContactDetails() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let contactUrl = NSURL(string: Constants.URL_REQUEST_USER_CONTACTDETAILS){
                var request = URLRequest(url: contactUrl as URL)
                guard let email = strContactEmail else{return}
                let contactParams: [String: Any] = ["email": email] as [String:Any]
                request.httpMethod = "POST"
                do {
                    let json = try JSONSerialization.data(withJSONObject: contactParams)
                    request.httpBody = json
                }
                catch let jsonErr {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any)
                        return
                    }
                    do {
                        guard let fetchData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)  as? [String:AnyObject] else{return}
                        print(fetchData)
                        guard  let strUserContactData = fetchData["contact_result"] as? [String: Any] else {return}
                        print(strUserContactData)
                        DispatchQueue.main.async {
                            if (fetchData["code"] as! NSNumber == 200) {
                                guard let contactEmail = strUserContactData["email"], let contactMobile = strUserContactData["mobile"], let contactName = strUserContactData["name"] else{
                                    return
                                }
                                
                                DispatchQueue.main.async {
                                    self.userEmailTextField.text = contactEmail as? String
                                    self.userNameTextField.text = contactName as? String
                                    self.userMobileNumberTxtField.text = contactMobile as? String
                                }
                                UserDefaults.standard.set(contactEmail, forKey: "email")
                                UserDefaults.standard.set(contactMobile, forKey: "mobile")
                                UserDefaults.standard.set(contactName, forKey: "name")
                            }
                                
                            else {
                                DispatchQueue.main.async {
                                    //  self.view.makeToast(registerResponse["description"]! as! String, duration: 4.0, position: CGPoint(x: 180, y: 360))
                                    guard let email = self.strContactEmail,let mobile = self.strContactMobile,let name = self.strContactName else{
                                        return}
                                    self.userEmailTextField.text = email
                                    self.userNameTextField.text = name
                                    self.userMobileNumberTxtField.text = mobile
                                }
                            }
                        }
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    
                }
                
            }
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
        {
            
            //guestButtontitle.isHidden = false
            
            guard let touch: UITouch = touches.first else
            {
                return
                
            }
        }
        
        
        override func viewDidAppear(_ animated: Bool) {
            self.perform(#selector(self.appendingSubjectListArray), with: nil, afterDelay: 0)
        }
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if text == "\n"  // Recognizes enter key in keyboard
            {
                textView.resignFirstResponder()
                return false
            }
            return true
        }
        @objc func appendingSubjectListArray(){
            let relatedIssue = contactListArray
            dropDownSelection.updateList(relatedIssue)
            self.dropDownSelection.changeSelectedIndex(0)
            
        }
        
        func getProblemList() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_CONTACTSUBJECTLIST) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    contactListArray.removeAll()
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    
                    guard let data = data else {
                        return
                    }
                    contactListData = [ContactsModel?]()
                    do {
                        
                        guard  let listData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        print(listData)
                        guard let contactsubjectList: [[String: String]] = listData["subject_list"] as? [[String: String]] else{return}
                        
                        for contactList in contactsubjectList {
                            var contacts = ContactsModel()
                            guard let subject = contactList["subject"],let subject_id = contactList["suject_id"] else{
                                return
                            }
                            contacts.subject = subject
                            contacts.subject_id = subject_id
                            contactListArray.append(subject)
                            contactListData.append(contacts)
                            if contactListData.count > 0 {
                                DispatchQueue.main.async {
                                    
                                }
                            }
                        }
                        
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                })
                task.resume()
                
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
        
        @IBAction func conatctSumitBtnAction(_ sender: Any) {
            if Reachability.isConnectedToNetwork(){
                self.submitButton()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    
                }
            }
        }
        
        func submitButton(){
            if (userNameTextField.text?.isEmpty)! || (userMobileNumberTxtField.text?.isEmpty)! || (userEmailTextField.text?.isEmpty)! || subjectId == nil || (messageTextView.text?.isEmpty)! || self.messageTextView.text == "Type your message" ||  self.messageTextView.text.isEmpty {
                DispatchQueue.main.async {
                    self.view.makeToast("Please fill all mandatory fields", duration: 3.0, position: .center)
                }
                
            }else{
                DispatchQueue.main.async {
                    SVProgressHUD.setDefaultStyle(.dark)
                    SVProgressHUD.show(withStatus: "please wait...")
                }
                var addConatcts:AddContactsModel = AddContactsModel()
                addConatcts.email = userEmailTextField.text
                addConatcts.mobile = userMobileNumberTxtField.text
                addConatcts.name = userNameTextField.text
                addConatcts.message = messageTextView.text
                addConatcts.subject = subjectId
                if let contactUrl = NSURL(string: Constants.URL_REQUEST_USER_ADDCONTACTUS){
                    var request = URLRequest(url: contactUrl as URL)
                    guard let userMobile = addConatcts.mobile,let userEmail = addConatcts.email,let userName = addConatcts.name,let userMessage = addConatcts.message,let subjectId =  addConatcts.subject else{
                        return
                    }
                    let addContactParams: [String: Any] = ["mobile":  userMobile, "name":userName, "email": userEmail,"message": userMessage,"subject":subjectId]
                    request.httpMethod = "POST"
                    do {
                        let json = try JSONSerialization.data(withJSONObject: addContactParams)
                        request.httpBody = json
                    }
                    catch let jsonErr {
                        
                        print(jsonErr.localizedDescription)
                    }
                    
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                        guard let data = data, error == nil else
                            
                        {
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                            }
                            print(error?.localizedDescription as Any)
                            return
                        }
                        
                        do {
                            let fetchData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                            print(fetchData)
                            if let registerResponse = fetchData as? [String: Any] {
                                print(registerResponse["description"]!)
                                DispatchQueue.main.async {
                                    if (registerResponse["code"] as! NSNumber == 200) {
                                        DispatchQueue.main.async {
                                            self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
                                            self.perform(#selector(self.handleToHomeScreenFromContactScreen), with: self, afterDelay: 3)
                                        }
                                    }
                                    else {
                                        DispatchQueue.main.async {
                                            
                                            //  self.view.makeToast(registerResponse["description"]! as! String, duration: 4.0, position: CGPoint(x: 180, y: 360))
                                            
                                        }
                                    }
                                }
                            }
                        }
                            
                        catch let jsonErr {
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            }
                            print(jsonErr.localizedDescription)
                        }
                    }
                    
                    task.resume()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                }
            }
        }
        
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            return true
        }
        
        func handleToHomeScreenFromContactScreen()
        {DispatchQueue.main.async {
            
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let homeNavigationcontroller = UINavigationController(rootViewController: homeController)
            self.present(homeNavigationcontroller, animated: true, completion: nil)
            }}
        
        func creatingPaddingForUITextView(){
            
            messageTextView.layer.borderColor = UIColor.init(rgb: 0x206ae0).cgColor
            messageTextView.layer.borderWidth = 2.0
            messageTextView.layer.cornerRadius = 2
            self.messageTextView.contentInset = UIEdgeInsetsMake(10, 10, 10, 10)
            
        }
    }//class
    
    
    extension UITextField {
        func contactUnderlined() {
            let border = CALayer()
            let width = CGFloat(1.0)
            border.borderColor = UIColor.lightGray.cgColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        }
    }
    
    extension ContactUsViewController:UITextFieldDelegate{
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            return true
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
        }
        
    }
    
