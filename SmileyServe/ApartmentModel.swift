//
//  ApartmentModel.swift
//  SmileyServe
//
//  Created by Apple on 27/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct ApartmentModel{
    
    var id : String?
    var title : String?
    var address : String?
    var location : String?
    
    
    init() {
        //
        
    }
    
    
    init(apartmentmodel:ApartmentModel) {
        
        
        self.id = apartmentmodel.id
        self.title = apartmentmodel.title
        self.address = apartmentmodel.address
        self.location = apartmentmodel.location
    }
    
    
    
}
