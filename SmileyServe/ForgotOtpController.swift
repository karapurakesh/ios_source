    //
    //  ForgotOtpController.swift
    //  SmileyServe
    //
    //  Created by Apple on 10/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //

    import UIKit
    import SVProgressHUD
    
    class ForgotOtpController: UIViewController,UITextFieldDelegate {
    @IBOutlet var forgotOtpNumberTextField: UITextField!
        @IBOutlet weak var verifybtn: UIButton!
        override func viewDidLoad() {
    super.viewDidLoad()
            verifybtn.layer.cornerRadius = 5
    forgotOtpNumberTextField.forgotOtpUnderlined()
    forgotOtpNumberTextField.delegate = self
    navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigation))
    UINavigationBar.appearance().barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
    UINavigationBar.appearance().tintColor = UIColor.white
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    self.addDoneButtonOnKeyboard()
    
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
          self.displayToast()
        }
        }
    func displayToast(){
    DispatchQueue.main.async {
   self.view.makeToast(UserDefaults.standard.value(forKey: "forgotpasswordotp")
          as? String, duration: 3.0, position: .bottom)
    }
    }
    func handleNavigation() {
    forgotOtpNumberTextField.resignFirstResponder()
    self.dismiss(animated: true, completion: nil)

    }

    @IBAction func handleForgotOtp(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)

    }

    @IBAction func forgotOtpButton(_ sender: Any) {
    if Reachability.isConnectedToNetwork(){
        self.forgotOtp()
    }else{
        DispatchQueue.main.async {
    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
    }
    }
    }

    func forgotOtp(){
     if (forgotOtpNumberTextField.text?.isEmpty)! {
        DispatchQueue.main.async {
     self.view.makeToast("please fill the otp", duration: 3.0, position: .center)
    }
    
    }
    else {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
    if let signupURL =  NSURL(string:Constants.URL_REQUEST_USER_FORGOTVERIFICATION) {
    forgotOtpNumberTextField.resignFirstResponder()
    var forgotOtp:ForgotPasswordOtp = ForgotPasswordOtp()
    forgotOtp.forgotOtp = forgotOtpNumberTextField.text
    guard let userForgotOtp =  forgotOtp.forgotOtp else{return}
    UserDefaults.standard.set(userForgotOtp, forKey: "otpcode")
    var  request = URLRequest(url:signupURL as URL)
    let forgotOtpParam:[String:Any] = ["otpcode":userForgotOtp]
    request.httpMethod = "POST"
    do {
    let  json = try JSONSerialization.data(withJSONObject: forgotOtpParam)
    request.httpBody = json
    }
    catch let jsonErr  {

    print(jsonErr.localizedDescription)
    }
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        DispatchQueue.main.async {
       SVProgressHUD.dismiss()
        }
       guard let data = data, error == nil else
    {
        DispatchQueue.main.async {
            self.view.makeToast("Unable to connect server", duration: 3.0, position: .center)
        }
    print(error?.localizedDescription as Any )
    return
    }
    do {

    let fetchData = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
    if let registerResponse = fetchData as? [String:Any] {
    print(registerResponse["message"]!)
    print(registerResponse["description"]!)
    DispatchQueue.main.async {
    if (registerResponse["code"] as! NSNumber == 200){
    DispatchQueue.main.async {
    UserDefaults.standard.set(registerResponse["description"]! as! String, forKey: "forgototp")
    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let otpcontroller = storyBoard.instantiateViewController(withIdentifier: "updatepassword") as! UpdatePasswordController
    let otpNavigation = UINavigationController(rootViewController: otpcontroller)
    self.present(otpNavigation, animated: true, completion: nil)
    }
    }
    else {
    DispatchQueue.main.async {
    self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
    }
    }
    }
    }
    }

    catch let jsonErr{
        DispatchQueue.main.async {
  self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
    print(jsonErr.localizedDescription)
    }
    }
    }
    task.resume()
    }else{
        DispatchQueue.main.async {
        self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)

     
            
    }
    }
    }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true

    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.view.endEditing(true)

    }

    /* creating a done button on number pad for forgot otp textfield */

    func addDoneButtonOnKeyboard() {
    let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
    doneToolbar.barStyle       = UIBarStyle.blackTranslucent
    let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
    let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ForgotOtpController.doneButtonAction))
    var items = [UIBarButtonItem]()
    items.append(flexSpace)
    items.append(done)
    doneToolbar.items = items
    doneToolbar.sizeToFit()
    self.forgotOtpNumberTextField.inputAccessoryView = doneToolbar
    }

    func doneButtonAction() {
    self.forgotOtpNumberTextField.resignFirstResponder()
    }


    }//class

    extension UITextField {
    func forgotOtpUnderlined() {
    let border = CALayer()
    let width = CGFloat(1.0)
    border.borderColor = UIColor.black.cgColor
    border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
    border.borderWidth = width
    self.layer.addSublayer(border)
    self.layer.masksToBounds = true
    }
    }

/* useful code
     
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named:"backArrow.png"), style: .plain, target: self, action: #selector(handleNavigation))
        navigationItem.title = "Forgot Password"
     
     
     
*/
    
    
    
    





