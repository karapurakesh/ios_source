//
//  CustomCollectionCell.swift
//  SmileyServe
//
//  Created by Apple on 20/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class CustomCollectionCell: UICollectionViewCell {
    @IBOutlet var imageViewCustom: UIImageView!
    @IBOutlet var productsTitle :  UILabel!
}
