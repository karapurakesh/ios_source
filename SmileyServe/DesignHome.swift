//
//  DesignHome.swift
//  SmileyServe
//
//  Created by Apple on 19/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class DesignHome: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLabelConstraints()
        backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let label : UILabel = {
        
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.backgroundColor = UIColor.red
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
        
    }()
    
    func setupLabelConstraints(){
        
        addSubview(label)
        
        //create views dictionary
        
        let views = [
        "label":self.label
        
        ]
        
        //set the horoozontal constraint
        
     let horizontalLabelConstraint = NSLayoutConstraint.constraints(withVisualFormat: "H:|[label]|", options: NSLayoutFormatOptions(), metrics: nil
        , views: views)
    let verticalLabelConstraint = NSLayoutConstraint.constraints(withVisualFormat: "V:[label(150)]", options: NSLayoutFormatOptions(), metrics: nil, views: views)
        addConstraints(horizontalLabelConstraint)
        addConstraints(verticalLabelConstraint)
        
        
    }
    
    
    
    

   }
