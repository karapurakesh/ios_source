  //
  //  MySubscriptionsController.swift
  //  SmileyServe
  //
  //  Created by Apple on 06/07/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //
  
  import UIKit
  import SVProgressHUD
  var buttonTag : Int?
  
  class MySubscriptionsController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var subscriptionNotFoundView: UIView!
    @IBOutlet weak var cartCountLabel : UILabel!
    @IBOutlet var subscriptionTableView : UITableView!
    var susbcriptionCalendarDateNames:[EditSubscriptionModelResponse?] = []
    var subscriptionUserId = UserDefaults.standard.value(forKey: "id") as? String
    var subscriptionList :[SubscriptionListModel] = []
    @IBAction func handleCartBtnAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
        let cartNavigationController = UINavigationController(rootViewController: controller)
        self.present(cartNavigationController, animated: true, completion: nil)
        
    }
    @IBOutlet weak var editSubscriptionRef: UIButton!
    @IBOutlet var subscribeReferenceBtn: UIButton!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //editSubscriptionRef.layer.cornerRadius = 5
        subscribeReferenceBtn.layer.cornerRadius = 22
        subscribeReferenceBtn.layer.masksToBounds = true
        //self.addSlideMenuButton()
        self.title = "My Subscriptions"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
        navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        self.navigationController?.navigationBar.tintColor =  UIColor.white
        self.subscriptionTableView.isHidden = true
        self.subscriptionNotFoundView.isHidden = true
        self.subscriptionTableView.separatorStyle = .none
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_tab_cart"), style: .plain, target: self, action: #selector(handleCart))
        //  navigationItem.rightBarButtonItem?.addBadge(number: 0)
        self.subscriptionServerCalls()
        
    }
    func handleCart(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
        let cartNavigationController = UINavigationController(rootViewController: controller)
        self.present(cartNavigationController, animated: true, completion: nil)
        
    }
    func subscriptionServerCalls(){
        
        if Reachability.isConnectedToNetwork(){
            if let userId = subscriptionUserId{
                self.getSubscriptionList(userId: userId)
            }
            if let userId = subscriptionUserId{
                getCartCountValue(userId: userId)
            }
        }else{
            
            DispatchQueue.main.async {
                self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
            }
        }
        
        
    }
    func handleNavigationArrow(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        let signupNavigation = UINavigationController(rootViewController: signupController)
        self.present(signupNavigation, animated: false, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.subscriptionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subscriptionCell", for: indexPath) as! SubscriptionTableViewCell
        cell.editSubscriptionBtnRef.addTarget(self, action: #selector(MySubscriptionsController.editSubscription), for: .touchUpInside)
        cell.editSubscriptionBtnRef.tag = indexPath.row
        cell.isUserInteractionEnabled = true
        cell.selectionStyle = .none
        
        
        let dcValue = self.subscriptionList[indexPath.row].delivery_charge!
        if dcValue != "0"{
             cell.productprice.text = "Rs. " + self.subscriptionList[indexPath.row].product_originalprice! +  "    D.C: Rs. " + self.subscriptionList[indexPath.row].delivery_charge!
            
        }else {
            cell.productprice.text = "Rs. " + self.subscriptionList[indexPath.row].product_originalprice!
        }
                
        
        cell.productUnits.text = self.subscriptionList[indexPath.row].product_units
        cell.productTitle.text = self.subscriptionList[indexPath.row].product_name
        
        
        if self.subscriptionList[indexPath.row].menu_id == "15" {
            cell.editSubscriptionBtnRef.isHidden = true
        }
        
        //    cell.productDeleiveryCharges.text = self.subscriptionList[indexPath.row].delivery_charge
        cell.productImageView.setImage(from: self.subscriptionList[indexPath.row].product_image!)
        return cell
    }
    
    func editSubscription(_sender:UIButton) {
        buttonTag = _sender.tag
        print(buttonTag!)
        self.createSmileyCashPayDialog()
    }
    
    
    @IBAction func handleSubscriptionBack(_ sender: Any) {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        let signupNavigation = UINavigationController(rootViewController: signupController)
        self.present(signupNavigation, animated: false, completion: nil)
    }
    @IBAction func subscribeActionBtn(_ sender: UIButton) {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        let signupNavigation = UINavigationController(rootViewController: signupController)
        self.present(signupNavigation, animated: true, completion: nil)
    }
    
    func getCartCountValue(userId:String){
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        
        if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_CARTCOUNT + userId) {
            var urlRequest = URLRequest(url: requestLocationListURL as URL)
            urlRequest.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                if (error != nil) {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                }
                if let response = response {
                    print(response)
                }
                guard let data = data else {
                    return
                }
                
                do {
                    
                    guard let cartData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                    
                    DispatchQueue.main.async {
                        
                        if let cartCount = cartData["cart_count"] {
                            //  let cartValue = String(describing: cartCount)
                            if cartCount as! Int == 0{
                                self.navigationItem.rightBarButtonItem?.removeBadge()
                            }else{
                                self.navigationItem.rightBarButtonItem?.addBadge(number: cartCount as! Int)
                            }
                            
                            //  self.cartCountLabel.text = cartValue
                        }
                    }
                }
                catch let jsonErr {
                    DispatchQueue.main.async {
                        
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
                
            })
            task.resume()
            
        }else{
            
            DispatchQueue.main.async {
                
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
            }
        }
    }
    
    func getSubscriptionList(userId : String) {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        if let subscriptionListUrl = NSURL(string: Constants.URL_REQUEST_USER_SUBSCRIPTION + userId) {
            var urlRequest = URLRequest(url: subscriptionListUrl as URL)
            urlRequest.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                
                if (error != nil) {
                    
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                }
                
                if let response = response {
                    
                    print(response)
                }
                
                guard let data = data else {
                    return
                }
                self.subscriptionList = [SubscriptionListModel]()
                do {
                    guard  let subscriptionListData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{
                        return
                    }
                    
                    print(subscriptionListData)
                    
                    if subscriptionListData["code"] as! NSNumber == 200 {
                        let subscriptionResultList: [[String: String]] = subscriptionListData["subscription_result"] as! [[String: String]]
                        let cartProductId  = (subscriptionListData["subscription_result"] as? [[String: AnyObject]])!.filter({ $0["productid"] != nil }).map({ $0["productid"]! })
                        print(cartProductId)
                        UserDefaults.standard.set(cartProductId, forKey: "productid")
                        let cartOrderId  = (subscriptionListData["subscription_result"] as? [[String: AnyObject]])!.filter({ $0["orderid"] != nil }).map({ $0["orderid"]! })
                        print(cartOrderId)
                        UserDefaults.standard.set(cartProductId, forKey: "orderid")
                        
                        for subscroiptionListItems in subscriptionResultList {
                            
                            var subscription = SubscriptionListModel()
                            
                            guard let product_title  = subscroiptionListItems["product_name"],let product_image = subscroiptionListItems["product_image"],let product_units = subscroiptionListItems["product_units"],let product_price = subscroiptionListItems["product_price"],let product_delieveryCharges = subscroiptionListItems["delivery_charge"],let productid = subscroiptionListItems["productid"],let orderid = subscroiptionListItems["orderid"],let start_date = subscroiptionListItems["start_date"] ,let end_date = subscroiptionListItems["enddate"],let product_originalprice = subscroiptionListItems["product_originalprice"], let menuID = subscroiptionListItems["menu_id"] else{
                                return
                            }
                            
                            subscription.delivery_charge = product_delieveryCharges
                            subscription.product_image = product_image
                            subscription.product_name = product_title
                            subscription.product_units = product_units
                            subscription.product_price = product_price
                            subscription.productid = productid
                            subscription.orderid = orderid
                            subscription.start_date = start_date
                            subscription.end_date = end_date
                            subscription.menu_id = menuID
                            subscription.product_originalprice = product_originalprice
                            self.subscriptionList.append(subscription)
                            UserDefaults.standard.set(  subscription.productid, forKey: "productid")
                            UserDefaults.standard.set(subscription.orderid, forKey: "orderid")
                        }
                        if self.subscriptionList.count > 0 {
                            DispatchQueue.main.async {
                                self.subscriptionTableView.reloadData()
                                self.subscriptionTableView.isHidden = false
                            }
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            self.subscriptionNotFoundView.isHidden = false
                        }
                    }
                }
                catch let jsonErr {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
                
            })
            task.resume()
            
        }else{
            
            DispatchQueue.main.async {
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
            }
        }
    }
    
    /* create a Alert Dialog while displaying Edit button */
    
    func createSmileyCashPayDialog(){
        
        let alertController = UIAlertController(title: "Information", message: "To increase/reduce/stop milk for any single day,please use My Calendar and refer to FAQS for more info", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            print("you have pressed OK button");
            
            if Reachability.isConnectedToNetwork(){
                self.EditSubscription()
            }else{
                self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
            }
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func EditSubscription(){
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        
        if let editsubscriptionUrl =   NSURL(string:Constants.URL_REQUEST_USER_EDITSUBSCRIPTION){
            var request = URLRequest(url:editsubscriptionUrl as URL)
            var editSubscriptionList:SubscriptionListModel = SubscriptionListModel()
            editSubscriptionList.userid  = userId
            //  editSubscriptionList.orderid = UserDefaults.standard.value(forKey: "orderid") as? String
            
            editSubscriptionList.orderid = (self.subscriptionList[buttonTag!].orderid)!
            editSubscriptionList.productid = (self.subscriptionList[buttonTag!].productid)!
            let editSubscriptionListParams:[String:Any] = ["userid":editSubscriptionList.userid!,"orderid":editSubscriptionList.orderid!,"itemid":editSubscriptionList.productid!]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: editSubscriptionListParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )
                    return
                }
                self.susbcriptionCalendarDateNames = [EditSubscriptionModelResponse]()
                
                do{
                    guard let fetchEditSubscriptionJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                    print(fetchEditSubscriptionJson)
                    print(fetchEditSubscriptionJson["description"]!)
                    print(fetchEditSubscriptionJson["message"]!)
                    if (fetchEditSubscriptionJson["code"] as! NSNumber == 200) {
                        if  let cartResult = fetchEditSubscriptionJson["cart_result"] {
                            guard let startDate = cartResult["start_date"] else{return}
                            print("startDate",startDate!)
                            
                        }
                        
                        let calendarDays  = (fetchEditSubscriptionJson["calender"] as? [[String: AnyObject]])!.filter({ $0["day_name"] != nil }).map({ $0["day_name"]! })
                        print(calendarDays)
                        UserDefaults.standard.set(calendarDays, forKey: "calendarDays")
                        let calendarProductQuantity  = (fetchEditSubscriptionJson["calender"] as? [[String: AnyObject]])!.filter({ $0["qty"] != nil }).map({ $0["qty"]! })
                        print(calendarProductQuantity)
                        UserDefaults.standard.set(calendarProductQuantity, forKey: "calendarProductQuantity")
                        
                        
                        if let calendarSubscriptionDatesJson = fetchEditSubscriptionJson["calender"] as? [[String:AnyObject]]{
                            
                            for calendarSubscriptionDates in calendarSubscriptionDatesJson {
                                var calendarSubscriptionDatesModelResponse = EditSubscriptionModelResponse()
                                guard let subscriptionCalendarDatesName = calendarSubscriptionDates["day_name"],let subscriptionQuantity = calendarSubscriptionDates["qty"],let subscriptionTotalQuantity = calendarSubscriptionDates["total_qty"] else{
                                    return
                                }
                                
                                calendarSubscriptionDatesModelResponse.day_name = subscriptionCalendarDatesName as? String
                                calendarSubscriptionDatesModelResponse.qty = subscriptionQuantity as? String
                                calendarSubscriptionDatesModelResponse.total_qty = subscriptionTotalQuantity as? String
                                self.susbcriptionCalendarDateNames.append(calendarSubscriptionDatesModelResponse)
                                
                            }
                        }
                        
                        DispatchQueue.main.async {
                            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let orderController = storyBoard.instantiateViewController(withIdentifier: "EditSubscription") as! EditSubscriptionController
                            orderController.subscriptionProductPrices = (self.subscriptionList[buttonTag!].product_originalprice)!
                            orderController.subscriptionProductTitles = (self.subscriptionList[buttonTag!].product_name)!
                            orderController.subscriptionProductUnit = (self.subscriptionList[buttonTag!].product_units)!
                            orderController.subscriptionProductImages = (self.subscriptionList[buttonTag!].product_image)!
                            orderController.subscriptionDeleieveryCharge = (self.subscriptionList[buttonTag!].delivery_charge)!
                            orderController.subscriptionProductEndDate = (self.subscriptionList[buttonTag!].end_date)!
                            orderController.subscriptionProductStartDate = (self.subscriptionList[buttonTag!].start_date)!
                            orderController.subscriptionProductId = (self.subscriptionList[buttonTag!].productid)!
                            orderController.subscriptionOrderId = (self.subscriptionList[buttonTag!].orderid)!
                            self.present(orderController, animated: true, completion: nil)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.view.makeToast(fetchEditSubscriptionJson["description"]! as? String, duration: 3.0, position: .center)
                        }
                    }
                }catch let jsonErr{
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            
            task.resume()
        }
        else{
            DispatchQueue.main.async {
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
            }
        }
    }
  }//class
  
  
