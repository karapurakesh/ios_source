  //
  //  FaqsViewController.swift
  //  SmileyServe
  //
  //  Created by Apple on 06/07/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //

  import UIKit
  import SVProgressHUD
  import SwiftSpinner

  class FaqsViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
  var faq:[Faqs?] = []
  @IBOutlet weak var tableView: UITableView!
  override func viewDidLoad() {
  super.viewDidLoad()
  //self.addSlideMenuButton()
  navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
  navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
    self.navigationController?.navigationBar.tintColor =  UIColor.white
    self.tableView.separatorColor = UIColor(white: 0.95, alpha: 1)
  navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
  self.title = "FAQS"
  tableView.isHidden = true
  /* Get method for faqs */

  if Reachability.isConnectedToNetwork() {
  self.getFaqs()
  } else {
  DispatchQueue.main.async {
  self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)

  }
  }
  }

  override func viewWillAppear(_ animated: Bool) {
  }
  func numberOfSections(in tableView: UITableView) -> Int {
  return 1
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  return faq.count
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FaqTableViewCell
  cell.contentView.backgroundColor = UIColor(white: 0.95, alpha: 1)
  cell.answerLabel?.text = faq[indexPath.item]?.answer!
  cell.questionLabel?.text = faq[indexPath.item]?.question!
  return cell
  }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
      return 165.0
    }
  
    
  func getFaqs(){
    DispatchQueue.main.async {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.show(withStatus: "please wait...")
    }
  if  let requestLocationListURL = NSURL(string:Constants.URL_REQUEST_USER_FAQ){
  var urlRequest = URLRequest(url: requestLocationListURL as URL)
  urlRequest.httpMethod = "GET"
  let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in

  self.faq.removeAll()
    DispatchQueue.main.async {
   SVProgressHUD.dismiss()
    }
  if (error != nil){
 DispatchQueue.main.async {
    self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
    }
  }

  self.faq = [Faqs]()
  if let response = response {
  print(response)
  }
  guard let data = data else {
  return
  }

  do{

  guard let listData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
  print(listData["message"]!)
  print(listData["description"]!)
  print(listData)
  guard let list: [[String: String]] = listData["faq_result"] as? [[String: String]] else{return}
  for faqs in list{
  let homeFaq = Faqs()
  guard let answer = faqs["asnswer"], let quetsion = faqs["question"] else{
  return
  }
  homeFaq.answer = answer
  homeFaq.question = quetsion
  self.faq.append(homeFaq)
  if self.faq.count > 0{
  DispatchQueue.main.async {
  self.tableView.reloadData()
  self.tableView.isHidden = false

  }
  }
  }

  }
  catch let jsonErr{
    DispatchQueue.main.async {
    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
    }
  print(jsonErr.localizedDescription)
  }

  })
  task.resume()

  }else{
  DispatchQueue.main.async {
self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
  }
  }
  }

  func handleNavigationArrow(){
  DispatchQueue.main.async {
  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
  let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
  let signupNavigation = UINavigationController(rootViewController: signupController)
  self.present(signupNavigation, animated: false, completion: nil)

  }
  }
  }//class




