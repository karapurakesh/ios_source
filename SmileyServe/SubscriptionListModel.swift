//
//  SubscriptionListModel.swift
//  SmileyServe
//
//  Created by Apple on 21/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct SubscriptionListModel {
    
    var cartid : String?
    var productid : String?
    var product_price : String?
    var delivery_charge : String?
    var product_originalprice : String?
    var product_name : String?
    var product_image : String?
    var product_units : String?
    var product_quantity : String?
    var start_date : String?
    var end_date : String?
    var endsupscription_status : String?
    var orderid : String?
    var userid : String?
    var menu_id : String?
  
}
