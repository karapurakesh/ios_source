//
//  GuestViewControllerModel.swift
//  SmileyServe
//
//  Created by Apple on 11/09/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct GuestViewControllerModel {
  
  var address : String?
  var  id    : String?
  var  location : String?
  var title   : String?

}
