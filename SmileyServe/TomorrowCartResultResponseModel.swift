//
//  TomorrowCartResultResponseModel.swift
//  SmileyServe
//
//  Created by Apple on 23/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct TomorrowCartResultResponseModel {
  
  
  var cartid : String?
  var productid : String?
  var product_price : String?
  var product_name : String?
  var product_image : String?
  var product_units : String?
  var qty : String?
  var orderdate : String?
  var product_orgprice : String?
  var deleivery_charge : String?
}
