    //
    //  AddMoneyViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 26/08/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    
    import UIKit
    import TextFieldEffects
    var walletAmount = Int()
    var payment_Id : String?
    var paymentStatus : String?
    import SVProgressHUD
    
    class AddMoneyViewController: UIViewController ,UITextFieldDelegate{
        var userId = UserDefaults.standard.value(forKey: "id") as? String
        private  var razorpay : Razorpay!
        var totalMoney : Int = 0
        
        @IBOutlet weak var addMoneyBtn: UIButton!
        var merchant:PGMerchantConfiguration!
        var checksumTransactValue :Bool = false
        
        @IBOutlet var smileyLabel: UILabel!

        @IBOutlet weak var twoHndrdBtn: UIButton!
        @IBOutlet weak var fiveHndrdBtn: UIButton!
        @IBOutlet weak var oneKHndrdBtn: UIButton!

        @IBAction func handleBackNavigationAddMoneyBtn(_ sender: Any) {
            self.addMoneyTextField.resignFirstResponder()
            self.dismiss(animated: true, completion: nil)
        }
        
        @IBAction func twoHundredAct(_ sender: Any) {
            
            let initialValue = 200
            
            self.setAmount(amount: initialValue)

            
//            if (self.addMoneyTextField.text?.isEmpty)! {
//
//                totalMoney = Int(self.addMoneyTextField.text!)!
//
//            }else {
//
//
//                self.addMoneyTextField.text = String(totalMoney) + "200"
//
//
//            }
            
        }
        
        @IBAction func fiveHundredAct(_ sender: Any) {
            
            let initialValue = 500
            
            self.setAmount(amount: initialValue)
            
        }
        
        func setAmount(amount:Int) {
            
            if (self.addMoneyTextField.text?.isEmpty)! {
                self.addMoneyTextField.text = String(amount)

            }else{
                
                let fieldvalue = Int(self.addMoneyTextField.text!)
                
                let newValue = fieldvalue! + amount
                
                self.addMoneyTextField.text = String(newValue)

                
            }
           
        }
        
        @IBAction func onekAction(_ sender: Any) {
            
            let initialValue = 1000
            
            self.setAmount(amount: initialValue)

        }
        
        @IBAction func addMoneyToWalletBtnAction(_ sender: Any) {
            
            DispatchQueue.main.async {
                //self.razorpay = Razorpay.initWithKey("rzp_test_uJmv0AK2JLwkj0", andDelegate: self)
                self.razorpay = Razorpay.initWithKey("rzp_live_2c8aqqKX844kbZ", andDelegate: self)
                self.showPaymentForm()
            }
        }
        
        func setMerchant(){
            merchant  = PGMerchantConfiguration.default()!
            //user your checksum urls here or connect to paytm developer team for this or use default urls of paytm
            merchant.checksumGenerationURL = "http://smileyserve.com/beta/generateChecksum.php";
            merchant.checksumValidationURL = "http://smileyserve.com/beta/verifyChecksum.php";
            
            // Set the client SSL certificate path. Certificate.p12 is the certificate which you received from Paytm during the registration process. Set the password if the certificate is protected by a password.
            merchant.clientSSLCertPath = nil; //[[NSBundle mainBundle]pathForResource:@"Certificate" ofType:@"p12"];
            merchant.clientSSLCertPassword = nil; //@"password";
            
            //configure the PGMerchantConfiguration object specific to your requirements
            /*  merchant.merchantID = "Smiley32368638707578";//paste here your merchant id  //mandatory
             merchant.website = "APP_STAGING";//mandatory
             merchant.industryID = "Retail";//mandatory
             merchant.channelID = "WAP"; //provided by PG WAP //mandatory*/
            
            //Prod
            merchant.merchantID = "SmileP27807646019839";//paste here your merchant id  //mandatory
            merchant.website = "SmilePWAP";//mandatory
            merchant.industryID = "Retail109";//mandatory
            merchant.channelID = "WEB"; //provided by PG WAP //mandatory
            
        }
        
        
        /* Razorpay paymentform calling Method*/

        func showPaymentForm() {
            
            if (self.addMoneyTextField.text?.isEmpty)! {
                self.view.makeToast("Please enter a valid amount", duration: 3.0, position: .center)
            }else if let actualNumber = Int(self.addMoneyTextField.text!) {
               // print("\"\(self.addMoneyTextField.text)\" has an integer value of \(actualNumber)")
                
                if actualNumber < 100 {
                    self.view.makeToast("Please enter the amount above 100", duration: 3.0, position: .center)

                }else {

                    DispatchQueue.main.async {
                        guard let  addingAmountToWallet = self.addMoneyTextField.text else{
                            return
                        }
                        
                        if let convertStringToInt = Float(addingAmountToWallet) {
                            /* walletAmount = Int(convertStringToInt)
                             let convertedCurrencyValue = Int(convertStringToInt) * 100
                             let options = ["amount":convertedCurrencyValue,"payment_capture":"1","currency":"INR",
                             "image": UIImage(named:"512x512.png") as Any, "name": "SmileyServe", "description": UserDefaults.standard.value(forKey: "name")!, "prefill": ["email": UserDefaults.standard.value(forKey: "email") as? String, "contact": UserDefaults.standard.value(forKey: "mobile")], "theme": ["color": "#206AE0"]] as [String : Any]
                             self.razorpay.open(options)*/
                            walletAmount = Int(convertStringToInt)
                            let convertedCurrencyValue = Int(convertStringToInt) * 100
                            
                            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                            
                            let image = UIImage(named: "paytm")
                            let imageView = UIImageView()
                            imageView.image = image
                            imageView.frame =  CGRect(x: 10, y: 0, width: 60, height: 60)
                            actionSheetController.view.addSubview(imageView)
                            
                            
                            let image2 = UIImage(named: "razor")
                            let imageView1 = UIImageView()
                            imageView1.image = image2
                            imageView1.frame =  CGRect(x: 10, y: 60, width: 50, height: 50)
                            actionSheetController.view.addSubview(imageView1)
                            
                            
                            // create an action
                            let firstAction: UIAlertAction = UIAlertAction(title: "BHIM/CC/DC/NB", style: .default) { action -> Void in
                                
                                //  Make a variable equal to a random number....
                                
                                let randomNum:UInt32 = arc4random() // range is 0 to 99
                                // convert the UInt32 to some other  types
                                // let randomTime:TimeInterval = TimeInterval(randomNum)
                                //let someInt:Int = Int(randomNum)
                                let someString:String = String(randomNum)
                                
                                print(someString)
                                
                                let finalAmount = Float(walletAmount)
                                
                                UserDefaults.standard.set(someString, forKey: "orderid_addMoney")
                                UserDefaults.standard.set(finalAmount, forKey: "convertedCurrencyValue")
                                
                                self.checksumHash(completionHandler: { (true) in
                                    
                                    self.createPayment()
                                    
                                })
                            }
                            
                            let secondAction: UIAlertAction = UIAlertAction(title: "Other Wallets", style: .default) { action -> Void in
                                
                                
                                let options = ["amount":convertedCurrencyValue,"payment_capture":"1","currency":"INR",
                                               "image": UIImage(named:"512x512.png") as Any, "name": "SmileyServe", "description": UserDefaults.standard.value(forKey: "name")!, "prefill": ["email": UserDefaults.standard.value(forKey: "email") as? String, "contact": UserDefaults.standard.value(forKey: "mobile")], "theme": ["color": "#206AE0"]] as [String : Any]
                                self.razorpay.open(options)
                            }
                            
                            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
                            
                            actionSheetController.addAction(firstAction)
                            actionSheetController.addAction(secondAction)
                            actionSheetController.addAction(cancelAction)
                            
                            self.present(actionSheetController, animated: true, completion: nil)
                            
                        }
                        
                    }
                }
            }else{
                self.view.makeToast("Please enter a valid amount", duration: 3.0, position: .center)

            }
            
          /*  if (self.addMoneyTextField.text?.isEmpty)! {
                self.view.makeToast("Please enter a valid amount", duration: 3.0, position: .center)
            }else if (Int(self.addMoneyTextField.text!)! < 100){
                self.view.makeToast("Please enter the amount above 100", duration: 3.0, position: .center)
            }else{
                DispatchQueue.main.async {
                    guard let  addingAmountToWallet = self.addMoneyTextField.text else{
                        return
                    }
                    
                    if let convertStringToInt = Float(addingAmountToWallet) {
                        /* walletAmount = Int(convertStringToInt)
                         let convertedCurrencyValue = Int(convertStringToInt) * 100
                         let options = ["amount":convertedCurrencyValue,"payment_capture":"1","currency":"INR",
                         "image": UIImage(named:"512x512.png") as Any, "name": "SmileyServe", "description": UserDefaults.standard.value(forKey: "name")!, "prefill": ["email": UserDefaults.standard.value(forKey: "email") as? String, "contact": UserDefaults.standard.value(forKey: "mobile")], "theme": ["color": "#206AE0"]] as [String : Any]
                         self.razorpay.open(options)*/
                        walletAmount = Int(convertStringToInt)
                        let convertedCurrencyValue = Int(convertStringToInt) * 100
                        
                        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        
                        let image = UIImage(named: "paytm")
                        let imageView = UIImageView()
                        imageView.image = image
                        imageView.frame =  CGRect(x: 10, y: 0, width: 60, height: 60)
                        actionSheetController.view.addSubview(imageView)
                        
                        
                        let image2 = UIImage(named: "razor")
                        let imageView1 = UIImageView()
                        imageView1.image = image2
                        imageView1.frame =  CGRect(x: 10, y: 60, width: 50, height: 50)
                        actionSheetController.view.addSubview(imageView1)
                        
                        
                        // create an action
                        let firstAction: UIAlertAction = UIAlertAction(title: "BHIM/CC/DC/NB", style: .default) { action -> Void in
                            
                            //  Make a variable equal to a random number....
                            
                            let randomNum:UInt32 = arc4random() // range is 0 to 99
                            // convert the UInt32 to some other  types
                            // let randomTime:TimeInterval = TimeInterval(randomNum)
                            //let someInt:Int = Int(randomNum)
                            let someString:String = String(randomNum)
                            
                            print(someString)
                            
                            let finalAmount = Float(walletAmount)
                            
                            UserDefaults.standard.set(someString, forKey: "orderid_addMoney")
                            UserDefaults.standard.set(finalAmount, forKey: "convertedCurrencyValue")
                            
                            self.checksumHash(completionHandler: { (true) in
                                
                                self.createPayment()
                                
                            })
                        }
                        
                        let secondAction: UIAlertAction = UIAlertAction(title: "Other Wallets", style: .default) { action -> Void in
                            
                            
                            let options = ["amount":convertedCurrencyValue,"payment_capture":"1","currency":"INR",
                                           "image": UIImage(named:"512x512.png") as Any, "name": "SmileyServe", "description": UserDefaults.standard.value(forKey: "name")!, "prefill": ["email": UserDefaults.standard.value(forKey: "email") as? String, "contact": UserDefaults.standard.value(forKey: "mobile")], "theme": ["color": "#206AE0"]] as [String : Any]
                            self.razorpay.open(options)
                        }
                        
                        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
                        
                        actionSheetController.addAction(firstAction)
                        actionSheetController.addAction(secondAction)
                        actionSheetController.addAction(cancelAction)
                        
                        self.present(actionSheetController, animated: true, completion: nil)
                        
                    }
                    
                }
            }*/
        }
        
        func createPayment(){
            
//            DispatchQueue.main.async {
//                SVProgressHUD.setDefaultStyle(.dark)
//                SVProgressHUD.show(withStatus: "please wait...")
//            }
            
            var orderDict = [String : Any]()
            
            
            let convertedCurrencyValue = UserDefaults.standard.value(forKey: "convertedCurrencyValue") as! Int
            let finalValue = String(describing:convertedCurrencyValue)
            orderDict["TXN_AMOUNT"] = finalValue; // amount to charge
            
            //  Prod
            
            orderDict["MID"] = "SmileP27807646019839";//paste here your merchant id   //mandatory
            orderDict["CHANNEL_ID"] = "WAP"; // paste here channel id                       // mandatory
            orderDict["INDUSTRY_TYPE_ID"] = "Retail109";//paste industry type              //mandatory
            orderDict["WEBSITE"] = "SmilePWAP";// paste website
            let orderid = UserDefaults.standard.value(forKey: "orderid_addMoney") as! String
            orderDict["ORDER_ID"] = orderid;//change order id every time on new transaction
            // orderDict["REQUEST_TYPE"] = "DEFAULT";// remain same
            orderDict["CUST_ID"] = self.userId; // change acc. to your database user/customers
            orderDict["MOBILE_NO"] = UserDefaults.standard.value(forKey: "mobile");// optional
            orderDict["EMAIL"] =  UserDefaults.standard.value(forKey: "email"); //optional
            
            orderDict["CALLBACK_URL"] = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderid)"
            // orderDict["CALLBACK_URL"] = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
            let checksumHsh = UserDefaults.standard.value(forKey: "CHECKSUMHASH_RES") as! String
            
            
            //https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=<orderid>
            
            orderDict["CHECKSUMHASH"] = checksumHsh
            print("orderDict::",orderDict)
                      
            
            DispatchQueue.main.async {
                let pgOrder = PGOrder(params: orderDict )
                
                let canBtn = UIButton.init(frame: CGRect(x: 0, y:20 , width: 100, height: 30))
                canBtn.setTitle("Cancel", for: .normal)
                //canBtn.tintColor = UIColor.red
                
                let topView = UIView(frame: CGRect(x: 0, y: 15, width: self.view.frame.width, height: 60))
                topView.backgroundColor = UIColor(red:253.0/255.0, green:109.0/255.0, blue:64/255.0, alpha:1.000)
                topView.addSubview(canBtn)
                
                
                let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
                
                transaction!.serverType = eServerTypeProduction
                transaction!.merchant = self.merchant
                transaction!.loggingEnabled = true
                transaction!.delegate = self
                transaction?.topBar = topView
                transaction?.cancelButton =  canBtn
                
                self.present(transaction!, animated: true, completion: {
                    SVProgressHUD.dismiss()
                    
                })
            }
            
           
        }
        
        
        func checksumHash(completionHandler: @escaping CompletionHandler) {
            
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            
            let url = prepareChecksum()
            
            print("URLCHECKSUM_",url)
            
            var request = URLRequest(url:url as URL)
            
            print("createOrderUrl",url)
            
            request.httpMethod = "GET"
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                
                guard let data = data, error == nil else
                {
                    print(error?.localizedDescription as Any )
                    return
                }
                do{
                    guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                    
                    guard let checksumHashRes = createOrderJson["CHECKSUMHASH"] as? String else{
                        return
                    }
                    
                    print("CheckSumHash_Rakesh",checksumHashRes)
                    
                    UserDefaults.standard.set(checksumHashRes, forKey: "CHECKSUMHASH_RES")
                    UserDefaults.standard.synchronize()
                    
                    completionHandler(true)
                    
                    //                    DispatchQueue.main.async {
                    //                        SVProgressHUD.dismiss()
                    //                    }
                    
                }catch let jsonErr{
                    
                    completionHandler(false)
                    
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            task.resume()
        }
        
        
        func prepareChecksum() -> URL{
            
            let orderid = UserDefaults.standard.value(forKey: "orderid_addMoney") as! String
            
            
            let convertedCurrencyValue = UserDefaults.standard.value(forKey: "convertedCurrencyValue") as! Int
            let finalValue = String(describing:convertedCurrencyValue)
            print("ADDMONEY_AMOUNT",finalValue)
            
            //staging  var url = URL.init(string: "http://smileyserve.com/beta/generateChecksum.php");
            var url = URL.init(string: "http://smileyserve.com/generateChecksum.php"); //prod
            
            if (url != nil) {
                
                let mobileNo = UserDefaults.standard.value(forKey: "mobile") as! String
                let email = UserDefaults.standard.value(forKey: "email") as! String
                
                if self.checksumTransactValue {
                    
                    url = addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "MID", value: "SmileP27807646019839"),
                                                                URLQueryItem.init(name: "CHANNEL_ID", value: "WAP"),
                                                                URLQueryItem.init(name: "INDUSTRY_TYPE_ID", value: "Retail109"),
                                                                URLQueryItem.init(name: "WEBSITE", value: "SmilePWAP"),
                                                                URLQueryItem.init(name: "TXN_AMOUNT", value: ""),
                                                                URLQueryItem.init(name: "ORDER_ID", value: orderid),
                                                                URLQueryItem.init(name: "CUST_ID", value: self.userId),
                                                                URLQueryItem.init(name: "CALLBACK_URL", value: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderid)"),
                                                                URLQueryItem.init(name: "MOBILE_NO", value: mobileNo),
                                                                URLQueryItem.init(name: "EMAIL", value: email)])
                    
                    
                }else {
                    
                    url = addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "MID", value: "SmileP27807646019839"),
                                                                URLQueryItem.init(name: "CHANNEL_ID", value: "WAP"),
                                                                URLQueryItem.init(name: "INDUSTRY_TYPE_ID", value: "Retail109"),
                                                                URLQueryItem.init(name: "WEBSITE", value: "SmilePWAP"),
                                                                URLQueryItem.init(name: "TXN_AMOUNT", value: finalValue),
                                                                URLQueryItem.init(name: "ORDER_ID", value: orderid),
                                                                URLQueryItem.init(name: "CUST_ID", value: self.userId),
                                                                URLQueryItem.init(name: "CALLBACK_URL", value: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderid)"),
                                                                URLQueryItem.init(name: "MOBILE_NO", value: mobileNo),
                                                                URLQueryItem.init(name: "EMAIL", value: email)])
                    
                }
                
            }
            
            return url!
        }
        
        func statusCheckService() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            
            
            self.checksumHash { (true) in
                
                let checksumHsh = UserDefaults.standard.value(forKey: "CHECKSUMHASH_RES") as! String
                
                print("checksumHsh_transaction_Success",checksumHsh)
                
                let orderid = UserDefaults.standard.value(forKey: "orderid_addMoney") as! String

               // let orderIDSTr =  String(describing: orderid)
                
                var url = URL.init(string: "https://securegw.paytm.in/merchant-status/getTxnStatus");
                
                var orderDict = [String : Any]()
                
                orderDict["MID"] = "SmileP27807646019839";//paste here your merchant id   //mandatory
                orderDict["CHECKSUMHASH"] = checksumHsh; // paste here channel id                       // mandatory
                orderDict["ORDER_ID"] = orderid;//paste industry type              //mandatory
                
                
                if let theJSONData = try? JSONSerialization.data(
                    withJSONObject: orderDict,
                    options: []) {
                    let theJSONText = String(data: theJSONData,
                                             encoding: .ascii)
                    print("JSON string = \(theJSONText!)")
                    
                    url = self.addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "JsonData", value: theJSONText)])
                    
                    
                }
                
                
                print("StatusCheckURL_Final",url)
                if (url != nil) {
                    
                    var request = URLRequest(url: url!)
                    
                    //     print("createOrderUrl",url)
                    
                    request.httpMethod = "GET"
                    
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                        
                        
                        guard let data = data, error == nil else
                        {
                            print(error?.localizedDescription as Any )
                            return
                        }
                        do{
                            guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                            print("createOrderJson",createOrderJson)
                            
                            guard let paytmOrderID = createOrderJson["ORDERID"] as? String else{
                                return
                            }
                            paymentStatus = "1"
                            payment_Id = paytmOrderID
                            
                            
                            self.paymentSucessSendingToServer()

//                            DispatchQueue.main.async {
//
//                                SVProgressHUD.dismiss()
//
//                                //UIAlertView.init(title: "Payment Successful", message: "You are successfully completed your transaction", delegate: self, cancelButtonTitle: "OK").show()
//
//                               // self.perform(#selector(self.delayToTransactionController), with: self, afterDelay: 3)
//
//                            }

                            
                        }catch let jsonErr{
                            
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            }
                            print(jsonErr.localizedDescription)
                        }
                    }
                    task.resume()
                }
            }
            
            
        }
        
        
        func orderPaymentSuccess(order_id : String, payTmorderID : String) {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            var serviceURL = NSURL(string: Constants.URL_REQUEST_USER_ORDERPAYMENTSUCCESS)
            
            if payTmorderID.count > 0 {
                
                let local = Constants.URL_REQUEST_USER_ORDERPAYMENTSUCCESS + order_id
                
                serviceURL = NSURL(string: local.appendingFormat("/%@",payTmorderID))
                
                //serviceURL = local?.a
                
            }else {
                serviceURL = NSURL(string: Constants.URL_REQUEST_USER_ORDERPAYMENTSUCCESS + order_id)
                
            }
            
            
            var urlRequest = URLRequest(url: serviceURL as! URL)
            urlRequest.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                if (error != nil) {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                }
                if let response = response {
                    print(response)
                }
                guard let data = data else {
                    return
                }
                do {
                    
                    guard let orderPaymentSuccessJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                    print("orderPaymentSuccessJson",orderPaymentSuccessJson)
                    if (orderPaymentSuccessJson["code"] as! NSNumber == 200) {
                        DispatchQueue.main.async {
                            self.view.makeToast(orderPaymentSuccessJson["description"]! as? String, duration: 3.0, position: .center)
                            
                            self.perform(#selector(self.delayToTransactionController), with: self, afterDelay: 3)

                          //  self.perform(#selector(self.delayToHomeScreenToDisplaySuccessToast), with: self, afterDelay: 1)
                        }
                        
                    }else{
                        
                        DispatchQueue.main.async {
                            self.view.makeToast(orderPaymentSuccessJson["description"]! as? String, duration: 3.0, position: .center)  }
                    }
                }catch let jsonErr {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
            })
            task.resume()
        }
        func addQueryParams(url: URL, newParams: [URLQueryItem]) -> URL? {
            let urlComponents = NSURLComponents.init(url: url, resolvingAgainstBaseURL: false)
            guard urlComponents != nil else { return nil; }
            if (urlComponents?.queryItems == nil) {
                urlComponents!.queryItems = [];
            }
            urlComponents!.queryItems!.append(contentsOf: newParams);
            return urlComponents?.url;
        }
        
        @IBOutlet  var addMoneyTextField: HoshiTextField!
       // @IBOutlet weak var smileyCashLabel: UILabel!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            navigationBarAppearance()
            addMoneyTextField.delegate = self
            addMoneyBtn.layer.cornerRadius = 5

            twoHndrdBtn.layer.borderWidth = 0.5
            twoHndrdBtn.layer.borderColor = UIColor(red:67/255, green:67/255, blue:67/255, alpha: 1).cgColor

            fiveHndrdBtn.layer.borderWidth = 0.5
            fiveHndrdBtn.layer.borderColor = UIColor(red:67/255, green:67/255, blue:67/255, alpha: 1).cgColor

            oneKHndrdBtn.layer.borderWidth = 0.5
            oneKHndrdBtn.layer.borderColor = UIColor(red:67/255, green:67/255, blue:67/255, alpha: 1).cgColor

            
            setMerchant()
            
            if Reachability.isConnectedToNetwork(){
                SVProgressHUD.dismiss()
                self.smileyCash()
            }else{
                SVProgressHUD.show()
                
            }
        }
        
        
        /* Navigationbar Appearnce */
        
        func navigationBarAppearance(){
            navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
            self.navigationController?.navigationBar.tintColor =  UIColor.white
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
            self.title = "Add money"
        }
        
        func handleNavigationArrow(){
            DispatchQueue.main.async {
                self.dismiss(animated: false, completion: nil)
            }
        }
        
        func smileyCash(){
            DispatchQueue.main.async {
                SVProgressHUD.show()
            }
            if let smileyCash =   NSURL(string:Constants.URL_REQUEST_USER_SMILEYCASH){
                var request = URLRequest(url:smileyCash as URL)
                var cashAmount:SmileyCash = SmileyCash()
                cashAmount.page_number = "1"
                cashAmount.userid = userId
                cashAmount.required_count = "1"
                guard let userId = cashAmount.userid  else{return}
                let cashListParams:[String:Any] = ["userid":userId,"page_number":cashAmount.page_number!,"required_count":cashAmount.required_count!]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cashListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data, error == nil else
                        
                    {
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let fetchSmileyData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print(fetchSmileyData)
                        print(fetchSmileyData["description"]!)
                        print(fetchSmileyData["message"]!)
                        if (fetchSmileyData["code"] as! NSNumber == 200) {
//                            DispatchQueue.main.async {
//                                guard let  smileyCashValue:Float = fetchSmileyData["smiley_cash"] as? Float else{return}
//                                self.smileyLabel.text = "Rs." + "\(smileyCashValue)"
//                            }
                            
                            DispatchQueue.main.async {
                                
                                if  let  smileyCashValue = fetchSmileyData["smiley_cash"]{
                                    
                                    let sCash = "Rs. " + "\(smileyCashValue)"
                                    
                                    self.smileyLabel.text = sCash
                                    
                                }
                                
                            }
                            
                            
                        }else{
                            
//                            DispatchQueue.main.async {
//                                guard let  smileyCashValue:Float = fetchSmileyData["smiley_cash"] as? Float else{return}
//                                self.smileyLabel.text = "Rs." + "\(smileyCashValue)"
//                            }
                            DispatchQueue.main.async {
                                
                                if  let  smileyCashValue = fetchSmileyData["smiley_cash"]{
                                    
                                    let sCash = "Rs. " + "\(smileyCashValue)"
                                    
                                    self.smileyLabel.text = sCash
                                    
                                }
                                
                            }
                        }
                    }catch let jsonErr{
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            self.addMoneyTextField.resignFirstResponder()
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.addMoneyTextField.resignFirstResponder()
            return true
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
            
        }
        
        func paymentSucessSendingToServer() {
            if let walletURL = URL(string:Constants.URL_REQUEST_ADD_MONEY) {
                var request = URLRequest(url:walletURL)
                request.httpMethod = "POST"// Compose a query string
                var walletModel:WalletModel = WalletModel()
                walletModel.amount = String(walletAmount)
                walletModel.paymentId = payment_Id
                walletModel.status = paymentStatus
                walletModel.userAPiKey = userId
                guard let amount =  walletModel.amount ,let paymentId =  walletModel.paymentId ,let paymentStatus = walletModel.status,let apiKey =  walletModel.userAPiKey else {
                    return
                }
                let postString = "userId=\(apiKey)&paymentId=\(paymentId)&amount=\(amount)&status=\(paymentStatus)"
                print("Wallet params",postString)
                request.httpBody = postString.data(using: String.Encoding.utf8);
                let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                    DispatchQueue.main.async {
                        
                    }
                    if error != nil
                    {
                        if error?._code ==  NSURLErrorTimedOut {
                            print("Time Out")
                            //Call your method here.
                            DispatchQueue.main.async {
                                self.view.makeToast("Your internet connection is very slow", duration: 3.0, position: .center)
                            }
                        }
                        else {
                            print("NO ERROR")
                        }
                        print("error debugging=\(String(describing: error?.localizedDescription))")
                        return
                    }
                    
                    // You can print out response object
                    print("response code = \(String(describing: response))")
                    //Let's convert response sent from a server side script to a NSDictionary object:
                    
                    guard let data = data else {
                        return
                    }
                    do {
                        guard  let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary else {
                            return
                        }
                        print("walllet json output : \(json)")
                      
                        
                        guard let message = json["message"] as? String else{
                            return
                        }
                        
                        if message == "Success" {
                            

                            DispatchQueue.main.async {

                                SVProgressHUD.dismiss()

                               /* let alertController = UIAlertController(title: "Payment Successful", message: "You are successfully completed your transaction", preferredStyle: .alert)
                                
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    
                                    // Code in this block will trigger when OK button tapped.
                                    print("Ok button tapped");
                                    self.perform(#selector(self.delayToTransactionController), with: self, afterDelay: 0)
                                }
                                
                                alertController.addAction(OKAction)
                                
                                self.present(alertController, animated: true, completion:nil)*/
                                
                                DispatchQueue.main.async {
                                    self.view.makeToast("Payment Successful", duration: 1.0, position: .center)
                                    self.perform(#selector(self.delayToTransactionController), with: self, afterDelay: 2)

                                }
                                

                                
                            }
                            return
                        }
                        
                        guard let errorResponse = json["error"] as? String else{
                            return
                        }
                        
                        if errorResponse == "false"{
                            DispatchQueue.main.async {
                                self.smileyLabel.text = message
                                self.addMoneyTextField.text = ""
                                self.view.makeToast(message, duration: 3.0, position: .center)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.view.makeToast(message, duration: 2.0, position: .center)
                            }
                        }
                    } catch let jsonErr {
                        print("jsonError Debugging",jsonErr.localizedDescription)
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to fetch data..please try again", duration: 3.0, position: .center)
                        }
                    }
                }
                task.resume()
            }
        }
        
        
        
        func delayToTransactionController() {
            DispatchQueue.main.async {
              /*  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let transactionController = storyBoard.instantiateViewController(withIdentifier: "Transaction") as! MyTransactionsViewController
                let transactionNavigation = UINavigationController(rootViewController: transactionController)
                self.present(transactionNavigation, animated: true, completion: nil)*/
                
               // self.dismiss(animated: true, completion: nil)
                
                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                let signupNavigation = UINavigationController(rootViewController: signupController)
                self.present(signupNavigation, animated: false, completion: nil)

            }
        }
        
    }//class
    
    extension AddMoneyViewController : RazorpayPaymentCompletionProtocol{
        func onPaymentSuccess(_ payment_id: String) {
            paymentStatus = "1"
            payment_Id = payment_id
            self.paymentSucessSendingToServer()
         //   UIAlertView.init(title: "Payment Successful", message: "You are successfully completed your transaction", delegate: self, cancelButtonTitle: "OK").show()
            
        }
        
        func onPaymentError(_ code: Int32, description str: String) {
            UIAlertView.init(title: "Error", message: str, delegate: self, cancelButtonTitle: "OK").show()
            DispatchQueue.main.async {
               /* let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let transactionController = storyBoard.instantiateViewController(withIdentifier: "Transaction") as! MyTransactionsViewController
                let transactionNavigation = UINavigationController(rootViewController: transactionController)
                self.present(transactionNavigation, animated: true, completion: nil)*/
                self.dismiss(animated: true, completion: nil)
            }
            //        alert.showAlert(inView:self, withTitle:"Razorpay", withSubtitle:"Payment fail", withCustomImage: nil, withDoneButtonTitle: nil, andButtons: nil)
            //self.paymentSucessSendingToServer()
            paymentStatus = "0"
        }
    }
    
    /*all actions related to transaction are catched here*/
    extension AddMoneyViewController : PGTransactionDelegate{
        func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
            print(responseString)
            //self.showAlert(title: "hurrayy", message: "transaction done")
            controller.dismiss(animated: true) {
                
                self.checksumTransactValue = true
                
                 self.statusCheckService()
                // self.presentAlertWithTitle(title: "Payment Successful", message: "Your order placed successfully")
            }
            
        }
        
        func didCancelTrasaction(_ controller: PGTransactionViewController!) {
            print("Cancelled")
            self.dismiss(animated: (controller != nil), completion: nil)

        }
        
        func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
            print(error)
            self.dismiss(animated: (controller != nil), completion: nil)

        }
        
    }

    extension UIColor {
        convenience init(rgb: UInt) {
            self.init(
                red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgb & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }
    }
