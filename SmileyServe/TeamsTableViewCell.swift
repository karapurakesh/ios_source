//
//  TeamsTableViewCell.swift
//  Axiom
//
//  Created by mac on 16/03/18.
//  Copyright © 2018 raqmiyat. All rights reserved.
//

import UIKit

class TeamsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //self.accessoryType = .disclosureIndicator
    }

    open class func height() -> CGFloat {
        return 65
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    open func setData(_ data: Any?) {
        if let menuText = data as? String {
            self.nameLbl?.text = menuText
        }
    }
    
}
