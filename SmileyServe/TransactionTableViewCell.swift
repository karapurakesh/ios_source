//
//  TransactionTableViewCell.swift
//  SmileyServe
//
//  Created by Apple on 26/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

  @IBOutlet weak var onlinePaymentLabel: UILabel!
  @IBOutlet weak var smileyCashAmountLabel: UILabel!
  @IBOutlet weak var orderDateLabel: UILabel!
  @IBOutlet weak var orderIdLabel: UILabel!
  @IBOutlet weak var totalAmountLbl: UILabel!
  @IBOutlet weak var headingTitleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
