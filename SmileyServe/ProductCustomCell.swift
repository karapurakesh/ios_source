//
//  ProductCustomCell.swift
//  SmileyServe
//
//  Created by Apple on 22/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class ProductCustomCell: UITableViewCell {

    @IBOutlet weak var designView: DesignView!
    @IBOutlet weak var lastCellView: UIView!
    @IBOutlet weak var custViewForLstCell: NSLayoutConstraint!
    @IBOutlet weak var orderbtnWidConst: NSLayoutConstraint!
    @IBOutlet var deleiveryCharges: UILabel!
    @IBOutlet var productPrice: UILabel!
    @IBOutlet var productUnits: UILabel!
    @IBOutlet var productsUnits: UILabel!
    @IBOutlet var productTitle: UILabel!
    @IBOutlet var productImage: CustomImageView!
    @IBOutlet var productOrderButtonRef : UIButton!
    @IBOutlet weak var orderBtnHightConst: NSLayoutConstraint!
    
    
    //QuantityChange
    @IBOutlet weak var qcStackViewHtCosnt: NSLayoutConstraint!
    @IBOutlet weak var decBtn: UIButton!
    @IBOutlet weak var incBtn: UIButton!
    @IBOutlet weak var quantityBtn: UIButton!

    @IBOutlet weak var qcStackView: UIStackView!
    @IBOutlet weak var subscribeLbl: UILabel!
    @IBOutlet weak var productUnitTopConst: NSLayoutConstraint!
    @IBOutlet weak var stockViewGapCosnt: NSLayoutConstraint!
    @IBOutlet weak var delCharStackView: UIStackView!
    @IBOutlet weak var stackViewActPrice: UIStackView!
   // @IBOutlet weak internal var delChargeLblWidConst: NSLayoutConstraint!
    @IBOutlet weak var actualAmnt: UILabel!
    @IBOutlet weak var discountedAmnt: UILabel!
    @IBOutlet weak var percentageAmnt: UILabel!

    var localSellingType : String!
    var stockstatus: String! {
        didSet {
            
            productOrderButtonRef.layer.cornerRadius = 5

             if stockstatus == "0" {
             productOrderButtonRef.setTitle("Sold Out", for: .normal)
             productOrderButtonRef.backgroundColor = UIColor.lightGray
             productOrderButtonRef.isEnabled = false
            orderbtnWidConst.constant = 80
            //orderBtnHightConst.constant = 50
             }else{
                
                productOrderButtonRef.layer.cornerRadius = 5
                subscribeLbl.layer.masksToBounds = true
                subscribeLbl.layer.cornerRadius = 5
                //
                productOrderButtonRef.isEnabled = true
                productOrderButtonRef.backgroundColor =  UIColor(red: 32/255, green: 106/255, blue: 224/255, alpha: 1)
                if localSellingType == "1"
                {
                    productOrderButtonRef.setTitle("Subscribe", for: .normal)
                    orderbtnWidConst.constant = 80
                    subscribeLbl.isHidden = true
                    productImage.isUserInteractionEnabled = false

                }else if localSellingType == "2"{
                    productOrderButtonRef.setTitle("Add", for: .normal)
                    orderbtnWidConst.constant = 60
                    subscribeLbl.isHidden = true
                    
                    productImage.isUserInteractionEnabled = false

                }else if localSellingType == "3"{
                    productOrderButtonRef.setTitle("Add", for: .normal)
                    orderbtnWidConst.constant = 60
                    subscribeLbl.isHidden = false
                    productImage.isUserInteractionEnabled = true

                    
                }
               
            }
        }
    }
    
    var sellingType: String! {
        didSet {
             localSellingType = sellingType
  
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // productOrderButtonRef.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
       
    }

}
