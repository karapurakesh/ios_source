//
//  UpdateCartModel.swift
//  SmileyServe
//
//  Created by Apple on 21/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct UpdateCartModel {
    
    var quantity : String?
    var userId : String?
    var order_date : String?
    var originalQuantity : String?
    var currentAction : String?
    var enable_smileycash : String?
    var cartId : String?
    
}
