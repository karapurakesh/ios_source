//
//  CartProductTableViewCell.swift
//  SmileyServe
//
//  Created by Apple on 22/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class CartProductTableViewCell: UITableViewCell {
    @IBOutlet weak var cartProductImage: CustomImageView!

  @IBOutlet var saturdayBtnRef: UIButton!
  @IBOutlet var fridayBtnRef: UIButton!
  @IBOutlet var thursdayBtnRef: UIButton!
  @IBOutlet var wednesdayBtnRef: UIButton!
  @IBOutlet var tuesdayBtnRef: UIButton!
  @IBOutlet var mondayBtnRef: UIButton!
  @IBOutlet var sundayBtnRef: UIButton!
    @IBOutlet weak var cartProductUnits: UILabel!
    @IBOutlet weak var cartProductName: UILabel!
    
    @IBOutlet weak var cartDeleteBTnRef: UIButton!
    
    @IBOutlet weak var cartProductDeleiveryCharges: UILabel!
    
    @IBOutlet weak var cartProductPrice: UILabel!
    
    @IBOutlet weak var cartProductQuantity: UILabel!
    @IBOutlet weak var cartFromDate : UILabel!
    @IBOutlet weak var cartToDate : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    override func layoutSubviews() {
        super.layoutSubviews()
    }

}


class CartInsatntProductCell: UITableViewCell {
    @IBOutlet weak var cartProductImage: CustomImageView!
    
    
    @IBOutlet weak var cartProductUnits: UILabel!
    @IBOutlet weak var cartProductName: UILabel!
    
    @IBOutlet weak var cartProductPrice: UILabel!
    
    @IBOutlet weak var incBtn: UIButton!
    @IBOutlet weak var decBtn: UIButton!
    
    @IBOutlet weak var qauntityValueBtn: UIButton!
    
    @IBOutlet weak var cartProductQuantity: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
   
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        incBtn.layer.shadowColor = UIColor.darkGray.cgColor
        incBtn.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        incBtn.layer.shadowOpacity = 1.0
        incBtn.layer.shadowRadius = 0.0
        incBtn.layer.masksToBounds = false
        incBtn.layer.cornerRadius = 4.0
        
        qauntityValueBtn.layer.shadowColor = UIColor.lightGray.cgColor
        qauntityValueBtn.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        qauntityValueBtn.layer.shadowOpacity = 1.0
        qauntityValueBtn.layer.shadowRadius = 0.0
        qauntityValueBtn.layer.masksToBounds = false
        
        decBtn.layer.shadowColor = UIColor.darkGray.cgColor
        decBtn.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        decBtn.layer.shadowOpacity = 1.0
        decBtn.layer.shadowRadius = 0.0
        decBtn.layer.masksToBounds = false
        decBtn.layer.cornerRadius = 4.0
    }
    
}


//class CustomIncButton:UIButton {
//    var qauntityValueBtnRefTag: Int = 0
//    var ctag : Int = 0
//
//    convenience init(qauntityValueBtn: UIButton, ctag:Int){
//        self.init()
//
//        self.qauntityValueBtnRefTag = qauntityValueBtn.tag
//        self.ctag = ctag
//    }
//}
//
//class CustomDecButton:UIButton {
//
//    var qauntityValueBtnRefTag: Int = 0
//    var ctag : Int = 0
//
//    convenience init(qauntityValueBtn: UIButton, ctag:Int){
//        self.init()
//
//        self.qauntityValueBtnRefTag = qauntityValueBtn.tag
//        self.ctag = ctag
//    }
//}
