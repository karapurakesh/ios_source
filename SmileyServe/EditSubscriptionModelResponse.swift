//
//  EditSubscriptionModelResponse.swift
//  SmileyServe
//
//  Created by Apple on 23/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct EditSubscriptionModelResponse {
    
    var id : String?
    var product_id : String?
    var pro_name : String?
    var pro_image : String?
    var product_price : String?
    var product_original_price : String?
    var deleivery_charges: String?
    var min_start_date : String?
    var min_end_date : String?
    var start_date : String?
    var end_date : String?
    var productunits : String?
    var day_name : String?
    var qty : String?
    var total_qty : String?
    
}
