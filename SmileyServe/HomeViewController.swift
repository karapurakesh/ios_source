    //
    //  HomeViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 04/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    
    import UIKit
    import SVProgressHUD
    import SDWebImage
    import Kingfisher
    import SwiftSpinner
    var index = 0
    var rightCartButtonItem = UIBarButtonItem()
    var rightNotificationButtonItem = UIBarButtonItem()
    
    
    func colorsWithHalfOpacity(_ colors: [CGColor]) -> [CGColor] {
        return colors.map({ $0.copy(alpha: $0.alpha * 0.5)! })
    }
    
    class HomeViewController: BaseViewController,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,AACarouselDelegate {
        @IBOutlet weak var collectionViewHeightConst: NSLayoutConstraint!
        var rightCartBtn = UIButton()
        var rightNavigationBtn = UIButton()
        @IBOutlet var staticMenuView: UIView!
        @IBOutlet var smileyCashLabelHeightConstraint: NSLayoutConstraint!
        
        var  menuListParams = [String:Any]()
        var apartmentsArrayList = [ApartmentModel]()
        //var orderId = ""
        //  var orderId = UserDefaults.standard.value(forKey: "orderid")
        
        
        @IBOutlet var containerView: UIView!
        @IBOutlet weak var carouselView: AACarousel!
        var userId = UserDefaults.standard.value(forKey: "id") as? String
        var pathArray = [String]()
        var apartmentID = UserDefaults.standard.value(forKey: "apartmentId")
        @IBAction func mySubscriptionBtnAct(_ sender: CircleButton) {
            let _ = sender.layer.addPulse { pulse in
                pulse.borderColors = [
                    UIColor(hue: CGFloat(arc4random()) / CGFloat(RAND_MAX), saturation: 1.0, brightness: 1.0, alpha: 1.0).cgColor
                ]
                pulse.backgroundColors = colorsWithHalfOpacity(pulse.borderColors)
            }
            self.perform(#selector(performSubscriptionWithDelay), with: self, afterDelay: 1)
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            //  SwiftSpinner.show(duration: 2.0, title: "connecting to smileyserve...")
            self.addSlideMenuButton()
            //   self.title = "SmileyServe"
            //  self.addingBarButtonItemsToNavigationBar()
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_tab_cart"), style: .plain, target: self, action: #selector(handleCart))
            navigationItem.title = "SmileyServe"
            self.collectionView.reloadData()
            //  orderPaymentSuccess(orderId: orderId as! String)
            rightCartButtonItem = UIBarButtonItem(image: UIImage(named:"ic_cart_new"), style: .plain, target: self, action: #selector(handleCart))
            rightNotificationButtonItem  = UIBarButtonItem(image: UIImage(named:"ic_bell_new"), style: .plain, target: self, action: #selector(handleNotification))
            navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
            //navigationController?.navigationBar.barTintColor = UIColor(rgb: 0xFF5722)
            // self.navigationController?.navigationBar.isTranslucent = false
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationItem.setRightBarButtonItems([rightCartButtonItem,rightNotificationButtonItem], animated: true)
            rightCartButtonItem.addBadge(number: 0)
            self.homeScreenNetworkCalls()
            //collectionViewHeightConst.constant = 320
            
            
            
            collectionView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
            
        }
        
        
        func performSubscriptionWithDelay(){
            if userId != nil {
                DispatchQueue.main.async {
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let subscriptionController = storyBoard.instantiateViewController(withIdentifier: "Subscription") as! MySubscriptionsController
                    let subscriptionNaviagation = UINavigationController(rootViewController: subscriptionController)
                    self.present(subscriptionNaviagation, animated: false, completion: nil)
                }
            }else{
                
                DispatchQueue.main.async {
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                }
            }
        }
        
        @IBAction func commingTmrwBtnAction(_ sender: CircleButton) {
            let _ = sender.layer.addPulse { pulse in
                pulse.borderColors = [
                    UIColor(hue: CGFloat(arc4random()) / CGFloat(RAND_MAX), saturation: 1.0, brightness: 1.0, alpha: 1.0).cgColor
                ]
                pulse.backgroundColors = colorsWithHalfOpacity(pulse.borderColors)
            }
            
            
            self.perform(#selector(performDealyForComingTmrw), with: self, afterDelay: 1)
            
        }
        
        func performDealyForComingTmrw(){
            
            if userId != nil{
                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let comingTomorrowController = storyBoard.instantiateViewController(withIdentifier: "ComingTomorrowController") as! ComingTomorrowController
                let comingTomorrowNaviagation = UINavigationController(rootViewController: comingTomorrowController)
                self.present(comingTomorrowNaviagation, animated: false, completion: nil)
            }else{
                
                DispatchQueue.main.async {
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                }
            }
        }
        
        @IBAction func calendarBtnAction(_ sender: CircleButton) {
            let _ = sender.layer.addPulse { pulse in
                pulse.borderColors = [
                    UIColor(hue: CGFloat(arc4random()) / CGFloat(RAND_MAX), saturation: 1.0, brightness: 1.0, alpha: 1.0).cgColor
                ]
                pulse.backgroundColors = colorsWithHalfOpacity(pulse.borderColors)
            }
            
            self.perform(#selector(performDelayForCalendar), with: self, afterDelay: 1)
            
        }
        
        func performDelayForCalendar(){
            if userId != nil {
                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let calendarController = storyBoard.instantiateViewController(withIdentifier: "Calendar") as! MyCalendarViewController
                let CalendarNavigation = UINavigationController(rootViewController: calendarController)
                self.present(CalendarNavigation, animated: false, completion: nil)
            }else{
                DispatchQueue.main.async {
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                }
            }
            
        }
        
        func downloadImages(_ url: String, _ index:Int) {
            let imageView = UIImageView()
            imageView.kf.setImage(with: URL(string: url)!, placeholder: UIImage.init(named: "defaultImage"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { (downloadImage, error, cacheType, url) in
                guard let downloadImage = downloadImage else{return}
                self.carouselView.images[index] = downloadImage
            })
            
        }
        
        func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
            
            imageView.kf.setImage(with: URL(string: url[index]), placeholder: UIImage.init(named: "defaultImage"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
            
        }
        
        func startAutoScroll() {
            //optional method
            carouselView.startScrollImageView()
            
        }
        
        func stopAutoScroll() {
            //optional method
            carouselView.stopScrollImageView()
        }
        
        
        
        @IBAction func smileyButtoncash(_ sender: Any) {
            
            if userId != nil {
                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let transactionController = storyBoard.instantiateViewController(withIdentifier: "Transaction") as! MyTransactionsViewController
                let transactionNavigation = UINavigationController(rootViewController: transactionController)
                self.present(transactionNavigation, animated: true, completion: nil)
            }else{
                DispatchQueue.main.async {
                    
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                }
            }
        }
        var menuItemArrayList:[MenuListImage?] = []
        @IBOutlet var notificationLabel: UILabel!
        @IBOutlet var smileyCashButton: UIButton!
        var slider:[SliderResult?] = []
        @IBOutlet var collectionView: UICollectionView!
        @IBOutlet var sliderImageView: UIImageView!
        @IBOutlet var smileyLabel: UILabel!
        
        override func viewDidAppear(_ animated: Bool) {
            
            //Rounding edges of UI label
            smileyLabel.layer.masksToBounds = true
            smileyLabel.layer.cornerRadius = 5
            
            SVProgressHUD.dismiss()
            guard let sliderArray = UserDefaults.standard.value(forKey: "slider") as? [String] else{
                return
            }
            pathArray = sliderArray
            carouselView.delegate = self
            carouselView.setCarouselData(paths: pathArray,  describedTitle:[""], isAutoScroll: true, timer: 5.0, defaultImage: "defaultImage")
            //optional method
            carouselView.setCarouselOpaque(layer: false, describedTitle: false, pageIndicator: false)
            carouselView.setCarouselLayout(displayStyle: 0, pageIndicatorPositon: 2, pageIndicatorColor: nil, describedTitleColor: nil, layerColor: nil)
            self.getCartCountValue(userId: userId!)
        }
        
       
        
        override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            if(keyPath == "contentSize"){
                if let newvalue = change?[.newKey]
                {
                    let newsize  = newvalue as! CGSize
                    collectionViewHeightConst.constant = newsize.height
                }
            }
        }
        
        override func viewWillDisappear(_ animated: Bool) {
           // collectionView.removeObserver(self, forKeyPath: "contentSize")
            super.viewWillDisappear(true)
        }
        
        func homeScreenNetworkCalls(){
            if Reachability.isConnectedToNetwork(){
                self.paymentGatewayDetails()
                self.getProblemList()
                if userId != nil{
                    self.getUserData(userApikey: userId)
                    self.getCartCountValue(userId: userId!)
                }
                if userId == nil{
                    self.menuList()
                }
                if userId != nil{
                    self.smileyCash()
                }
                self.setupSlider()
                self.collectionView.isHidden = false
                self.staticMenuView.isHidden = false
                
            }else{
                
                DispatchQueue.main.async {
                    self.collectionView.isHidden = true
                    self.staticMenuView.isHidden = true
                    self.smileyCashLabelHeightConstraint.constant = -222
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                }
            }
        }
        
        func addingBarButtonItemsToNavigationBar(){
            
            let clipBtn: UIButton = UIButton(type: UIButtonType.custom)
            clipBtn.setImage(UIImage(named: "ic_tabnotification"), for: [])
            clipBtn.addTarget(self, action: #selector(handleNotification), for: UIControlEvents.touchUpInside)
            clipBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            rightNotificationButtonItem = UIBarButtonItem(customView: clipBtn)
            let pencilBtn: UIButton = UIButton(type: UIButtonType.custom)
            pencilBtn.setImage(UIImage(named: "ic_tab_cart"), for: [])
            pencilBtn.addTarget(self, action: #selector(handleCart), for: UIControlEvents.touchUpInside)
            pencilBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            rightCartButtonItem = UIBarButtonItem(customView: pencilBtn)
            self.navigationItem.rightBarButtonItems = [rightCartButtonItem, rightNotificationButtonItem]
        }
        
        
        func handleNotification(){
            if userId != nil{
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "Notification") as! NotificationViewController
                let notificationNavigationController = UINavigationController(rootViewController: controller)
                self.present(notificationNavigationController, animated: true, completion: nil)
            }else{
                
                DispatchQueue.main.async {
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                }
            }
        }
        
        func handleCart(){
            if userId != nil {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
                let cartNavigationController = UINavigationController(rootViewController: controller)
                self.present(cartNavigationController, animated: true, completion: nil)
            }else{
                
                DispatchQueue.main.async {
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                }
                
            }
        }
        
        override func viewWillAppear(_ animated: Bool) {
            //self.menuList()
            //    SwiftSpinner.hide()
            //  self.collectionView.reloadData()
            
            let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
            if launchedBefore  {
                print("Not first launch.")
            } else {
                print("First launch, setting UserDefault.")
                UserDefaults.standard.set(true, forKey: "launchedBefore")
                //  self.view.makeToast( UserDefaults.standard.value(forKey: "message") as? String, duration: 3.0, position: CGPoint(x: 180, y: 460))
                //    self.view.makeToast(UserDefaults.standard.value(forKey: "message") as? String, duration: 3.0, position: .center)
            }
            
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0.0
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return menuItemArrayList.count
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            var size = CGSize(width: 95, height: 95)
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1136:
                    print("iPhone 5 or 5S or 5C")
                    size = CGSize(width: 88, height: 88)
                case 1334:
                    print("iPhone 6/6S/7/8")
                    size = CGSize(width: 88, height: 88)

                case 2208:
                    print("iPhone 6+/6S+/7+/8+")
                    size = CGSize(width: 95, height: 95)

                case 2436:
                    print("iPhone X")
                    size = CGSize(width: 95, height: 95)

                default:
                    print("unknown")
                    size = CGSize(width: 95, height: 95)

                }
            }
            
            return size
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if menuItemArrayList.count > 0 && userId != nil{
                
                let menu:MenuListImage = menuItemArrayList[indexPath.row]!
                let itemId = menu.menuId
                let itemTitle = menu.menuTitle
                let itemEnableStatus = menu.enableStatus
                print("itemTitle:\(itemTitle!)")
                print("item id :\(itemId!)")

                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let productController = storyBoard.instantiateViewController(withIdentifier: "product") as! ProductsViewController
                productController.strItemId = itemId!
                productController.strItemTitle = itemTitle!
                productController.strEnableStatus  = itemEnableStatus!
                let productNavigation = UINavigationController(rootViewController: productController)
                self.present(productNavigation, animated: false, completion: nil)
            }
            else{
                DispatchQueue.main.async {
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                }
            }
        }
        
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection", for: indexPath) as! CustomCollectionCell
            if menuItemArrayList.count > 0{
                cell.productsTitle.text = self.menuItemArrayList[indexPath.row]?.menuTitle
                if let imageCell = self.menuItemArrayList[indexPath.row]?.menuImage {
                    cell.imageViewCustom.sd_setImage(with:URL(string: imageCell))
                    //  cell.imageViewCustom.setImage(from: imageCell)
                }
            }
            return cell
        }
        
        
        
        /* payment gate way details */
        
        func paymentGatewayDetails(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_PAYMENTGATEWAYDETAILS ) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    do {
                        guard let paymentGatewayJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        print(paymentGatewayJson)
                        guard let cutOffTime = paymentGatewayJson["ios_order_cutoff_time"] as? String else{
                            return
                        }
                        iosCuttOfftime = cutOffTime
                    }  catch let jsonErr {
                        print(jsonErr.localizedDescription)
                    }
                })
                task.resume()
            }
        }
        
        
        
        func setupSlider(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if  let url = NSURL(string:Constants.URL_REQUEST_USER_SLIDER) {
                let  request = URLRequest(url:url as URL)
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data else{
                        return
                    }
                    if error != nil{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    DispatchQueue.main.async {
                        do {
                            
                            guard  let  json  = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                                return
                            }
                            SVProgressHUD.dismiss()
                            let result  = (json["slider_result"] as? [[String: AnyObject]])!.filter({ $0["slider"] != nil }).map({ $0["slider"]! })
                            UserDefaults.standard.set(result, forKey: "slider")
                            DispatchQueue.main.async {
                                self.notificationLabel.text = json["notification_message"]! as? String
                            }
                            
                        }catch let jsonErr{
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                            }
                            print(jsonErr.localizedDescription)
                        }
                    }
                }
                
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                }
            }
            
        }
        
        
        func menuList(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if  let menuList =   NSURL(string:Constants.URL_REQUEST_USER_MENULIST) {
                var request = URLRequest(url:menuList as URL)
                var menu:MenuList = MenuList()
                if userId != nil {
                    menu.apartmentId = UserDefaults.standard.value(forKey: "apartmentId") as? String
                    menu.userId = self.userId
                    guard let userApartmentid =  menu.apartmentId,let userId =  menu.userId  else{return}
                    menuListParams = ["userid":userId,"apartmentid":userApartmentid]
                }else{
                    menu.apartmentId = GuestapartmentId
                    guard let guestApartmentId =  menu.apartmentId else{return}
                    menuListParams = ["apartmentid":guestApartmentId]
                }
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: menuListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let fetchData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print(fetchData)
                        
                        if (fetchData["code"] as! NSNumber == 200) {
                            if let menuResponse = fetchData["menu_result"] as? [[String:AnyObject]]{
                                for menuResult in menuResponse {
                                    if menuResult.count > 0 {
                                        let menulistImages = MenuListImage()
                                        guard let menuImage = menuResult["image"] as? String , let menuId = menuResult["id"] as? String ,let menuTitle = menuResult["title"] as? String ,let enableStatus = menuResult["enable_status"] as? String else{
                                            
                                            return
                                        }
                                        
                                        menulistImages.menuImage = menuImage
                                        menulistImages.menuId = menuId
                                        menulistImages.menuTitle = menuTitle
                                        menulistImages.enableStatus = enableStatus
                                        self.menuItemArrayList.append(menulistImages)
                                        
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    self.collectionView.reloadData()
                                }
                            }
                            
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                            
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                task.resume()
            }else{
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                    
                }
            }
        }
        
        func smileyCash(){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let smileyCash =   NSURL(string:Constants.URL_REQUEST_USER_SMILEYCASH) {
                var request = URLRequest(url:smileyCash as URL)
                var cashAmount:SmileyCash = SmileyCash()
                cashAmount.page_number = "1"
                cashAmount.userid = userId
                cashAmount.required_count = "1"
                guard let pageNumber =  cashAmount.page_number,let userId = cashAmount.userid,let required_count =  cashAmount.required_count else{return}
                let cashListParams:[String:Any] = ["userid":userId,"page_number":pageNumber,"required_count":required_count] as [String:Any]
                request.httpMethod = "POST"
                do {
                    let  json = try JSONSerialization.data(withJSONObject: cashListParams)
                    request.httpBody = json
                }
                catch let jsonErr  {
                    
                    print(jsonErr.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    
                    guard let data = data, error == nil else
                        
                    {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let fetchSmileyData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                            return
                        }
                        print("fetchSmileyData",fetchSmileyData)
                        print(fetchSmileyData["description"]!)
                        print(fetchSmileyData["message"]!)
                       // print("CASH",fetchSmileyData["smiley_cash"])
                        DispatchQueue.main.async {
                            
                            if  let  smileyCashValue = fetchSmileyData["smiley_cash"]{
                                
                                let sCash = "Rs. " + "\(smileyCashValue)"

                                self.smileyLabel.text = sCash

                            }
                            
                            
                        }
                        if (fetchSmileyData["code"] as! NSNumber == 200) {
                            
                            if let smileyResult = fetchSmileyData["smileycash_result"] as? [[String:AnyObject]]{
                                for smileyResponse in smileyResult {
                                    
                                    if smileyResponse.count > 0 {
                                        
                                        guard (smileyResponse["id"] as? String) != nil else{
                                            
                                            return
                                        }
                                        
                                        print(smileyResponse["id"] as! String)
                                        
                                    }
                                }
                            }
                            
                            //    guard (fetchSmileyData["smiley_cash"] as? String) != nil else{
                            //
                            //            return
                            //        }
                            
                        }else{
                            
                            //
                            //    DispatchQueue.main.async {
                            //
                            //       self.view.makeToast(fetchSmileyData["description"]! as! String, duration: 4.0, position: CGPoint(x: 180, y: 340))
                            //
                            //    }
                            
                        }
                    }catch let jsonErr{
                        DispatchQueue.main.async
                            {
                                self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                                
                                
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                }
                
                task.resume()
            }else{
                
                DispatchQueue.main.async
                    {
                        self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                        
                        
                }
                
            }
        }
        
        
        /* checking the order payment success */
        
        func orderPaymentSuccess(orderId:String){
            if let subscriptionListUrl = NSURL(string: Constants.URL_REQUEST_USER_ORDERPAYMENTSUCCESS + orderId) {
                var urlRequest = URLRequest(url: subscriptionListUrl as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    
                    if (error != nil) {
                        
                        print(error!)
                    }
                    
                    if let response = response {
                        
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    do {
                        
                        let orderPaymentSuccessJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String: AnyObject]
                        
                        print(orderPaymentSuccessJson)
                        
                        if orderPaymentSuccessJson["code"] as! NSNumber == 200 {
                            
                        }
                        else {
                            
                            DispatchQueue.main.async {
                            }
                        }
                    }
                    catch let jsonErr {
                        
                        print(jsonErr.localizedDescription)
                    }
                    
                })
                task.resume()
                
            }
            
        }
        
        /* creating the function for order payment fail */
        
        func orderPaymentFail(orderId:String){
            if let subscriptionListUrl = NSURL(string: Constants.URL_REQUEST_USER_ORDERPAYMENTFAIL + orderId) {
                var urlRequest = URLRequest(url: subscriptionListUrl as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    
                    if (error != nil) {
                        
                        print(error!)
                    }
                    
                    if let response = response {
                        
                        print(response)
                    }
                    
                    guard let data = data else {
                        
                        return
                    }
                    do {
                        
                        let orderPaymentFailJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String: AnyObject]
                        
                        print(orderPaymentFailJson)
                        
                        if orderPaymentFailJson["code"] as! NSNumber == 200 {
                            
                        }
                        else {
                            
                            DispatchQueue.main.async {
                            }
                        }
                    }
                    catch let jsonErr {
                        
                        print(jsonErr.localizedDescription)
                    }
                    
                })
                task.resume()
                
            }
        }
        
        func getUserData(userApikey: String?) {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestGetUserData = NSURL(string: Constants.URL_REQUEST_USER_USERDATA + userApikey!) {
                var urlRequest = URLRequest(url: requestGetUserData as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    
                    if let response = response {
                        
                        print(response)
                    }
                    
                    guard let data = data else {
                        
                        return
                    }
                    
                    do {
                        guard let userDataJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{
                            return
                        }
                        print(userDataJsonObject)
                        if userDataJsonObject["code"] as! NSNumber == 200 {
                            guard  let userData = userDataJsonObject["user_result"] as?
                                [String: AnyObject] else{return}
                            DispatchQueue.main.async {
                                guard let apartmentId =  userData["appartment_id"] as? String else{return}
                                UserDefaults.standard.set(apartmentId, forKey: "apartmentId")
                                if Reachability.isConnectedToNetwork(){
                                    self.menuList()
                                    
                                }else{
                                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                                    
                                }
                            }
                            
                            //  let apartmentsJsonArray = userDataJsonObject["apartment_result"] as? [String: AnyObject]
                            //  let apartmentResultJson = apartmentsJsonArray?["apartment_result"] as? [[String: AnyObject]]
                            //  for apartmentArray in apartmentResultJson! {
                            //  let id = apartmentArray["id"]
                            //  let title = apartmentArray["title"]
                            //  let address = apartmentArray["address"]
                            //  let location = apartmentArray["location"]
                            //  var apartmentModel = ApartmentModel()
                            //  apartmentModel.address = address as? String
                            //  apartmentModel.id = id as? String
                            //  apartmentModel.location = location as? String
                            //  apartmentModel.title = title as? String
                            //  self.apartmentsArrayList.append(apartmentModel)
                        }
                        else {
                            
                            DispatchQueue.main.async {
                                //  self.view.makeToast("Unable to fetch Your Data", duration: 4.0, position: CGPoint(x: 180, y: 340))
                                
                            }
                            
                        }
                    }
                    catch let jsonErr {
                        
                        print(jsonErr.localizedDescription)
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)    }
                    }
                    
                })
                
                task.resume()
                
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                }
            }
        }
        
        func getCartCountValue(userId:String){
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_CARTCOUNT + userId) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    
                    do {
                        guard let cartData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                            return
                        }
                        
                        DispatchQueue.main.async {
                            if  let  cartCount = cartData["cart_count"] {
                                //    self.navigationItem.rightBarButtonItem?.addBadge(number: cartCount as! Int)
                                if cartCount as! Int == 0{
                                    rightCartButtonItem.removeBadge()
                                }else{  rightCartButtonItem.addBadge(number: cartCount as! Int)
                                    
                                }
                            }
                        }
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async
                            {
                                self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                                
                                
                        }
                        print(jsonErr.localizedDescription)
                    }
                    
                })
                task.resume()
                
            }else{
                DispatchQueue.main.async
                    {
                        self.view.makeToast("Unable To Fecth Data", duration: 3.0, position: .center)
                        
                        
                }
                
            }
        }
        
        /* setting the status bar style */
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        
        @IBAction func transactionBtnAction(_ sender: Any) {
            if userId != nil {
              /*  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let transactionController = storyBoard.instantiateViewController(withIdentifier: "Transaction") as! MyTransactionsViewController
                let transactionNavigation = UINavigationController(rootViewController: transactionController)
                self.present(transactionNavigation, animated: true, completion: nil)*/
                
                let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let addMoneyController = storyBoard.instantiateViewController(withIdentifier: "AddMoney") as! AddMoneyViewController
                let addMoneyNavigationController = UINavigationController(rootViewController: addMoneyController)
                self.present(addMoneyNavigationController, animated: true, completion: nil)
                
            }else{
                DispatchQueue.main.async {
                    
                    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                    self.present(signInController, animated: false, completion: nil)
                }
            }
        }
        
        
        func getProblemList() {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_CONTACTSUBJECTLIST) {
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    contactListArray.removeAll()
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    
                    guard let data = data else {
                        return
                    }
                    contactListData = [ContactsModel?]()
                    do {
                        
                        guard  let listData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        print(listData)
                        guard let contactsubjectList: [[String: String]] = listData["subject_list"] as? [[String: String]] else{return}
                        
                        for contactList in contactsubjectList {
                            var contacts = ContactsModel()
                            guard let subject = contactList["subject"],let subject_id = contactList["suject_id"] else{
                                return
                            }
                            contacts.subject = subject
                            contacts.subject_id = subject_id
                            contactListArray.append(subject)
                            contactListData.append(contacts)
                            if contactListData.count > 0 {
                                DispatchQueue.main.async {
                                    
                                }
                            }
                        }
                        
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                })
                task.resume()
                
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
            }
        }
        
    }//class
    
    extension UIImageView{
        
        func download(url:String?){
            let request = URLRequest(url: URL(string: url!)!)
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if error != nil{
                    
                    print(error!)
                }
                else{
                    DispatchQueue.main.async {
                        self.image = UIImage(data: data!)
                    }
                }
            }
            task.resume()
        }
    }
    
    extension UIImageView{
        func menuImageDownload(url:String) {
            if  let menuList =   NSURL(string:url) {
                let request = URLRequest(url:menuList as URL)
                let task = URLSession.shared.dataTask(with: request) { (data, reponse, error) in
                    guard let data = data else {
                        return
                    }
                    
                    if error != nil{
                        print(error!)
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.image = UIImage(data: data)
                    }
                }
                task.resume()
            }
        }
    }
    
    
   

    
    /*
     
     
     
     
     let firstFrame = CGRect(x: 350, y: 0, width: 19, height: 21)
     let secondFrame = CGRect(x: 300, y: 0, width: 19, height: 21)
     let firstLabel = UILabel(frame: firstFrame)
     firstLabel.backgroundColor = UIColor.white
     firstLabel.textColor = UIColor.orange
     firstLabel.textAlignment = .center
     firstLabel.text = "0"
     firstLabel.layer.cornerRadius = firstLabel.frame.width / 2
     firstLabel.layer.masksToBounds = true
     let secondLabel = UILabel(frame: secondFrame)
     secondLabel.text = "0"
     secondLabel.backgroundColor = UIColor.white
     secondLabel.textColor = UIColor.orange
     secondLabel.layer.cornerRadius = secondLabel.frame.width / 2
     secondLabel.textAlignment = .center
     secondLabel.layer.masksToBounds = true
     self.navigationController?.navigationBar.addSubview(firstLabel)
     self.navigationController?.navigationBar.addSubview(secondLabel)
     
     
     
     
     */
    
    
