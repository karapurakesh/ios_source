  //
  //  ForgotPasswordController.swift
  //  SmileyServe
  //
  //  Created by Apple on 04/07/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //

  import UIKit
  import SVProgressHUD
  class ForgotPasswordController: UIViewController,UITextFieldDelegate {
  @IBOutlet var forgotPasswordTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    override func viewDidLoad() {
  super.viewDidLoad()

  //self.navigationItem.hidesBackButton = true
submitBtn.layer.cornerRadius = 5
  navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigation))
  UINavigationBar.appearance().barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)

  UINavigationBar.appearance().tintColor = UIColor.white
  UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
  forgotPasswordTextField.delegate = self
  forgotPasswordTextField.underlined()

  }
  func handleNavigation() {
  self.dismiss(animated: false, completion: nil)
  }
  @IBAction func forgotPasswordButton(_ sender: Any) {
  if Reachability.isConnectedToNetwork(){
  self.forgotPassword()
  }else{

  self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
  }
  }
  func forgotPassword(){
  forgotPasswordTextField.resignFirstResponder()
  var forgot:ForgotPassword = ForgotPassword()
  forgot.forgotpassword =  forgotPasswordTextField.text
  if (forgotPasswordTextField.text?.isEmpty)!{
  DispatchQueue.main.async {
  self.view.makeToast("please fill the mobile number", duration: 3.0, position: .center)
  }
  }else if !((forgotPasswordTextField.text?.isPhone())!){
  DispatchQueue.main.async {
  self.view.makeToast("phone number is not valid", duration: 3.0, position: .center)
  }
  }
  else{
  DispatchQueue.main.async {
  SVProgressHUD.setDefaultStyle(.dark)
  SVProgressHUD.show(withStatus: "please wait...")
  }
  if let signupURL =  NSURL(string:Constants.URL_REQUEST_USER_FORGOTPASSWORD) {
  var  request = URLRequest(url:signupURL as URL)
  guard let forgotPassword = forgot.forgotpassword else {return}
  let  signupParams:[String:Any] = ["mobile":forgotPassword]
  request.httpMethod = "POST"
  do {
  let  json = try JSONSerialization.data(withJSONObject: signupParams)
  request.httpBody = json
  }
  catch let jsonErr  {
  print(jsonErr.localizedDescription)
  }
  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
  let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
  DispatchQueue.main.async {
      SVProgressHUD.dismiss()
  }
  guard let data = data, error == nil else
  {
  DispatchQueue.main.async {
  self.view.makeToast("Unable to connect server", duration: 3.0, position: .center)
      }
      print(error?.localizedDescription as Any )
      return
  }
  do {
      let fetchData = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
      if let registerResponse = fetchData as? [String:Any] {
          print(registerResponse["message"]!)
          print(registerResponse["description"]!)
          DispatchQueue.main.async {
              if (registerResponse["code"] as! NSNumber == 200){
                  DispatchQueue.main.async {
                    
              UserDefaults.standard.set(registerResponse["description"]! as! String, forKey: "forgotpasswordotp")
                  }
                  DispatchQueue.main.async {
                      let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                      let otpcontroller = storyBoard.instantiateViewController(withIdentifier: "forgotOtp") as! ForgotOtpController
                      let signInnavigationcontroller = UINavigationController(rootViewController: otpcontroller)
                      self.present(signInnavigationcontroller, animated: true, completion: nil)
                  }
              }
              else {
                  DispatchQueue.main.async {
                    
                  self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
                  }
              }
          }
      }
  }
  catch let jsonErr{
       DispatchQueue.main.async {
  self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
      print(jsonErr.localizedDescription)
  }
  }
  }
  task.resume()
  }else{
  DispatchQueue.main.async {
  self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
  }

  }
  }
  }
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
  textField.resignFirstResponder()
  return true
  }

  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
  self.view.endEditing(true)

  }
  }//class

  extension UITextField {
  func underlined(){
  let border = CALayer()
  let width = CGFloat(1.0)
  border.borderColor = UIColor.lightGray.cgColor
  border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
  border.borderWidth = width
  self.layer.addSublayer(border)
  self.layer.masksToBounds = true
  }
  }














