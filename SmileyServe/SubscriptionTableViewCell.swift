//
//  SubscriptionTableViewCell.swift
//  SmileyServe
//
//  Created by Apple on 21/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class SubscriptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var editSubscriptionBtnRef: UIButton!
    @IBOutlet weak var productDeleiveryCharges: UILabel!
    @IBOutlet weak var productprice: UILabel!
    @IBOutlet weak var productUnits: UILabel!
    
    @IBOutlet weak var productImageView:CustomImageView!
    
    @IBOutlet weak var productTitle: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        editSubscriptionBtnRef.layer.cornerRadius = 5
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
