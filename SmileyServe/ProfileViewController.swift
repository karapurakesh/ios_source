    //
    //  ProfileViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 06/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    
    //
    
    
    import UIKit
    import SVProgressHUD
    
    class ProfileViewController: BaseViewController,UITextFieldDelegate,MCountryPickerDelegate,LBZSpinnerDelegate {
        
        
       // @IBOutlet weak var apartmentDropDownSpinnerView: LBZSpinner!
        @IBOutlet weak var blockListDropDownSpinnerView: LBZSpinner!
        var apartmentListArray = [String]()
        var blockListArray = [String]()
        var apartmentId =  UserDefaults.standard.value(forKey: "apartmentid") as? String
        var blockId    : String?
        var selectedBlockItemPosition: Int = 0
        var apartmentsArrayList = [ApartmentModel]()
        var blockArrayList = [BlockModel]()
        let userApiKey = UserDefaults.standard.value(forKey: "id")
        var locationListData = [String]()
        var blockListData = [String]()
        @IBOutlet var profilePlotNoTextField: UITextField!
        @IBOutlet var profileEmailIDTextField: UITextField!
        @IBOutlet var profileScrollView: UIScrollView!
        @IBOutlet var designViewHeightConstraint: NSLayoutConstraint!
        @IBOutlet var flatNoHeihtConstraint: NSLayoutConstraint!
        @IBOutlet var profilePlotNoTopSpaceConstraint: NSLayoutConstraint!
        @IBOutlet var profileMobileNumberTextField: UITextField!
        @IBOutlet var profileNameTextField: UITextField!
        
        @IBOutlet var profileEditBtntitle: UIButton!
        @IBOutlet var blockLabel: UILabel!
        
        @IBOutlet weak var blockLblTopSpaceCont: NSLayoutConstraint!
        @IBOutlet weak var blockViewTopSpaceCosnt: NSLayoutConstraint!
        @IBOutlet var apartmentTextField: UITextField!
       // @IBOutlet var blockTextField: UITextField!
        var apartmentsNamesGbl = [MCountry]()


        @IBOutlet weak var blockLblHtConstnt: NSLayoutConstraint!
        override func viewDidLoad() {
            super.viewDidLoad()
            profileEditBtntitle.layer.cornerRadius = 5
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationUPdateProfile))
            profilePlotNoTextField.isUserInteractionEnabled = false
            profileMobileNumberTextField.isUserInteractionEnabled = false
            profileEmailIDTextField.isUserInteractionEnabled = false
            profileNameTextField.isUserInteractionEnabled = false
           // apartmentDropDownSpinnerView.isUserInteractionEnabled = false
            blockListDropDownSpinnerView.isUserInteractionEnabled = false
            apartmentTextField.isUserInteractionEnabled = false
            //blockTextField.isUserInteractionEnabled = false
            
            profilePlotNoTextField.profileUnderLined()
            profileMobileNumberTextField.profileUnderLined()
            profileEmailIDTextField.profileUnderLined()
            profileNameTextField.profileUnderLined()
            
            apartmentTextField.profileUnderLined()
           // blockTextField.profileUnderLined()
            
            self.initializeViews()
            if Reachability.isConnectedToNetwork() {
                if userId != nil{
                    self.getUserData(userApikey: userApiKey as? String)
                }else{
                    
                    DispatchQueue.main.async {
                        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
                        self.present(signInController, animated: false, completion: nil)
                    }
                }
            } else {
                self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
            }
        }
        
        func spinnerChoose(_ spinner: LBZSpinner, index: Int, value: String) {
            var spinnerName = ""
          /*  if spinner == apartmentDropDownSpinnerView { spinnerName = "spinnerTop"
                selectedBlockItemPosition = index
                if apartmentId != self.apartmentsArrayList[index].id {
                    self.showAlertDialog(apartmentId: self.apartmentsArrayList[index].id)
                }
                print("Spinner : \(spinnerName) : { Index : \(index) - \(value) }")
            }else{
                if spinner == blockListDropDownSpinnerView { spinnerName = "spinnerTop"
                }
                print("Spinner : \(spinnerName) : { Index : \(index) - \(value) }")
                if blockArrayList.count == 0 {
                    blockId = "0"
                } else {
                    blockId = self.blockArrayList[index].blockId
                    print("blockID:\(String(describing: blockId))")
                }
            }*/
            
            if spinner == blockListDropDownSpinnerView { spinnerName = "spinnerTop"
            }
            print("Spinner : \(spinnerName) : { Index : \(index) - \(value) }")
            if blockArrayList.count == 0 {
                blockId = "0"
            } else {
                blockId = self.blockArrayList[index].blockId
                print("blockID:\(String(describing: blockId))")
            }
            
        }
        
        func initializeViews() {
            profileScrollView.delaysContentTouches = false
           // apartmentDropDownSpinnerView.delegate = self
            blockListDropDownSpinnerView.delegate = self
            profilePlotNoTextField.delegate = self
            profileEmailIDTextField.delegate = self
            profileMobileNumberTextField.delegate = self
            profileNameTextField.delegate = self
            profileEditBtntitle.isHidden = false
            apartmentTextField.delegate = self
           // blockTextField.delegate = self
            self.title = "Profile"
        }
        
        
        
        func getUserData(userApikey: String?) {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestGetUserData = NSURL(string: Constants.URL_REQUEST_USER_USERDATA + userApikey!) {
                var urlRequest = URLRequest(url: requestGetUserData as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        self.apartmentListArray.removeAll()
                        self.apartmentsArrayList.removeAll()
                        SVProgressHUD.dismiss()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    do {
                        
                        guard let userDataJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{
                            return
                        }
                        print("userDataJsonObject",userDataJsonObject)
                        if userDataJsonObject["code"] as! NSNumber == 200 {
                            guard  let userData = userDataJsonObject["user_result"] as?
                                [String: AnyObject] else{return}
                            DispatchQueue.main.async {
                                guard  let userName = userData["name"],  let userEmail = userData["email"],let userMobile = userData["mobile"], let apartmentName = userData["apartment"] as? String,let blockName = userData["block"] as? String,let platNo = userData["plotno"],  let blockId=userData["block_id"] else {return}
                                self.apartmentId = userData["appartment_id"] as? String
                                self.blockId = blockId as? String
                                self.profileNameTextField.text = userName as? String
                                self.profileEmailIDTextField.text = userEmail as? String
                                self.profileMobileNumberTextField.text = userMobile as? String
                                self.profilePlotNoTextField.text = platNo as? String
                                //self.apartmentDropDownSpinnerView.text = apartmentName
                                self.apartmentTextField.text = apartmentName
                                self.blockListDropDownSpinnerView.text = blockName
                            }
                            guard let apartmentsJsonArray = userDataJsonObject["apartment_result"] as? [String: AnyObject] else{
                                return}
                            guard let apartmentResultJson = apartmentsJsonArray["apartment_result"] as? [[String: AnyObject]] else{return}
                            for apartmentArray in apartmentResultJson {
                                let id = apartmentArray["id"]
                                let title = apartmentArray["title"] as? String
                                let address = apartmentArray["address"]
                                let location = apartmentArray["location"]
                                var apartmentModel = ApartmentModel()
                                apartmentModel.address = address as? String
                                apartmentModel.id = id as? String
                                apartmentModel.location = location as? String
                                apartmentModel.title = title
                                self.apartmentsArrayList.append(apartmentModel)
                                if let apartmentTitle = title{
                                    
                                    let apartWithLocation = String(format: "%@, %@", apartmentTitle,apartmentModel.location!)

                                    print("apartWithLocation",apartWithLocation)
                                    
                                    self.apartmentListArray.append(apartWithLocation)
                                }
                            }
                            DispatchQueue.main.async {
                                if self.apartmentListArray.count > 0 {
                                    
                                    var items = [MCountry]()
                                    
                                    for value in self.apartmentListArray {
                                        items.append(MCountry.init(name: value))
                                    }
                                    
                                    self.apartmentsNamesGbl = items

                                    //self.apartmentDropDownSpinnerView.updateList(self.apartmentListArray)
                                }
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable to  fetch  Data", duration: 3.0, position: .center)
                            }
                        }
                    }
                    catch let jsonErr {
                        print(jsonErr.localizedDescription)
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to  fetch  Data", duration: 3.0, position: .center)
                        }
                    }
                })
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable to  fetch  Data", duration: 3.0, position: .center)    }
            }
        }
        
        
        func countryPicker(_ picker: CountriesViewController, didSelectCountryWithName name: String) {
            picker.navigationController?.popViewController(animated: true)
            
            self.selectedBlockItemPosition = apartmentListArray.index(of: name)!
            print("INDEX",self.selectedBlockItemPosition)

            self.apartmentTextField.text = name
            
            
            self.showAlertDialog(apartmentId: self.apartmentsArrayList[self.selectedBlockItemPosition].id)


        }
        
        
        func getBlockListData(apartmentId: String?) {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_BLOCKLISTDATA + apartmentId!) {
                print(requestLocationListURL)
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.blockListArray.removeAll()
                        self.blockArrayList.removeAll()
                    }
                    if (error != nil) {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                        }
                    }
                    if let response = response {
                        print(response)
                    }
                    guard let data = data else {
                        return
                    }
                    do {
                        
                        guard let blockData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                        print(blockData)
                        guard  let blockJsonArray = blockData["blocks"] as? [[String: AnyObject]] else{return}
                        for blockArray in blockJsonArray {
                            let blockId = blockArray["block_id"]
                            let blockName = blockArray["block_name"] as? String
                            var blockModel = BlockModel()
                            blockModel.blockId = blockId as? String
                            blockModel.blockName = blockName
                            self.blockArrayList.append(blockModel)
                            if let block_Name = blockName{
                                self.blockListArray.append(block_Name)
                            }
                        }
                        DispatchQueue.main.async {
                            if self.blockArrayList.count > 0 {
                                
                               self.blockListDropDownSpinnerView.isUserInteractionEnabled = true
                                self.blockListDropDownSpinnerView.isHidden = false
                                self.blockListDropDownSpinnerView.text = "Please select a block"
                                self.blockLabel.isHidden = false
                                self.blockListDropDownSpinnerView.updateList(self.blockListArray)
                                self.blockListDropDownSpinnerView.textColor = UIColor.black

                            } else {
                                self.blockId = "0"
                                self.blockListDropDownSpinnerView.isUserInteractionEnabled = false
                                self.blockListDropDownSpinnerView.textColor = UIColor.lightGray
                                self.blockListDropDownSpinnerView.text = "Blocks are not available"
                            }
                        }
                    }
                    catch let jsonErr {
                        print(jsonErr.localizedDescription)
                        self.blockId = "0"
                    }
                })
                task.resume()
            }
        }
        
        
        @IBAction func profileEditBtnAction(_ sender: Any) {
            if profileEditBtntitle.currentTitle == "EDIT"{
                profilePlotNoTextField.isUserInteractionEnabled = true
                profileMobileNumberTextField.isUserInteractionEnabled = true
                profileEmailIDTextField.isUserInteractionEnabled = false
                profileNameTextField.isUserInteractionEnabled = true
                //apartmentDropDownSpinnerView.isUserInteractionEnabled = true
                profileEditBtntitle.setTitle("UPDATE", for: .normal)
                apartmentTextField.isUserInteractionEnabled = true
              //  blockTextField.isUserInteractionEnabled = true
            }else{
                
                
                if Reachability.isConnectedToNetwork(){
                    self.updateProfile()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)      }
                }
            }
        }
        
        
        func updateProfile(){
            var updateProfile: UpdateProfile = UpdateProfile()
            if (profileNameTextField.text?.isEmpty)! || (profileEmailIDTextField.text?.isEmpty)! || (profilePlotNoTextField.text?.isEmpty)! || (profileMobileNumberTextField.text?.isEmpty)! {
                DispatchQueue.main.sync {
                    self.view.makeToast("please fill all the fields", duration: 3.0, position: .center)
                }
            }
            else {
                DispatchQueue.main.async {
                    SVProgressHUD.setDefaultStyle(.dark)
                    SVProgressHUD.show(withStatus: "please wait...")
                }
                updateProfile.city = "NA"
                updateProfile.state = "NA"
                updateProfile.area = "NA"
                updateProfile.address = "NA"
                updateProfile.pincode = "NA"
                updateProfile.plotno = profilePlotNoTextField.text
                updateProfile.username = profileNameTextField.text
                updateProfile.apartment = apartmentId
                updateProfile.userEmail = profileEmailIDTextField.text
                if self.blockId == nil{
                    updateProfile.blockid = "0"
                }else{
                    updateProfile.blockid = self.blockId
                }
                updateProfile.usermobile = profileMobileNumberTextField.text
                updateProfile.userid = userApiKey as? String
                if let updateProfileURL = NSURL(string: Constants.URL_REQUEST_USER_UPDATEPROFILE) {
                    var request = URLRequest(url: updateProfileURL as URL)
                    guard let userName =  updateProfile.username,let userId =  updateProfile.userid,let
                        userCity =  updateProfile.city,let userArea =   updateProfile.area,let userState =  updateProfile.state,let userPlotNo =  updateProfile.plotno,let userMobile = updateProfile.usermobile,let userApartment =  updateProfile.apartment,let userAddress = updateProfile.address,let userPincode = updateProfile.pincode,let userBlockId =  updateProfile.blockid  else{return}
                    let updateProfileParams = ["username": userName, "userid": userId, "city": userCity, "area": userArea, "state":userState, "plotno": userPlotNo, "usermobile": userMobile, "apartment":userApartment, "address": userAddress, "pincode": userPincode, "blockId":userBlockId] as [String:Any]
                    print(updateProfileParams)
                    request.httpMethod = "POST"
                    do {
                        let json = try JSONSerialization.data(withJSONObject: updateProfileParams)
                        request.httpBody = json
                    }
                    catch let jsonErr {
                        
                        print(jsonErr.localizedDescription)
                    }
                    
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                        guard let data = data, error == nil else
                            
                        {
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                            }
                            print(error?.localizedDescription as Any)
                            return
                        }
                        
                        do {
                            let fetchData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                            if let registerResponse = fetchData as? [String: Any] {
                                if (registerResponse["code"] as! NSNumber == 200) {
                                    DispatchQueue.main.async {
                                        self.profileEmailIDTextField.isUserInteractionEnabled = true
                                        UserDefaults.standard.set(updateProfile.username, forKey: "name")
                                        UserDefaults.standard.set(updateProfile.userEmail, forKey: "email")
                                        UserDefaults.standard.set(updateProfile.usermobile, forKey: "mobile")
                                        UserDefaults.standard.set(self.blockId, forKey: "blockid")
                                        UserDefaults.standard.set(updateProfile.apartment, forKey: "apartmentid")
                                        UserDefaults.standard.set(updateProfile.plotno, forKey: "plotno")
                                        //UserDefaults.standard.set(self.apartmenttextField.text, forKey: "apartmentname")
                                        self.profileEmailIDTextField.text =  updateProfile.userEmail
                                        self.profileMobileNumberTextField.text = UserDefaults.standard.value(forKey: "mobile") as? String
                                        self.profileNameTextField.text = UserDefaults.standard.value(forKey: "name") as? String
                                        DispatchQueue.main.async {
                                            self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
                                        }
                                        self.perform(#selector(self.delayToHome), with: self, afterDelay: 1)
                                        UserDefaults.standard.set(registerResponse["message"] as? String, forKey: "updatesuccess")
                                    }
                                }
                                else {
                                    DispatchQueue.main.async {
                                        self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
                                    }
                                }
                            }
                        } catch let jsoerr {
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                            }
                            print(jsoerr.localizedDescription)
                        }
                        
                    }
                    task.resume()
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    
                }
                
            }
        }
        
        func delayToHome(){
            DispatchQueue.main.async {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                let signupNavigation = UINavigationController(rootViewController: signupController)
                self.present(signupNavigation, animated: true, completion: nil)
            }
        }
        
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            if textField.tag == 40 {
                
                textField.resignFirstResponder()

                print("MOVE TO APARTMENT SCREEN")
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let countriesViewController = storyBoard.instantiateViewController(withIdentifier: "countriesViewController") as! CountriesViewController
                countriesViewController.unsourtedCountries = self.apartmentsNamesGbl
                countriesViewController.delegate = self
                self.navigationController?.pushViewController(countriesViewController, animated: true)
                
            }
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
        
        
        func handleNavigationUPdateProfile() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let signupNavigation = UINavigationController(rootViewController: signupController)
            self.present(signupNavigation, animated: false, completion: nil)
        }
        
        
        func customizeNavigationBars() {
            
            self.navigationController?.navigationBar.tintColor =  UIColor.white;            navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 255 / 255, green: 87 / 255, blue: 34 / 255, alpha: 1)
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        }
        
        
        func showAlertDialog(apartmentId:String?){
            let alertController = UIAlertController(title: "Confirmation Message", message: "All your subscribed orders will be cancelled and amount refund to Smileycash.Are you sure want to change your apartment?", preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "YES", style: .default) { (action:UIAlertAction!) in
                print("you have pressed OK button");
                self.apartmentId = apartmentId
                if  Reachability.isConnectedToNetwork(){
                    self.getBlockListData(apartmentId: apartmentId)
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    }
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
    }//class
    
    extension UITextField {
        func profileUnderLined(){
            let border = CALayer()
            let width = CGFloat(1.0)
            border.borderColor = UIColor.black.cgColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        }
    }
    
    
    
    
    
    
    
