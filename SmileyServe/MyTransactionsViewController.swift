    //
    //  MyTransactionsViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 06/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //

    import UIKit
    import SVProgressHUD

    class MyTransactionsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    let userId = UserDefaults.standard.value(forKey: "id") as? String
   // @IBOutlet var addMoneyReferenceButton: UIButton!
    @IBAction func handleBackTransactionBarBtn(_ sender: Any) {
    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
    let signupNavigation = UINavigationController(rootViewController: signupController)
    self.present(signupNavigation, animated: false, completion: nil)
    }
    var tranactionArrayList:[TransactionModel?] = []
        
    @IBAction func addMoneySmileyWalletBtn(_ sender: Any) {
    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let addMoneyController = storyBoard.instantiateViewController(withIdentifier: "AddMoney") as! AddMoneyViewController
    let addMoneyNavigationController = UINavigationController(rootViewController: addMoneyController)
    self.present(addMoneyNavigationController, animated: true, completion: nil)

    }
    //@IBOutlet var smileyLabel: UILabel!
    @IBOutlet var transactionTableView : UITableView!
    override func viewDidLoad() {
    super.viewDidLoad()
    transactionTableView.delegate  = self
    transactionTableView.dataSource = self
    //self.addSlideMenuButton()
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_tab_cart.png"), style: .plain, target: self, action: #selector(handleCart))
    navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        self.navigationController?.navigationBar.tintColor =  UIColor.white
        self.getCartCountValue(userId: userId!)
    self.title = "MyTransactions"
    self.transactionTableView.isHidden = true
    self.transactionTableView.separatorStyle = .none
  //  self.addMoneyReferenceButton.isHidden = false
    self.transactionNetworkCall()

    }
    func handleCart() {
    DispatchQueue.main.async {
    SVProgressHUD.setDefaultStyle(.dark)
    SVProgressHUD.show(withStatus: "please wait...")
    }
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
    let cartNavigationController = UINavigationController(rootViewController: controller)
    self.present(cartNavigationController, animated: true, completion: nil)
    }
    func getCartCountValue(userId:String){
    if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_CARTCOUNT + userId) {
    var urlRequest = URLRequest(url: requestLocationListURL as URL)
    urlRequest.httpMethod = "GET"
    let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    }
    if (error != nil) {
    DispatchQueue.main.async {
    self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
    }
    }
    if let response = response {
    print(response)
    }
    guard let data = data else {
    return
    }

    do {
    guard let cartData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
    return
    }

    DispatchQueue.main.async {
    if  let  cartCount = cartData["cart_count"] {
    if cartCount as! Int == 0{
    self.navigationItem.rightBarButtonItem?.removeBadge()
    }else{
    self.navigationItem.rightBarButtonItem?.addBadge(number: cartCount as! Int)
    }


    }
    SVProgressHUD.dismiss()
    }
    }
    catch let jsonErr {
    DispatchQueue.main.async
    {
    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
    }
    print(jsonErr.localizedDescription)
    }

    })
    task.resume()

    }else{
    DispatchQueue.main.async
    {
    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
    }

    }
    }


    func transactionNetworkCall(){

    if Reachability.isConnectedToNetwork(){
    self.smileyCash()
    }else{
    DispatchQueue.main.async {
    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
    }
    }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.tranactionArrayList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell", for: indexPath) as! TransactionTableViewCell
    cell.selectionStyle = .none
    cell.headingTitleLbl.text = self.tranactionArrayList[indexPath.row]?.headingTitle
    if cell.headingTitleLbl.text == "Transaction" {
    cell.totalAmountLbl.text = "- Rs." + (self.tranactionArrayList[indexPath.row]?.total_amount)!

    }else{

    cell.totalAmountLbl.text = "+ Rs." + (self.tranactionArrayList[indexPath.row]?.total_amount)!
    }


    cell.orderIdLabel.text = "Order Id:" + (self.tranactionArrayList[indexPath.row]?.txn_number)!
    cell.orderDateLabel.text = self.tranactionArrayList[indexPath.row]?.txn_date
    cell.smileyCashAmountLabel.text = "Smiley Cash : Rs." + (self.tranactionArrayList[indexPath.row]?.smileycash_amount)!
    cell.onlinePaymentLabel.text = "Payment :Rs." +  (self.tranactionArrayList[indexPath.row]?.onlinepaid_amount)!

    return cell

    }

    func smileyCash(){
    DispatchQueue.main.async {
    SVProgressHUD.setDefaultStyle(.dark)
    SVProgressHUD.show(withStatus: "please wait...")
    }
    if let smileyCash =   NSURL(string:Constants.URL_REQUEST_USER_SMILEYCASH){
    var request = URLRequest(url:smileyCash as URL)
    var cashAmount:SmileyCash = SmileyCash()
    cashAmount.page_number = "1"
    cashAmount.userid = userId
    cashAmount.required_count = "10"
    guard let pageNumber =  cashAmount.page_number,let userId =  cashAmount.userid ,let requiredCount = cashAmount.required_count else{return}
    let cashListParams:[String:Any] = ["userid":userId,"page_number":pageNumber,"required_count":requiredCount]
    request.httpMethod = "POST"
    do {
    let  json = try JSONSerialization.data(withJSONObject: cashListParams)
    request.httpBody = json
    }
    catch let jsonErr  {

    print(jsonErr.localizedDescription)
    }
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    }
    guard let data = data, error == nil else
    {
    DispatchQueue.main.async {
    self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
    }
    print(error?.localizedDescription as Any )
    return
    }

    self.tranactionArrayList = [TransactionModel]()

    do{
    guard let fetchSmileyData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
    print(fetchSmileyData)
    print(fetchSmileyData["description"]!)
    print(fetchSmileyData["message"]!)
    print(fetchSmileyData["smiley_cash"]!)
//    DispatchQueue.main.async {
//    //guard let  smileyCashValue:Float = fetchSmileyData["smiley_cash"] as? Float else{return}
////    self.smileyLabel.text =  "Rs." + "\(smileyCashValue)"
//    }
    if (fetchSmileyData["code"]! as! NSNumber == 200){
    if let smileyResult = fetchSmileyData["smileycash_result"] as? [[String:AnyObject]]{
    for smileyResponse in smileyResult {
    var tranactionList = TransactionModel()
    guard  let headingTitle = smileyResponse["heading_title"] as? String,let txn_number = smileyResponse["txn_number"] as? String,let txn_date = smileyResponse["txn_date"] as? String,let smileycash_amount = smileyResponse["smileycash_amount"] as? String,let total_amount = smileyResponse["total_amount"] as? String,let onlinePaid_amount = smileyResponse["onlinepaid_amount"] as? String else{

    return
    }

    tranactionList.headingTitle = headingTitle
    tranactionList.smileycash_amount = smileycash_amount
    tranactionList.txn_date = txn_date
    tranactionList.txn_number = txn_number
    tranactionList.onlinepaid_amount = onlinePaid_amount
    tranactionList.total_amount = total_amount
    self.tranactionArrayList.append(tranactionList)

    }
    }
    DispatchQueue.main.async {
    self.transactionTableView.reloadData()
    self.transactionTableView.isHidden = false
    }

    }else{

    DispatchQueue.main.async {

    self.view.makeToast(fetchSmileyData["description"]! as! String, duration: 3.0, position: .center)

    }
    }

    }catch let jsonErr{
    DispatchQueue.main.async {
    self.view.makeToast("Unable To Fetch  Data", duration: 3.0, position: .center)

    }
    print(jsonErr.localizedDescription)
    }
    }
    task.resume()
    }else{
    DispatchQueue.main.async {
    self.view.makeToast("Unable To Fetch  Data", duration: 3.0, position: .center)

    }
    }
    }
    func handleNavigationArrow(){
    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
    let signupNavigation = UINavigationController(rootViewController: signupController)
    self.present(signupNavigation, animated: false, completion: nil)

    }

    /* creating a function for applying gradient color to the smiley cash card in My TransactionController */

    func setGradientBackground() {
    let colorTop =  UIColor(red: 38.0/255.0, green: 38.0/255.0, blue: 38.0/255.0, alpha: 1.0).cgColor
    let colorBottom = UIColor(red: 101.0/255.0, green: 101.0/255.0, blue: 102.0/255.0, alpha: 1.0).cgColor

    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [ colorTop, colorBottom]
    gradientLayer.locations = [ 0.5, 1.0]
    gradientLayer.frame = self.view.bounds

    self.view.layer.addSublayer(gradientLayer)
    }
    //AddMoney
    func seagueToAddMoneyScreen(){
    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let addMoneyController = storyBoard.instantiateViewController(withIdentifier: "AddMoney") as! AddMoneyViewController
    let addMoneyNavigationController = UINavigationController(rootViewController: addMoneyController)
    self.present(addMoneyNavigationController, animated: false, completion: nil)

    }


    }//class

    @IBDesignable
    class GradientView: UIView {

    @IBInspectable var cornerRadius : CGFloat = 0
    @IBInspectable var shadowColor : UIColor? = UIColor.black
    @IBInspectable var shadowOffsetWidth : Int = 0
    @IBInspectable var shadowOffsetHeight : Int = 1
    @IBInspectable var shadowOpacity : Float = 0.2
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}

    override class var layerClass: AnyClass { return CAGradientLayer.self }

    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }

    func updatePoints() {
    if horizontalMode {
    gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
    gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
    } else {
    gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
    gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
    }
    }
    func updateLocations() {
    gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
    gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
    }

    override func layoutSubviews() {
    super.layoutSubviews()
    updatePoints()
    updateLocations()
    updateColors()
    layer.cornerRadius = cornerRadius
    layer.shadowColor = shadowColor?.cgColor
    layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
    let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
    layer.shadowPath = shadowPath.cgPath
    layer.shadowOpacity  = shadowOpacity
    }
    }





