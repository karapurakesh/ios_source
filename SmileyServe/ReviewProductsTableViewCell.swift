//
//  ReviewProductsTableViewCell.swift
//  SmileyServe
//
//  Created by Apple on 24/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class ReviewProductsTableViewCell: UITableViewCell {

  @IBOutlet weak var reviewOrderImageView:CustomImageView!
  @IBOutlet weak var reviewQuantityTotalPrice: UILabel!
  @IBOutlet weak var reviewOrderDate: UILabel!
  @IBOutlet weak var reviewOrderDelieveryCharges: UILabel!
  @IBOutlet weak var reviewOrderPricew: UILabel!
  @IBOutlet weak var reviewOrderProductUnits: UILabel!
  @IBOutlet weak var reviewOrderProductName: UILabel!
  @IBOutlet weak var reviewDeleteCartBtnActionRef: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
