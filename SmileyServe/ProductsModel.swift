//
//  Products.swift
//  SmileyServe
//
//  Created by Apple on 22/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct Products {
    
    var categoryId : String?
    var apartmentId : String?
    var userId  : String?
    
    init() {
        //
    }
    
    
    init(productsList:Products) {
        
        self.categoryId = productsList.categoryId
        self.apartmentId = productsList.apartmentId
        self.userId = productsList.userId
    }
}
