//
//  CalendarProductTableViewCell.swift
//  SmileyServe
//
//  Created by Apple on 19/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class CalendarProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var calendarProductImageView: CustomImageView!
    
  @IBOutlet weak var calendarProductQunatityDecrementBtn: UIButton!
    @IBOutlet weak var calendarProductDeleiveryCharges: UILabel!
    @IBOutlet weak var calendarProductPrice: UILabel!
    @IBOutlet weak var calendarProductTitle: UILabel!
    
    @IBOutlet weak var calendarProductQunatity: UILabel!
    @IBOutlet weak var calendarProductUnits: UILabel!
  
    @IBOutlet weak var dcWidthConst: NSLayoutConstraint!
    @IBOutlet weak var rcWidthConst: NSLayoutConstraint!
    @IBOutlet weak var drGapWidthConst: NSLayoutConstraint!

  @IBOutlet weak var calendarProductQuantityBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
