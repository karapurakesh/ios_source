//
//  ProductsLIst.swift
//  SmileyServe
//
//  Created by Apple on 22/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import UIKit

class ProductsList: NSObject {
    
    var productTitle : String?
    var productImage : String?
    var productUnits : String?
    var deleieveryCharges : String?
    var productPrice : String?
    var productId : String?
    var menuId : String?
    var stock_status : String?
    
    var actual_price : String?
    var discounted_price : Int?
    var discount_percentage : String?
    var selling_type : String?

    var subscribed_type : String?
    var cart_qty : String?
    var cartid : String?
    
    
}

class InstantItems : NSObject {
    
    var cart_count : String?
    var cart_id : String?
}
