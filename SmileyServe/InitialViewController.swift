  //
  //  InitialViewController.swift
  //  SmileyServe
  //
  //  Created by Apple on 04/07/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //
  
  import UIKit
  
    class InitialViewController: UIViewController {
    @IBOutlet var enterAsGuestBtnReference: UIButton!
    @IBOutlet var initialViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var signInButtonReference : UIButton!
    @IBOutlet var signUpButtonRefernec : UIButton!
    @IBAction func guestButton(_ sender: Any) {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let guestController = storyBoard.instantiateViewController(withIdentifier:"guest" ) as! GuestViewController
        let signupNavigation = UINavigationController(rootViewController: guestController)
        self.present(signupNavigation, animated: true, completion: nil)
    }
    
    static let signInIdentifier = "Sign"
    static let signUpIdentifier = "SignUp"
    override func viewDidLoad() {
        super.viewDidLoad()
        signInButtonReference.layer.cornerRadius = 5
        signUpButtonRefernec.layer.cornerRadius = 5
        self.enterAsGuestBtnReference.isHidden = true
    }
    
    @IBAction func signUpButton(_ sender: UIButton) {
        sender.pulsate()
        self.perform(#selector(self.signUpButtonAction), with: self, afterDelay: 3)
        signUpButtonAction()
    }
    
    func signUpButtonAction(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signUpIdentifier) as! SignUpController
        self.present(signupController, animated: true, completion: nil)
    }
    
    @IBAction func signInButton(_ sender: UIButton) {
        sender.pulsate()
        signInAction()
    }
    
    func signInAction(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signInController = storyBoard.instantiateViewController(withIdentifier: InitialViewController.signInIdentifier) as! SignInPageViewController
        self.present(signInController, animated: false, completion: nil)
        
    }
  }//class
  
  extension UIButton {
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.6
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 2
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        layer.add(pulse, forKey: "pulse")
    }
    
    func flash() {
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.5
        flash.fromValue = 1
        flash.toValue = 0.1
        flash.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 3
        layer.add(flash, forKey: nil)
    }
    
    
    func shake() {
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        shake.fromValue = fromValue
        shake.toValue = toValue
        layer.add(shake, forKey: "position")
    }
  }
  
  
