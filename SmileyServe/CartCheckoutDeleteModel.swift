//
//  CartCheckoutDeleteModel.swift
//  SmileyServe
//
//  Created by Apple on 21/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct CartCheckoutDeleteModel {
    
    
    var userid : String?
    var checkoutid : String?
    var checkout_date : String?

}
