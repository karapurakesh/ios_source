//
//  CartCheckOutResponseModel.swift
//  SmileyServe
//
//  Created by Apple on 27/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct CartCheckOutResponseModel {
  
  
  var checkoutid : String?
  var selling_price : String?
  var productid : String?
  var deliverycharge : String?
  var  product_org_price : String?
  var current_qty : String?
  var order_date : String?
  var product_name : String?
  var product_image : String?
  var product_units : String?
  var current_total : String?

}
