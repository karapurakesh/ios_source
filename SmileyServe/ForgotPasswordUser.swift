//
//  ForgotPasswordUser.swift
//  SmileyServe
//
//  Created by Apple on 07/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


class Forgot:NSObject{
    
    var view:AnyObject?
    
    var forgot:ForgotPassword?
    
    init(view:AnyObject,forgot:ForgotPassword) {
       self.view = view
      self.forgot = forgot
    }
    
    
    func forgotPassword() {
        let signupURL =  NSURL(string:Constants.URL_REQUEST_USER_FORGOTPASSWORD)
        var  request = URLRequest(url:signupURL! as URL)
        let  signupParams:[String:Any] = ["mobile":forgot!.forgotpassword!]
        request.httpMethod = "POST"
        do {
            let  json = try JSONSerialization.data(withJSONObject: signupParams)
            request.httpBody = json
        }
        catch let jsonErr  {
            
            print(jsonErr.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let data = data, error == nil else
                
            {
                print(error?.localizedDescription as Any )
                
                return
            }
            
            do {
                
                let fetchData = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
                
                if let registerResponse = fetchData as? [String:Any] {
                    
                    print(registerResponse["message"]!)
                    
                    print(registerResponse["description"]!)
                    
                    
                }}
                
            catch let jsonErr{
                
                
                print(jsonErr.localizedDescription)
            }
            
            
        }
        
        
        task.resume()
        
    }
    
 
    
    
    
}//class

