//
//  UpdatePassword.swift
//  SmileyServe
//
//  Created by Apple on 10/07/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct UpdatePassword {
    
    
    var userId : String?
    var userOldPassword : String?
    var userNewPassword : String?
    
    
    init() {
        //
    }
    
    
    init(updateUserPwd:UpdatePassword) {
        self.userId = updateUserPwd.userId
        self.userNewPassword = updateUserPwd.userNewPassword
        self.userOldPassword = updateUserPwd.userOldPassword
    }
    
}
