    //
    //  NotificationViewController.swift
    //  SmileyServe
    //
    //  Created by Apple on 02/08/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //

    import UIKit
    import SVProgressHUD
    import UserNotifications

    class NotificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    let userdId = UserDefaults.standard.value(forKey: "id")
    @IBOutlet var notificationTableView : UITableView!
    var notificationArrayList:[NotificationModel] = []
    override func viewDidLoad() {
    super.viewDidLoad()
    notificationTableView.delegate = self
    notificationTableView.dataSource = self
    notificationTableView.isHidden = true
    self.notificationNetworkCall()
    self.pushNotificationPermissionChecking()
        navigationBarAppearance()
    }


    func navigationBarAppearance(){
    navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        self.navigationController?.navigationBar.tintColor =  UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationArrow))
    self.title = "Notifications"
    }


    func handleNavigationArrow(){
    DispatchQueue.main.async {
    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
    let signupNavigation = UINavigationController(rootViewController: signupController)
    self.present(signupNavigation, animated: false, completion: nil)
    }
    }


    func pushNotificationPermissionChecking(){
    if #available(iOS 10.0, *) {
    let current = UNUserNotificationCenter.current()
    current.getNotificationSettings(completionHandler: { (settings) in
    if settings.authorizationStatus == .notDetermined {
    // Means you can request
    }

    if settings.authorizationStatus == .denied {
    let lowerBound = 1
    let upperBound = 6
    let randomNumber: Int!
    randomNumber = Int(arc4random_uniform(UInt32(upperBound))) + lowerBound
    if randomNumber == 5 {
    self.presentAlertWithTitle(title: "Push Notifications Denied", message:"You can enable this push notification in settings & privacy")
    }
    // User should enable notifications from settings & privacy
    let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
    if launchedBefore  {
    } else {
    self.presentAlertWithTitle(title: "Push Notifications Denied", message:"You can enable this push notification in settings & privacy")
    UserDefaults.standard.set(true, forKey: "launchedBefore")
    }
    }

    if settings.authorizationStatus == .authorized {
    // It's okay, no need to request
    }
    })
    }
    }

    override func presentAlertWithTitle(title: String, message : String)
    {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "OK", style: .default) {
    (action: UIAlertAction) in
    print("Youve pressed OK Button")
    //        UIApplication.shared.open(URL(string:"App-Prefs:root=Privacy")!, options: [:], completionHandler: nil)
    }
    alertController.addAction(OKAction)
    self.present(alertController, animated: true, completion: nil)
    }


    func notificationNetworkCall(){
    if Reachability.isConnectedToNetwork(){
    self.notificationList()
    }else{
    DispatchQueue.main.async {
    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
    }
    }
    }

    override func viewWillAppear(_ animated: Bool) {
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.notificationArrayList.count

    }
    @IBAction func notificationBarBtn(_ sender: Any) {
    self.dismiss(animated: false, completion: nil)

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath as IndexPath) as! NotificationTableViewCell
    cell.selectionStyle = UITableViewCellSelectionStyle.none
    cell.notificationDate?.font = UIFont.systemFont(ofSize: 15.0)
    cell.notificationMessage?.font = UIFont.systemFont(ofSize: 15.0)
    cell.notificationTitle?.font = UIFont.systemFont(ofSize: 15.0)
    cell.notificationDate.text = self.notificationArrayList[indexPath.row].notification_date
    cell.notificationMessage.text = self.notificationArrayList[indexPath.row].notification_message
    cell.notificationTitle.text = self.notificationArrayList[indexPath.row].notification_title
    cell.notificationMessage?.numberOfLines = 0
    cell.notificationDate?.numberOfLines = 1
    cell.notificationTitle?.numberOfLines = 1
    return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 165.0
    }

    func notificationList(){
    DispatchQueue.main.async {
    SVProgressHUD.setDefaultStyle(.dark)
    SVProgressHUD.show(withStatus: "please wait...")
    }
    if let notificationURL =   NSURL(string:Constants.URL_REQUEST_USER_NOTIFICATIONLIST) {
    var request = URLRequest(url:notificationURL as URL)
    var notifications:NotificationParamModel = NotificationParamModel()
    notifications.page_number = "1"
    notifications.userid = userId
    notifications.required_count = "30"

    guard let userId =  notifications.userid else{return}
    let cashListParams:[String:Any] = ["userid":userId,"page_number":notifications.page_number!,"required_count":notifications.required_count!]
    request.httpMethod = "POST"
    do {
    let  json = try JSONSerialization.data(withJSONObject: cashListParams)
    request.httpBody = json
    }
    catch let jsonErr  {

    print(jsonErr.localizedDescription)
    }
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    }
    guard let data = data, error == nil else

    {
    DispatchQueue.main.async {
    self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
    }
    print(error?.localizedDescription as Any )
    return
    }

    self.notificationArrayList = [NotificationModel]()
    do{
    guard let fetchNotificationData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
    print(fetchNotificationData)
    print(fetchNotificationData["description"]!)
    print(fetchNotificationData["message"]!)

    if (fetchNotificationData["code"] as! NSNumber == 200) {


    //    pushCount = fetchNotificationData["receent_notification_count"]
    /* if let notificationCount = fetchNotificationData["receent_notification_count"] as? String{
    if let notificationCount = Int(notificationCount) {
    pushCount = notificationCount
    }
    }*/

    if let notificationResult = fetchNotificationData["notifications_result"] as? [[String:AnyObject]]{
    for notificationResponse in notificationResult {
    var notificationList = NotificationModel()
    guard  let notificationTitle = notificationResponse["notification_title"] as? String,let notificationMessage  = notificationResponse["notification_message"] as? String,let notificationDate = notificationResponse["notification_date"] as? String else{
    return
    }
    notificationList.notification_title = notificationTitle
    notificationList.notification_message = notificationMessage
    notificationList.notification_date = notificationDate
    self.notificationArrayList.append(notificationList)
    }
    }

    DispatchQueue.main.async {
    self.notificationTableView.isHidden = false
    self.notificationTableView.reloadData()
    }
    }
    }catch let jsonErr{
    DispatchQueue.main.async {
    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
    }
    print(jsonErr.localizedDescription)
    }
    }
    task.resume()
    }else{
    DispatchQueue.main.async {
    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
    }
    }
    }
    }//class


