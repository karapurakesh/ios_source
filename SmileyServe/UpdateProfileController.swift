    //
    //  UpdateProfileController.swift
    //  SmileyServe
    //
    //  Created by Apple on 07/07/17.
    //  Copyright © 2017 smileyserve. All rights reserved.
    //
    
    import UIKit
    import SVProgressHUD
    
    
    class UpdateProfileController: UIViewController, UITextFieldDelegate,LBZSpinnerDelegate,MCountryPickerDelegate {
        
        @IBOutlet weak var flatNumberHeightConstraint: NSLayoutConstraint!
        @IBOutlet weak var apartmentDropDownSpinnerView: LBZSpinner!
        @IBOutlet weak var blockListDropDownSpinnerView: LBZSpinner!
        var apartmentId: String?
        var blockId: String?
        var apartmentListArray = [String]()
        var blockListArray = [String]()
        var selectedBlockItemPosition: Int = 0
        var apartmentsArrayList = [ApartmentModel]()
        var blockArrayList = [BlockModel]()
        let userApiKey = UserDefaults.standard.value(forKey: "id") as? String
        var locationListData = [String]()
        var blockListData = [String]()
        @IBOutlet var updateProfileReferenceBtn: UIButton!
        @IBOutlet var flatnumberTextField: UITextField!
        @IBOutlet var emailTextField: UITextField!
        @IBOutlet var mobilenumberTextField: UITextField!
        @IBOutlet var nameTextField: UITextField!
        let  updateProfileUserId = UserDefaults.standard.value(forKey:"id") as? String
        var apartmentsNamesGbl = [MCountry]()
        
        @IBOutlet weak var apartmentTextFld: DesignableTextField!
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initializeViews()
            updateProfileReferenceBtn.layer.cornerRadius = 5
        }
        
        /* Initializing the all UI related elements
         
         */
        
        func initializeViews() {
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"ic_left_arrow.png"), style: .plain, target: self, action: #selector(handleNavigationUPdateProfile))
            navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1)
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
            self.navigationController?.navigationBar.tintColor =  UIColor.white
            flatnumberTextField.delegate = self
            emailTextField.delegate = self
            mobilenumberTextField.delegate = self
            nameTextField.delegate = self
            apartmentTextFld.delegate = self
            // apartmentDropDownSpinnerView.delegate = self
            blockListDropDownSpinnerView.delegate = self
            flatnumberTextField.updateProfileUnderlined()
            apartmentTextFld.updateProfileUnderlined()
            emailTextField.updateProfileUnderlined()
            mobilenumberTextField.updateProfileUnderlined()
            nameTextField.updateProfileUnderlined()
            updateProfileReferenceBtn.isHidden = false
            blockListDropDownSpinnerView.isUserInteractionEnabled = false
            self.title = "Update Profile"
            self.updateProfileNetworkCalls()
        }
        
        /* LBZ Spinner delegate method */
        
        func spinnerChoose(_ spinner: LBZSpinner, index: Int, value: String) {
            var spinnerName = ""
            /*  if spinner == apartmentDropDownSpinnerView { spinnerName = "spinnerTop"
             selectedBlockItemPosition = index
             apartmentId = self.apartmentsArrayList[index].id
             self.getBlockListData(apartmentId: apartmentId)
             print("Spinner : \(spinnerName) : { Index : \(index) - \(value) }")
             }else{
             
             if spinner == blockListDropDownSpinnerView { spinnerName = "spinnerTop"
             }
             print("Spinner : \(spinnerName) : { Index : \(index) - \(value) }")
             if blockArrayList.count == 0 {
             blockId = "0"
             } else {
             blockId = self.blockArrayList[index].blockId
             print("blockID:\(String(describing: blockId))")
             }
             }*/
            
            if spinner == blockListDropDownSpinnerView { spinnerName = "spinnerTop"
            }
            print("Spinner : \(spinnerName) : { Index : \(index) - \(value) }")
            if blockArrayList.count == 0 {
                blockId = "0"
            } else {
                blockId = self.blockArrayList[index].blockId
                print("blockID:\(String(describing: blockId))")
            }
        }
        
        
        /* Performing the network call to get the user data */
        
        func updateProfileNetworkCalls(){
            if Reachability.isConnectedToNetwork() {
                if userApiKey != nil {
                    self.getUserData(userApikey:userApiKey!)
                }
            }
            else {
                emailTextField.text = UserDefaults.standard.value(forKey: "email") as? String
                mobilenumberTextField.text = UserDefaults.standard.value(forKey: "mobile") as? String
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                }
            }
        }
        
        /*Method handling the navigation back */
        
        func handleNavigationUPdateProfile() {
            self.dismiss(animated: true, completion: nil)
        }
        
        /* Get Method to get the user data */
        
        func getUserData(userApikey: String) {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestGetUserData = NSURL(string: Constants.URL_REQUEST_USER_USERDATA + userApikey) {
                var urlRequest = URLRequest(url: requestGetUserData as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        self.apartmentListArray.removeAll()
                        self.apartmentsArrayList.removeAll()
                        SVProgressHUD.dismiss()
                    }
                    guard let data = data else{
                        return
                    }
                    if error != nil{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server", duration: 3.0, position: .center)
                        }
                    }
                    
                    do {
                        guard let userDataJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{
                            return
                        }
                        print(userDataJsonObject)
                        if userDataJsonObject["code"] as! NSNumber == 200 {
                            guard let userData = userDataJsonObject["user_result"] as?
                                [String: AnyObject] else{return}
                            DispatchQueue.main.async {
                                guard let userEmail = userData["email"], let userMobile = userData["mobile"] else {return}
                                self.emailTextField.text = userEmail as? String
                                self.mobilenumberTextField.text = userMobile as? String
                                self.apartmentId = userData["appartment_id"] as? String
                                self.blockId = userData["block_id"] as? String
                            }
                            
                            guard  let apartmentsJsonArray = userDataJsonObject["apartment_result"] as? [String: AnyObject] else{return}
                            guard let apartmentResultJson = apartmentsJsonArray["apartment_result"] as? [[String: AnyObject]] else{return}
                            for apartmentArray in apartmentResultJson {
                                guard let id = apartmentArray["id"],let title = apartmentArray["title"] as? String, let address = apartmentArray["address"], let location = apartmentArray["location"]
                                    else{return}
                                var apartmentModel = ApartmentModel()
                                apartmentModel.address = address as? String
                                apartmentModel.id = id as? String
                                apartmentModel.location = location as? String
                                apartmentModel.title = title
                                self.apartmentsArrayList.append(apartmentModel)
                                
                                let apartWithLocation = String(format: "%@, %@", title,apartmentModel.location!)
                                
                                print("apartWithLocation",apartWithLocation)
                                
                                self.apartmentListArray.append(apartWithLocation)
                                
                            }
                            DispatchQueue.main.async {
                                if self.apartmentListArray.count > 0 {
                                    
                                    var items = [MCountry]()
                                    
                                    for value in self.apartmentListArray {
                                        items.append(MCountry.init(name: value))
                                    }
                                    
                                    self.apartmentsNamesGbl = items
                                    //self.apartmentDropDownSpinnerView.updateList(self.apartmentListArray)
                                }
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable fetch Your Data", duration: 3.0, position: .center)
                            }
                        }
                    }
                    catch let jsonErr {
                        print(jsonErr.localizedDescription)
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable fetch Your Data", duration: 3.0, position: .center)
                        }
                    }
                })
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable fetch Your Data", duration: 3.0, position: .center)
                }
            }
        }
        
        func countryPicker(_ picker: CountriesViewController, didSelectCountryWithName name: String) {
            picker.navigationController?.popViewController(animated: true)
            
            self.selectedBlockItemPosition = apartmentListArray.index(of: name)!
            print("INDEX",self.selectedBlockItemPosition)
            
            self.apartmentTextFld.text = name
            
            apartmentId = self.apartmentsArrayList[self.selectedBlockItemPosition].id
            
            self.getBlockListData(apartmentId: self.apartmentsArrayList[self.selectedBlockItemPosition].id)
            
        }
        
        func getBlockListData(apartmentId: String?) {
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultStyle(.dark)
                SVProgressHUD.show(withStatus: "please wait...")
            }
            if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_BLOCKLISTDATA + apartmentId!) {
                print(requestLocationListURL)
                var urlRequest = URLRequest(url: requestLocationListURL as URL)
                urlRequest.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        self.blockListArray.removeAll()
                        self.blockArrayList.removeAll()
                        SVProgressHUD.dismiss()
                    }
                    
                    guard let data = data else{
                        return
                    }
                    if error != nil{
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable to connect server", duration: 3.0, position: .center)
                        }
                    }
                    do {
                        
                        guard  let blockData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{
                            return
                        }
                        
                        print(blockData)
                        guard (blockData["error"] as? String) != nil else{return}
                        guard let blockJsonArray = blockData["blocks"] as? [[String: AnyObject]] else{return}
                        for blockArray in blockJsonArray {
                            let blockId = blockArray["block_id"]
                            let blockName = blockArray["block_name"] as? String
                            var blockModel = BlockModel()
                            blockModel.blockId = blockId as? String
                            blockModel.blockName = blockName
                            self.blockArrayList.append(blockModel)
                            if let block_Name = blockName{
                                self.blockListArray.append(block_Name)
                            }
                        }
                        DispatchQueue.main.async {
                            if self.blockArrayList.count > 0 {
                                self.blockListDropDownSpinnerView.isUserInteractionEnabled = true
                                self.blockListDropDownSpinnerView.isHidden = false
                                self.blockListDropDownSpinnerView.text = "Please select a block"
                                self.flatNumberHeightConstraint.constant = 30
                                self.blockListDropDownSpinnerView.updateList(self.blockListArray)
                            } else {
                                self.blockId = "0"
                                self.blockListDropDownSpinnerView.isHidden = true
                                self.flatNumberHeightConstraint.constant = -30
                                self.blockListDropDownSpinnerView.isUserInteractionEnabled = false
                            }
                        }
                    }
                    catch let jsonErr {
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable fetch Your Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                        self.blockId = "0"
                    }
                })
                task.resume()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("Unable fetch Your Data", duration: 3.0, position: .center)
                }
            }
        }
        
        
        /* Button performing the update profile action */
        
        @IBAction func saveButton(_ sender: Any) {
            if Reachability.isConnectedToNetwork(){
                self.updateProfile()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                }
            }
        }
        
        
        /* Method updating the profile details given by the user */
        
        func updateProfile(){
            var updateProfile: UpdateProfile = UpdateProfile()
            if (nameTextField.text?.isEmpty)! ||  (flatnumberTextField.text?.isEmpty)! || (mobilenumberTextField.text?.isEmpty)! || (emailTextField.text?.isEmpty)!  {
                DispatchQueue.main.async {
                    self.view.makeToast("please fill all the fields", duration: 3.0, position: .center)
                }
            }
            else {
                DispatchQueue.main.async {
                    SVProgressHUD.setDefaultStyle(.dark)
                    SVProgressHUD.show(withStatus: "please wait...")
                }
                updateProfile.city = "NA"
                updateProfile.state = "NA"
                updateProfile.area = "NA"
                updateProfile.address = "NA"
                updateProfile.pincode = "NA"
                updateProfile.plotno = flatnumberTextField.text
                updateProfile.username = nameTextField.text
                updateProfile.apartment = apartmentId
                updateProfile.blockid = blockId
                updateProfile.usermobile = mobilenumberTextField.text
                updateProfile.userEmail = emailTextField.text
                updateProfile.userid = userApiKey
                if  let updateProfileURL = NSURL(string: Constants.URL_REQUEST_USER_UPDATEPROFILE){
                    var request = URLRequest(url: updateProfileURL as URL)
                    guard let userName =  updateProfile.username,let userId =  updateProfile.userid,let
                        userCity =  updateProfile.city,let userArea =   updateProfile.area,let userState =  updateProfile.state,let userPlotNo =  updateProfile.plotno,let userMobile = updateProfile.usermobile,let userApartment =  updateProfile.apartment,let userAddress = updateProfile.address,let userPincode = updateProfile.pincode,let userBlockId =  updateProfile.blockid  else{return}
                    let updateProfileParams = ["username":userName, "userid": userId, "city": userCity, "area": userArea, "state": userState, "plotno": userPlotNo, "usermobile": userMobile, "apartment": userApartment, "address":userAddress, "pincode": userPincode, "blockId": userBlockId] as [String:Any]
                    request.httpMethod = "POST"
                    do {
                        let json = try JSONSerialization.data(withJSONObject: updateProfileParams)
                        request.httpBody = json
                    }
                    catch let jsonErr {
                        
                        print(jsonErr.localizedDescription)
                    }
                    
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                        
                        guard let data = data, error == nil else
                            
                        {
                            DispatchQueue.main.async {
                                self.view.makeToast("Unable to connect server", duration: 3.0, position: .center)
                            }
                            print(error?.localizedDescription as Any)
                            return
                        }
                        do {
                            let fetchData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                            if let registerResponse = fetchData as? [String: Any] {
                                if (registerResponse["code"] as! NSNumber == 200) {
                                    DispatchQueue.main.async {
                                        
                                        UserDefaults.standard.set(updateProfile.username, forKey: "name")
                                        UserDefaults.standard.set(updateProfile.userEmail, forKey: "email")
                                        UserDefaults.standard.set(updateProfile.usermobile, forKey: "mobile")
                                        UserDefaults.standard.set(updateProfile.blockid, forKey: "blockid")
                                        UserDefaults.standard.set(updateProfile.apartment, forKey: "apartmentid")
                                        UserDefaults.standard.set(updateProfile.plotno, forKey: "plotno")
                                    }
                                    DispatchQueue.main.async {
                                        guard  let fcmUserId = self.userApiKey else{return}
                                        if let token = UserDefaults.standard.value(forKey: "fcmToken")
                                            as? String {
                                            let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
                                            print("InstanceID token:\(token)")
                                            let paramCheckVersionCode = "userId=\(fcmUserId)&fcmToken=\(token)&versionCode=\(version)&deviceType=\(deviceType)"
                                            self.checkVersionCode(Constants.URL_REQUEST_USER_CHECKVERSIONCODE, "\(paramCheckVersionCode)")
                                            let paramString = "userId=\(fcmUserId)&fcmToken=\(token)"
                                            self.updateFcmToken(Constants.URL_REQUEST_USER_FCMTOKEN, "\(paramString)")
                                        }
                                    }
                                    DispatchQueue.main.async {
                                        self.createSmileyCashPayDialog()
                                    }
                                }
                                else {
                                    DispatchQueue.main.async {
                                        self.view.makeToast(registerResponse["description"]! as? String, duration: 3.0, position: .center)
                                    }
                                }
                            }
                        } catch let jsoerr {
                            DispatchQueue.main.async {
                                self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                            }
                            print(jsoerr.localizedDescription)
                        }
                    }
                    task.resume()
                }
                else{
                    DispatchQueue.main.async {
                        self.view.makeToast("unable to fetch your data", duration: 3.0, position: .center)
                    }
                }
            }
        }
        
        
        
        ///creating the AlertviewDialog To read Faqs
        
        func createSmileyCashPayDialog(){
            let alertController = UIAlertController(title: "Confirmation Message", message: "Before ordering the products please go and review the faqs", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                self.faqs()
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "Cancel", style:.default) { (action:UIAlertAction!) in
                self.home()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        /* Method performing seague to home screen */
        
        func home(){
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let homeNavigation = UINavigationController(rootViewController: homeController)
            self.present(homeNavigation, animated: false, completion: nil)
        }
        
        
        /* Method performing seague to Faq screen */
        
        func faqs(){
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let faqController = storyBoard.instantiateViewController(withIdentifier: "Faqs") as! FaqsViewController
            let faqNavigation = UINavigationController(rootViewController: faqController)
            self.present(faqNavigation, animated: false, completion: nil)
        }
        
        
        // MARK: UITextFieldDelegate
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            updateProfileReferenceBtn.isHidden = false
        }
        func textFieldDidBeginEditing(_ textField: UITextField) {
            //TODO Based on the app requirement
            
            if textField.tag == 40 {
                
                textField.resignFirstResponder()
                
                print("MOVE TO APARTMENT SCREEN")
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let countriesViewController = storyBoard.instantiateViewController(withIdentifier: "countriesViewController") as! CountriesViewController
                countriesViewController.unsourtedCountries = self.apartmentsNamesGbl
                countriesViewController.delegate = self
                self.navigationController?.pushViewController(countriesViewController, animated: true)
                
            }
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
        
        //Mark -- Customize the navigationbar appearance
        func customizeNavigationBars() {
            navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 255 / 255, green: 87 / 255, blue: 34 / 255, alpha: 1)
            self.navigationController?.navigationBar.tintColor = UIColor.white

            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        }
        
        // Move the text field in a pretty animation!
        
        func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
            let moveDuration = 0.3
            let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
            UIView.beginAnimations("animateTextField", context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(moveDuration)
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
            UIView.commitAnimations()
        }
        
        //Mark--textfield delegate
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            return true
        }
        
        
        
        /* Method performing the update fcm token */
        
        func updateFcmToken(_ url: String, _ paramString: String) {
            if let url: NSURL = NSURL(string: url){
                let session = URLSession.shared
                let request = NSMutableURLRequest(url: url as URL)
                request.httpMethod = "POST"
                request.httpBody = paramString.data(using: String.Encoding.utf8)
                let task = session.dataTask(with: request as URLRequest) {
                    (
                    data, response, error) in
                    guard let data = data else{
                        return
                    }
                    self.extract_data(data)
                }
                task.resume()
            }
        }
        
        
        /* Method extracting the json data */
        
        func extract_data(_ data:Data?) {
            let json: Any?
            if(data == nil)
            {
                return
            }
            do {
                guard let data = data else{return}
                json = try JSONSerialization.jsonObject(with:data, options: [])
            }
            catch
            {
                return
            }
            guard let stringvalue = json as? NSDictionary else
                
            {
                return
            }
            print(stringvalue)
            let stringresponse = stringvalue["error"] as? String
            DispatchQueue.main.async {
                if stringresponse == "false" {
                    print(stringvalue["message"]!)
                }
            }
        }
        
        
        
        /* Method performing the version code */
        
        func checkVersionCode(_ url: String, _ paramCheckVersionCode: String) {
            if let url: NSURL = NSURL(string: url){
                let session = URLSession.shared
                let request = NSMutableURLRequest(url: url as URL)
                request.httpMethod = "POST"
                request.httpBody = paramCheckVersionCode.data(using: String.Encoding.utf8)
                let task = session.dataTask(with: request as URLRequest) {
                    (
                    data, response, error) in
                    guard let data = data else{
                        return
                    }
                    self.extractVersionCode(data)
                }
                task.resume()
            }
        }
        
        
        /* Method extracting the json data */
        
        func extractVersionCode(_ data: Data?) {
            let json: Any?
            if(data == nil)
            {
                return
            }
            guard let data = data else{
                return
            }
            do {
                json = try JSONSerialization.jsonObject(with:data, options:.mutableContainers)
            }
            catch
            {
                return
            }
            guard let stringvalue = json as? NSDictionary else
            {
                return
            }
            print(stringvalue)
            let stringresponse = stringvalue["error"] as? String
            DispatchQueue.main.async {
                if stringresponse == "false" {
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    let signupNavigation = UINavigationController(rootViewController: signupController)
                    self.present(signupNavigation, animated: true, completion: nil)
                } else {
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let forceUpdateViewController = storyBoard.instantiateViewController(withIdentifier: "ForceUpdateViewController") as! ForceUpdateViewController
                    self.present(forceUpdateViewController, animated: true, completion: nil)
                    print(stringvalue["message"]!)
                    print(stringvalue["tag"]!)
                    
                }
            }
        }
        
        
    }//UpdateProfileViewController
    
    
    
    /* Creating an extension for underlining the textfields */
    
    extension UITextField {
        func updateProfileUnderlined(){
            let border = CALayer()
            let width = CGFloat(1.0)
            border.borderColor = UIColor.lightGray.cgColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        }
    }
    
