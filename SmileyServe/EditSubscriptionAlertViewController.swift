  //
  //  EditSubscriptionAlertViewController.swift
  //  SmileyServe
  //
  //  Created by Apple on 04/09/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //
  
  import UIKit
  import SVProgressHUD
  class EditSubscriptionAlertViewController: UIViewController,BEMCheckBoxDelegate,RazorpayPaymentCompletionProtocol {
    var smileyCashEnable : String?

    
    var paymentActionVal :Bool = false
    var checksumTransactValue :Bool = false
    var merchant:PGMerchantConfiguration!
    
    @IBOutlet var alertView: UIView!
    private  var razorpay : Razorpay!
    var bemCheckBox : BEMCheckBox!
    @IBOutlet var designAlertView: DesignView!
    var userId = UserDefaults.standard.value(forKey: "id")
    @IBOutlet var smileyCashLabel: UILabel!
    @IBOutlet var paymentCheckBoxListener: BEMCheckBox!
    @IBOutlet var proceedToPayButtonReference: UIButton!
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        paymentCheckBoxListener.delegate = self
        if Reachability.isConnectedToNetwork(){
            self.smileyCashAmount()
            
        }else{
            DispatchQueue.main.async {
                self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
            }
        }
        
        setMerchant()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if let payableAmount = UserDefaults.standard.value(forKey: "debit_amount") as? Double {
            self.proceedToPayButtonReference.setTitle("ProceedToPay Rs." + "\(payableAmount)", for: .normal)
        }
    }
    
    func setMerchant(){
        merchant  = PGMerchantConfiguration.default()!
        //user your checksum urls here or connect to paytm developer team for this or use default urls of paytm
        merchant.checksumGenerationURL = "http://smileyserve.com/beta/generateChecksum.php";
        merchant.checksumValidationURL = "http://smileyserve.com/beta/verifyChecksum.php";
        
        // Set the client SSL certificate path. Certificate.p12 is the certificate which you received from Paytm during the registration process. Set the password if the certificate is protected by a password.
        merchant.clientSSLCertPath = nil; //[[NSBundle mainBundle]pathForResource:@"Certificate" ofType:@"p12"];
        merchant.clientSSLCertPassword = nil; //@"password";
        
        //configure the PGMerchantConfiguration object specific to your requirements
        /*  merchant.merchantID = "Smiley32368638707578";//paste here your merchant id  //mandatory
         merchant.website = "APP_STAGING";//mandatory
         merchant.industryID = "Retail";//mandatory
         merchant.channelID = "WAP"; //provided by PG WAP //mandatory*/
        
        //Prod
        merchant.merchantID = "SmileP27807646019839";//paste here your merchant id  //mandatory
        merchant.website = "SmilePWAP";//mandatory
        merchant.industryID = "Retail109";//mandatory
        merchant.channelID = "WEB"; //provided by PG WAP //mandatory
        
    }
    
    @IBAction func closeAlertAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func proceedToPayBtnAction(_ sender: Any) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let image = UIImage(named: "paytm")
        let imageView = UIImageView()
        imageView.image = image
        imageView.frame =  CGRect(x: 10, y: 0, width: 60, height: 60)
        actionSheetController.view.addSubview(imageView)
        
        
        let image2 = UIImage(named: "razor")
        let imageView1 = UIImageView()
        imageView1.image = image2
        imageView1.frame =  CGRect(x: 10, y: 60, width: 50, height: 50)
        actionSheetController.view.addSubview(imageView1)
        
        
        
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "BHIM/CC/DC/NB", style: .default) { action -> Void in
            
            print("First Action pressed")
            self.paymentActionVal = true
            //  self.proceedToPayWithPaymentGateway()
            
            let randomNum:UInt32 = arc4random() // range is 0 to 99
            // convert the UInt32 to some other  types
            // let randomTime:TimeInterval = TimeInterval(randomNum)
            //let someInt:Int = Int(randomNum)
            let someString:String = String(randomNum)
            
            print(someString)
            
            UserDefaults.standard.set(someString, forKey: "orderid_addMoney")
            
            if let payableAmount = UserDefaults.standard.value(forKey: "debit_amount") as? Float {
                let convertStringToInt = Int(payableAmount.rounded(.toNearestOrAwayFromZero))
                //let convertedCurrencyValue = convertStringToInt * 100
                UserDefaults.standard.set(convertStringToInt, forKey: "convertedCurrencyValue")
                
                print("payableAmount",payableAmount)
                
            }


            self.checksumHash(completionHandler: { (true) in

                self.createPayment()

            })
        }
        
        
        
        
        
        let secondAction: UIAlertAction = UIAlertAction(title: "Other Wallets", style: .default) { action -> Void in
            
            self.designAlertView.isHidden = true
            if Reachability.isConnectedToNetwork(){
                DispatchQueue.main.async {
//                                        if self.bemCheckBox != nil {
//                                          //  self.subscriptionSuccess()
//                                        }else{
//                                            self.razorpay = Razorpay.initWithKey("rzp_live_2c8aqqKX844kbZ", andDelegate: self)
//                                            self.showPaymentForm()
//
//                                        }
                    
                    self.razorpay = Razorpay.initWithKey("rzp_live_2c8aqqKX844kbZ", andDelegate: self)
                    self.showPaymentForm()
                    
                }
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                }
            }
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)
        
        present(actionSheetController, animated: true, completion: nil)
        
        
    }
    
    
    func createPayment(){
        
//        DispatchQueue.main.async {
//            SVProgressHUD.setDefaultStyle(.dark)
//            SVProgressHUD.show(withStatus: "please wait...")
//        }
        
        var orderDict = [String : Any]()
        
        
        let convertedCurrencyValue = UserDefaults.standard.value(forKey: "convertedCurrencyValue") as! Int
        let finalValue = String(describing:convertedCurrencyValue)
        orderDict["TXN_AMOUNT"] = finalValue; // amount to charge
        
        //  Prod
        
        orderDict["MID"] = "SmileP27807646019839";//paste here your merchant id   //mandatory
        orderDict["CHANNEL_ID"] = "WAP"; // paste here channel id                       // mandatory
        orderDict["INDUSTRY_TYPE_ID"] = "Retail109";//paste industry type              //mandatory
        orderDict["WEBSITE"] = "SmilePWAP";// paste website
        let orderid = UserDefaults.standard.value(forKey: "orderid_addMoney") as! String
        orderDict["ORDER_ID"] = orderid;//change order id every time on new transaction
        // orderDict["REQUEST_TYPE"] = "DEFAULT";// remain same
        orderDict["CUST_ID"] = self.userId; // change acc. to your database user/customers
        orderDict["MOBILE_NO"] = UserDefaults.standard.value(forKey: "mobile");// optional
        orderDict["EMAIL"] =  UserDefaults.standard.value(forKey: "email"); //optional
        
        orderDict["CALLBACK_URL"] = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderid)"
        // orderDict["CALLBACK_URL"] = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
        let checksumHsh = UserDefaults.standard.value(forKey: "CHECKSUMHASH_RES") as! String
        
        
        //https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=<orderid>
        
        orderDict["CHECKSUMHASH"] = checksumHsh
        print("orderDict::",orderDict)
        
        
        DispatchQueue.main.async {
            let pgOrder = PGOrder(params: orderDict )
            
            let canBtn = UIButton.init(frame: CGRect(x: 0, y:20 , width: 100, height: 30))
            canBtn.setTitle("Cancel", for: .normal)
            //canBtn.tintColor = UIColor.red
            
            let topView = UIView(frame: CGRect(x: 0, y: 15, width: self.view.frame.width, height: 60))
            topView.backgroundColor = UIColor(red:253.0/255.0, green:109.0/255.0, blue:64/255.0, alpha:1.000)
            topView.addSubview(canBtn)
            
            
            let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
            
            transaction!.serverType = eServerTypeProduction
            transaction!.merchant = self.merchant
            transaction!.loggingEnabled = true
            transaction!.delegate = self
            transaction?.topBar = topView
            transaction?.cancelButton =  canBtn
            
            self.present(transaction!, animated: true, completion: {
                SVProgressHUD.dismiss()
                
            })
        }
        
        
        
    }
    
    
    func checksumHash(completionHandler: @escaping CompletionHandler) {
        
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        
        
        let url = prepareChecksum()
        
        print("URLCHECKSUM_",url)
        
        var request = URLRequest(url:url as URL)
        
        print("createOrderUrl",url)
        
        request.httpMethod = "GET"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            
            guard let data = data, error == nil else
            {
                print(error?.localizedDescription as Any )
                return
            }
            do{
                guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                
                guard let checksumHashRes = createOrderJson["CHECKSUMHASH"] as? String else{
                    return
                }
                
                print("CheckSumHash_Rakesh",checksumHashRes)
                
                UserDefaults.standard.set(checksumHashRes, forKey: "CHECKSUMHASH_RES")
                UserDefaults.standard.synchronize()
                
                completionHandler(true)
                
                //                    DispatchQueue.main.async {
                //                        SVProgressHUD.dismiss()
                //                    }
                
            }catch let jsonErr{
                
                completionHandler(false)
                
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
                print(jsonErr.localizedDescription)
            }
        }
        task.resume()
    }
    
    func addQueryParams(url: URL, newParams: [URLQueryItem]) -> URL? {
        let urlComponents = NSURLComponents.init(url: url, resolvingAgainstBaseURL: false)
        guard urlComponents != nil else { return nil; }
        if (urlComponents?.queryItems == nil) {
            urlComponents!.queryItems = [];
        }
        urlComponents!.queryItems!.append(contentsOf: newParams);
        return urlComponents?.url;
    }
    
    func prepareChecksum() -> URL{
        
        let orderid = UserDefaults.standard.value(forKey: "orderid_addMoney") as! String
        
        
        let convertedCurrencyValue = UserDefaults.standard.value(forKey: "convertedCurrencyValue") as! Int
        let finalValue = String(describing:convertedCurrencyValue)
        print("ADDMONEY_AMOUNT",finalValue)
        
        //staging  var url = URL.init(string: "http://smileyserve.com/beta/generateChecksum.php");
        var url = URL.init(string: "http://smileyserve.com/generateChecksum.php"); //prod
        
        if (url != nil) {
            
            let mobileNo = UserDefaults.standard.value(forKey: "mobile") as! String
            let email = UserDefaults.standard.value(forKey: "email") as! String
            
            if self.checksumTransactValue {
                
                url = addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "MID", value: "SmileP27807646019839"),
                                                            URLQueryItem.init(name: "CHANNEL_ID", value: "WAP"),
                                                            URLQueryItem.init(name: "INDUSTRY_TYPE_ID", value: "Retail109"),
                                                            URLQueryItem.init(name: "WEBSITE", value: "SmilePWAP"),
                                                            URLQueryItem.init(name: "TXN_AMOUNT", value: ""),
                                                            URLQueryItem.init(name: "ORDER_ID", value: orderid),
                                                            URLQueryItem.init(name: "CUST_ID", value: self.userId as? String),
                                                            URLQueryItem.init(name: "CALLBACK_URL", value: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderid)"),
                                                            URLQueryItem.init(name: "MOBILE_NO", value: mobileNo),
                                                            URLQueryItem.init(name: "EMAIL", value: email)])
                
                
            }else {
                
                url = addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "MID", value: "SmileP27807646019839"),
                                                            URLQueryItem.init(name: "CHANNEL_ID", value: "WAP"),
                                                            URLQueryItem.init(name: "INDUSTRY_TYPE_ID", value: "Retail109"),
                                                            URLQueryItem.init(name: "WEBSITE", value: "SmilePWAP"),
                                                            URLQueryItem.init(name: "TXN_AMOUNT", value: finalValue),
                                                            URLQueryItem.init(name: "ORDER_ID", value: orderid),
                                                            URLQueryItem.init(name: "CUST_ID", value: self.userId as? String),
                                                            URLQueryItem.init(name: "CALLBACK_URL", value: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=\(orderid)"),
                                                            URLQueryItem.init(name: "MOBILE_NO", value: mobileNo),
                                                            URLQueryItem.init(name: "EMAIL", value: email)])
                
            }
            
        }
        
        return url!
    }
    
    func statusCheckService() {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        
        
        self.checksumHash { (true) in
            
            let checksumHsh = UserDefaults.standard.value(forKey: "CHECKSUMHASH_RES") as! String
            
            print("checksumHsh_transaction_Success",checksumHsh)
            
            let orderid = UserDefaults.standard.value(forKey: "orderid_addMoney") as! String
            
            // let orderIDSTr =  String(describing: orderid)
            
            var url = URL.init(string: "https://securegw.paytm.in/merchant-status/getTxnStatus");
            
            var orderDict = [String : Any]()
            
            orderDict["MID"] = "SmileP27807646019839";//paste here your merchant id   //mandatory
            orderDict["CHECKSUMHASH"] = checksumHsh; // paste here channel id                       // mandatory
            orderDict["ORDER_ID"] = orderid;//paste industry type              //mandatory
            
            
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: orderDict,
                options: []) {
                let theJSONText = String(data: theJSONData,
                                         encoding: .ascii)
                print("JSON string = \(theJSONText!)")
                
                url = self.addQueryParams(url: url!, newParams: [URLQueryItem.init(name: "JsonData", value: theJSONText)])
                
                
            }
            
            
            print("StatusCheckURL_Final",url)
            if (url != nil) {
                
                var request = URLRequest(url: url!)
                
                //     print("createOrderUrl",url)
                
                request.httpMethod = "GET"
                
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    
                    
                    guard let data = data, error == nil else
                    {
                        print(error?.localizedDescription as Any )
                        return
                    }
                    do{
                        guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                        print("createOrderJson",createOrderJson)
                        
                        guard let paytmOrderID = createOrderJson["ORDERID"] as? String else{
                            return
                        }
                        
                        //let orderIDSTr = UserDefaults.standard.value(forKey: "orderid_addMoney") as! String
                        
                        self.subscriptionSuccessPaymentGateWay()
                        
                        DispatchQueue.main.async {
                            self.presentAlertWithTitleForPayment(title: "Payment Successful", message: "Your updated Successfully")
                        }
                        
                        
                    }catch let jsonErr{
                        
                        DispatchQueue.main.async {
                            self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        }
                        print(jsonErr.localizedDescription)
                    }
                }
                task.resume()
            }
        }
        
        
    }
    
    func orderPaymentSuccess(order_id : String, payTmorderID : String) {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        var serviceURL = NSURL(string: Constants.URL_REQUEST_USER_ORDERPAYMENTSUCCESS)
        
        if payTmorderID.count > 0 {
            
            let local = Constants.URL_REQUEST_USER_ORDERPAYMENTSUCCESS + order_id
            
            serviceURL = NSURL(string: local.appendingFormat("/%@",payTmorderID))
            
            //serviceURL = local?.a
            
        }else {
            serviceURL = NSURL(string: Constants.URL_REQUEST_USER_ORDERPAYMENTSUCCESS + order_id)
            
        }
        
        print("SERVICE_URL",serviceURL)
        
        var urlRequest = URLRequest(url: serviceURL as! URL)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            if (error != nil) {
                DispatchQueue.main.async {
                    self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                }
            }
            if let response = response {
                print(response)
            }
            guard let data = data else {
                return
            }
            do {
                
                guard let orderPaymentSuccessJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                print("orderPaymentSuccessJson",orderPaymentSuccessJson)
                if (orderPaymentSuccessJson["code"] as! NSNumber == 200) {
                    DispatchQueue.main.async {
                        self.view.makeToast(orderPaymentSuccessJson["description"]! as? String, duration: 3.0, position: .center)
                        
                        self.perform(#selector(self.handleHome), with: self, afterDelay: 1)
                        
                        
                    }
                    
                }else{
                    
                    DispatchQueue.main.async {
                        self.view.makeToast(orderPaymentSuccessJson["description"]! as? String, duration: 3.0, position: .center)  }
                }
            }catch let jsonErr {
                DispatchQueue.main.async {
                    self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                }
                print(jsonErr.localizedDescription)
            }
        })
        task.resume()
    }
    
    
    func subscriptionSuccess() {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        if let subscriptionSuccessUrl =  NSURL(string:Constants.URL_REQUEST_SUBSCRIPTIONSUCCESS) {
            var subscriptionSucessModel:SubscriptionSuccessModel = SubscriptionSuccessModel()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let now = Date()
            let dateString = formatter.string(from: now)
            subscriptionSucessModel.checkoutdate = dateString
            subscriptionSucessModel.userid = userId as? String
            subscriptionSucessModel.enablesmileycash = "1"
            guard let checkOutDate =  subscriptionSucessModel.checkoutdate,let userId =  subscriptionSucessModel.userid else {return}
            var  request = URLRequest(url:subscriptionSuccessUrl as URL)
            let subscriptionSuccessParams:[String:Any] = ["userid":userId,"checkoutdate":checkOutDate,"enablesmileycash":subscriptionSucessModel.enablesmileycash!]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: subscriptionSuccessParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async{
                    SVProgressHUD.dismiss()
                }
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )
                    return
                }
                do{
                    let subscriptionSuccessJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    
                    if let subscriptionSuccessResponse = subscriptionSuccessJson as? [String:Any]{
                        print(subscriptionSuccessResponse)
                        print(subscriptionSuccessResponse["description"]!)
                        print(subscriptionSuccessResponse["message"]!)
                        
                        if (subscriptionSuccessResponse["code"] as! NSNumber == 200) {
                            
                            DispatchQueue.main.async {
                                self.view.makeToast(subscriptionSuccessResponse["description"]! as? String, duration: 3.0, position: .center)
                                self.perform(#selector(self.handleHome), with: self, afterDelay: 1)
                            }
                        }
                            
                        else {
                            
                            DispatchQueue.main.async {
                                self.view.makeToast(subscriptionSuccessResponse["description"]! as? String, duration: 3.0, position: .center)
                                
                            }
                        }
                    }
                }catch let jsonErr{
                    DispatchQueue.main.async {
                        
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            task.resume()
        }else{
            DispatchQueue.main.async {
                
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                
            }
        }
    }
    
    /* creating the subcscription success using paymentgateway */
    func subscriptionSuccessPaymentGateWay() {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        if let subscriptionSuccessUrl =  NSURL(string:Constants.URL_REQUEST_SUBSCRIPTIONSUCCESS) {
            var subscriptionSucessModel:SubscriptionSuccessModel = SubscriptionSuccessModel()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let now = Date()
            let dateString = formatter.string(from: now)
            subscriptionSucessModel.checkoutdate = dateString
            subscriptionSucessModel.userid = userId as? String
            if (bemCheckBox != nil){
                subscriptionSucessModel.enablesmileycash = "1"

            }else{
                subscriptionSucessModel.enablesmileycash = "0"
            }
            guard let checkOutDate =  subscriptionSucessModel.checkoutdate,let userId =  subscriptionSucessModel.userid else {return}
            var  request = URLRequest(url:subscriptionSuccessUrl as URL)
            let subscriptionSuccessParams:[String:Any] = ["userid":userId,"checkoutdate":checkOutDate,"enablesmileycash":subscriptionSucessModel.enablesmileycash!]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: subscriptionSuccessParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async{
                    SVProgressHUD.dismiss()
                }
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )
                    return
                }
                do{
                    let subscriptionSuccessJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    
                    if let subscriptionSuccessResponse = subscriptionSuccessJson as? [String:Any]{
                        print(subscriptionSuccessResponse)
                        print(subscriptionSuccessResponse["description"]!)
                        print(subscriptionSuccessResponse["message"]!)
                        
                        if (subscriptionSuccessResponse["code"] as! NSNumber == 200) {
                            
                            DispatchQueue.main.async {
                                self.view.makeToast(subscriptionSuccessResponse["description"]! as? String, duration: 3.0, position: .center)
                                
//                                if self.bemCheckBox != nil {
//
//                                    print("bemCheckBox Called")
//
//                                    self.subscriptionSuccess()
//
//                                }
                                self.perform(#selector(self.handleHome), with: self, afterDelay: 1)
                            }
                        }
                            
                        else {
                            
                            DispatchQueue.main.async {
                                self.view.makeToast(subscriptionSuccessResponse["description"]! as? String, duration: 3.0, position: .center)
                                
                            }
                        }
                    }
                }catch let jsonErr{
                    DispatchQueue.main.async {
                        
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            task.resume()
        }else{
            DispatchQueue.main.async {
                
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                
            }
        }
    }
    
    func handleHome(){
        DispatchQueue.main.async {
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let signupNavigation = UINavigationController(rootViewController: signupController)
            self.present(signupNavigation, animated: true, completion: nil)
            
        }
    }
    
    func smileyCashAmount(){
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        if let smileyCash =   NSURL(string:Constants.URL_REQUEST_USER_SMILEYCASH){
            var request = URLRequest(url:smileyCash as URL)
            var cashAmount:SmileyCash = SmileyCash()
            cashAmount.page_number = "1"
            cashAmount.userid = userId as? String
            cashAmount.required_count = "1"
            guard let userId = cashAmount.userid else {return}
            let cashListParams:[String:Any] = ["userid":userId,"page_number":cashAmount.page_number!,"required_count":cashAmount.required_count!]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: cashListParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async{
                    SVProgressHUD.dismiss()
                }
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )
                    return
                }
                do{
                    guard let fetchSmileyData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else {return}
                    print(fetchSmileyData)
                    print(fetchSmileyData["description"]!)
                    print(fetchSmileyData["message"]!)
                    DispatchQueue.main.async {
//                        guard let  smileyCashValue:Float = fetchSmileyData["smiley_cash"] as? Float else{return}
//                        self.smileyCashLabel.text = "SmileyCash Rs. " + "\(smileyCashValue)"
//                        UserDefaults.standard.set("\(smileyCashValue)", forKey: "smileyCashValue")
//
                        
                        if  let  smileyCashValue = fetchSmileyData["smiley_cash"]{
                            
                            UserDefaults.standard.set("\(smileyCashValue)", forKey: "smileyCashValue")
                            
                          //  let sCash = "Rs. " + "\(smileyCashValue)"
                            
                            self.smileyCashLabel.text = "SmileyCash Rs. " + "\(smileyCashValue)"

                        }
                        
                        
                    }
                    if (fetchSmileyData["code"] as! NSNumber == 200) {
                        if let smileyResult = fetchSmileyData["smileycash_result"] as? [[String:AnyObject]]{
                            for smileyResponse in smileyResult {
                                if smileyResponse.count > 0 {
                                    guard (smileyResponse["id"] as? String) != nil else{
                                        
                                        return
                                    }
                                    print(smileyResponse["id"] as! String)
                                }
                            }
                        }
                    }else{
                    }
                }catch let jsonErr{
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            task.resume()
        }else{
            DispatchQueue.main.async {
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
            }
        }
    }
    
    /* BemCheckBoxDelegate method */
    
    func didTap(_ checkBox: BEMCheckBox) {
        print("check box :\(checkBox.tag):\(checkBox.on)")
        bemCheckBox = checkBox
        
        
        if checkBox.on{
            //  self.proceedToPayButtonReference.setTitle("ProceedToPay Rs.0", for: .normal)
            
            var number = [Double]()
            
            
           if let smileyCashAmount = (UserDefaults.standard.value(forKey: "smileyCashValue") as? String) {
                
                print("smileyCashAmount",smileyCashAmount)
                
                if let payableAmount = UserDefaults.standard.value(forKey: "debit_amount") as? Double {
                    
                    print("totalCartPriceArray",payableAmount)
                    
                    
                    if let smileyCash = Double(smileyCashAmount){
                        if smileyCash  >= payableAmount {
                            
                            print("minusValue",payableAmount)
                           // self.proceedToPayButtonReference.setTitle("ProceedToPay Rs.0", for: .normal)
                            
                           // UserDefaults.standard.set(0, forKey: "finalAmountValue")
                            smileyCashEnable = "1"

                            self.createSmileyCashPayDialog(title:"Thank You",message:"Use Smiley Cash")

                            
                        }else{
                            print("please use payment gateway your smileycash amount is low to pay the order")
                            
                            self.proceedToPayButtonReference.setTitle("ProceedToPay Rs." + "\(payableAmount-smileyCash)", for: .normal)
                            smileyCashEnable = "0"

                            print("minusValue_ElsePart",payableAmount-smileyCash)

                            UserDefaults.standard.set(payableAmount-smileyCash, forKey: "debit_amount")
                            
                        }
                    }
                }
            }
            
        }else{
            
            if let payableAmount = UserDefaults.standard.value(forKey: "debit_amount") as? Double {
                
                self.proceedToPayButtonReference.setTitle("ProceedToPay Rs." + "\(payableAmount)", for: .normal)
                UserDefaults.standard.set(payableAmount, forKey: "debit_amount")

            }
            self.bemCheckBox = nil
        }
    }
    
    /* create a Alert Dialog for displaying smileycashpay */
    
    func createSmileyCashPayDialog(title:String,message: String){
        let alertController = UIAlertController(title: "Confirmation Message", message: "Do you want to pay Amount Using Smiley Cash?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
            print("you have pressed the Cancel button");
            self.bemCheckBox.on = !self.bemCheckBox.on
            self.bemCheckBox = nil
            
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            print("you have pressed OK button");
            
            if Reachability.isConnectedToNetwork(){
                self.subscriptionSuccess()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                }
            }
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    
    func orderCheckoutPaymentSuccess(){
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        let date = Date()
        let todayDate = formatter.string(from: date)
        if let orderCheckoutPaymentSuccessUrl =   NSURL(string:Constants.URL_REQUEST_USER_ORDERCHECKOUTPAYMENTSUCCESS){
            var request = URLRequest(url:orderCheckoutPaymentSuccessUrl as URL)
            var orderPaymentSuccessModelObject = OrderCheckOutPaymentSuccessModel()
            orderPaymentSuccessModelObject.userid = userId as? String
            orderPaymentSuccessModelObject.checkout_date = todayDate
            orderPaymentSuccessModelObject.smileycash = smileyCashEnable
            guard let userId =  orderPaymentSuccessModelObject.userid,let checkoutDate =  orderPaymentSuccessModelObject.checkout_date,let smileyCash = orderPaymentSuccessModelObject.smileycash else{return}
            let cartCheckOutListParams:[String:Any] = ["userid":userId,"checkout_date":checkoutDate,"smileycash": smileyCash]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: cartCheckOutListParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )
                    return
                }
                
                self.perform(#selector(self.handleHome), with: self, afterDelay: 1)

                do{
                    guard let orderPaymentSuccessJsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                    print(orderPaymentSuccessJsonObject)
                    DispatchQueue.main.async {
                        self.view.makeToast("order updated Successfully", duration: 3.0, position: .center)
                        DispatchQueue.main.async {
                            self.perform(#selector(self.handleHome), with: self, afterDelay: 1)
                        }
                    }
                }
                catch let jsonErr{
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
                
            }
            
            task.resume()
        }else{
            DispatchQueue.main.async {
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
            }
        }
    }
    
    func proceedToPayWithSmileyCash(){
        
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        if let createOrderUrl =   NSURL(string:Constants.URL_REQUEST_USER_CREATEORDER){
            var request = URLRequest(url:createOrderUrl as URL)
            var createOrder:CreateOrderModel = CreateOrderModel()
            createOrder.order_amount = UserDefaults.standard.value(forKey: "cartTotalPrice") as? String
            //  createOrder.smileycash = UserDefaults.standard.value(forKey: "smileyCashValue") as? String
            createOrder.smileycash = "1"
            createOrder.userid = userId as? String
            createOrder.captcha = "Smileyserve"
            guard let smileyCash =  createOrder.smileycash,let userId =  createOrder.userid,let captcha = createOrder.captcha,let orderAmount = createOrder.order_amount else{return}
            let createOrderListParams = ["user_id":userId,"smileycash":smileyCash,"captcha":captcha,"order_amount":orderAmount] as [String:Any]
            print(createOrderListParams)
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: createOrderListParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )
                    return
                }
                do{
                    guard let createOrderJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                    print(createOrderJson)
                    if (createOrderJson["code"] as! NSNumber == 200) {
                        DispatchQueue.main.async {
                            guard let orderid = createOrderJson["order_id"] as? NSNumber else{
                                
                                return
                            }
                            self.orderPaymentSuccess(order_id: "\(orderid)", payTmorderID: "")
                            
                        }
                        
                    } else{
                        
                        DispatchQueue.main.async {
                            guard let orderid = createOrderJson["order_id"] as? NSNumber else{
                                
                                return
                            }
                            self.orderPaymentFail(order_id:"\(orderid)")
                            
                            UserDefaults.standard.set(orderid, forKey: "orderid")
                        }
                        
                    }
                }catch let jsonErr{
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            task.resume()
        }else{
            
            DispatchQueue.main.async {
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
            }
        }
    }
    
    
    func orderPaymentFail(order_id : String) {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_ORDERPAYMENTFAIL + order_id) {
            var urlRequest = URLRequest(url: requestLocationListURL as URL)
            urlRequest.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                if (error != nil) {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                }
                if let response = response {
                    print(response)
                }
                guard let data = data else {
                    return
                }
                do {
                    
                    guard let orderPaymentFailJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else{return}
                    print(orderPaymentFailJson)
                    if (orderPaymentFailJson["code"] as! NSNumber == 200) {
                        DispatchQueue.main.async {
                            self.view.makeToast(orderPaymentFailJson["description"]! as? String, duration: 3.0, position: .center)
                            self.perform(#selector(self.handleHome), with: self, afterDelay: 1)
                        }
                        
                    }else{
                        
                        DispatchQueue.main.async {
                            self.view.makeToast(orderPaymentFailJson["description"]! as? String, duration: 3.0, position: .center)
                        }
                    }
                }catch let jsonErr {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    
                    print(jsonErr.localizedDescription)
                }
            })
            task.resume()
        }else{
            
            DispatchQueue.main.async {
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
            }
            
        }
    }
    
    
    
    func onPaymentSuccess(_ payment_id: String) {
        self.subscriptionSuccessPaymentGateWay()
        self.presentAlertWithTitleForPayment(title: "Payment Successful", message: "Your updated Successfully")
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        self.presentAlertWithTitleForPayment(title: "Payment Failed", message: "Your order not updated")
        //UIAlertView.init(title: "Error", message: str, delegate: self, cancelButtonTitle: "OK").show()
        //  self.presentAlertWithTitle(title: "Error", message: "Your order not placed")
    }
    
    /* Razorpay paymentform calling Method*/
    
    func showPaymentForm() {
        DispatchQueue.main.async {
            if let payableAmount = UserDefaults.standard.value(forKey: "debit_amount") as? Float {
                let convertStringToInt = Int(payableAmount.rounded(.toNearestOrAwayFromZero))
                let convertedCurrencyValue = convertStringToInt * 100
                let options = ["amount":convertedCurrencyValue,"payment_capture":"1","currency":"INR",
                               "image": UIImage(named:"512x512.png") as Any, "name": "SmileyServe", "description": UserDefaults.standard.value(forKey: "name")!, "prefill": ["email": UserDefaults.standard.value(forKey: "email") as? String, "contact": UserDefaults.standard.value(forKey: "mobile")], "theme": ["color": "#206AE0"]] as [String : Any]
                self.razorpay.open(options)
            }
        }
    }
    
    func presentAlertWithTitleForPayment(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
            print("Youve pressed OK Button")
            self.handleHome()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
  }//class
  
  
  /*all actions related to transaction are catched here*/
  extension EditSubscriptionAlertViewController : PGTransactionDelegate{
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        print(responseString)
        //self.showAlert(title: "hurrayy", message: "transaction done")
        controller.dismiss(animated: true) {
            
            self.checksumTransactValue = true
            
            self.statusCheckService()
            // self.presentAlertWithTitle(title: "Payment Successful", message: "Your order placed successfully")
        }
        
    }
    
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        print("Cancelled")
        self.dismiss(animated: (controller != nil), completion: nil)
        
    }
    
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        print(error)
        self.dismiss(animated: (controller != nil), completion: nil)
        
    }
    
  }

