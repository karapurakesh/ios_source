  //
  //  EditSubscriptionController.swift
  //  SmileyServe
  //
  //  Created by Apple on 10/08/17.
  //  Copyright © 2017 smileyserve. All rights reserved.
  //
  
  import UIKit
  import SVProgressHUD
  class EditSubscriptionController: UIViewController {
    
    @IBOutlet weak var editSubscriptionProfileView: UIScrollView!
    var updatePaymentDeatilsArray :[PaymentDetailModel?] = []
    @IBOutlet weak var editSubscriptionProductView: UIView!
    var subscriptionProductUnit = ""
    var subscriptionProductTitles = ""
    var subscriptionProductPrices = ""
    var subscriptionDeleieveryCharge = ""
    var subscriptionProductImages = ""
    var subscriptionProductStartDate = ""
    var subscriptionProductEndDate = ""
    var subscriptionProductDayName = ""
    var subscriptionProductQty = ""
    var subscriptionProductId = ""
    var subscriptionOrderId = ""
    var subscriptionProductTotalQty = ""
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var endSubBtn: UIButton!

    @IBOutlet weak var rcWidthConstant: NSLayoutConstraint!

    @IBOutlet weak var dcWidthConstant: NSLayoutConstraint!
    /* creating the strings for prodcuts quantitis */
    
    var monProductQuantity : String?
    var tueProductQuantity : String?
    var wedProductQuantity : String?
    var thuProductQuantity : String?
    var friProductQuantity : String?
    var satProductQuantity : String?
    var sunProductQuantity : String?
    
    
    //    var userId = "1"
    var userId = UserDefaults.standard.value(forKey: "id")
    @IBOutlet weak var cartCountLabel : UILabel!
    @IBOutlet weak var productDeleiveryCharges: UILabel!
    @IBOutlet weak var productprice: UILabel!
    @IBOutlet weak var productUnits: UILabel!
    @IBOutlet weak var productImageView:CustomImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productStartDateTextField: UITextField!
    @IBOutlet weak var productEndDateTextField: UITextField!
    @IBOutlet var editSubscriptionBtnRefernce: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBtn.layer.cornerRadius = 5
        endSubBtn.layer.cornerRadius = 5
        
        let today = Date()
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let tomorrowString = dateFormatter.string(from: tomorrow!)
        tomorrowDate = tomorrowString
        /* creating corner radius for UILabel*/
        sunLabel.layer.cornerRadius = sunLabel.frame.width / 2
        sunLabel.layer.masksToBounds = true
        monLabel.layer.cornerRadius = monLabel.frame.width / 2
        monLabel.layer.masksToBounds = true
        tueLabel.layer.cornerRadius = tueLabel.frame.width / 2
        tueLabel.layer.masksToBounds = true
        wedLabel.layer.cornerRadius = wedLabel.frame.width / 2
        wedLabel.layer.masksToBounds = true
        thuLabel.layer.cornerRadius = thuLabel.frame.width / 2
        thuLabel.layer.masksToBounds = true
        friLabel.layer.cornerRadius = friLabel.frame.width / 2
        friLabel.layer.masksToBounds = true
        satLabel.layer.cornerRadius = satLabel.frame.width / 2
        satLabel.layer.masksToBounds = true
        self.productStartDateTextField.isUserInteractionEnabled = false
        self.productStartDateTextField.delegate = self
        self.productEndDateTextField.delegate   = self
        self.getDayOfWeekString()
        self.editSubscriptionNetworkCalls()
        

    }
    
    func editSubscriptionNetworkCalls(){
        if Reachability.isConnectedToNetwork(){
            self.getCartCountValue(userId: userId! as! String)
        }else{
            DispatchQueue.main.async {
                self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        productTitle.text = subscriptionProductTitles
        productprice.text = subscriptionProductPrices
        productUnits.text = subscriptionProductUnit
        productImageView.setImage(from: subscriptionProductImages)
        productStartDateTextField.text = subscriptionProductStartDate
        productEndDateTextField.text = subscriptionProductEndDate
        
        if subscriptionDeleieveryCharge  != "0"{
            self.dcWidthConstant.constant = 36
            self.rcWidthConstant.constant = 24
            productDeleiveryCharges.text = subscriptionDeleieveryCharge
            
        }else {
            self.dcWidthConstant.constant = 0
            self.rcWidthConstant.constant = 0
            productDeleiveryCharges.text = ""
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        
      
    }
    
    func getCartCountValue(userId:String){
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        if let requestLocationListURL = NSURL(string: Constants.URL_REQUEST_USER_CARTCOUNT + userId) {
            var urlRequest = URLRequest(url: requestLocationListURL as URL)
            urlRequest.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                if (error != nil) {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                }
                if let response = response {
                    print(response)
                }
                guard let data = data else {
                    return
                }
                
                do {
                    
                    guard let cartData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                    DispatchQueue.main.async {
                        if let cartCount = cartData["cart_count"] {
                            self.editSubscriptionBtnRefernce.addBadge(number: cartCount as! Int)
                            
                        }
                    }
                }
                catch let jsonErr {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        
                    }
                    print(jsonErr.localizedDescription)
                }
                
            })
            task.resume()
            
        }else{
            
            DispatchQueue.main.async {
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                
            }
        }
    }
    
    @IBAction func handleEditSubsBack(_ sender: Any) {
        //  let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //  let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        //  let signupNavigation = UINavigationController(rootViewController: signupController)
        //  self.present(signupNavigation, animated: false, completion: nil)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func handleCartButtonAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "cart") as! CartViewController
        let cartNavigationController = UINavigationController(rootViewController: controller)
        self.present(cartNavigationController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func updateSubscriptionBtnAction(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork(){
            self.updateSubscription()
        }else{
            DispatchQueue.main.async {
                self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                
            }
            
        }
    }
    
    /* creating a dialog when endsubscription button is pressed */
    func showAlertConfirmationEndSubscriptionDialog(){
        let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to delete this item?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
            print("you have pressed the Cancel button");
            
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            print("you have pressed OK button");
            
            if Reachability.isConnectedToNetwork(){
                self.endSubscription()
                
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("No Internet Connection", duration: 3.0, position: .center)
                    
                }
            }
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    
    @IBAction func endSubscriptionButtonAction(_ sender: Any) {
        
        self.showAlertConfirmationEndSubscriptionDialog()
        
    }
    
    
    func updateSubscription(){
        
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        
        if let updateSubscriptionUrl =   NSURL(string:Constants.URL_REQUEST_UPDATESUBSCRIPTION){
            var request = URLRequest(url:updateSubscriptionUrl as URL)
            var updateSubscriptionList:UpdateSubscriptionModel = UpdateSubscriptionModel()
            updateSubscriptionList.userid  = userId as? String
            //  updateSubscriptionList.productid = UserDefaults.standard.value(forKey: "product_id") as? String
            updateSubscriptionList.productid = subscriptionProductId
            updateSubscriptionList.enddate =  productEndDateTextField.text
            updateSubscriptionList.startdate =  productStartDateTextField.text
            updateSubscriptionList.sunday = sunLabel.text
            updateSubscriptionList.monday = monLabel.text
            updateSubscriptionList.tuesday = tueLabel.text
            updateSubscriptionList.wednesday = wedLabel.text
            updateSubscriptionList.thursday = thuLabel.text
            updateSubscriptionList.friday = friLabel.text
            updateSubscriptionList.satday = satLabel.text
            guard let userId =  updateSubscriptionList.userid,let productId =  updateSubscriptionList.productid,let endDate =  updateSubscriptionList.enddate,let startDate = updateSubscriptionList.startdate,let sunday =  updateSubscriptionList.sunday,let monday =  updateSubscriptionList.monday,let tuesday =  updateSubscriptionList.tuesday,let wednesday = updateSubscriptionList.wednesday,let thursday =  updateSubscriptionList.thursday,let friday = updateSubscriptionList.friday,let satday =  updateSubscriptionList.satday else{return}
            let updateSubscriptionListParams:[String:Any] = ["userid":userId,"productid": productId,"enddate":endDate,"startdate":startDate,"sunday":sunday,"monday":monday,"tuesday":tuesday,"wednesday":wednesday,"thursday":thursday,"friday":friday,"satday":satday]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: updateSubscriptionListParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )
                    return
                }
                
                self.updatePaymentDeatilsArray = [PaymentDetailModel]()
                do{
                    guard let fetchUpdateSubscriptionJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                        return
                    }
                    print(fetchUpdateSubscriptionJson)
                    print(fetchUpdateSubscriptionJson["description"]!)
                    print("update message",fetchUpdateSubscriptionJson["message"]!)
                    
                    if (fetchUpdateSubscriptionJson["code"] as! NSNumber == 200) {
                        if let paymentDetails = fetchUpdateSubscriptionJson["payment_detais"] as? [String:AnyObject] {
                            
                            if let  paymentDetailsDebit = paymentDetails["debit_amount"] {
                                print(paymentDetailsDebit)
                                UserDefaults.standard.set(paymentDetailsDebit, forKey: "debit_amount")
                            }
                        }
                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let myAlert = storyboard.instantiateViewController(withIdentifier: "EditSubscriptionAlertViewController") as! EditSubscriptionAlertViewController
                            myAlert.modalPresentationStyle =  UIModalPresentationStyle.overCurrentContext
                            myAlert.modalTransitionStyle   =  UIModalTransitionStyle.crossDissolve
                            self.present(myAlert, animated: true, completion: nil)
                        }
                        //   self.perform(#selector(self.handleHome), with: self, afterDelay: 3)
                        //
                    } else{
                        
                        DispatchQueue.main.async {
                            self.view.makeToast(fetchUpdateSubscriptionJson["description"]! as? String, duration: 3.0, position: .center)
                            
                        }
                        
                    }
                }catch let jsonErr{
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        
                    }
                    print(jsonErr.localizedDescription)
                }
                
            }
            
            task.resume()
        }else{
            DispatchQueue.main.async {
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                
            }
        }
    }
    
    func showAlertConfirmationPayingUsingSmileyCash(){
        let alertController = UIAlertController(title: "Confirmation Message", message: "Thank you", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Proceed to pay", style: .default) { (action:UIAlertAction!) in
            print("you have pressed OK button");
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    
    
    func handleHome(){
        DispatchQueue.main.async {

            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let signupController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let signupNavigation = UINavigationController(rootViewController: signupController)
            self.present(signupNavigation, animated: true, completion: nil)
            
        }
        
        
    }
    
    func endSubscription(){
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        if let endSubscriptionUrl =   NSURL(string:Constants.URL_REQUEST_USER_ENDSUBSCRIPTION) {
            var request = URLRequest(url:endSubscriptionUrl as URL)
            var endSubscriptionList:EndSubscriptionModel = EndSubscriptionModel()
            endSubscriptionList.userId = userId as?  String
            //  endSubscriptionList.orderId = UserDefaults.standard.value(forKey: "orderid") as? String
            //  endSubscriptionList.itemId = UserDefaults.standard.value(forKey: "productid") as? String
            
            endSubscriptionList.orderId = subscriptionOrderId
            endSubscriptionList.itemId = subscriptionProductId
            endSubscriptionList.orderdate =  productStartDateTextField.text
            guard let orderId = endSubscriptionList.orderId,let itemId =  endSubscriptionList.itemId,let orderDate =  endSubscriptionList.orderdate else{return}
            let endSubscriptionListParams = ["orderdate":orderDate,"userid":endSubscriptionList.userId!,"itemid":itemId,"orderid":orderId] as [String:Any]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: endSubscriptionListParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )
                    return
                }
                do{
                    guard let fetchEndSubscriptionJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{return}
                    print(fetchEndSubscriptionJson)
                    print(fetchEndSubscriptionJson["description"]!)
                    print(fetchEndSubscriptionJson["message"]!)
                    if (fetchEndSubscriptionJson["code"] as! NSNumber == 200) {
                        DispatchQueue.main.async {
                            self.view.makeToast(fetchEndSubscriptionJson["description"]! as! String, duration: 3.0, position: .center)
                        }
                        
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            self.editSubscriptionProductView.removeFromSuperview()
                            self.handleToHome()
                        }
                    }else{
                        
                        DispatchQueue.main.async {
                            
                            self.view.makeToast(fetchEndSubscriptionJson["description"]! as? String, duration: 3.0, position: .center)
                        }
                    }
                }catch let jsonErr{
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            task.resume()
        }else{
            DispatchQueue.main.async {
                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
            }
        }
    }
    
    /* Handling thee function Home */
    
    func handleToHome(){
        DispatchQueue.main.async {
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeController = storyBoard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let signupNavigation = UINavigationController(rootViewController: homeController)
            self.present(signupNavigation, animated: true, completion: nil)
        }
    }
    
    
    /* creating the calendar logic */
    
    @IBOutlet weak var sunButtonRef: UIButton!
    @IBOutlet weak var sunLabel: UILabel!
    @IBOutlet weak var sunDecBtnRef: UIButton!
    @IBAction func sunIncBtnRef(_ sender: Any) {
        if let sunLabelIncrementText = sunLabel.text{
            if let sunIncvalue = Int(sunLabelIncrementText) {
                var sunIncrementValue = sunIncvalue
                sunIncrementValue += 1
                sunLabel.text = String(sunIncrementValue)
            }
        }
    }
    
    @IBAction func sunDecBtnActn(_ sender: Any) {
        
        if let calendayDayNames = UserDefaults.standard.value(forKey: "calendarDays") as? [String] {
            if let calendarProductQunatitys = UserDefaults.standard.value(forKey: "calendarProductQuantity") as? [String] {
                let dictionary = Dictionary(elements: Array(zip(calendayDayNames, calendarProductQunatitys)))
                for (key,value) in dictionary{
                    var found = false
                    for calendarnames in calendayDayNames{
                        if key == calendarnames && calendarnames == "7"{
                            print("Inside 4",value)
                            let intVal = Int(value)
                            if let sunDecrementText = sunLabel.text{
                                if let sunDecrementvalue = Int(sunDecrementText){
                                    
                                    if intVal == sunDecrementvalue {
                                        print("Both are same")
                                        
                                        let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to decrease quantity for your subscription period", preferredStyle: .alert)
                                        
                                        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                                            print("you have pressed the Cancel button");
                                            
                                        }
                                        alertController.addAction(cancelAction)
                                        
                                        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                                            print("you have pressed OK button");
                                            
                                            var sunDecValue = sunDecrementvalue
                                            if sunDecValue > 0 {
                                                sunDecValue -= 1
                                                self.sunLabel.text = String(sunDecValue)
                                            }
                                            
                                            self.decrementSubscription()

                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                        
                                        
                                    }else {
                                        var sunDecValue = sunDecrementvalue
                                        if sunDecValue > 0 {
                                            sunDecValue -= 1
                                            sunLabel.text = String(sunDecValue)
                                        }
                                    }
                                }
                            }
                            
                            found = true
                            break
                        }
                    }
                }
            }
        }
        
        /*if let sunDecrementText = sunLabel.text{
            if let sunDecrementvalue = Int(sunDecrementText){
                var sunDecValue = sunDecrementvalue
                if sunDecValue > 0 {
                    sunDecValue -= 1
                    sunLabel.text = String(sunDecValue)
                }
            }
        }*/
    }
    
    @IBOutlet weak var monBtnRef: UIButton!
    @IBOutlet weak var monLabel: UILabel!
    @IBOutlet weak var monBtnDecRef: UIButton!
    @IBAction func monIncBtnAction(_ sender: Any) {
        if let monIncrementText = monLabel.text{
            if let monIncvalue = Int(monIncrementText){
                var monIncrementValue = monIncvalue
                monIncrementValue += 1
                monLabel.text = String(monIncrementValue)
            }
        }
    }
    
    @IBAction func monDecBtnAction(_ sender: Any) {
       /* if let monDecrementText = monLabel.text{
            if let monDecrementvalue = Int(monDecrementText){
                var monDecValue = monDecrementvalue
                if monDecValue > 0 {
                    monDecValue -= 1
                    monLabel.text = String(monDecValue)
                }
            }
        }*/
        
        
        if let calendayDayNames = UserDefaults.standard.value(forKey: "calendarDays") as? [String] {
            if let calendarProductQunatitys = UserDefaults.standard.value(forKey: "calendarProductQuantity") as? [String] {
                let dictionary = Dictionary(elements: Array(zip(calendayDayNames, calendarProductQunatitys)))
                for (key,value) in dictionary{
                    var found = false
                    for calendarnames in calendayDayNames{
                        if key == calendarnames && calendarnames == "1"{
                            print("Inside 4",value)
                            let intVal = Int(value)
                            if let monDecrementText = monLabel.text{
                                if let monDecrementvalue = Int(monDecrementText){
                                    
                                    if intVal == monDecrementvalue {
                                        print("Both are same")
                                        
                                        let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to decrease quantity for your subscription period", preferredStyle: .alert)
                                        
                                        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                                            print("you have pressed the Cancel button");
                                            
                                        }
                                        alertController.addAction(cancelAction)
                                        
                                        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                                            print("you have pressed OK button");
                                            
                                            var monDecValue = monDecrementvalue
                                            if monDecValue > 0 {
                                                monDecValue -= 1
                                                self.monLabel.text = String(monDecValue)
                                            }
                                            
                                            self.decrementSubscription()

                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                        
                                        
                                    }else {
                                        var monDecValue = monDecrementvalue
                                        if monDecValue > 0 {
                                            monDecValue -= 1
                                            monLabel.text = String(monDecValue)
                                        }
                                    }
                                }
                            }
                            
                            found = true
                            break
                        }
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var tueBtnIncRef: UIButton!
    @IBOutlet weak var tueLabel: UILabel!
    @IBOutlet weak var tueDecRefBtn: UIButton!
    @IBAction func tueBtnAction(_ sender: Any) {
        if let tueIncrementText = tueLabel.text{
            if let tueIncValue = Int(tueIncrementText){
                var tueIncrementValue = tueIncValue
                tueIncrementValue += 1
                tueLabel.text = String(tueIncrementValue)
            }
        }
    }
    
    @IBAction func tueDecBtnAction(_ sender: Any) {
       /* if let tueDecrementText = tueLabel.text{
            if let tueDecvalue = Int(tueDecrementText){
                var tueDecrementValue = tueDecvalue
                if tueDecrementValue > 0 {
                    tueDecrementValue -= 1
                    tueLabel.text = String(tueDecrementValue)
                }
            }
        }*/
        
        
        if let calendayDayNames = UserDefaults.standard.value(forKey: "calendarDays") as? [String] {
            if let calendarProductQunatitys = UserDefaults.standard.value(forKey: "calendarProductQuantity") as? [String] {
                let dictionary = Dictionary(elements: Array(zip(calendayDayNames, calendarProductQunatitys)))
                for (key,value) in dictionary{
                    var found = false
                    for calendarnames in calendayDayNames{
                        if key == calendarnames && calendarnames == "2"{
                            print("Inside 4",value)
                            let intVal = Int(value)
                            if let tueDecrementText = tueLabel.text{
                                if let tueDecvalue = Int(tueDecrementText){
                                    
                                    if intVal == tueDecvalue {
                                        print("Both are same")
                                        
                                        let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to decrease quantity for your subscription period", preferredStyle: .alert)
                                        
                                        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                                            print("you have pressed the Cancel button");
                                            
                                        }
                                        alertController.addAction(cancelAction)
                                        
                                        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                                            print("you have pressed OK button");
                                            
                                            var tueDecrementValue = tueDecvalue
                                            if tueDecrementValue > 0 {
                                                tueDecrementValue -= 1
                                                self.tueLabel.text = String(tueDecrementValue)
                                            }
                                            
                                            self.decrementSubscription()

                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                        
                                        
                                    }else {
                                        var tueDecrementValue = tueDecvalue
                                        if tueDecrementValue > 0 {
                                            tueDecrementValue -= 1
                                            tueLabel.text = String(tueDecrementValue)
                                        }
                                    }
                                }
                            }
                            
                            found = true
                            break
                        }
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var wedBtnRef: UIButton!
    @IBOutlet weak var wedLabel: UILabel!
    @IBOutlet weak var wedBtnDecRef: UIButton!
    @IBAction func wedBtnAction(_ sender: Any) {
        if let wedIncrementText = wedLabel.text{
            if let wedIncValue = Int(wedIncrementText){
                var wedIncrementValue = wedIncValue
                wedIncrementValue += 1
                wedLabel.text = String(wedIncrementValue)
                
            }
        }
    }
    
    @IBAction func webDecBtnAction(_ sender: Any) {
        /*if let wedDecrementText = wedLabel.text{
            if  let wedDecvalue = Int(wedDecrementText) {
                var wedDecrementValue = wedDecvalue
                if wedDecrementValue > 0 {
                    wedDecrementValue -= 1
                    wedLabel.text = String(wedDecrementValue)
                }
            }
        }*/
        
        if let calendayDayNames = UserDefaults.standard.value(forKey: "calendarDays") as? [String] {
            if let calendarProductQunatitys = UserDefaults.standard.value(forKey: "calendarProductQuantity") as? [String] {
                let dictionary = Dictionary(elements: Array(zip(calendayDayNames, calendarProductQunatitys)))
                for (key,value) in dictionary{
                    var found = false
                    for calendarnames in calendayDayNames{
                        if key == calendarnames && calendarnames == "3"{
                            print("Inside 4",value)
                            let intVal = Int(value)
                            if let wedDecrementText = wedLabel.text{
                                if let wedDecvalue = Int(wedDecrementText){
                                    
                                    if intVal == wedDecvalue {
                                        print("Both are same")
                                        
                                        let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to decrease quantity for your subscription period", preferredStyle: .alert)
                                        
                                        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                                            print("you have pressed the Cancel button");
                                            
                                        }
                                        alertController.addAction(cancelAction)
                                        
                                        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                                            print("you have pressed OK button");
                                            
                                            var wedDecrementValue = wedDecvalue
                                            if wedDecrementValue > 0 {
                                                wedDecrementValue -= 1
                                                self.wedLabel.text = String(wedDecrementValue)
                                            }
                                            
                                            self.decrementSubscription()

                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                        
                                        
                                    }else {
                                        var wedDecrementValue = wedDecvalue
                                        if wedDecrementValue > 0 {
                                            wedDecrementValue -= 1
                                            wedLabel.text = String(wedDecrementValue)
                                        }
                                    }
                                }
                            }
                            
                            found = true
                            break
                        }
                    }
                }
            }
        }
        
    }
    
    @IBOutlet weak var thuBtnRef: UIButton!
    @IBOutlet weak var thuLabel: UILabel!
    @IBOutlet weak var thuBtnDecRef: UIButton!
    @IBAction func thuBtnAction(_ sender: Any) {
        if let thuIncrementLabeltext = thuLabel.text{
            if let thuIncLabelvalue = Int(thuIncrementLabeltext){
                var thuIncrementValue = thuIncLabelvalue
                thuIncrementValue += 1
                thuLabel.text = String(thuIncrementValue)
            }
        }
    }
    
    @IBAction func thuBtnDecAction(_ sender: Any) {
     /*   if let thuLabelDecrementText = thuLabel.text {
            if let thuLabelvalue = Int(thuLabelDecrementText){
                var thuDecrementValue = thuLabelvalue
                if thuDecrementValue > 0 {
                    thuDecrementValue -= 1
                    thuLabel.text = String(thuDecrementValue)
                }
            }
        }*/
        
        if let calendayDayNames = UserDefaults.standard.value(forKey: "calendarDays") as? [String] {
            if let calendarProductQunatitys = UserDefaults.standard.value(forKey: "calendarProductQuantity") as? [String] {
                let dictionary = Dictionary(elements: Array(zip(calendayDayNames, calendarProductQunatitys)))
                for (key,value) in dictionary{
                    var found = false
                    for calendarnames in calendayDayNames{
                        if key == calendarnames && calendarnames == "4"{
                            print("Inside 4",value)
                            let intVal = Int(value)
                            if let thuLabelDecrementText = thuLabel.text{
                                if let thuLabelvalue = Int(thuLabelDecrementText){
                                    
                                    if intVal == thuLabelvalue {
                                        print("Both are same")
                                        
                                        let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to decrease quantity for your subscription period", preferredStyle: .alert)
                                        
                                        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                                            print("you have pressed the Cancel button");
                                         
                                        }
                                        alertController.addAction(cancelAction)
                                        
                                        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                                            print("you have pressed OK button");
                                            
                                            var thuDecrementValue = thuLabelvalue
                                            if thuDecrementValue > 0 {
                                                thuDecrementValue -= 1
                                                self.thuLabel.text = String(thuDecrementValue)
                                            }
                                            
                                            self.decrementSubscription()

                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                        
                                        
                                    }else {
                                        var thuDecrementValue = thuLabelvalue
                                        if thuDecrementValue > 0 {
                                            thuDecrementValue -= 1
                                            thuLabel.text = String(thuDecrementValue)
                                        }
                                    }
                                }
                            }
                            
                            found = true
                            break
                        }
                    }
                }
            }
        }
        
    }
    
    @IBOutlet weak var friBtnRef: UIButton!
    @IBOutlet weak var friLabel: UILabel!
    @IBOutlet weak var friDecBtnRef: UIButton!
    @IBAction func friBtnAction(_ sender: Any) {
        if let friIncrementText = friLabel.text{
            if let friIncValue = Int(friIncrementText){
                var friIncrementValue = friIncValue
                friIncrementValue += 1
                friLabel.text = String(friIncrementValue)
            }
        }
    }
    
    @IBAction func friDecBtnAction(_ sender: Any) {
       /* if let friDecrementText = friLabel.text{
            if let friDecvalue = Int(friDecrementText){
                var friDecrementValue = friDecvalue
                if friDecrementValue > 0 {
                    friDecrementValue -= 1
                    friLabel.text = String(friDecrementValue)
                }
            }
        }*/
   
   
        if let calendayDayNames = UserDefaults.standard.value(forKey: "calendarDays") as? [String] {
            if let calendarProductQunatitys = UserDefaults.standard.value(forKey: "calendarProductQuantity") as? [String] {
                let dictionary = Dictionary(elements: Array(zip(calendayDayNames, calendarProductQunatitys)))
                for (key,value) in dictionary{
                    var found = false
                    for calendarnames in calendayDayNames{
                        if key == calendarnames && calendarnames == "5"{
                            print("Inside 4",value)
                            let intVal = Int(value)
                            if let friDecrementText = friLabel.text{
                                if let friDecvalue = Int(friDecrementText){
                                    
                                    if intVal == friDecvalue {
                                        print("Both are same")
                                        
                                        let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to decrease quantity for your subscription period", preferredStyle: .alert)
                                        
                                        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                                            print("you have pressed the Cancel button");
                                            
                                        }
                                        alertController.addAction(cancelAction)
                                        
                                        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                                            print("you have pressed OK button");
                                            
                                            var friDecrementValue = friDecvalue
                                            if friDecrementValue > 0 {
                                                friDecrementValue -= 1
                                                self.friLabel.text = String(friDecrementValue)
                                            }
                                            
                                            self.decrementSubscription()

                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                        
                                        
                                    }else {
                                        var friDecrementValue = friDecvalue
                                        if friDecrementValue > 0 {
                                            friDecrementValue -= 1
                                            friLabel.text = String(friDecrementValue)
                                        }
                                    }
                                }
                            }
                            
                            found = true
                            break
                        }
                    }
                }
            }
        }
   
    }
    
    @IBOutlet weak var satBtnRef: UIButton!
    @IBOutlet weak var satLabel: UILabel!
    @IBOutlet weak var satDecBtnRef: UIButton!
    @IBAction func satBtnIncAction(_ sender: Any) {
        if let satIncrementtext = satLabel.text{
            if let satIncvalue = Int(satIncrementtext){
                var satIncrementValue = satIncvalue
                satIncrementValue += 1
                satLabel.text = String(satIncrementValue)
            }
        }
    }
    
    @IBAction func satDecBtnAction(_ sender: Any) {
       /* if  let satDecrementText = satLabel.text{
            if let satDecvalue = Int(satDecrementText) {
                var satDecrementValue = satDecvalue
                if satDecrementValue > 0 {
                    satDecrementValue -= 1
                    satLabel.text = String(satDecrementValue)
                }
            }
        }*/
        
        if let calendayDayNames = UserDefaults.standard.value(forKey: "calendarDays") as? [String] {
            if let calendarProductQunatitys = UserDefaults.standard.value(forKey: "calendarProductQuantity") as? [String] {
                let dictionary = Dictionary(elements: Array(zip(calendayDayNames, calendarProductQunatitys)))
                for (key,value) in dictionary{
                    var found = false
                    for calendarnames in calendayDayNames{
                        if key == calendarnames && calendarnames == "6"{
                            print("Inside 4",value)
                            let intVal = Int(value)
                            if let satDecrementText = satLabel.text{
                                if let satDecvalue = Int(satDecrementText){
                                    
                                    if intVal == satDecvalue {
                                        print("Both are same")
                                        
                                        let alertController = UIAlertController(title: "Confirmation Message", message: "Are you sure want to decrease quantity for your subscription period", preferredStyle: .alert)
                                        
                                        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
                                            print("you have pressed the Cancel button");
                                            
                                        }
                                        alertController.addAction(cancelAction)
                                        
                                        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
                                            print("you have pressed OK button");
                                            
                                            var satDecrementValue = satDecvalue
                                            if satDecrementValue > 0 {
                                                satDecrementValue -= 1
                                                self.satLabel.text = String(satDecrementValue)
                                            }
                                            
                                            self.decrementSubscription()
                                            
                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                        
                                        
                                    }else {
                                        var satDecrementValue = satDecvalue
                                        if satDecrementValue > 0 {
                                            satDecrementValue -= 1
                                            satLabel.text = String(satDecrementValue)
                                        }
                                    }
                                }
                            }
                            
                            found = true
                            break
                        }
                    }
                }
            }
        }
    }
    
    /* opening the weekdays */
    
    func getDayOfWeekString()  {
        self.clearDayNames()
        if let calendayDayNames = UserDefaults.standard.value(forKey: "calendarDays") as? [String] {
            if let calendarProductQunatitys = UserDefaults.standard.value(forKey: "calendarProductQuantity") as? [String] {
                let dictionary = Dictionary(elements: Array(zip(calendayDayNames, calendarProductQunatitys)))
                print("dictionary",dictionary)
                for (key,value) in dictionary{
                    var found = false
                    for calendarnames in calendayDayNames{
                        if key == calendarnames && calendarnames == "1" {
                            monBtnRef.isUserInteractionEnabled = true
                            monBtnDecRef.isUserInteractionEnabled = true
                            monLabel.isHidden = false
                            monLabel.text = value
                            print("Inside 1",value)
                            found = true
                            break
                        }else if key == calendarnames && calendarnames == "2"{
                            tueBtnIncRef.isUserInteractionEnabled = true
                            tueDecRefBtn.isUserInteractionEnabled = true
                            tueLabel.isHidden = false
                            tueLabel.text = value
                            print("Inside 2",value)
                            found = true
                            break
                        }else if key == calendarnames && calendarnames == "3"{
                            wedBtnRef.isUserInteractionEnabled = true
                            wedBtnDecRef.isUserInteractionEnabled = true
                            wedLabel.isHidden = false
                            wedLabel.text  = value
                            print("Inside 3",value)
                            found = true
                            break
                            
                        }else if key == calendarnames && calendarnames == "4"{
                            thuBtnRef.isUserInteractionEnabled = true
                            thuBtnDecRef.isUserInteractionEnabled = true
                            thuLabel.isHidden = false
                            thuLabel.text  = value
                            print("Inside 4",value)
                            found = true
                            break
                            
                        }else if key == calendarnames && calendarnames == "5"{
                            friBtnRef.isUserInteractionEnabled = true
                            friDecBtnRef.isUserInteractionEnabled = true
                            friLabel.isHidden = false
                            friLabel.text = value
                            print("Inside 5",value)
                            found = true
                            break
                            
                        }else if key == calendarnames && calendarnames == "6"{
                            satBtnRef.isUserInteractionEnabled = true
                            satDecBtnRef.isUserInteractionEnabled = true
                            satLabel.isHidden = false
                            satLabel.text = value
                            print("Inside 6",value)
                            found = true
                            break
                            
                        }else if key == calendarnames && calendarnames == "7"{
                            sunButtonRef.isUserInteractionEnabled = true
                            sunDecBtnRef.isUserInteractionEnabled = true
                            sunLabel.isHidden = false
                            sunLabel.text = value
                            print("Inside 7",value)
                            found = true
                            break
                        }
                    }
                }
            }
        }
    }
    
    func clearDayNames(){
        
        monBtnRef.isUserInteractionEnabled = false
        monBtnDecRef.isUserInteractionEnabled = false
        monLabel.isHidden = true
        tueBtnIncRef.isUserInteractionEnabled = false
        tueDecRefBtn.isUserInteractionEnabled = false
        tueLabel.isHidden = true
        wedBtnRef.isUserInteractionEnabled = false
        wedBtnDecRef.isUserInteractionEnabled = false
        wedLabel.isHidden = true
        thuBtnRef.isUserInteractionEnabled = false
        thuBtnDecRef.isUserInteractionEnabled = false
        thuLabel.isHidden = true
        friBtnRef.isUserInteractionEnabled = false
        friDecBtnRef.isUserInteractionEnabled = false
        friLabel.isHidden = true
        sunButtonRef.isUserInteractionEnabled = false
        sunDecBtnRef.isUserInteractionEnabled = false
        sunLabel.isHidden = true
        satBtnRef.isUserInteractionEnabled = false
        satDecBtnRef.isUserInteractionEnabled = false
        satLabel.isHidden = true
        
    }
    
    @IBAction func helpBtnAction(_ sender: Any) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myAlert = storyboard.instantiateViewController(withIdentifier: "popover") as! PopViewController
            myAlert.modalPresentationStyle =  UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle   =  UIModalTransitionStyle.crossDissolve
            self.present(myAlert, animated: true, completion: nil)
        }
    }
    
    func decrementSubscription(){
        
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        
        if let updateSubscriptionUrl =   NSURL(string:Constants.URL_REQUEST_UPDATESUBSCRIPTION){
            var request = URLRequest(url:updateSubscriptionUrl as URL)
            var updateSubscriptionList:UpdateSubscriptionModel = UpdateSubscriptionModel()
            updateSubscriptionList.userid  = userId as? String
            //  updateSubscriptionList.productid = UserDefaults.standard.value(forKey: "product_id") as? String
            updateSubscriptionList.productid = subscriptionProductId
            updateSubscriptionList.enddate =  productEndDateTextField.text
            updateSubscriptionList.startdate =  productStartDateTextField.text
            updateSubscriptionList.sunday = sunLabel.text
            updateSubscriptionList.monday = monLabel.text
            updateSubscriptionList.tuesday = tueLabel.text
            updateSubscriptionList.wednesday = wedLabel.text
            updateSubscriptionList.thursday = thuLabel.text
            updateSubscriptionList.friday = friLabel.text
            updateSubscriptionList.satday = satLabel.text
            guard let userId =  updateSubscriptionList.userid,let productId =  updateSubscriptionList.productid,let endDate =  updateSubscriptionList.enddate,let startDate = updateSubscriptionList.startdate,let sunday =  updateSubscriptionList.sunday,let monday =  updateSubscriptionList.monday,let tuesday =  updateSubscriptionList.tuesday,let wednesday = updateSubscriptionList.wednesday,let thursday =  updateSubscriptionList.thursday,let friday = updateSubscriptionList.friday,let satday =  updateSubscriptionList.satday else{return}
            let updateSubscriptionListParams:[String:Any] = ["userid":userId,"productid": productId,"enddate":endDate,"startdate":startDate,"sunday":sunday,"monday":monday,"tuesday":tuesday,"wednesday":wednesday,"thursday":thursday,"friday":friday,"satday":satday]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: updateSubscriptionListParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )

                    return
                }
                
                self.updatePaymentDeatilsArray = [PaymentDetailModel]()
                do{
                    guard let fetchUpdateSubscriptionJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] else{
                        return
                    }
                    print(fetchUpdateSubscriptionJson)
                    print(fetchUpdateSubscriptionJson["description"]!)
                    print("update message",fetchUpdateSubscriptionJson["message"]!)
                    
                    if (fetchUpdateSubscriptionJson["code"] as! NSNumber == 200) {
                        if let paymentDetails = fetchUpdateSubscriptionJson["payment_detais"] as? [String:AnyObject] {
                            
                            if let  paymentDetailsDebit = paymentDetails["debit_amount"] {
                                print(paymentDetailsDebit)
                                UserDefaults.standard.set(paymentDetailsDebit, forKey: "debit_amount")
                            }
                        }
                        self.subscriptionSuccess()
                        //   self.perform(#selector(self.handleHome), with: self, afterDelay: 3)
                        //
                    } else{
                        SVProgressHUD.dismiss()

                        DispatchQueue.main.async {
                            self.view.makeToast(fetchUpdateSubscriptionJson["description"]! as? String, duration: 3.0, position: .center)
                            
                        }
                        
                    }
                }catch let jsonErr{
                    SVProgressHUD.dismiss()

                    DispatchQueue.main.async {
                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        
                    }
                    print(jsonErr.localizedDescription)
                }
                
            }
            
            task.resume()
        }else{
            
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()

                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                
            }
        }
    }
    
    func subscriptionSuccess() {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(.dark)
            SVProgressHUD.show(withStatus: "please wait...")
        }
        if let subscriptionSuccessUrl =  NSURL(string:Constants.URL_REQUEST_SUBSCRIPTIONSUCCESS) {
            var subscriptionSucessModel:SubscriptionSuccessModel = SubscriptionSuccessModel()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let now = Date()
            let dateString = formatter.string(from: now)
            subscriptionSucessModel.checkoutdate = dateString
            subscriptionSucessModel.userid = userId as? String
            subscriptionSucessModel.enablesmileycash = "1"
            guard let checkOutDate =  subscriptionSucessModel.checkoutdate,let userId =  subscriptionSucessModel.userid else {return}
            var  request = URLRequest(url:subscriptionSuccessUrl as URL)
            let subscriptionSuccessParams:[String:Any] = ["userid":userId,"checkoutdate":checkOutDate,"enablesmileycash":subscriptionSucessModel.enablesmileycash!]
            request.httpMethod = "POST"
            do {
                let  json = try JSONSerialization.data(withJSONObject: subscriptionSuccessParams)
                request.httpBody = json
            }
            catch let jsonErr  {
                
                print(jsonErr.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async{
                    SVProgressHUD.dismiss()

                }
                guard let data = data, error == nil else
                    
                {
                    DispatchQueue.main.async {
                        self.view.makeToast("Unable to connect server internet connection is too slow", duration: 3.0, position: .center)
                    }
                    print(error?.localizedDescription as Any )
                    return
                }
                do{
                    let subscriptionSuccessJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    
                    if let subscriptionSuccessResponse = subscriptionSuccessJson as? [String:Any]{
                        print(subscriptionSuccessResponse)
                        print(subscriptionSuccessResponse["description"]!)
                        print(subscriptionSuccessResponse["message"]!)
                        
                        if (subscriptionSuccessResponse["code"] as! NSNumber == 200) {
                            
                            DispatchQueue.main.async {
                                self.view.makeToast(subscriptionSuccessResponse["description"]! as? String, duration: 3.0, position: .center)
                                self.perform(#selector(self.handleHome), with: self, afterDelay: 1)
                            }
                        }
                            
                        else {
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()

                                self.view.makeToast(subscriptionSuccessResponse["description"]! as? String, duration: 3.0, position: .center)
                                
                            }
                        }
                    }
                }catch let jsonErr{
                    DispatchQueue.main.async {
                        
                        SVProgressHUD.dismiss()

                        self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                        
                    }
                    print(jsonErr.localizedDescription)
                }
            }
            task.resume()
        }else{
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()

                self.view.makeToast("Unable To Fetch Data", duration: 3.0, position: .center)
                
            }
        }
    }
    
  }//class
  
  extension EditSubscriptionController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == productEndDateTextField{
            
            self.view.makeToast("To order this product for future dates,Please place new order and refer FAQS for more info", duration: 3.0, position: .center)
            
            return false
        }
        return true
    }
  }
  
  

