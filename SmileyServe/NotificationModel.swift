//
//  NotificationModel.swift
//  SmileyServe
//
//  Created by Apple on 09/08/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation


struct NotificationModel {
    
    var notification_title : String?
    var notification_message : String?
    var notification_date : String?
    
    init() {
        //
        
    }
    
    init(notification:NotificationModel) {
        
        self.notification_date = notification.notification_date
        self.notification_title = notification.notification_title
        self.notification_message = notification.notification_message
    }
    
    
}
