//
//  WalletModel.swift
//  SmileyServe
//
//  Created by Venkata Naresh Chattala on 20/12/17.
//  Copyright © 2017 smileyserve. All rights reserved.
//

import Foundation

struct WalletModel {
    
    var userAPiKey     : String?
    var amount     : String?
    var paymentId  : String?
    var status     : String?
    
}
